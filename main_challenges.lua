-- Genesis Custom LUA Challenges --


-- Manage challenge starting items (ONLY MOD ITEMS AND ITEMS THAT SHOULDN'T APPLY COSTUMES) --
local ChallengeItemList = RegisterMod("ChallengeItemList",1)
function ChallengeItemList:init(player)
	if Game():GetFrameCount() < 5 then --make sure its a new game, not a continued run
		if Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_OLDSCHOOL then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.PONGER, 0, false)
		end
		if Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_HOLYMERP then
			player:AddCollectible(GENESIS_ITEMS.FAMILIAR.MERP, 0, false)
		end
		if Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_LAMBYSHADOW then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.LIL_LAMBY, 3, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("A Salty Challenge") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.SALT, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Isaac's Superpowers") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.SUPERPOWERS, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("STRIKE!") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.BOWLING_BALL, 3, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Costume Party!") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.WHITE_MASK, 4, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Lucifer's Paradise") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.CEREMONIAL_DAGGER, 4, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.LUCIFERS_WALLET, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.CURSED_CONTRACT, 0, false)
		end
		if Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_TECHG then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.TECH_G, 6, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Byoux's Power") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.BYOUXS_HEAD, 3, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Goodnight Isaac!") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.PILLOW, 6, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("It's time to Draw!") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.CRAYON, 1, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Ninja Practice") then
			--player:AddCollectible(GENESIS_ITEMS.ACTIVE.DASH, 1, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.BLACK_BELT, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.PURPLE_BELT, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.BANDANA, 0, false)
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.DUMMY, 4, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Grant your Wish") then
			player:AddCollectible(Isaac.GetItemIdByName("Wish for a Day"), 12, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Become the Brain!") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.EXPLOSIVE_TEMPER, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Rainbow Party") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.DYSLEXIA, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("The Devil of Fire") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.SPLIT_CROSS, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.RING_OF_FIRE, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Hell from Above") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.HELLBEAM, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Good or Evil") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.COUNTERFEIT_SOUL, 12, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.SATANS_CHALICE, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.LUCIFERS_WALLET, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Soul Collector") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.CURSED_CONTRACT, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Technology Expert") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.TECH_G, 6, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.TECHNOLOGY_3, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.TECH_BOOM, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("The Weather Man") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.RAINSTORM, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.RAINBOOTS, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.BALLOON, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.ZEUS_TOUCH, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Soy Light") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.LIGHTSHOT, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("So Many *** Options!") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.EVENMOREOPTIONS, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("The Broski Challenge") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.BYOUXS_HEAD, 3, false)
			--player:AddCollectible(GENESIS_ITEMS.FAMILIAR.LIL_BROSKI, 0, false)
			player:AddCollectible(14, 0, false)
			player:TryRemoveCollectibleCostume(14, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("Slice & Dice") then
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.SCRATCH, 1, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("The Lost God") then
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.LOST_COURAGE, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.LOST_POWER, 0, false)
			player:AddCollectible(GENESIS_ITEMS.PASSIVE.LOST_WISDOM, 0, false)
		end
		if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("The D3 Challenge") then
			player:AddCollectible(GENESIS_ITEMS.FAMILIAR.MERP, 0, false)
			player:AddCollectible(259, 0, false)
			player:TryRemoveCollectibleCostume(259, false)
			player:AddCollectible(245, 0, false)
			player:TryRemoveCollectibleCostume(245, false)
			player:AddCollectible(306, 0, false)
			player:TryRemoveCollectibleCostume(306, false)
		end
	end
end
ChallengeItemList:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, ChallengeItemList.init)

-- Old School Gamer ---
local OldSchool = RegisterMod("Old School Gamer",1)
function OldSchool:onUpdate()
local player = Isaac.GetPlayer(0)
if Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_OLDSCHOOL then
  Isaac.DebugString("OLDSCHOOL CHALLENGE LOADED")
  if Game():GetFrameCount() % 500 == 0 then
    Game():AddPixelation(300)
  end
end
end
OldSchool:AddCallback(ModCallbacks.MC_POST_UPDATE,OldSchool.onUpdate)

-- Rewind ---

local curse = RegisterMod("Genesis+ Curses",1)
curse.game = Game()
curse.spawn = false

function curse:init()
	curse.RemovedTrapDoor = false
  curse.RemovedTrophy = false
end

curse:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT,curse.init)

function curse:CurseUpdate()
	local level = curse.game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local seed = curse.game:GetSeeds()
	local music = MusicManager()
  local room = curse.game:GetRoom()

	if Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_REWIND then
    local gridSize = room:GetGridSize()
    if not Genesis.moddata.curseRemovedTrapDoor and room:GetType() == RoomType.ROOM_BOSS then
      SFXManager():Stop(SoundEffect.SOUND_HELL_PORTAL1 )
      for i = 0, gridSize-1 do
        if room:GetGridEntity(i) ~= nil then
          if room:GetGridEntity(i):ToTrapdoor() then
            room:RemoveGridEntity(i,1,false)
            Genesis.moddata.curseRemovedTrapDoor = true
          end
        end
      end
    end
   end
      local entities = Isaac.GetRoomEntities()
    if not Genesis.moddata.curseRemovedTrophy and room:GetType() == RoomType.ROOM_BOSS and (level:GetStage() == LevelStage.STAGE6_1 or level:GetStage() == LevelStage.STAGE7) then
      SFXManager():Stop(SoundEffect.SOUND_HELL_PORTAL1 )
      for i = 1, #entities do
        if entities[i].Type == EntityType.ENTITY_PICKUP and entities[i].Variant == PickupVariant.PICKUP_TROPHY then
            entities[i]:Remove()
            Genesis.moddata.curseRemovedTrophy = true
          end
        end
      end
    end

curse:AddCallback(ModCallbacks.MC_POST_UPDATE, curse.CurseUpdate)

curse.pressSpaceCounter = -1

function curse:OnEnter()
  Genesis.moddata.spawnedCurseGift = false
  Genesis.moddata.curseRemovedTrapDoor = false
  Genesis.moddata.curseRemovedTrophy = false
  if Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_REWIND then
	local level = curse.game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local seed = curse.game:GetSeeds()
  local index = level:GetCurrentRoomIndex()
	local room = Game():GetRoom()
    player:UseCard(Card.CARD_EMPEROR)
    curse.pressSpaceCounter = 6
    if SFXManager():IsPlaying(SoundEffect.SOUND_THE_EMPEROR) then SFXManager():Stop(SoundEffect.SOUND_THE_EMPEROR) end
    SFXManager():Stop(SoundEffect.SOUND_HELL_PORTAL1 )
    SFXManager():Stop(SoundEffect.SOUND_HELL_PORTAL2 )
	end
end

curse:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL,curse.OnEnter)

curse.lastHookFrame = 0

--Press space to skip vs screen
function curse:InputHook(ent, hook, action)
  if hook == InputHook.IS_ACTION_PRESSED and action == ButtonAction.ACTION_MENUCONFIRM then
    if curse.pressSpaceCounter == 0 or Input.IsActionPressed(ButtonAction.ACTION_MENUCONFIRM, Isaac.GetPlayer(0).ControllerIndex) then
      curse.pressSpaceCounter = -1
      return true
    end
  end
  if curse.lastHookFrame ~= curse.game:GetFrameCount() then
    curse.pressSpaceCounter = curse.pressSpaceCounter - 1
    curse.lastHookFrame = curse.game:GetFrameCount()
  end
end

curse:AddCallback(ModCallbacks.MC_INPUT_ACTION,curse.InputHook)

function curse:OnRoom()
	local level = curse.game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local seed = curse.game:GetSeeds()
	local room = curse.game:GetRoom()
  local randompos = Isaac.GetRandomPosition()
  if Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_REWIND and level:GetCurrentRoomIndex() == level:GetStartingRoomIndex() and not (level:GetStage() == LevelStage.STAGE2_2 and Genesis.IsSeptic()) then
    local hasTrapdoor = false
		for i = 0, room:GetGridSize()-1 do
			if room:GetGridEntity(i) ~= nil then
				if room:GetGridEntity(i):GetType() == GridEntityType.GRID_TRAPDOOR then
					hasTrapdoor = true
				end
			end
		end
  elseif Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_REWIND and level:GetCurrentRoomIndex() == level:GetStartingRoomIndex() and  (level:GetStage() == LevelStage.STAGE6_1 or level:GetStage() == LevelStage.STAGE7) then
       local hasTrophy = false
       local entities = Isaac.GetRoomEntities()
		for i = 1, #entities do
			if entities[i].Type == EntityType.ENTITY_PICKUP and entities[i].Variant == PickupVariant.PICKUP_TROPHY then
					hasTrophy = true
				end
			end
    if not hasTrophy then Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_TROPHY,randompos,Vector(0,0),nil) end
  end
  end
curse:AddCallback(ModCallbacks.MC_POST_NEW_ROOM,curse.OnRoom)

-- Broski Challenge
local broski = RegisterMod("Broski Challenge",1)

function broski:onBroski()
  local player = Isaac.GetPlayer(0)
  broski.HasCostume = false
  if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("The Broski Challenge") and broski.HasCostume == false then
	local playersprite = player:GetSprite()
  playersprite:ReplaceSpritesheet(0,"gfx/characters/costumes/character_broski.png")
	playersprite:LoadGraphics()
	player:AddNullCostume(Isaac.GetCostumeIdByPath("gfx/characters/broski.anm2"))
    broski.HasCostume = true
  end
end
broski:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT,broski.onBroski)

-- Holy Merp

local merp = RegisterMod("HOLY MERP!",1)

function merp:onMerp()
    local player = Isaac.GetPlayer(0)
  if Isaac.GetChallenge() == GENESIS_CHALLENGES.CHALLENGE_HOLYMERP then
    local entities = Isaac.GetRoomEntities()
    for i=1, #entities do
      if entities[i].Type == EntityType.ENTITY_FAMILIAR then
        if entities[i].Variant == 100 then
          local holymerp = entities[i]
          local holymerpsprite = holymerp:GetSprite()
          holymerpsprite:ReplaceSpritesheet(0,"gfx/familiar/familiar_smerp.png")
          holymerpsprite:LoadGraphics()
		end
	  end
	end
  end
end
merp:AddCallback(ModCallbacks.MC_POST_UPDATE,merp.onMerp)

-- D3 Challenge
local D3 = RegisterMod("D3 Challenge",1)

function D3:onD3()
  local player = Isaac.GetPlayer(0)
  D3.HasCostume = false
  if Isaac.GetChallenge() == Isaac.GetChallengeIdByName("The D3 Challenge") and D3.HasCostume == false then
	local playersprite = player:GetSprite()
	playersprite:ReplaceSpritesheet(1,"gfx/characters/costumes/character_d3monpixel.png")
	playersprite:LoadGraphics()
	player:AddNullCostume(Isaac.GetCostumeIdByPath("gfx/characters/d3.anm2"))
	D3.HasCostume = true
  end
end
D3:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT,D3.onD3)

Isaac.DebugString("Genesis+: Loaded Challenges!")
error({ hack = true })