--Code by Filloax too, so wait before accusing the mod of copying some of his stuff (that sorta accusing things happen, you never know)
--Why some vars are locals and some are in the table? uh....good question
--To change the price of Drawn pickups, go to the bottom in the InkMenu table

local chars = RegisterMod('Genesis+ Characters', 1)

--For performance reasons
Genesis.VEC_ZERO = Vector(0,0)

--####################
--Unlocks
--####################
chars.UNLOCKS = {
  ["The Soulless"] = {
    SATAN = {"RAG_MINI_UNLOCKED","achievement_RagMini.png"},
    RUSH = {"CURSED_BONE_UNLOCKED","achievement_cursed_bone.png"},
    BABY = {"LOST_WISDOM_UNLOCKED","achievement_LostWisdom.png"},
    LAMB = {"LOST_POWER_UNLOCKED","achievement_LostPower.png"}
  },
  ["Lucifer"] = {
    SATAN = {"LUCIFER_WALLET_UNLOCKED","achievement_lost_courage.png"},
    RUSH = {"BOOK_OF_LUCIFER_UNLOCKED","achievement_LuciferBook.png"},
    LAMB = {"LIL_LAMBY_UNLOCKED","achievement_LilLamby.png"},
    BABY = {"CURSED_CONTRACT_UNLOCKED","achievement_CursedContract.png"},
    HUSH = {"HELLBEAM_UNLOCKED","achievement_Hellbeam.png"},
    DELIRIUM = {"SATANS_CHALICE_UNLOCKED","achievement_SatansChalice.png"}
  },
  ["Xaphan"] = {
    RUSH = {"RING_OF_FIRE_UNLOCKED","achievement_RingOfFire.png"},
    SATAN = {"COUNTERFEIT_SOUL_UNLOCKED","achievement_CounterfeitSoul.png"},
    BABY = {"ZEUS_TOUCH_UNLOCKED","achievement_ZeusTouch.png"}
  },
  ["The Drawn"] = {
    SATAN = {"CRAYON_UNLOCKED","achievement_Crayon.png"}
  },
  ["Barnael"] = {
    SATAN = {"AZAZELS_LOST_HORN_UNLOCKED","achievement_AzazelsLostHorn.png"},
    RUSH = {"TECHNOLOGY_3_UNLOCKED","achievement_Tech3.png"}
  }
}

--####################
--Variables, constants, objects, etc
--####################

chars.FamiliarDevilPool = {8, 278, 113, 163, 269, 268, 275, 67, 360, 417}
chars.FamiliarAngelPool = {112, 178, 390, 363, 98}
chars.DevilPool = {8, 51, 67, 79, 80, 81, 82, 113, 114, 118, 122, 134, 159, 163, 172, 187, 212, 215, 216, 225, 230, 237, 241, 259, 262, 269, 268, 275, 278, 311, 412, 408, 399, 391, 360, 409, 433, 431, 420, 417, 441, 498, 477, 475, 462, 442, 468, GENESIS_ITEMS.PASSIVE.RING_OF_FIRE, GENESIS_ITEMS.PASSIVE.HELLBEAM, GENESIS_ITEMS.PASSIVE.AZAZELS_LOST_HORN, GENESIS_ITEMS.PASSIVE.CURSED_CONTRACT, GENESIS_ITEMS.PASSIVE.SATANS_CHALICE, GENESIS_ITEMS.PASSIVE.EVIL_OPTIONS, GENESIS_ITEMS.PASSIVE.QUAKESHOT }

--##########################
--BARNAEL
chars.BarnaelEvilEffects = {EntityFlag.FLAG_CONFUSION, EntityFlag.FLAG_FEAR, EntityFlag.FLAG_BLEED_OUT}
chars.BarnaelCostumes = {
  NEUTRAL = Isaac.GetCostumeIdByPath("gfx/characters/barnael.anm2"),
  BAD = Isaac.GetCostumeIdByPath("gfx/characters/barnael evil.anm2"),
  GOOD = Isaac.GetCostumeIdByPath("gfx/characters/barnael holy.anm2")
}
chars.MAX_KARMA = 48;
chars.rng = RNG()
chars.checkedHearts = {}
chars.prevStage = 0;
chars.BARNAEL_TEAR_SUB = 343287;
chars.BarnaelBar = {
  base = Sprite(),
  good = Sprite(),
  bad = Sprite(),
  bar = Sprite(),
}
do
  local a = chars.BarnaelBar
  for k,v in pairs(a) do
    v:Load("gfx/ui/ui_barnael_bar.anm2", true);
  end
  a.base:Play("BarEmpty", true)
  a.good:Play("BarGood", true)
  a.bad:Play("BarEvil", true)
  a.bar:Play("LittleBar", true)
end

--##########################
--SOULLESS

chars.BONE_SUB = 65487;
chars.BONE_SUB_2 = 65488;
local MimicToChest = {
  [Isaac.GetEntityVariantByName("Chest Host")] = PickupVariant.PICKUP_CHEST,
  [Isaac.GetEntityVariantByName("Golden Chest Host")] = PickupVariant.PICKUP_LOCKEDCHEST,
  [Isaac.GetEntityVariantByName("Spiked Chest Host")] = PickupVariant.PICKUP_SPIKEDCHEST
}

--##########################
--XAPHAN
chars.hasecandlecostume = false
chars.roomsetonfire = false
chars.r = 0
chars.firstfirerot = 0
chars.firesoulscount = 0
chars.firesoul_sprite = Sprite()
--local fire_sprite = Sprite()
chars.firesoul_sprite:Load("gfx/hud_firesoul.anm2", true)
chars.firesoul_sprite:Play("Idle", true)
stats = false

--##########################
--THE FUCKING DRAWN HELL YE
chars.drawnSelected = 1
chars.drawing = false;
chars.finishedDrawing = false; --used to check if the chars.player has stopped holding e before drawing
chars.drawingCountdown = 0

chars.MAX_INK = 200;
chars.DEBUG = false; --free drawing, spam drawing

chars.DrawnUI = {
  base = Sprite(),
  full = Sprite(),
  menu = Sprite()
}

--Drawn anm2s to get them with the entity variant
chars.DrawnPickupAnm = {
  [PickupVariant.PICKUP_HEART] = "gfx/pickup_drawnheart.anm2",
  [PickupVariant.PICKUP_KEY] = "gfx/pickup_drawnkey.anm2",
  [PickupVariant.PICKUP_COIN] = "gfx/pickup_drawncoin.anm2"
}

--Drawn spritesheets saved by variant and subtype, 1 is the anm2 default
chars.DrawnPickupSprite = {
  [PickupVariant.PICKUP_HEART] = {
    [2] = "gfx/pickup_drawnheart_half.png",
    [3] = "gfx/pickup_drawnheart_soul.png",
    [6] = "gfx/pickup_drawnheart_black.png",
    [7] = "gfx/pickup_drawnheart_gold.png"
  },
  [PickupVariant.PICKUP_COIN] = {
    [2] = "gfx/pickup_drawncoin_nickel.png",
    [3] = "gfx/pickup_drawncoin_dime.png"
  }
}

--Drawn anm2 saved by bombvariant
chars.DrawnBombAnm = {
  [BombVariant.BOMB_NORMAL] = "gfx/drawnbomb.anm2",
  [BombVariant.BOMB_SAD] = "gfx/drawnbomb_sad.anm2",
  [BombVariant.BOMB_POISON] = "gfx/drawnbomb_poison.anm2",
  [BombVariant.BOMB_GLITTER] = "gfx/drawnbomb_glitter.anm2",
  [BombVariant.BOMB_HOT] = "gfx/drawnbomb_fire.anm2",
  [BombVariant.BOMB_BUTT] = "gfx/drawnbomb_butt.anm2",
  [BombVariant.BOMB_MR_MEGA] = "gfx/drawnbomb_big.anm2",
  [BombVariant.BOMB_POISON_BIG] = "gfx/drawnbomb_big_poison.anm2",
  [BombVariant.BOMB_BOBBY] = "gfx/drawnbomb_bobby.anm2"
}

--PickupVariant, Subtype
chars.PickupValues = {
  [PickupVariant.PICKUP_HEART] = {
    [1] = GENESIS_ENTITIES.VARIANT.INK_SMALL, --Normal
    [2] = GENESIS_ENTITIES.VARIANT.INK_SMALL, --Half
    [3] = GENESIS_ENTITIES.VARIANT.INK_MED,   --Soul
    [4] = GENESIS_ENTITIES.VARIANT.INK_LARGE, --Eternal
    [5] = GENESIS_ENTITIES.VARIANT.INK_SMALL, --Double
    [6] = GENESIS_ENTITIES.VARIANT.INK_MED,   --Black
    [7] = GENESIS_ENTITIES.VARIANT.INK_LARGE, --Gold
    [8] = GENESIS_ENTITIES.VARIANT.INK_MED,   --Half soul
    [9] = GENESIS_ENTITIES.VARIANT.INK_SMALL, --Scared
    [10] = GENESIS_ENTITIES.VARIANT.INK_MED   --Blended
  },
  [PickupVariant.PICKUP_KEY] = {
    [1] = GENESIS_ENTITIES.VARIANT.INK_SMALL, --Normal
    [2] = GENESIS_ENTITIES.VARIANT.INK_LARGE, --Golden
    [3] = GENESIS_ENTITIES.VARIANT.INK_MED,   --Double
    [4] = GENESIS_ENTITIES.VARIANT.INK_SMALL  --Charged
  },
  [PickupVariant.PICKUP_BOMB] = {
    [1] = GENESIS_ENTITIES.VARIANT.INK_SMALL, --Normal
    [2] = GENESIS_ENTITIES.VARIANT.INK_MED,   --Double
    [4] = GENESIS_ENTITIES.VARIANT.INK_LARGE  --Golden
  },
  [PickupVariant.PICKUP_COIN] = {
    [1] = GENESIS_ENTITIES.VARIANT.INK_SMALL, --Penny
    [2] = GENESIS_ENTITIES.VARIANT.INK_MED,   --Nickel
    [3] = GENESIS_ENTITIES.VARIANT.INK_LARGE, --Dime
    [4] = GENESIS_ENTITIES.VARIANT.INK_MED,   --Double Penny
    [5] = GENESIS_ENTITIES.VARIANT.INK_SMALL, --Lucky Penny
    [6] = GENESIS_ENTITIES.VARIANT.INK_MED    --Sticky Nickel
  }
}

--gfx filename (starts with "gfx/ui/inkmenu_" and ends with ".png", cost, spawn function (with position as argument)
do
  local a = chars.DrawnUI
  a.base:Load("gfx/ui/ui_inkbar.anm2", true);
  a.full:Load("gfx/ui/ui_inkbar.anm2", true);
  a.menu:Load("gfx/ui/inkmenu.anm2", true);
  a.base:Play("Empty", true)
  a.full:Play("Full", true)
  a.menu:Play("Idle", true);
end


--############################
--Callbacks and functions
--############################
function chars:Init(fromSave, fromLua)
  if fromLua then --if not from lua they are defined in newRoom as its called before
    chars.game = Game()
    chars.player = Isaac.GetPlayer(0)
  end


  local name = chars.player:GetName()
  local seed = chars.game:GetSeeds():GetStartSeed()

  chars.rng:SetSeed(seed, math.floor(math.random(1000)))

    --##########################
    --LUCIFER
  if name == "Lucifer" and not fromSave then
    if Genesis.moddata.unlocks.BOOK_OF_LUCIFER_UNLOCKED then
      local item
      repeat item = chars.DevilPool[math.ceil(1+chars.rng:RandomInt(#chars.DevilPool))]
        local ci = GetConfigItem(item)
        until ci.MaxCharges == 0 and ci.MaxCooldown == 0
      chars.player:AddCollectible(item, 16, true)
      chars.player:AddCollectible(GENESIS_ITEMS.ACTIVE.BOOK_OF_LUCIFER,4,false)
    else
      chars.player:AddCollectible(chars.DevilPool[math.ceil(seed%100/99*#chars.DevilPool)], 16, true)
    end
    chars.player:ClearCostumes()
  end

    --##########################
    --XAPHAN
  if not fromSave then
    chars.hasecandlecostume = false
		Genesis.moddata.fireysoullv = 0
    Genesis.moddata.firesouls = 0
		chars.roomsetonfire = false
    if name == "Xaphan" then
      chars.player:AddCollectible(GENESIS_ITEMS.ACTIVE.EXTINGUISHED_CANDLE, 0, true)
      chars.player:AddNullCostume(GENESIS_COSTUMES.EXTINGUISHED_CANDLE_COSTUME)
      chars.player:AddNullCostume(GENESIS_COSTUMES.FIRE_HEAD_COSTUME)
      chars.hasecandlecostume = true
    end
  elseif name == "Xaphan" then
		chars.hasecandlecostume = true
  end

    --##########################
    --BARNAEL
  if not fromSave then
    Genesis.moddata.barnael = 0;
    Genesis.moddata.barnaelAngelDevilVisited = false;

    if name == "Barnael" then
      chars.player:AddCollectible(CollectibleType.COLLECTIBLE_DUALITY, 0, false);
      chars.player:ClearCostumes();
      chars.player:AddNullCostume(chars.BarnaelCostumes.NEUTRAL, -1);
    end
  end

    --##########################
    --DRAWN
  if name == "The Drawn" and not fromSave then
    Genesis.moddata.ink = 0
  end
end

function chars:Unlock(name, ach)
  Isaac.DebugString("Tried to unlock "..ach.." for "..name)
  if chars.UNLOCKS[name] and chars.UNLOCKS[name][ach] and not Genesis.moddata.unlocks[chars.UNLOCKS[name][ach][1]] then
    Genesis.moddata.unlocks[chars.UNLOCKS[name][ach][1]] = true
    PlayUnlockAnimation(chars.UNLOCKS[name][ach][2])
  end
  Isaac.DebugString("Unlocked "..ach.." for "..name)
end

function chars:Update()
   chars.player = Isaac.GetPlayer(0)
   local name = chars.player:GetName();
  --##########################
  --XAPHAN
  chars.firesoul_sprite:Update()
--	chars.firesoul_sprite:LoadGraphics()
--	fire_sprite:Update()
--	fire_sprite:LoadGraphics()
	local room = chars.game:GetRoom()
	local flames = 0

	chars.firesoulscount = Genesis.moddata.firesouls
	if chars.firesoulscount >= 15 then
		if room:GetFrameCount() == 1 or Genesis.moddata.fireysoullv < 1 then
			local npc = Isaac.Spawn(EntityType.ENTITY_EFFECT, 20, 0, chars.player.Position, Genesis.VEC_ZERO, chars.player)
			local sprite = npc:GetSprite()
			sprite:Load("gfx/fire.anm2", true)
			sprite:Play("NormalFlame", true)
			sprite.Scale = Vector(0.75,0.75)
		end
		if Genesis.moddata.fireysoullv == 0 then Genesis.moddata.fireysoullv = 1 end
	end
	if chars.firesoulscount >= 25 then
		if Genesis.moddata.fireysoullv == 1 then Genesis.moddata.fireysoullv = 2 end
	end
	if chars.firesoulscount >= 35 then
		if Genesis.moddata.fireysoullv == 2 then Genesis.moddata.fireysoullv = 3 end
		local rand = math.random(3,7)
		if chars.player.FrameCount % rand == 0 then
			local npc = Isaac.Spawn(GENESIS_ENTITIES.BURN_CREEP, 200, 0, chars.player.Position+Vector(0,-16), Genesis.VEC_ZERO, chars.player)
			npc:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
			rand = tostring(math.random(1,12))
			npc:GetSprite():Play("Creep"..rand, true)
		end
	end
	if chars.firesoulscount >= 45 then
		if room:GetFrameCount() == 1 or Genesis.moddata.fireysoullv < 4 then
			local npc = Isaac.Spawn(EntityType.ENTITY_EFFECT, 20, 0, chars.player.Position, Genesis.VEC_ZERO, chars.player)
			local sprite = npc:GetSprite()
			sprite:Load("gfx/fire.anm2", true)
			sprite:Play("NormalFlame", true)
			sprite.Scale = Vector(0.75,0.75)
		end
		if Genesis.moddata.fireysoullv == 3 then Genesis.moddata.fireysoullv = 4 end
	end
	if chars.firesoulscount >= 55 then
		if Genesis.moddata.fireysoullv == 4 then Genesis.moddata.fireysoullv = 5 end
	end
	if chars.firesoulscount >= 70 then
		if Genesis.moddata.fireysoullv == 5 then Genesis.moddata.fireysoullv = 6 end
	end
	if chars.firesoulscount >= 85 then
		if room:GetFrameCount() == 1 then
			local rand = math.random(1,3)
			if rand == 1 then
				for i=1, #entities do
					if e:IsVulnerableEnemy() then
						e:AddBurn(EntityRef(chars.player), 90, 0.3)
					end
				end
			end
		end
		if Genesis.moddata.fireysoullv == 6 then Genesis.moddata.fireysoullv = 7 end
	end
	if chars.firesoulscount >= 99 then
		if Genesis.moddata.fireysoullv == 7 then
			Genesis.moddata.fireysoullv = 8
			chars.player:AddCollectible(GENESIS_ITEMS.ACTIVE.ETERNAL_CANDLE, 4, true)
      if not Genesis.moddata.unlocks.ETERNAL_CANDLE_UNLOCKED then
        Genesis.moddata.unlocks.ETERNAL_CANDLE_UNLOCKED = true
        PlayUnlockAnimation("achievement_EterenalCandle.png")
      end
		end
	end
	if chars.firesoulscount >= 100 then
		chars.firesoulscount = 99
	end
	chars.firstfirerot = chars.firstfirerot + 0.1

  --##########################
  --DRAWN
  if name == "The Drawn" then
    chars.DrawnUI.full:Update();
    chars.DrawnUI.menu:Update()
  end
  if chars.finishedDrawing and not Input.IsActionPressed(ButtonAction.ACTION_BOMB, chars.player.ControllerIndex) then
    chars.finishedDrawing = false
    chars.drawing = false
  end
  if chars.drawingCountdown > 0 then --failsafe in case a sprite is replaced by whatever mod and so doesn't have FinishDrawing
    chars.drawingCountdown = chars.drawingCountdown-1
  elseif chars.drawingCountdown == 0 then
    if chars.drawing then
      chars.finishedDrawing = true
    end
    chars.drawingCountdown = -1
  end

  --##########################
  --SOULLESS

  if chars.player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_COURAGE) and not Genesis.moddata.unlocks.LOST_COURAGE_UNLOCKED then
    Genesis.moddata.unlocks.LOST_COURAGE_UNLOCKED = true
    PlayUnlockAnimation("achievement_lost_courage.png")
  end

  --############################
  --ENTITIES LOOP
  --############################

  local entities = Genesis.getRoomEntities()

  for i,e in ipairs(entities) do

    --##########################
    --SOULLESS
    if name == "The Soulless" then
      if e.Type == EntityType.ENTITY_PICKUP and e.Variant == GENESIS_ENTITIES.VARIANT.SPIKED_GOLDEN_CHEST  then
        if chars.player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_COURAGE) then
          e:ToPickup():Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_LOCKEDCHEST, 0, true)
        elseif e.FrameCount > 33 then
          local dist = e.Position:Distance(chars.player.Position);

          if dist <= 26 and not e:IsDead() then
            chars.player:TakeDamage(2, DamageFlag.DAMAGE_CHEST, EntityRef(e), 10);
          end
        elseif e:GetSprite():IsEventTriggered("DropSound") then
          SFXManager():Play(SoundEffect.SOUND_CHEST_DROP, 0.7, 0, false, 1)
        end

      elseif e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_LOCKEDCHEST and not chars.player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_COURAGE) then
        e:ToPickup():Morph(EntityType.ENTITY_PICKUP, GENESIS_ENTITIES.VARIANT.SPIKED_GOLDEN_CHEST, 0, true)
      end
    end

    --##########################
    --XAPHAN
		if e.Type == EntityType.ENTITY_EFFECT and e.SpawnerType == EntityType.ENTITY_TEAR and chars.player:GetName() == "Xaphan" then
			if e.Variant == EffectVariant.TEAR_POOF_A or e.Variant == EffectVariant.TEAR_POOF_B then
				e:GetSprite().Color = Color(0.2,0.2,0.2,1, 255, 75, 0)
			end
		end
		if Game():GetRoom():GetFrameCount() <= 1 and chars.player:GetName() == "Xaphan" and chars.player:GetActiveItem() == 0 then
			chars.player:AddCollectible(GENESIS_ITEMS.ACTIVE.EXTINGUISHED_CANDLE, 0, true)
		end
		if e.SpawnerType == EntityType.ENTITY_PLAYER and e.Type == EntityType.ENTITY_EFFECT and e.Variant == 20 then
			flames = flames + 1
			local sprite = e:GetSprite()
			if flames == 1 then e.Position = chars.player.Position+(Vector(math.cos(chars.firstfirerot), math.sin(chars.firstfirerot))*35)
			else e.Position = chars.player.Position+(Vector(math.cos(chars.firstfirerot+135), math.sin(chars.firstfirerot+135))*35) end
			e.Velocity = chars.player.Velocity
			for q=1, #entities do
				if entities[q]:IsVulnerableEnemy() and (e.Position-entities[q].Position):Length() <= 32 then
					entities[q]:TakeDamage(0.7, EntityFlag.FLAG_POISON, EntityRef(chars.player), 0)
				end
			end
			if chars.firesoulscount >= 25 and flames == 1 and chars.firesoulscount < 70 then
				if not sprite:IsPlaying("RedFlame") then
					sprite:Play("RedFlame", true)
				end
				if e.FrameCount % 60 == 0 then
					local nearestenemy = 10000
					local nearestenemypos = nil
					for q=1, #entities do
						if entities[q]:IsVulnerableEnemy() and (e.Position-entities[q].Position):Length() <= nearestenemy then
							nearestenemy = (e.Position-entities[q].Position):Length()
							nearestenemypos = e.Position-entities[q].Position
						end
					end
					if nearestenemypos ~= nil then
						local tear = Isaac.Spawn(EntityType.ENTITY_TEAR, TearVariant.BLOOD, 0, e.Position, (nearestenemypos*(6 / nearestenemypos:Length()))*-1, e)
						tear.CollisionDamage = chars.player.Damage*2
					end
				end
			end
			if chars.firesoulscount >= 55 and flames == 2 or chars.firesoulscount >= 70 then
				if not sprite:IsPlaying("PurpleFlame") then
					sprite:Play("PurpleFlame", true)
				end
				if e.FrameCount % 60 == 0 then
					local nearestenemy = 10000
					local nearestenemypos = nil
					for q=1, #entities do
						if entities[q]:IsVulnerableEnemy() and (e.Position-entities[q].Position):Length() <= nearestenemy then
							nearestenemy = (e.Position-entities[q].Position):Length()
							nearestenemypos = e.Position-entities[q].Position
						end
					end
					if nearestenemypos ~= nil then
						local tear = Isaac.Spawn(EntityType.ENTITY_TEAR, 0, 0, e.Position, (nearestenemypos*(6 / nearestenemypos:Length()))*-1, e)
						tear.CollisionDamage = chars.player.Damage*3
						tear:GetSprite().Color = Color(1,1,1,1,75,0,175)
					end
				end
			end
		end

		if e.Type == GENESIS_ENTITIES.BURN_CREEP and e.Variant == 200 then
			for q=1, #entities do
				if entities[q]:IsVulnerableEnemy() and ((e.Position+Vector(0,16))-entities[q].Position):Length() <= 24 then
					entities[q]:AddBurn(EntityRef(chars.player), 1, 1)
				end
			end
			if e.FrameCount == 60 then e:Remove() end
		end
		if e.Type == EntityType.ENTITY_TEAR and e.SpawnerType == EntityType.ENTITY_EFFECT and e.SpawnerVariant == 20 and e.Variant == 0 then
			local nearestenemy = 10000
			local nearestenemypos = nil
			for q=1, #entities do
				if entities[q]:IsVulnerableEnemy() and (e.Position-entities[q].Position):Length() <= nearestenemy then
					nearestenemy = (e.Position-entities[q].Position):Length()
					nearestenemypos = e.Position-entities[q].Position
				end
			end
			if nearestenemypos ~= nil then e.Velocity = (nearestenemypos*(6 / nearestenemypos:Length()))*-1 end
		end

		if chars.roomsetonfire then
			if e:IsVulnerableEnemy() then
				e:AddBurn(EntityRef(chars.player), 1, 0)
				e:TakeDamage(0.2, EntityFlag.FLAG_POISON, EntityRef(chars.player), 0)
			end
		end


    --##########################
    --BARNAEL
    if name == "Barnael" then
      if e.Type == EntityType.ENTITY_PICKUP then
        local spr = e:GetSprite();

        if e.Variant == GENESIS_ENTITIES.VARIANT.RESURRECTION_HEART or e.Variant == GENESIS_ENTITIES.VARIANT.NECRO_HEART then

          if e.FrameCount > 33 then
            if spr:IsPlaying("Collect") and spr:GetFrame() > 4 then
              e:Remove();
            elseif e.Position:Distance(chars.player.Position) <= 26 and not spr:IsPlaying("Collect")  and ((_G.Genesis.moddata.barnael < chars.MAX_KARMA and e.Variant == GENESIS_ENTITIES.VARIANT.RESURRECTION_HEART) or (_G.Genesis.moddata.barnael > -chars.MAX_KARMA and e.Variant == GENESIS_ENTITIES.VARIANT.NECRO_HEART)) then
              e.Velocity = Vector(0,0)
              e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE;
              spr:Play("Collect", false)
              if e.Variant == GENESIS_ENTITIES.VARIANT.RESURRECTION_HEART then
                SFXManager():Play(SoundEffect.SOUND_SUPERHOLY, 0.9, 0, false, 1)
                chars:AdjustKarma(4)
              else
                SFXManager():Play(SoundEffect.SOUND_SATAN_CHARGE_UP, 0.9, 0, false, 1)
                chars:AdjustKarma(-4)
              end
            end
          elseif e:GetSprite():IsEventTriggered("DropSound") then
            SFXManager():Play(SoundEffect.SOUND_FETUS_LAND, 0.9, 0, false, 0.8)
          end

        elseif e.Variant == PickupVariant.PICKUP_HEART and not chars.checkedHearts[e.Index] and spr:IsPlaying("Appear") then
          e = e:ToPickup();

          if math.random() > 0.5 then
            e:Morph(EntityType.ENTITY_PICKUP, GENESIS_ENTITIES.VARIANT.RESURRECTION_HEART, 0, true);
          else
            e:Morph(EntityType.ENTITY_PICKUP, GENESIS_ENTITIES.VARIANT.NECRO_HEART, 0, true);
          end

          chars.checkedHearts[e.Index] = true;

        elseif e.Variant == PickupVariant.PICKUP_COLLECTIBLE and e.SubType == CollectibleType.COLLECTIBLE_POLAROID and _G.Genesis.moddata.barnael < -chars.MAX_KARMA/2 then
          e:Remove();
        elseif e.Variant == PickupVariant.PICKUP_COLLECTIBLE and e.SubType == CollectibleType.COLLECTIBLE_NEGATIVE and _G.Genesis.moddata.barnael > chars.MAX_KARMA/2 then
          e:Remove();
        elseif e.Variant == PickupVariant.PICKUP_COLLECTIBLE and e.SubType == CollectibleType.COLLECTIBLE_DUALITY then
          chars.player:UseActiveItem(CollectibleType.COLLECTIBLE_D6, false, false, true, false)
        end

      elseif e.Type == EntityType.ENTITY_EFFECT and e.Variant == GENESIS_ENTITIES.VARIANT.BARNAEL_GLOW then
        e.Position = chars.player.Position;
        e.Velocity = chars.player.Velocity;

        chars:UpdateGlow(e)
      end
    end

    --##########################
    --DRAWN
    if name == "The Drawn" then
      local spr = e:GetSprite();

      if e.Type == EntityType.ENTITY_PICKUP then
        e = e:ToPickup()

        if e.Variant == GENESIS_ENTITIES.VARIANT.INK_SMALL or e.Variant == GENESIS_ENTITIES.VARIANT.INK_MED or e.Variant == GENESIS_ENTITIES.VARIANT.INK_LARGE then

          if e.FrameCount > 33 then
            if spr:IsPlaying("Collect") and spr:GetFrame() > 6 then
              e:Remove();

            elseif e.Position:Distance(chars.player.Position) <= 26 and not spr:IsPlaying("Collect")  and Genesis.moddata.ink < chars.MAX_INK then
              e.Velocity = Vector(0,0)
              e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE;
              spr:Play("Collect", false)
              SFXManager():Play(SoundEffect.SOUND_BLOBBY_WIGGLE, 0.9, 0, false, 1)

              if e.Variant == GENESIS_ENTITIES.VARIANT.INK_SMALL then
                chars:AdjustInk(5)
              elseif e.Variant == GENESIS_ENTITIES.VARIANT.INK_MED then
                chars:AdjustInk(10)
              else
                chars:AdjustInk(20)
              end
            end

          elseif e:GetSprite():IsEventTriggered("DropSound") then
            SFXManager():Play(SoundEffect.SOUND_FETUS_LAND, 0.9, 0, false, 0.8)
          end

        elseif spr:IsPlaying("Appear") and chars.PickupValues[e.Variant] and chars.PickupValues[e.Variant][e.SubType] and not e:GetData().drawn then
          e:Morph(EntityType.ENTITY_PICKUP, chars.PickupValues[e.Variant][e.SubType], 0, true)
          spr:Play("Appear", true);
        elseif spr:IsPlaying("Appear") and e:GetData().drawn then --something else replaced the drawn pickup with a normal one
          e:Morph(EntityType.ENTITY_PICKUP, chars.PickupValues[e.Variant][e.SubType], 0, true)
          spr:LoadGraphics()
          spr:Play("AppearDrawn", true)
        elseif spr:IsFinished("AppearDrawn") and not spr:IsPlaying("Collect") then
          spr:Play("Idle", false)
          e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
        end

      elseif e.Type == EntityType.ENTITY_BOMBDROP and chars.DrawnBombAnm[e.Variant] and spr:GetFilename() ~= chars.DrawnBombAnm[e.Variant] and e.SpawnerType == EntityType.ENTITY_PLAYER then
        local frames = spr:GetFrame()
        spr:Load(chars.DrawnBombAnm[e.Variant], true);
        spr:Play("Pulse", false)
      end
    end

    if e:GetSprite():IsEventTriggered("FinishDrawing") then
      chars.finishedDrawing = true;
    end

  --Unlocks
  if chars.game:GetStateFlag(GameStateFlag.STATE_BOSSRUSH_DONE) and chars.UNLOCKS[name] and chars.UNLOCKS[name].RUSH and not Genesis.moddata.unlocks[chars.UNLOCKS[name].RUSH[1]] then
    chars:Unlock(name, "RUSH")
  end
end
end

function chars:PlayerUpdate(pl)
  local name = pl:GetName()

    --##########################
    --SOULLESS
	if name == "The Soulless" then
		local blackHeartsCount = 0
    local soulHearts = pl:GetSoulHearts();
		local blackHearts = pl:GetBlackHearts();

		for i=0, 24 do
			if blackHearts & 1<<i ~= 0 then
				blackHeartsCount = blackHeartsCount + 1
			end
		end

		for i=1, math.floor(soulHearts / 2 - blackHeartsCount) do
			pl:AddBlueSpider(chars.player.Position)
			pl:AddSoulHearts(-1)
		end

    soulHearts = pl:GetSoulHearts(); --get them again to see if we have 0.5 left or not
    if soulHearts/2 - blackHeartsCount == 0.5 then
			pl:AddBlueSpider(chars.player.Position)
			pl:AddSoulHearts(-1)
    end

		for i=1, pl:GetMaxHearts() / 2 do
			pl:AddMaxHearts(-2)
			pl:AddBlackHearts(2)
		end

    --##########################
    --LUCIFER
  elseif name == "Lucifer" then
		local roomType = chars.game:GetRoom():GetType()
		local level = chars.game:GetLevel()
    local room = chars.game:GetRoom()
		if roomType == RoomType.ROOM_DEVIL or roomType == RoomType.ROOM_ANGEL and devilspawn == false then
      if level:IsDevilRoomDisabled() then
		end
    end
    if room:GetDevilRoomChance() > 0 then
    end
    if level:GetAngelRoomChance() > 0 then

    end
		if pl:HasCollectible(CollectibleType.COLLECTIBLE_GOAT_HEAD) and Game():GetStateFlag()== GameStateFlag.STATE_DEVILROOM_VISITED then
			pl:RemoveCollectible(CollectibleType.COLLECTIBLE_GOAT_HEAD)
		end

    --##########################
    --DRAWN
  elseif name == "The Drawn" then
    local bombs = chars.player:GetNumBombs()
    if bombs > 0 then
      chars.player:AddBombs(-bombs)
      chars:AdjustInk(5*bombs)
    end
  end

  --Tears
  if name == "Xaphan" or name == "The Drawn" or name == "Barnael" or name == "Lucifer" then
    for i,e in ipairs(Genesis.RoomTears) do
      chars:ForEveryTear(e, name)
    end
  end
end

function chars:NewRoom()
	chars.roomsetonfire = false
  if not (chars.game and chars.player) then --this is called before game_start
    chars.game = Game()
    chars.player = Isaac.GetPlayer(0)
  end

  local room = chars.game:GetRoom();
  local ents = Genesis.getRoomEntities()
  local name = chars.player:GetName()

  --##########################
  --SOULLESS
  if name == "The Soulless" then
    for i,e in ipairs(ents) do
      if e.Type == EntityType.ENTITY_BONY or e.Type == EntityType.ENTITY_BLACK_BONY and math.max(0, math.min( chars.player.Luck, 5) )* 2 + 20 > e.InitSeed%100+1 then
        e:AddEntityFlags(EntityFlag.FLAG_CHARM);
      end
    end
  end

  --##########################
  --BARNAEL
  if name == "Barnael" then
    chars.checkedHearts = {};
    local seedFamiliar, seedChance = room:GetSpawnSeed(), room:GetDecorationSeed();

    if Genesis.moddata.barnael < -chars.MAX_KARMA*0.70 or Genesis.moddata.barnael > chars.MAX_KARMA*0.70 then

      --~50% chance each room, up to 80% at 10 luck, to spawn a familiar
      if math.abs(_G.Genesis.moddata.barnael) == chars.MAX_KARMA and math.max(0,chars.player.Luck)*3+50 > seedChance%100 then
        local itemid

        if _G.Genesis.moddata.barnael > 0 then
          itemid = chars:GetItemFromSeed(chars.FamiliarAngelPool,seedFamiliar);
        else
          itemid = chars:GetItemFromSeed(chars.FamiliarDevilPool,seedFamiliar);
        end

--          Isaac.DebugString(itemid);

        chars.player:GetEffects():AddCollectibleEffect(itemid, false);
      end
    end

    for i=1,2 do --repeat 2 times so we have 2 glow entities so the glow is brighter
      local glow = Isaac.Spawn(EntityType.ENTITY_EFFECT, GENESIS_ENTITIES.VARIANT.BARNAEL_GLOW, 0, chars.player.Position, Genesis.VEC_ZERO, chars.player);
      glow.DepthOffset = -10;
      glow:GetSprite():Play("HeadDown", true);
      chars:UpdateGlow(glow)
    end

    if not Genesis.moddata.barnaelAngelDevilVisited then
      if room:GetType() == RoomType.ROOM_DEVIL or Genesis.moddata.isSatanicRitualRoom then
        chars:AdjustKarma(-16)
        Genesis.moddata.barnaelAngelDevilVisited = true;
      elseif room:GetType() == RoomType.ROOM_ANGEL then
        chars:AdjustKarma(16)
        Genesis.moddata.barnaelAngelDevilVisited = true;
      end
    end
  end

  --##########################
  --DRAWN
  if name == "The Drawn" then
    local level = chars.game:GetLevel()

    if level:GetStartingRoomIndex() == level:GetCurrentRoomIndex() and level:GetStage() == 1 and not chars.game:IsGreedMode() then
      local e = Isaac.Spawn(EntityType.ENTITY_EFFECT, GENESIS_ENTITIES.VARIANT.DRAWN_CONTROLS, 1, Vector(210,160), Genesis.VEC_ZERO, chars.player)
      if level:GetStageType() == StageType.STAGETYPE_AFTERBIRTH then
        e:GetSprite().Color = Color(0.5,0.5,0.5,1,0,0,0)
      end
      e:AddEntityFlags(EntityFlag.FLAG_RENDER_FLOOR)
    end

    local isFirstVisit = chars.game:GetRoom():IsFirstVisit()

    for i,e in ipairs(ents) do
      local spr = e:GetSprite()

      if e.Type == EntityType.ENTITY_FAMILIAR and e.Variant == GENESIS_ENTITIES.VARIANT.CRAYON_TURRET then
        e:Remove()
      elseif e.Type == EntityType.ENTITY_PICKUP then
        e = e:ToPickup()
        if chars.PickupValues[e.Variant] then
          e:Morph(EntityType.ENTITY_PICKUP, chars.PickupValues[e.Variant][e.SubType], 0, true)
          spr:Play("Idle", false)
        elseif chars.DrawnPickupAnm[e.Variant] then
          spr:Load(chars.DrawnPickupAnm[e.Variant], false)
          if chars.DrawnPickupSprite[e.Variant] and chars.DrawnPickupSprite[e.Variant][e.SubType] then
            spr:ReplaceSpritesheet(0, chars.DrawnPickupSprite[e.Variant][e.SubType])
          end
          spr:LoadGraphics()
          spr:Play("Collect", false)
        end
      elseif e.Type == EntityType.ENTITY_BOMBDROP then
        if chars.DrawnBombAnm[e.Variant] then
          spr:Load(chars.DrawnBombAnm[e.Variant], true)
        end
      end
    end
  end
end

function chars:NewFloor()
  Genesis.moddata.barnaelAngelDevilVisited = false;
end

--Uses input_hook: is called more often, and surely at least once per frame, but can lead to more lag so should be used only when the normal one doesn't work
function chars:ForEveryTearForce(e, name)
  --##########################
  --SOULLESS
  if (chars.player:HasCollectible(GENESIS_ITEMS.PASSIVE.CURSED_BONE) or name == "The Soulless") and (e.Variant == TearVariant.BLUE or e.Variant == TearVariant.BONE) and e.SubType ~= chars.BONE_SUB and e.SubType ~= chars.BONE_SUB_2 and e.SpawnerType == EntityType.ENTITY_PLAYER then
   e = e:ToTear();

    if e.Variant ~= TearVariant.BONE then
      e:ChangeVariant(TearVariant.BONE);
    end

    e.FallingSpeed = e.FallingSpeed+0.6;
--    Isaac.DebugString(e.InitSeed%100)
    if math.max(0, math.min( chars.player.Luck, 15) ) * 6 + 13 > e.InitSeed%100 then
      local spr = e:GetSprite();
      spr:ReplaceSpritesheet(0, "gfx/tears_cursed_bone_2.png");
      spr:LoadGraphics();
      e.SubType = chars.BONE_SUB_2
    else
      local spr = e:GetSprite();
      spr:ReplaceSpritesheet(0, "gfx/tears_cursed_bone.png");
      spr:LoadGraphics();
      e.SubType = chars.BONE_SUB
    end
  end
end

--This is simply in peffect update
function chars:ForEveryTear(e, name)
  --##########################
  --XAPHAN
  if name == "Xaphan" and e.SpawnerType == EntityType.ENTITY_PLAYER and e.Variant == TearVariant.BLUE and not e:GetData().gReplaced then
    e.Variant = TearVariant.BLOOD
    e:GetSprite():Load("gfx/002.001_blood tear.anm2", true);
    e:GetData().gReplaced = true;

  --##########################
  --LUCIFER
  elseif name == "Lucifer" and e.SpawnerType == EntityType.ENTITY_PLAYER and e.Variant == TearVariant.BLUE and not e:GetData().gReplaced then
    e.Variant = TearVariant.BLOOD
    e:GetSprite():Load("gfx/002.001_blood tear.anm2", true);
    e:GetData().gReplaced = true;

  --##########################
  --BARNAEL
  elseif name == "Barnael" and chars.player.Luck*70/15+30 > e.InitSeed%100 and Genesis.moddata.barnael == -chars.MAX_KARMA and not e:GetData().gReplaced and (e.Variant == TearVariant.BLOOD or e.Variant == TearVariant.BLUE) then
    e:GetData().gReplaced = true;
    if e.Variant == TearVariant.BLUE then
      e.Variant = TearVariant.BLOOD
      e:GetSprite():Load("gfx/002.001_blood tear.anm2", true);
    end
    e:GetSprite().Color = Color(0.25,0.25,0.25,1,0,0,0)
    e.SubType = chars.BARNAEL_TEAR_SUB

  --##########################
  --DRAWN
  elseif name == "The Drawn" and (e.Variant == TearVariant.BLUE or e.Variant == TearVariant.BLOOD) and e.FrameCount <= 1 and (e.SpawnerType == EntityType.ENTITY_PLAYER or (e.SpawnerType == EntityType.ENTITY_FAMILIAR and e.SpawnerVariant == GENESIS_ENTITIES.VARIANT.CRAYON_TURRET)) then
    e = e:ToTear();

    local spr = e:GetSprite();
    spr:ReplaceSpritesheet(0, "gfx/tears_drawn"..tostring(math.random(3))..".png");
    spr:LoadGraphics();

    e.SpriteRotation = math.floor(math.random(3))*90
  end
end

local lastHookLoopFrames = 0;
local ranHookLoopThisFrame = false;
function chars:InputHook(ent, hook, action)
  if chars.player and not chars.game:IsPaused() then
    --##########################
    --DRAWN
    if chars.player:GetName() == "The Drawn" then
      if hook == InputHook.IS_ACTION_TRIGGERED then
        --if bomb is triggered, spawn the drawing and cancel bomb drop
        if Input.IsActionPressed(ButtonAction.ACTION_BOMB, chars.player.ControllerIndex) and action == ButtonAction.ACTION_BOMB then
          if (Genesis.moddata.ink >= chars.InkMenu[chars.drawnSelected][2] or chars.DEBUG) and not chars.drawing then
            if not chars.DEBUG then
              chars:AdjustInk(-chars.InkMenu[chars.drawnSelected][2])
            end
            chars.InkMenu[chars.drawnSelected][3](chars, chars.player.Position)
            chars.drawing = true;
          elseif Genesis.moddata.ink < chars.InkMenu[chars.drawnSelected][2] and not chars.DEBUG and not chars.drawing and not SFXManager():IsPlaying(SoundEffect.SOUND_THUMBS_DOWN) then
            SFXManager():Play(SoundEffect.SOUND_THUMBS_DOWN, 1, 0, false, 1)
          end
          return false

        --if ctrl is pressed, switch selected drawing left
      elseif action == ButtonAction.ACTION_DROP and ((Input.IsActionTriggered(ButtonAction.ACTION_DROP, chars.player.ControllerIndex) and chars.player.ControllerIndex > 0) or (Input.IsButtonTriggered(Keyboard.KEY_LEFT_SHIFT, 0)and chars.player.ControllerIndex == 0)) then
          chars.drawnSelected = chars:GetMenuIcon(chars.drawnSelected+1)
    --      Isaac.DebugString(chars.drawnSelected)

          chars.DrawnUI.menu:Play("Left", true)
          chars.DrawnUI.menu:ReplaceSpritesheet(0, "gfx/ui/inkmenu_"..chars.InkMenu[chars.drawnSelected][1]..".png")
          chars.DrawnUI.menu:ReplaceSpritesheet(1, "gfx/ui/inkmenu_"..chars.InkMenu[chars:GetMenuIcon(chars.drawnSelected-1)][1]..".png")
          chars.DrawnUI.menu:ReplaceSpritesheet(2, "gfx/ui/inkmenu_"..chars.InkMenu[chars:GetMenuIcon(chars.drawnSelected+1)][1]..".png")
          chars.DrawnUI.menu:ReplaceSpritesheet(3, "gfx/ui/inkmenu_"..chars.InkMenu[chars:GetMenuIcon(chars.drawnSelected-2)][1]..".png")
          chars.DrawnUI.menu:LoadGraphics()
        end
      end
    end

    --Tears
    local frames = chars.game:GetFrameCount()

    if frames ~= lastHookLoopFrames then
      ranHookLoopThisFrame = false
      frames = lastHookLoopFrames
    end

    local name = chars.player:GetName();

    if not ranHookLoopThisFrame then
      if name == "The Soulless" or chars.player:HasCollectible(GENESIS_ITEMS.PASSIVE.CURSED_BONE) then
        Genesis.UpdateEntities()
        for i, e in ipairs(Genesis.RoomTears) do
          chars:ForEveryTearForce(e, name)
        end
      end

      ranHookLoopThisFrame = true
    end
  end
end

function chars:Cache(pl, flag)

  local name = pl:GetName()
  local hasBone = pl:HasCollectible(GENESIS_ITEMS.PASSIVE.CURSED_BONE);

  --##########################
  --SOULLESS
  if flag == CacheFlag.CACHE_DAMAGE then
    if name  == "The Soulless" then
      pl.Damage = pl.Damage + 1;
    end
	if hasBone then
	  pl.Damage = pl.Damage + pl:GetCollectibleNum(_G.GENESIS_ITEMS.PASSIVE.CURSED_BONE);
    end
    local data = pl:GetData()
    local count = pl:GetCollectibleNum(GENESIS_ITEMS.PASSIVE.LOST_COURAGE)
    if count > 0 and data.lostCourageDmgBoost then
      pl.Damage = pl.Damage * (1 + data.lostCourageDmgBoost / 100 * count)
    end
  end

  if flag == CacheFlag.CACHE_SHOTSPEED then
    if hasBone then
      pl.ShotSpeed = pl.ShotSpeed+0.6*pl:GetCollectibleNum(_G.GENESIS_ITEMS.PASSIVE.CURSED_BONE)
    end
    if name  == "The Soulless" then
      pl.ShotSpeed = pl.ShotSpeed+0.6
    end
  end

  if (hasBone or name == "The Soulless") and flag == CacheFlag.CACHE_TEARCOLOR then
    local spoon = Isaac.GetItemIdByName("Spoon Bender")
    if pl:GetEffects():HasCollectibleEffect(spoon) or pl:HasCollectible(spoon) then
      pl.LaserColor = Color(0.05, 0.05, 0.2, 1, 20, 0, 70);
    else
      pl.LaserColor = Color(0.05, 0.05, 0.05, 1, 0, 0, 0);
    end
  end

  --##########################
  --XAPHAN
  --statsup stuff moved to genesis as it was more generic use

	if flag == CacheFlag.CACHE_TEARCOLOR and name == "Xaphan" then
    pl.TearColor = Color(0.2,0.2,0.2,1, 255, 75, 0)
    pl.LaserColor = Color(0.2,0.2,0.2,1, 255, 75, 0)
  end

	if Isaac.GetPlayer(0):HasCollectible(GENESIS_ITEMS.ACTIVE.EXTINGUISHED_CANDLE) and chars.hasecandlecostume == false then
		chars.hasecandlecostume = true
		pl:AddNullCostume(GENESIS_COSTUMES.EXTINGUISHED_CANDLE_COSTUME)
		pl:AddNullCostume(GENESIS_COSTUMES.FIRE_HEAD_COSTUME)
	end

  --##########################
  --BARNAEL

  if name == "Barnael" then
    if flag == CacheFlag.CACHE_FLYING and _G.Genesis.moddata.barnael == chars.MAX_KARMA then
      pl.CanFly = true;
    elseif flag == CacheFlag.CACHE_DAMAGE then
      pl.Damage = (pl.Damage-0.5)*1.2;
    elseif flag == CacheFlag.CACHE_RANGE then
      pl.TearHeight = pl.TearHeight+6;
    end
  end
end

function chars:DamagePlayer(ent, dmg, flag, source, countdown)
  local name = chars.player:GetName();

  --##########################
  --SOULLESS
  if (flag == DamageFlag.DAMAGE_SPIKES or flag == DamageFlag.DAMAGE_CURSED_DOOR or flag == DamageFlag.DAMAGE_ACID) and name == "The Soulless" and chars.game:GetRoom():GetType() ~= RoomType.ROOM_SACRIFICE then
    return false;
  end
end

function chars:DamageNpc(npc, dmg, flag, source, countdown)
  if source.Entity then
    local name = chars.player:GetName()
    local contDamageMult
    if chars.player:HasWeaponType(WeaponType.WEAPON_BRIMSTONE) or chars.player:HasWeaponType(WeaponType.WEAPON_KNIFE) or chars.player:HasWeaponType(WeaponType.WEAPON_LUDOVICO_TECHNIQUE)
     or chars.player:HasCollectible(Isaac.GetItemIdByName("Technology 2")) or chars.player:HasWeaponType(WeaponType.WEAPON_TECH_X) then --continuous damage sources
      contDamageMult = 2.5
    else
      contDamageMult = 1
    end
    source = source.Entity;

    --##########################
    --SOULLESS
    local boneOrChar = name == "The Soulless" or chars.player:HasCollectible(_G.GENESIS_ITEMS.PASSIVE.CURSED_BONE)

    --Spaghetti code that will make you raise your brow but somehow works incoming
    if source and (source.SpawnerType == EntityType.ENTITY_PLAYER or source.Type == EntityType.ENTITY_PLAYER) and boneOrChar and not npc:IsBoss() then
      if source.Type == EntityType.ENTITY_TEAR or chars.player:HasWeaponType(WeaponType.WEAPON_BOMBS) or chars.player:HasWeaponType(WeaponType.WEAPON_ROCKETS) or (chars.player:HasWeaponType(WeaponType.WEAPON_LASER) and not contDamage) then
        --[[If it's a black bone tear glowing red shot by us, then do the charm
        If it's a different tear/projectile, give a chance to apply charm]]
        if source.SubType == chars.BONE_SUB_2 or
         (source.Variant ~= TearVariant.BONE and math.max(0, math.min( chars.player.Luck, 4) )* 5 + 5 > chars.rng:RandomInt(100)) then
          npc:AddEntityFlags(EntityFlag.FLAG_CHARM);
        end

      --If chars.player has continuous damage source then give a reduced chance to apply charm
      elseif contDamage and math.max(0, math.min( chars.player.Luck, 4) ) * 5 + 5 > chars.rng:RandomInt(250) then

        npc:AddEntityFlags(EntityFlag.FLAG_CHARM);
      end
    end

    --##########################
    --XAPHAN
    if name == "Xaphan" and (source.Type == EntityType.ENTITY_PLAYER or source.SpawnerType == EntityType.ENTITY_PLAYER) then
      npc:AddBurn(EntityRef(chars.player), 12, 0.3)
    end

    --##########################
    --BARNAEL   (with added spaghetti)
    if (source.SpawnerType == EntityType.ENTITY_PLAYER or source.Type == EntityType.ENTITY_PLAYER) and npc:IsVulnerableEnemy() and name == "Barnael" and Genesis.moddata.barnael == -chars.MAX_KARMA then
      if source.SubType == chars.BARNAEL_TEAR_SUB or (chars.player.Luck*70/15+30 > chars.rng:RandomInt(100*contDamageMult) and not (source.Variant == TearVariant.BLOOD and source.Type == EntityType.ENTITY_TEAR)) then
        npc:AddEntityFlags(chars.BarnaelEvilEffects[math.random(1,#chars.BarnaelEvilEffects)])
      end
    end
  end

  --Unlocks
  if npc.HitPoints-dmg < 0 and npc:IsVulnerableEnemy() then
    if npc.Type == EntityType.ENTITY_MOMS_HEART then
      chars:Unlock(name, "HEART")
    elseif npc.Type == EntityType.ENTITY_THE_LAMB then
      chars:Unlock(name, "LAMB")
    elseif npc.Type == EntityType.ENTITY_SATAN then
      chars:Unlock(name, "SATAN")
    elseif npc.Type == EntityType.ENTITY_ISAAC and npc.Variant == 0 then
      chars:Unlock(name, "ISAAC")
    elseif npc.Type == EntityType.ENTITY_ISAAC and npc.Variant == 1 then
      chars:Unlock(name, "BABY")
    elseif npc.Type == EntityType.ENTITY_HUSH then
      chars:Unlock(name, "HUSH")
    elseif npc.Type == EntityType.ENTITY_DELIRIUM then
      chars:Unlock(name, "DELIRIUM")
    end
  end
end

local fireSoulsPos = Vector(27,33) --why some are locals and some are in the table? different coders that's why
chars.BAR_POS = Vector(140,11);
chars.INK_POS = Vector(118,18);
chars.INKMENU_POS = chars.INK_POS+Vector(15,20)
chars.INK_SPRITE_HEIGTH = 15

function chars:Render()
  local name = chars.player:GetName()
	local room = chars.game:GetRoom()

  if not (room:GetType() == RoomType.ROOM_BOSS and not room:IsClear() and room:GetFrameCount() < 1) then
    --##########################
    --XAPHAN
    math.randomseed(chars.r + 1)
    chars.r = chars.r + 1
    if chars.player:GetActiveItem() == GENESIS_ITEMS.ACTIVE.EXTINGUISHED_CANDLE then
      chars.firesoul_sprite:Render(fireSoulsPos, Genesis.VEC_ZERO, Genesis.VEC_ZERO)
      Genesis.RenderPickupNumber(chars.firesoulscount, fireSoulsPos+Vector(17,3), 0, 0, 1, 2)
    elseif chars.player:GetActiveItem() == GENESIS_ITEMS.ACTIVE.ETERNAL_CANDLE then
      chars.firesoul_sprite:Render(fireSoulsPos, Genesis.VEC_ZERO, Genesis.VEC_ZERO)
      Genesis.RenderPickupNumber(chars.firesoulscount, fireSoulsPos+Vector(17,3), 0, 0, 1, 2, Color(1,0.5,0,1,0,0,0))
    end

    --##########################
    --BARNAEL
    if name == "Barnael" then
      local goodClampOffset = _G.Genesis.moddata.barnael/chars.MAX_KARMA*24; --how much the top left corner of the good bar is moved from the center position;
                                                                     --24 is half the lenght of the bar sprite
      chars.BarnaelBar.base:Render(chars.BAR_POS, Genesis.VEC_ZERO, Genesis.VEC_ZERO);
      chars.BarnaelBar.bad:Render(chars.BAR_POS, Genesis.VEC_ZERO, Genesis.VEC_ZERO);
      chars.BarnaelBar.good:Render(chars.BAR_POS, Vector(28-goodClampOffset,0), Genesis.VEC_ZERO);
      chars.BarnaelBar.bar:Render(chars.BAR_POS+Vector(-goodClampOffset,0), Genesis.VEC_ZERO, Genesis.VEC_ZERO);

    --##########################
    --DRAWN
    elseif name == "The Drawn" then
      chars.DrawnUI.menu:Render(chars.INKMENU_POS, Genesis.VEC_ZERO, Genesis.VEC_ZERO);
      if chars.InkMenu[chars.drawnSelected][2] <= Genesis.moddata.ink then
        Genesis.RenderPickupNumber(chars.InkMenu[chars.drawnSelected][2], chars.INKMENU_POS+Vector(-1,6), 1)
      else
        Genesis.RenderPickupNumber(chars.InkMenu[chars.drawnSelected][2], chars.INKMENU_POS+Vector(-1,6), 1, 0, 1, 1, Color(0.5,0,0,1,0,0,0))
      end

      local clamp = chars.INK_SPRITE_HEIGTH-Genesis.moddata.ink/chars.MAX_INK*chars.INK_SPRITE_HEIGTH;
      local inkOffset;
      if chars.player:GetExtraLives() > 0 and chars.player:GetMaxHearts() + chars.player:GetSoulHearts() >= 11 then
        inkOffset = Vector(20,0)
      else
        inkOffset = Genesis.VEC_ZERO
      end

      chars.DrawnUI.base:Render(chars.INK_POS+inkOffset, Genesis.VEC_ZERO, Genesis.VEC_ZERO);
      chars.DrawnUI.full:Render(chars.INK_POS+inkOffset, Vector(0,clamp), Genesis.VEC_ZERO);

      Genesis.RenderPickupNumber(Genesis.moddata.ink, chars.INK_POS+Vector(15,0)+inkOffset, 0, 1)
    end
  end
end

function chars:UpdateNpcHost(e)
  if chars.player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_COURAGE) then
    if MimicToChest[e.Variant] then
      Isaac.Spawn(EntityType.ENTITY_PICKUP, MimicToChest[e.Variant], 0, e.Position, Genesis.VEC_ZERO, e);
      e:Remove();
    end
  end
end

function chars:UseECandle()
    local ents = Genesis.getRoomEntities()
    for i,e in ipairs(ents) do
        if e.Type == EntityType.ENTITY_FIREPLACE then
			local effect = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.POOF01, 0, e.Position, Genesis.VEC_ZERO, chars.player)
			local sprite = effect:GetSprite()
            if e.Variant == 0 then -- normal fireplace
				sprite.Color = Color(1,1,1,1,255,150,0)
				Genesis.moddata.firesouls = Genesis.moddata.firesouls + 1
				local rand = math.random(1,6)
				if rand == 1 then Genesis.moddata.statsup.TEARS = Genesis.moddata.statsup.TEARS+((chars.player.MaxFireDelay*0.98)-chars.player.MaxFireDelay)
				elseif rand == 2 then Genesis.moddata.statsup.DAMAGE = Genesis.moddata.statsup.DAMAGE+(chars.player.Damage*1.02)-chars.player.Damage
				elseif rand == 3 then Genesis.moddata.statsup.LUCK = Genesis.moddata.statsup.LUCK+0.3
				elseif rand == 4 then Genesis.moddata.statsup.RANGE = Genesis.moddata.statsup.RANGE+(chars.player.TearHeight*1.02)-chars.player.TearHeight
				elseif rand == 5 then Genesis.moddata.statsup.SHOTSPEED = Genesis.moddata.statsup.SHOTSPEED+(chars.player.ShotSpeed*1.02)-chars.player.ShotSpeed
				elseif rand == 6 then Genesis.moddata.statsup.SPEED = Genesis.moddata.statsup.SPEED+(chars.player.MoveSpeed*1.02)-chars.player.MoveSpeed end
			elseif e.Variant == 1 then -- red fireplace
				sprite.Color = Color(1,1,1,1,255,75,0)
				Genesis.moddata.firesouls = Genesis.moddata.firesouls + 2
				local rand = math.random(1,6)
				if rand == 1 then Genesis.moddata.statsup.TEARS = Genesis.moddata.statsup.TEARS+((chars.player.MaxFireDelay*0.96)-chars.player.MaxFireDelay)
				elseif rand == 2 then Genesis.moddata.statsup.DAMAGE = Genesis.moddata.statsup.DAMAGE+(chars.player.Damage*1.04)-chars.player.Damage
				elseif rand == 3 then Genesis.moddata.statsup.LUCK = Genesis.moddata.statsup.LUCK+0.7
				elseif rand == 4 then Genesis.moddata.statsup.RANGE = Genesis.moddata.statsup.RANGE+(chars.player.TearHeight*1.04)-chars.player.TearHeight
				elseif rand == 5 then Genesis.moddata.statsup.SHOTSPEED = Genesis.moddata.statsup.SHOTSPEED+(chars.player.ShotSpeed*1.04)-chars.player.ShotSpeed
				elseif rand == 6 then Genesis.moddata.statsup.SPEED = Genesis.moddata.statsup.SPEED+(chars.player.MoveSpeed*1.04)-chars.player.MoveSpeed end
            elseif e.Variant == 2 then -- blue fireplace
				sprite.Color = Color(1,1,1,1,0,200,255)
				Genesis.moddata.firesouls = Genesis.moddata.firesouls + 3
				Genesis.moddata.statsup.DAMAGE = Genesis.moddata.statsup.DAMAGE+(chars.player.Damage*1.05)-chars.player.Damage
				local rand = math.random(1,4)
				if rand == 1 then chars.player:AddSoulHearts(2) end
            elseif e.Variant == 3 then -- purple fireplace
				sprite.Color = Color(1,1,1,1,125,0,255)
				Genesis.moddata.firesouls = Genesis.moddata.firesouls + 4
				Genesis.moddata.statsup.LUCK = Genesis.moddata.statsup.LUCK + 0.5
				Genesis.moddata.statsup.TEARS = Genesis.moddata.statsup.TEARS+((chars.player.MaxFireDelay*0.95)-chars.player.MaxFireDelay)
				local rand = math.random(1,10)
				if rand == 1 then chars.player:AddBlackHearts(2) end
			end
			e:Remove()
        end
    end
    return true
end

function chars:UseEternalCandle()
	chars.roomsetonfire = true
	return true
end

function chars:GetItemFromSeed(pool, seed)
  local a = math.ceil(seed % 1000 --[[last 3 digits of the seed]] / 999 * #pool);
  return pool[a]
end

function chars:UpdateGlow(e)
  local spr = e:GetSprite();
  local alpha = math.abs(_G.Genesis.moddata.barnael/chars.MAX_KARMA)

  if _G.Genesis.moddata.barnael > 0 then
    spr.Color = Color(1,1,1,alpha,0,0,0)
  else
    spr.Color = Color(1,0,0,alpha,155,0,0)
  end
end

function chars:AdjustKarma(a)
  if Genesis.moddata.barnael <= chars.MAX_KARMA and Genesis.moddata.barnael>= -chars.MAX_KARMA then
    if Genesis.moddata.barnael + a > chars.MAX_KARMA or Genesis.moddata.barnael + a < -chars.MAX_KARMA then
      a = chars.MAX_KARMA-Genesis.moddata.barnael;
    end

    --Update costume if chars.player is at full karma, and add item effect with a random chance
    --FULLY GOOD
    if _G.Genesis.moddata.barnael + a == chars.MAX_KARMA then
      chars.player:TryRemoveNullCostume(chars.BarnaelCostumes.NEUTRAL)
      chars.player:AddNullCostume(chars.BarnaelCostumes.GOOD, -1);

    --FULLY EVIL
    elseif _G.Genesis.moddata.barnael + a == -chars.MAX_KARMA then
      chars.player:TryRemoveNullCostume(chars.BarnaelCostumes.NEUTRAL)
      chars.player:AddNullCostume(chars.BarnaelCostumes.BAD, -1);

    --INBETWEEN
    elseif _G.Genesis.moddata.barnael == -chars.MAX_KARMA or _G.Genesis.moddata.barnael == chars.MAX_KARMA then
      if _G.Genesis.moddata.barnael == -chars.MAX_KARMA then
        chars.player:TryRemoveNullCostume(chars.BarnaelCostumes.BAD)
        chars.player:RemoveCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE);
      else
        chars.player:TryRemoveNullCostume(chars.BarnaelCostumes.GOOD)
      end
      chars.player:AddNullCostume(chars.BarnaelCostumes.NEUTRAL, -1);

    end

    --If the chars.player is between -20% and 20%, add duality, else remove it
    if _G.Genesis.moddata.barnael+a <= chars.MAX_KARMA*0.2 and _G.Genesis.moddata.barnael+a >= -chars.MAX_KARMA*0.2 and (_G.Genesis.moddata.barnael > chars.MAX_KARMA*0.2 or _G.Genesis.moddata.barnael < -chars.MAX_KARMA*0.2) then
      chars.player:AddCollectible(CollectibleType.COLLECTIBLE_DUALITY, 0, false);
      chars.player:RemoveCostume(GetConfigItem(CollectibleType.COLLECTIBLE_DUALITY));
    elseif _G.Genesis.moddata.barnael <= chars.MAX_KARMA*0.2 and _G.Genesis.moddata.barnael >= -chars.MAX_KARMA*0.2 and (_G.Genesis.moddata.barnael+a > chars.MAX_KARMA*0.2 or _G.Genesis.moddata.barnael+a < -chars.MAX_KARMA*0.2) then
      chars.player:RemoveCollectible(CollectibleType.COLLECTIBLE_DUALITY);
    end

    _G.Genesis.moddata.barnael = _G.Genesis.moddata.barnael+a;
    chars.player:AddCacheFlags(CacheFlag.CACHE_FLYING);
    chars.player:EvaluateItems();
  end
end

function chars:UseCrayon()
  local turret = chars:SpawnTurret(chars.player.Position);
  turret:GetSprite():ReplaceSpritesheet(1, "gfx/Crayon2.png")
  turret:GetSprite():LoadGraphics()

  return true;
end

--Basically gets the value and wraps it around the table if it is out of bounds
function chars:GetMenuIcon(a)
  if a >= 1 and a <= #chars.InkMenu then
    return a
  elseif a < 1 then
    return #chars.InkMenu
  elseif a > #chars.InkMenu then
    return 1
  end
end

function chars:AdjustInk(a)
  if Genesis.moddata.ink <= chars.MAX_INK then
    if Genesis.moddata.ink + a > chars.MAX_INK then
      a = chars.MAX_INK-chars.ink;
    end

    Genesis.moddata.ink = Genesis.moddata.ink+a

--    Isaac.DebugString(Genesis.moddata.ink)
  end
end

function chars:PickTurretTarget(tur) --PickEnemyTarget crashes the chars.game so made a workaround function
  local ents = Genesis.getRoomEntities()
  local target = nil;
  local targetDist;

  for i,e in ipairs(ents) do
    local dist = (e.Position-tur.Position):Length()
    if e:IsVulnerableEnemy() and dist<200 then
      if not target or (target.Position-e.Position):Length() > dist then
        target = e
      end
    end
  end

  return target
end

function chars:GetStringDirFromVector(v)
  if v.X > 0 and math.abs(v.X) > math.abs(v.Y) then
    return "Right"
  elseif v.X < 0 and math.abs(v.X) > math.abs(v.Y) then
    return "Left"
  elseif v.Y > 0 and math.abs(v.Y) > math.abs(v.X) then
    return "Down"
  elseif v.Y < 0 and math.abs(v.Y) > math.abs(v.X) then
    return "Up"
  end
end

function chars:UpdateTurret(turret)
  turret = turret:ToFamiliar()

  local data = turret:GetData()
  local spr = turret:GetSprite()

  if spr:IsPlaying("AppearDrawn") and spr:IsEventTriggered("FinishDrawing") then
    spr:Play("FloatDown", false)
    data.animDir = "Down"
  elseif not spr:IsPlaying("AppearDrawn") then
    if data.FireDelay == nil or data.FireDelay == 0 then
      data.FireDelay = math.max(6,turret.Player.MaxFireDelay*1.5)

      local target = chars:PickTurretTarget(turret)
      if target then
        local tearDir = (target.Position - turret.Position):Normalized();
        local tearVelocity = tearDir*turret.Player.ShotSpeed*10
        data.animDir = chars:GetStringDirFromVector(target.Position - turret.Position)

        chars.player:FireTear(turret.Position,tearVelocity,false,true,false)
		if data.animDir ~= nil then
			spr:Play("FloatShoot"..data.animDir, true)
		end
        data.animTimeout = 3
      end
    elseif data.FireDelay > 0 then
      data.FireDelay = data.FireDelay-1
    end

    if not data.animTimeout then
      data.animTimeout = 60;
    elseif data.animTimeout == 0 then
      if spr:IsPlaying("FloatShoot"..data.animDir) then
        spr:Play("Float"..data.animDir, true)
        data.animTimeout = 60
      else
        spr:Play("FloatDown", false)
        data.animTimeout = -1
      end
    elseif data.animTimeout > 0 then
      data.animTimeout = data.animTimeout - 1
    end
  end
end

function chars:SpawnTurret(pos)
	local turret = Isaac.Spawn(EntityType.ENTITY_FAMILIAR, GENESIS_ENTITIES.VARIANT.CRAYON_TURRET, 0, Isaac.GetFreeNearPosition(pos+Vector(0,20), 16), Genesis.VEC_ZERO, chars.player):ToFamiliar();
  turret.GridCollisionClass = GridCollisionClass.COLLISION_NONE;
  turret.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ENEMIES;
  turret.Friction = 0.1;
  turret.IsFollower = false;
  turret:GetSprite():Play("AppearDrawn", false)
  turret:ClearEntityFlags(EntityFlag.FLAG_APPEAR)

  chars.prevRoom = chars.game:GetRoom():GetDecorationSeed()

  return turret
end

function chars:SpawnHeart()
  local room = chars.game:GetRoom()
  local pos
  repeat
    pos = room:GetRandomPosition(0)
  until room:CheckLine(chars.player.Position, pos, 0, 0, false, false)

  local e = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, 0, pos, Genesis.VEC_ZERO, chars.player):ToPickup();
  local spr = e:GetSprite()

  if (e.InitSeed/100)%100 <= 15+math.max(0,math.min(chars.player.Luck/15*40,40)) then --randomly replace with one "special" heart 15% - 0L, 55% - 15L
    if e.InitSeed%100 < 5 then --eternal heart: 5%
      e:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, 4, false)
    elseif e.InitSeed%100 < 10 then --golden heart: 5%
      e:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, 7, false)
    elseif e.InitSeed%100 < 30 then --black: 20%
      e:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, 6, false)
    else --soul: 70%
      e:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, 3, false)
    end
  end

  spr:Load(chars.DrawnPickupAnm[PickupVariant.PICKUP_HEART], false)
  if chars.DrawnPickupSprite[PickupVariant.PICKUP_HEART][e.SubType] then
    spr:ReplaceSpritesheet(0, chars.DrawnPickupSprite[e.Variant][e.SubType])
  end
  spr:LoadGraphics()
  spr:Play("AppearDrawn", true)

  e:GetData().drawn = true
end

function chars:SpawnKey()
  local room = chars.game:GetRoom()
  local pos
  repeat
    pos = room:GetRandomPosition(0)
  until room:CheckLine(chars.player.Position, pos, 0, 0, false, false)

  local e = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_KEY, 1, pos, Genesis.VEC_ZERO, chars.player):ToPickup();
  local spr = e:GetSprite()

  if (e.InitSeed/100)%100 <= 15+math.max(0,math.min(chars.player.Luck/15*40,40)) then --randomly replace with one "special" key 15% - 0L, 55% - 15L
    if e.InitSeed%100 < 15 then --golden : 15%
      e:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_KEY, 2, false)
      elseif e.InitSeed%100 < 30 then --charged: 20%
      e:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_KEY, 4, false)
    elseif e.InitSeed%100 < 70 then --double key: 30%
      e:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_KEY, 3, false)
    end
  end

  spr:Load(chars.DrawnPickupAnm[PickupVariant.PICKUP_KEY], true)
  spr:Play("AppearDrawn", true)

  e:GetData().drawn = true
end

function chars:SpawnBomb(pos)
  local e = chars.player:FireBomb(pos, Genesis.VEC_ZERO)
  local spr = e:GetSprite()

  spr:Load(chars.DrawnBombAnm[e.Variant] or chars.DrawnBombAnm[BombVariant.BOMB_NORMAL], true);
end

function chars:SpawnCoin()
  local room = chars.game:GetRoom()
  local pos
  repeat
    pos = room:GetRandomPosition(0)
  until room:CheckLine(chars.player.Position, pos, 0, 0, false, false)

  local e = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, 1, pos, Genesis.VEC_ZERO, chars.player):ToPickup();
  local spr = e:GetSprite()

  if e.InitSeed % 100 <= 5 then
    e:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, 3, false)  --Dime
  elseif e.InitSeed % 100 <= 20 then
    e:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, 2, false)  --Nickel
  end

  spr:Load(chars.DrawnPickupAnm[e.Variant], false)
  if chars.DrawnPickupSprite[e.Variant][e.SubType] then
    spr:ReplaceSpritesheet(0, chars.DrawnPickupSprite[e.Variant][e.SubType])
  end
  spr:LoadGraphics()
  spr:Play("AppearDrawn", true)

  e:GetData().drawn = true
end

function chars:SpawnStomp(pos)
  local e = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.MOM_FOOT_STOMP, 1, pos, Genesis.VEC_ZERO, chars.player)
  local spr = e:GetSprite()
  spr:Load("gfx/drawnstomp.anm2", true)
  spr:Play("Stomp", true)
end

chars.InkMenu = {
  {"turret",30,chars.SpawnTurret},
  {"heart",10,chars.SpawnHeart},
  {"coin",10,chars.SpawnCoin},
  {"key",15,chars.SpawnKey},
  {"bomb",15,chars.SpawnBomb},
  {"stomp",18,chars.SpawnStomp}
}

--Prevent crashes
function chars:PreGameEnd()
  chars.game = nil
  chars.player = nil
end

function LostCourageUpdate(_, player)
    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_COURAGE) then
        local closestenemydistance = 999999999
        for _, e in pairs(Genesis.getRoomEntities()) do
            if e:IsVulnerableEnemy() then
		        local disttoenemy = e.Position:DistanceSquared(player.Position)
		        if disttoenemy < closestenemydistance then
		    	    closestenemydistance = disttoenemy
                end
            end
        end
        local data = player:GetData()
        data.lostCourageDmgBoost = 90 - math.sqrt(closestenemydistance) * 70 / 128
        if data.lostCourageDmgBoost < 0 then
            data.lostCourageDmgBoost = 0
        end
        if data.lostCourageDmgBoost > 0 then
            player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
            player:EvaluateItems()
        end
    end
end
chars:AddCallback(ModCallbacks.MC_POST_PLAYER_UPDATE, LostCourageUpdate)

chars:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, chars.Init);
chars:AddCallback(ModCallbacks.MC_POST_UPDATE, chars.Update);
chars:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, chars.NewRoom)
chars:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, chars.NewFloor)
chars:AddCallback(ModCallbacks.MC_INPUT_ACTION, chars.InputHook);
chars:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, chars.PlayerUpdate);
chars:AddCallback(ModCallbacks.MC_POST_RENDER, chars.Render);
chars:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, chars.Cache);
chars:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, chars.DamagePlayer, EntityType.ENTITY_PLAYER);
chars:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, chars.DamageNpc);
chars:AddCallback(ModCallbacks.MC_PRE_GAME_EXIT, chars.PreGameEnd);

chars:AddCallback(ModCallbacks.MC_NPC_UPDATE, chars.UpdateNpcHost, EntityType.ENTITY_HOST);
chars:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, chars.UpdateTurret, GENESIS_ENTITIES.VARIANT.CRAYON_TURRET)

chars:AddCallback(ModCallbacks.MC_USE_ITEM, chars.UseECandle, GENESIS_ITEMS.ACTIVE.EXTINGUISHED_CANDLE)
chars:AddCallback(ModCallbacks.MC_USE_ITEM, chars.UseEternalCandle, GENESIS_ITEMS.ACTIVE.ETERNAL_CANDLE)
chars:AddCallback(ModCallbacks.MC_USE_ITEM, chars.UseCrayon, GENESIS_ITEMS.ACTIVE.CRAYON)

if Isaac.GetPlayer(0) then chars:Init(true, true) end --do init if the mod was reloaded ingame

Isaac.DebugString("Genesis+: Loaded Characters!")
error({ hack = true })