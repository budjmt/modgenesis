---------------
--SEPTIC BOSS--
---------------

-----------------------------------------------------------------------
-- BOSS PHASES --
-- -1 = NOT INIT --
-- 0 = Start (he will go back to the water after scream) --
-- 1 = Underwater
-- 2 = Underwater break --
-- 3 = Shoot on the place --
-----------------------------------------------------------------------

local septicBoss = RegisterMod("Genesis+ Septic Boss", 1)

local SERPENT = {
    Background_Floor = nil,
    Background_Wall = nil,
    jump_delay = 0,
    platform = {},
    bridges = {},
    bossEntity = nil,
    numAround = 0,
    targetPlatform = 0,
    staticPosition = nil,
    phase = -1,
    brokenPlatform = 0
}

Genesis.SerpentPool = {
    CollectibleType.COLLECTIBLE_BEAN,
    CollectibleType.COLLECTIBLE_BOBS_ROTTEN_HEAD,
    CollectibleType.COLLECTIBLE_MEGA_BEAN,
    CollectibleType.COLLECTIBLE_BLACK_BEAN,
    CollectibleType.COLLECTIBLE_BOBS_BRAIN,
    CollectibleType.COLLECTIBLE_BOBS_CURSE,
    CollectibleType.COLLECTIBLE_COMMON_COLD,
    CollectibleType.COLLECTIBLE_IPECAC,
    CollectibleType.COLLECTIBLE_SCORPIO,
    CollectibleType.COLLECTIBLE_SERPENTS_KISS,
    CollectibleType.COLLECTIBLE_TOXIC_SHOCK,
    CollectibleType.COLLECTIBLE_VIRUS,
    GENESIS_ITEMS.PASSIVE.POISON_RAY,
    GENESIS_ITEMS.PASSIVE.EXPLOSIVE_TEMPER
}

-- BOSS VARIABLES --
Genesis.moddata.SerpentDefeated = false

function checkForJump(player, gridPos, playerDir)
    if player:IsFlying() then
        return
    end
    if SERPENT.jump_delay > 0 then
        return
    end
    local doGrid = false
    local leftGrids = {51, 66, 81, 55, 70, 85}
    local rightGrids = {49, 64, 79, 53, 68, 83}

    if playerDir == Direction.LEFT then
        for k, v in pairs(leftGrids) do
            if gridPos == v then
                player.Velocity = Vector(-2, 0)
                SERPENT.jump_delay = 20
                return true
            end
        end
    end

    if playerDir == Direction.RIGHT then
        for k, v in pairs(rightGrids) do
            if gridPos == v then
                player.Velocity = Vector(2, 0)
                SERPENT.jump_delay = 20
                return true
            end
        end
    end
    return false
end

function septicBoss:PeffectUpdate(player)
    local level = Game():GetLevel()
    local room = level:GetCurrentRoom()

    --On the last boss room of the last septic floor
    --if (level:GetStage() == LevelStage.STAGE2_2 or
        --(level:GetCurseName() == "Curse of the Labyrinth" and level:GetStage() == LevelStage.STAGE2_1)) and
    if Genesis.IsSeptic2() and
        level:GetCurrentRoomDesc().ListIndex == level:GetLastBossRoomListIndex() and
        room:GetFrameCount() > 1 and
        room:IsClear()
     then
        if not SERPENT.removedTrapdoor then
            local gridSize = room:GetGridSize()
            for i = 0, gridSize - 1 do
                if room:GetGridEntity(i) ~= nil then
                    SERPENT.removedTrapdoor = true
                    break
                end
            end
        end

        Isaac.DebugString(tostring(SERPENT.spawnedKey) .. ";" .. tostring(SERPENT.removedTrapdoor))

        if not SERPENT.spawnedKey then
            Isaac.DebugString(#Genesis.RoomPickups)
            for i, e in ipairs(Genesis.RoomPickups) do
                Isaac.DebugString(e.Variant)
                if e.Variant == PickupVariant.PICKUP_COLLECTIBLE then
                    --if StageSystem.currentstage == StageSystem.GetStageIdByName("new_Septic") and level:GetStage() == LevelStage.STAGE2_2 and level:GetStageType() == StageType.STAGETYPE_WOTL then
                        e:Morph(5, 100, GENESIS_ITEMS.PASSIVE.RARE_KEY, false)
                        SERPENT.spawnedKey = true
                        break
                    --end
                end
            end
        end
    end

    --Update the trapdoor effect
    if SERPENT.trapdoor then
        local grid = room:GetGridEntity(67)
        if grid then
            local trapdooranimations = {"Opened", "Closed", "Open Animation", "Player Exit"}
            for i = 1, #trapdooranimations do
                if grid.Sprite:IsPlaying(trapdooranimations[i]) or grid.Sprite:IsFinished(trapdooranimations[i]) then
                    SERPENT.trapdoor:GetSprite():SetFrame(trapdooranimations[i], grid.Sprite:GetFrame())
                end
            end
        end
    end
end

septicBoss:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, septicBoss.PeffectUpdate)

local SerpentBackdrop = StageAPI.BackdropHelper({
    Walls = {"serpent_boss"},
    NFloors = {"new_Septic1_nfloor"},
    LFloors = {"new_Septic1_lfloor"},
    Corners = {"new_Septic1_corner"}
}, "gfx/backdrop/", ".png")

local SerpentGrid = Genesis.SepticGrid()
SerpentGrid:SetPits({{
    File = "stageapi/none.png",
    HasExtraFrames = true
}})

local SerpentRoomGfx = StageAPI.RoomGfx(SerpentBackdrop, SerpentGrid, "_default", "stageapi/shading/shading")
Genesis.SepticStage:SetRoomGfx(SerpentRoomGfx, {"SerpentBoss"})

local SerpentRooms = StageAPI.RoomsList('SepticSerpent', {
    Name = 'SepticGenesis+ - Serpent',
    Rooms = require('resources.luarooms.septic-serpent')
})
if SepticRoomCompat.Serpent then
    SerpentRooms:AddRooms(table.unpack(SepticRoomCompat.Serpent))
end

StageAPI.AddCallback("Genesis+", "POST_STAGEAPI_NEW_ROOM_GENERATION", 2, function()
    if not Genesis.IsSeptic2() then return end

    local level = Game():GetLevel()
    if level:GetStartingRoomIndex() == level:GetCurrentRoomIndex() then
        if level:GetCurrentRoom():IsFirstVisit() then
            local room = StageAPI.LevelRoom(nil, SerpentRooms)
            room:SetTypeOverride("SerpentBoss")
            return room
        end
    end
end)

StageAPI.AddCallback("Genesis+", "POST_SELECT_STAGE_MUSIC", 1, function(stage, musicID, roomType, rng)
    if StageAPI.GetCurrentRoomType() ~= "SerpentBoss" then return end

    if SERPENT.bossEntity then
        return GENESIS_MUSIC.THE_SERPENT
    elseif not SERPENT.justEntered and Genesis.moddata.SerpentDefeated then
        return GENESIS_MUSIC.ALT_BOSS_JINGLE -- stageapi workaround
        --return Music.MUSIC_JINGLE_BOSS_OVER
    end
end)

local function UpdateGrids()
    local rtype = StageAPI.GetCurrentRoomType()
    local grids = StageAPI.CurrentStage.RoomGfx[rtype].Grids

    StageAPI.ChangeGrids(grids)
end

function septicBoss:OnRoomEnter()
    local player = Isaac.GetPlayer(0)
    local level = Game():GetLevel()
    local room = Game():GetRoom()

    SERPENT.justEntered = true
    SERPENT.trapdoor = nil

    if StageAPI.GetCurrentRoomType() == "SerpentBoss" then
            if Genesis.moddata.SerpentDefeated == false and SERPENT.phase ~= -1 then
                SERPENT.phase = -1
            end

            -- Spawn platforms --

            SERPENT.platform[1] =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                GENESIS_ENTITIES.VARIANT.SEPTIC_PLATFORM,
                0,
                room:GetGridPosition(47) + Vector(-20, -20),
                Vector(0, 0),
                nil
            )
            SERPENT.platform[1]:GetSprite():Play("Idle", true)
            SERPENT.platform[1]:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            SERPENT.platform[1]:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)
            makePlatform(1, false, false)

            SERPENT.platform[2] =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                GENESIS_ENTITIES.VARIANT.SEPTIC_PLATFORM,
                0,
                room:GetGridPosition(51) + Vector(-20, -20),
                Vector(0, 0),
                nil
            )
            if Genesis.moddata.SerpentDefeated == false then
                SERPENT.platform[2]:GetSprite():ReplaceSpritesheet(0, "gfx/septic_boss/platform_locked.png")
                SERPENT.platform[2]:GetSprite():LoadGraphics()
            end
            SERPENT.platform[2]:GetSprite():Play("Idle", true)
            SERPENT.platform[2]:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            SERPENT.platform[2]:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)
            makePlatform(2, false, false)

            SERPENT.platform[3] =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                GENESIS_ENTITIES.VARIANT.SEPTIC_PLATFORM,
                0,
                room:GetGridPosition(55) + Vector(-20, -20),
                Vector(0, 0),
                nil
            )
            SERPENT.platform[3]:GetSprite():Play("Idle", true)
            SERPENT.platform[3]:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            SERPENT.platform[3]:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)
            makePlatform(3, false, false)

            -- BRIDGES --
            fixBridge()

            SERPENT.bridges[1] =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                GENESIS_ENTITIES.VARIANT.SEPTIC_PLATFORM,
                0,
                room:GetGridPosition(61) + Vector(-20, -20),
                Vector(0, 0),
                nil
            )
            SERPENT.bridges[1]:GetSprite():ReplaceSpritesheet(0, "gfx/grid/septic_bridge.png")
            SERPENT.bridges[1]:GetSprite():Load("gfx/septic_boss/septic_bridge.anm2", true)
            SERPENT.bridges[1]:GetSprite():Play("Idle", true)
            SERPENT.bridges[1]:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            SERPENT.bridges[1]:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)

            SERPENT.bridges[2] =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                GENESIS_ENTITIES.VARIANT.SEPTIC_PLATFORM,
                0,
                room:GetGridPosition(22) + Vector(-20, -20),
                Vector(0, 0),
                nil
            )
            SERPENT.bridges[2]:GetSprite():ReplaceSpritesheet(0, "gfx/grid/septic_bridge.png")
            SERPENT.bridges[2]:GetSprite():Load("gfx/septic_boss/septic_bridge.anm2", true)
            SERPENT.bridges[2]:GetSprite():Play("Idle", true)
            SERPENT.bridges[2]:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            SERPENT.bridges[2]:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)

            SERPENT.bridges[3] =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                GENESIS_ENTITIES.VARIANT.SEPTIC_PLATFORM,
                0,
                room:GetGridPosition(37) + Vector(-20, -20),
                Vector(0, 0),
                nil
            )
            SERPENT.bridges[3]:GetSprite():ReplaceSpritesheet(0, "gfx/grid/septic_bridge.png")
            SERPENT.bridges[3]:GetSprite():Load("gfx/septic_boss/septic_bridge.anm2", true)
            SERPENT.bridges[3]:GetSprite():Play("Idle", true)
            SERPENT.bridges[3]:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            SERPENT.bridges[3]:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)

            SERPENT.bridges[4] =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                GENESIS_ENTITIES.VARIANT.SEPTIC_PLATFORM,
                0,
                room:GetGridPosition(73) + Vector(-20, -20),
                Vector(0, 0),
                nil
            )
            SERPENT.bridges[4]:GetSprite():ReplaceSpritesheet(0, "gfx/grid/septic_bridge.png")
            SERPENT.bridges[4]:GetSprite():Load("gfx/septic_boss/septic_bridge.anm2", true)
            SERPENT.bridges[4]:GetSprite():Play("Idle", true)
            SERPENT.bridges[4]:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            SERPENT.bridges[4]:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)

            SERPENT.bridges[5] =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                GENESIS_ENTITIES.VARIANT.SEPTIC_PLATFORM,
                0,
                room:GetGridPosition(97) + Vector(-20, -20),
                Vector(0, 0),
                nil
            )
            SERPENT.bridges[5]:GetSprite():ReplaceSpritesheet(0, "gfx/grid/septic_bridge.png")
            SERPENT.bridges[5]:GetSprite():Load("gfx/septic_boss/septic_bridge.anm2", true)
            SERPENT.bridges[5]:GetSprite():Play("Idle", true)
            SERPENT.bridges[5]:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            SERPENT.bridges[5]:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)

            SERPENT.bridges[6] =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                GENESIS_ENTITIES.VARIANT.SEPTIC_PLATFORM,
                0,
                room:GetGridPosition(112) + Vector(-20, -20),
                Vector(0, 0),
                nil
            )
            SERPENT.bridges[6]:GetSprite():ReplaceSpritesheet(0, "gfx/grid/septic_bridge.png", true)
            SERPENT.bridges[6]:GetSprite():Load("gfx/septic_boss/septic_bridge.anm2", true)
            SERPENT.bridges[6]:GetSprite():Play("Idle", true)
            SERPENT.bridges[6]:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            SERPENT.bridges[6]:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)

            --Spawn the trapdoor effect so it can be seen over the platform
            if Genesis.moddata.SerpentDefeated then
                local grid = room:GetGridEntity(67)
                if not grid or grid.Type ~= GridEntityType.GRID_TRAPDOOR then
                    if grid then
                        room:RemoveGridEntity(67, 0, false)
                    end
                    room:SpawnGridEntity(67, GridEntityType.GRID_TRAPDOOR, 0, 0, math.random(10000), 0)
                end
                grid = room:GetGridEntity(67)
                SERPENT.trapdoor =
                    Isaac.Spawn(
                    1000,
                    GENESIS_ENTITIES.VARIANT.TRAPDOOR_EFFECT,
                    0,
                    room:GetGridPosition(67),
                    Genesis.VEC_ZERO,
                    nil
                )
                local trapdooranimations = {"Opened", "Closed", "Open Animation", "Player Exit"}
                for i = 1, #trapdooranimations do
                    if grid.Sprite:IsPlaying(trapdooranimations[i]) or grid.Sprite:IsFinished(trapdooranimations[i]) then
                        SERPENT.trapdoor:GetSprite():SetFrame(trapdooranimations[i], grid.Sprite:GetFrame())
                    end
                end
            end
    --    local ents = Genesis.getRoomEntities()
    --    for k, v in pairs(ents) do
    --      if v.Type > 3 then
    --       -- Isaac.DebugString(tostring(v.Type) .. " | " .. tostring(v.Variant) .. " | " .. tostring(v.Subtype))
    --      end
    --    end
        UpdateGrids()
    end
end

function makePlatform(n, breaking, playAppear)
    local room = Game():GetRoom()

    if playAppear == nil then
        playAppear = true
    end

    local ids
    if n == 1 then
        ids = {47, 48, 49, 62, 63, 64, 77, 78, 79}
    elseif n == 2 then
        ids = {51, 52, 53, 66, 67, 68, 81, 82, 83}
    elseif n == 3 then
        ids = {55, 56, 57, 70, 71, 72, 85, 86, 87}
    end

    if not ids then return end

    for k, v in pairs(ids) do
        if breaking then
            room:SpawnGridEntity(v, GridEntityType.GRID_PIT, 0, 0, 0)
            for i = 0, 3 do
                local player = Isaac.GetPlayer(i)
                if not player:IsFlying() and room:GetGridIndex(player.Position) == v and
                        not player:GetSprite():IsPlaying("Jump") and
                        not player:IsDead() then
                    if player.HitPoints <= 0 then
                        player:Die()
                    else
                        player:AnimatePitfallIn()
                    end
                end
            end
        else
            room:RemoveGridEntity(v, 0, false)
        end
        if breaking then
            if n == 2 then
                SERPENT.platform[n]:GetSprite():ReplaceSpritesheet(0, "gfx/septic_boss/platform.png")
                SERPENT.platform[n]:GetSprite():LoadGraphics()
            end
            SERPENT.platform[n]:GetSprite():Play("Die", true)
        elseif playAppear then
            SERPENT.platform[n]:GetSprite():Play("Appear", true)
        end
    end
end

function fixBridge()
    local room = Game():GetRoom()
    room:RemoveGridEntity(22, 0, false)
    room:RemoveGridEntity(37, 0, false)
    room:RemoveGridEntity(61, 0, false)
    room:RemoveGridEntity(73, 0, false)
    room:RemoveGridEntity(97, 0, false)
    room:RemoveGridEntity(112, 0, false)
    for k, v in pairs(SERPENT.bridges) do
        v:GetSprite():Play("Appear", true)
    end
end

function breakBridge()
    local room = Game():GetRoom()
    room:SpawnGridEntity(22, GridEntityType.GRID_PIT, 0, 0, 0)
    room:SpawnGridEntity(37, GridEntityType.GRID_PIT, 0, 0, 0)
    room:SpawnGridEntity(61, GridEntityType.GRID_PIT, 0, 0, 0)
    room:SpawnGridEntity(73, GridEntityType.GRID_PIT, 0, 0, 0)
    room:SpawnGridEntity(97, GridEntityType.GRID_PIT, 0, 0, 0)
    room:SpawnGridEntity(112, GridEntityType.GRID_PIT, 0, 0, 0)
    for k, v in pairs(SERPENT.bridges) do
        v:GetSprite():Play("Dissapear", true)
    end
end

function septicBoss:render()
    for i = 0, 3 do
        local player = Isaac.GetPlayer(i)
        local room = Game():GetRoom()
        if StageAPI.GetCurrentRoomType() == "SerpentBoss" then
                -- Delay --
                if SERPENT.jump_delay > 0 then
                    SERPENT.jump_delay = SERPENT.jump_delay - 1
                end
                -- Check for Isaac jumping over platforms --
                if checkForJump(player, room:GetGridIndex(player.Position), player:GetMovementDirection()) then
                    player:UseActiveItem(CollectibleType.COLLECTIBLE_HOW_TO_JUMP)
                end
        end

        if SERPENT.trapdoor then
            if player.Position:Distance(SERPENT.trapdoor.Position) < 32 then
                player:Render(Genesis.VEC_ZERO)
            end
        end
    end
end

function septicBoss:bossFight()
    local level = Game():GetLevel()
    local room = Game():GetRoom()
    local entities = Genesis.getRoomEntities()
    if StageAPI.GetCurrentRoomType() == "SerpentBoss" then
            if SERPENT.bossEntity == nil then
                SERPENT.phase = -1
            end


            for i = 0, 3 do
                local player = Isaac.GetPlayer(i)
                if player:GetSprite():IsFinished("FallIn") then
                    if SERPENT.brokenPlatform == 1 then
                        player.Position = room:GetGridPosition(67)
                    elseif SERPENT.brokenPlatform == 1 then
                        player.Position = room:GetGridPosition(71)
                    else
                        player.Position = room:GetGridPosition(63)
                    end
                end
                if SERPENT.phase ~= -1 and not player:IsFlying()
                and room:GetGridCollisionAtPos(player.Position) == GridCollisionClass.COLLISION_PIT
                and not player:GetSprite():IsPlaying("Jump")
                and not player:GetSprite():IsPlaying("FallIn")
                and Genesis.moddata.SerpentDefeated == false
                and not player:IsDead() then
                    if player.HitPoints <= 0 then
                      player:Die()
                    else
                      player:AnimatePitfallIn ()
                    end
                end
            end

            -- Remove Water Splash --
            for k, v in ipairs(entities) do
                if
                    v.Type == 1000 and v.Variant == GENESIS_ENTITIES.VARIANT.SEPTIC_SPLASH and
                        v:GetSprite():IsFinished("Default")
                 then
                    v:Remove()
                end
                -- Check if boss projectiles touch the player --
				if v:Exists() and v:GetData().IsSerpentBubble then
					--            Isaac.DebugString(v:GetSprite():GetFilename())
					v = v:ToProjectile()
					v.Scale = v.Scale + 0.06
					if v.Scale > 6 then
						v:Die()
					end
				end
				--despawn potato dummy to stop it breaking room clear effect
				if v.Type == 231 and v.Variant == 2 and v.SubType == 1 then
					v:Remove()
				end
            end

            local player = Isaac.GetPlayer(0)
            if SERPENT.phase == -1 and room:GetGridIndex(player.Position) == 67 and
                player:HasCollectible(GENESIS_ITEMS.PASSIVE.RARE_KEY) and
                Genesis.moddata.SerpentDefeated == false and SERPENT.bossEntity == nil then
                -- TODO delay spawning the serpent and play this animation
                --StageAPI.PlayBossAnimation(StageAPI.GetBossData("Serpent"), true)
                SERPENT.phase = 0
                Game():ShakeScreen(40)
                SERPENT.bossEntity =
                    Isaac.Spawn(
                    GENESIS_ENTITIES.THE_SERPENT,
                    GENESIS_ENTITIES.VARIANT.THE_SERPENT,
                    0,
                    room:GetGridPosition(22) + Vector(0, 30),
                    Vector(0, 0),
                    nil
                ):ToNPC()
                SERPENT.bossEntity.Friction = 1
                SERPENT.bossEntity.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
                SERPENT.staticPosition = SERPENT.bossEntity.Position
                SERPENT.numAround = 2
                --player:RemoveCollectible(GENESIS_ITEMS.PASSIVE.LIGHTSHOT)
                room:SetClear(false)
                for i = 0, 3 do
                    if room:GetDoor(i) ~= nil then
                        room:GetDoor(i):Close(true)
                    end
                end
                --StageAPI.Music:Play(GENESIS_MUSIC.THE_SERPENT, 0.1)
                breakBridge()
                UpdateGrids()
            end

            --SERPENT AI
            if SERPENT.bossEntity ~= nil then
                local targetPos = SERPENT.bossEntity:GetPlayerTarget().Position
                local sprite = SERPENT.bossEntity:GetSprite()
                if sprite:IsEventTriggered("Appear") then
                    local n = math.random(0, 1)
                    SFXManager():Play(GENESIS_SFX.SERPENT_ANGRY + n, 1, 0, false, 1)
                end
                if sprite:IsEventTriggered("Shoot") then
                    SFXManager():Play(GENESIS_SFX.SERPENT_ATTACK, 1, 0, false, 1)
                    --for i=0, 1 do
                    --local t =
                    --    Isaac.Spawn(
                    --    2,
                    --    26,
                    --    0,
                    --    SERPENT.bossEntity.Position,
                    --    (player.Position - SERPENT.bossEntity.Position) / 40,
                    --    SERPENT.bossEntity
                    --)
                    local t = Isaac.Spawn(9,0,0,
                        SERPENT.bossEntity.Position,
                        (targetPos - SERPENT.bossEntity.Position) / 40,
                        SERPENT.bossEntity):ToProjectile()
                    t.Color = Color(0.2, 0.6, 0.4, 1, 0, 80, 0)
                    t:GetSprite():ReplaceSpritesheet(0,"gfx/projectiles/booger_bullets.png")
                    t:GetSprite():LoadGraphics()
                    t.Scale = 1.8
                    t.Height = -100
                    t.FallingSpeed = -6
                    t.FallingAccel = 0.3
                --end
                end

                if sprite:IsEventTriggered("Underwater") then
                    --local u = Isaac.Spawn(1000,GENESIS_ENTITIES.VARIANT.SEPTIC_SPLASH,0, bossEntity.Position, Vector(0,0), bossEntity)
                    --u.DepthOffset = -1050

                    local t =
                        Isaac.Spawn(
                        9,
                        0,
                        0,
                        SERPENT.bossEntity.Position,
                        Vector(math.random(-4, 4), math.random(-4, 4)) / 4,
                        SERPENT.bossEntity
                    ):ToProjectile()
                    --t:SetColor(Color(0.2,1,0.4,1,50,70,50),-1,1,false,false)
                    t:GetSprite():Load("gfx/poisonbubble.anm2", true)
                    --t:GetSprite():Play("RegularTear6", true)
                    --t:GetSprite():ReplaceSpritesheet(0, "gfx/poisonbubbles.png")
                    --t:GetSprite():LoadGraphics()
                    t.Color = Color(0.2, 0.6, 0.4, 1, 0, 80, 0)
                    t.Scale = 0.3
                    t.Height = -5
                    t.FallingSpeed = 0
                    t.FallingAccel = -0.1
                    t:GetData().IsSerpentBubble = true
                end

                if SERPENT.phase == 0 then
                    if SERPENT.bossEntity.Friction == 0 then
                        SERPENT.bossEntity.Position = SERPENT.staticPosition
                        SERPENT.bossEntity.Velocity = Vector(0, 0)
                    end
                    if sprite:IsFinished("Appear") and SERPENT.bossEntity.FrameCount > 4 then
                        sprite:Play("WaterRight", true)
                        SERPENT.bossEntity.GridCollisionClass = GridCollisionClass.COLLISION_NONE
                        SERPENT.bossEntity.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
                        SERPENT.numAround = math.random(1, 2)
                        SERPENT.bossEntity.Friction = 1
                    end

                    if SERPENT.numAround == 0 then
                        -- Set the target platform --
                        if targetPos.X < 235 then
                            SERPENT.targetPlatform = 1
                        elseif targetPos.X < 402 then
                            SERPENT.targetPlatform = 2
                        else
                            SERPENT.targetPlatform = 3
                        end
                        SERPENT.phase = 1
                    end
                    if SERPENT.numAround == 1 then
                        makePlatform(SERPENT.brokenPlatform, false)
                        SERPENT.brokenPlatform = 0
                        UpdateGrids()
                    end
                    -- Underwater --
                    if sprite:IsPlaying("WaterRight") then
                        SERPENT.bossEntity.Velocity = Vector(9, 0)
                        if SERPENT.bossEntity.Position.X >= 555 then
                            sprite:Play("WaterDown", true)
                            SERPENT.numAround = SERPENT.numAround - 1
                        end
                    end

                    if sprite:IsPlaying("WaterDown") then
                        SERPENT.bossEntity.Velocity = Vector(0, 9)
                        if SERPENT.bossEntity.Position.Y >= 400 then
                            sprite:Play("WaterLeft", true)
                            SERPENT.numAround = SERPENT.numAround - 1
                        end
                    end

                    if sprite:IsPlaying("WaterLeft") then
                        SERPENT.bossEntity.Velocity = Vector(-9, 0)
                        if SERPENT.bossEntity.Position.X <= 80 then
                            sprite:Play("WaterUp", true)
                            SERPENT.numAround = SERPENT.numAround - 1
                        end
                    end

                    if sprite:IsPlaying("WaterUp") then
                        SERPENT.bossEntity.Velocity = Vector(0, -9)
                        if SERPENT.bossEntity.Position.Y <= 200 then
                            sprite:Play("WaterRight", true)
                            SERPENT.numAround = SERPENT.numAround - 1
                        end
                    end
                end

                if SERPENT.phase == 1 then
                    -- Underwater --
                    if sprite:IsPlaying("WaterRight") then
                        SERPENT.bossEntity.Velocity = Vector(9, 0)

                        -- Go under the platform --
                        if
                            room:GetGridIndex(SERPENT.bossEntity.Position + Vector(-20, 0)) == 33 and
                                SERPENT.targetPlatform == 1
                         then
                            sprite:Play("WaterDown", true)
                            SERPENT.bossEntity.DepthOffset = -310
                        end
                        if
                            room:GetGridIndex(SERPENT.bossEntity.Position + Vector(-20, 0)) == 37 and
                                SERPENT.targetPlatform == 2
                         then
                            sprite:Play("WaterDown", true)
                            SERPENT.bossEntity.DepthOffset = -310
                        end
                        if
                            room:GetGridIndex(SERPENT.bossEntity.Position + Vector(-20, 0)) == 41 and
                                SERPENT.targetPlatform == 3
                         then
                            sprite:Play("WaterDown", true)
                            SERPENT.bossEntity.DepthOffset = -310
                        end

                        if SERPENT.bossEntity.Position.X >= 555 then
                            sprite:Play("WaterDown", true)
                        end
                    end

                    if sprite:IsPlaying("WaterDown") then
                        SERPENT.bossEntity.Velocity = Vector(0, 9)

                        if SERPENT.bossEntity.Position.Y >= 400 then
                            sprite:Play("WaterLeft", true)
                        end
                    end

                    if sprite:IsPlaying("WaterLeft") then
                        SERPENT.bossEntity.Velocity = Vector(-9, 0)

                        -- Go under the platform --
                        if
                            room:GetGridIndex(SERPENT.bossEntity.Position + Vector(20, 0)) == 108 and
                                SERPENT.targetPlatform == 1
                         then
                            sprite:Play("WaterUp", true)
                            SERPENT.bossEntity.DepthOffset = -310
                        end
                        if
                            room:GetGridIndex(SERPENT.bossEntity.Position + Vector(20, 0)) == 112 and
                                SERPENT.targetPlatform == 2
                         then
                            sprite:Play("WaterUp", true)
                            SERPENT.bossEntity.DepthOffset = -310
                        end
                        if
                            room:GetGridIndex(SERPENT.bossEntity.Position + Vector(20, 0)) == 116 and
                                SERPENT.targetPlatform == 3
                         then
                            sprite:Play("WaterUp", true)
                            SERPENT.bossEntity.DepthOffset = -310
                        end

                        if SERPENT.bossEntity.Position.X <= 80 then
                            sprite:Play("WaterUp", true)
                        end
                    end
                    if sprite:IsPlaying("WaterUp") then
                        SERPENT.bossEntity.Velocity = Vector(0, -9)

                        if SERPENT.bossEntity.Position.X >= 560 then
                            sprite:Play("WaterUp", true)
                        --BOSS_VARIABLES.phase = 2
                        end

                        if SERPENT.bossEntity.Position.Y <= 200 then
                            sprite:Play("WaterRight", true)
                        end
                    end

                    -- Check for same X or Y than the platform

                    if room:GetGridIndex(SERPENT.bossEntity.Position) == 63 and SERPENT.targetPlatform == 1 then -- 67, 71
                        SERPENT.bossEntity.DepthOffset = 0
                        SERPENT.bossEntity.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
                        sprite:Play("Attack1", true)
                        SERPENT.phase = 2
                        SERPENT.bossEntity.Friction = 0
                        SERPENT.bossEntity.Velocity = Vector(0, 0)
                        SERPENT.staticPosition = SERPENT.bossEntity.Position
                        SFXManager():Play(SoundEffect.SOUND_HELLBOSS_GROUNDPOUND, 2, 0, false, 1)
                        makePlatform(1, true)
                        SERPENT.brokenPlatform = 1
                        Game():ShakeScreen(5)
                        UpdateGrids()
                    end

                    if room:GetGridIndex(SERPENT.bossEntity.Position) == 67 and SERPENT.targetPlatform == 2 then -- 67, 71
                        SERPENT.bossEntity.DepthOffset = 0
                        SERPENT.bossEntity.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
                        sprite:Play("Attack1", true)
                        SERPENT.bossEntity.Velocity = Vector(0, 0)
                        SERPENT.bossEntity.Friction = 0
                        SERPENT.phase = 2
                        SERPENT.staticPosition = SERPENT.bossEntity.Position
                        SFXManager():Play(SoundEffect.SOUND_HELLBOSS_GROUNDPOUND, 2, 0, false, 1)
                        makePlatform(2, true)
                        Game():ShakeScreen(5)
                        SERPENT.brokenPlatform = 2
                        UpdateGrids()
                    end

                    if room:GetGridIndex(SERPENT.bossEntity.Position) == 71 and SERPENT.targetPlatform == 3 then -- 67, 71
                        SERPENT.bossEntity.DepthOffset = 0
                        SERPENT.bossEntity.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
                        sprite:Play("Attack1", true)
                        SERPENT.bossEntity.Velocity = Vector(0, 0)
                        SERPENT.bossEntity.Friction = 0
                        SERPENT.phase = 2
                        SERPENT.staticPosition = SERPENT.bossEntity.Position
                        SFXManager():Play(SoundEffect.SOUND_HELLBOSS_GROUNDPOUND, 2, 0, false, 1)
                        makePlatform(3, true)
                        SERPENT.brokenPlatform = 3
                        Game():ShakeScreen(5)
                        UpdateGrids()
                    end
                end
                if SERPENT.phase == 2 then
                    SERPENT.bossEntity.Position = SERPENT.staticPosition
                    SERPENT.bossEntity.Velocity = Vector(0, 0)
                    if sprite:IsFinished("Attack1") then
                        SERPENT.bossEntity.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
                        sprite:Play("WaterUp", true)
                        SERPENT.numAround = math.random(1, 2)
                        SERPENT.bossEntity.Friction = 1
                        SERPENT.phase = 0
                    end
                end
                if room:GetAliveBossesCount() == 0 and Genesis.moddata.SerpentDefeated == false and SERPENT.phase > -1 then
                    if not SERPENT.platform[1]:GetSprite():IsPlaying("Appear") then
                        makePlatform(1, false)
                    end
                    if not SERPENT.platform[2]:GetSprite():IsPlaying("Appear") then
                        makePlatform(2, false)
                    end
                    if not SERPENT.platform[3]:GetSprite():IsPlaying("Appear") then
                        makePlatform(3, false)
                    end
                    fixBridge()
                    --MusicManager():Play(Music.MUSIC_JINGLE_BOSS_OVER, 1)
                    Isaac.Spawn(
                        5,
                        100,
                        Genesis.SerpentPool[math.random(#Genesis.SerpentPool)],
                        room:GetGridPosition(82),
                        Vector(0, 0),
                        nil
                    )
                    SERPENT.bossEntity = nil

                    Genesis.moddata.SerpentDefeated = true
                    SERPENT.justEntered = false
                    room:SetClear(true)

                    room:RemoveGridEntity(67, 0, false)
                    if room:SpawnGridEntity(67, GridEntityType.GRID_TRAPDOOR, 0, math.random(10000), 0) then
                        local grid = room:GetGridEntity(67)
                        SERPENT.trapdoor =
                            Isaac.Spawn(
                            1000,
                            GENESIS_ENTITIES.VARIANT.TRAPDOOR_EFFECT,
                            0,
                            room:GetGridPosition(67),
                            Genesis.VEC_ZERO,
                            nil
                        )
                        local trapdooranimations = {"Opened", "Closed", "Open Animation", "Player Exit"}
                        for i = 1, #trapdooranimations do
                            if
                                grid.Sprite:IsPlaying(trapdooranimations[i]) or
                                    grid.Sprite:IsFinished(trapdooranimations[i])
                             then
                                SERPENT.trapdoor:GetSprite():SetFrame(trapdooranimations[i], grid.Sprite:GetFrame())
                            end
                        end
                        --Isaac.DebugString(tostring(grid:ToTrapdoor()))
                    else
                        Isaac.DebugString("Genesis+: Couldn't spawn trapdoor")
                    end
                    UpdateGrids()
            end
        end
    end
end

function septicBoss:bossDmg(entity, amount, flag, source, countdownFrames)
    -- When the boss is hurt --
    local room = Game():GetRoom()
    --  Isaac.DebugString("Isboss Defeated?" .. tostring(Genesis.moddata.SerpentDefeated))
    if
        entity.Type == GENESIS_ENTITIES.THE_SERPENT and entity.Variant == GENESIS_ENTITIES.VARIANT.THE_SERPENT and
            amount >= entity.HitPoints
     then
        if not SERPENT.platform[1]:GetSprite():IsPlaying("Appear") then
            makePlatform(1, false)
        end
        if not SERPENT.platform[2]:GetSprite():IsPlaying("Appear") then
            makePlatform(2, false)
        end
        if not SERPENT.platform[3]:GetSprite():IsPlaying("Appear") then
            makePlatform(3, false)
        end
        fixBridge()
        UpdateGrids()
        --MusicManager():Play(Music.MUSIC_JINGLE_BOSS_OVER, 1)
        Isaac.Spawn(5, 100, 0, room:GetGridPosition(82), Vector(0, 0), nil)
        SERPENT.bossEntity = nil
        room:SpawnGridEntity(67, GridEntityType.GRID_TRAPDOOR, 0, 0, 0)
        Genesis.moddata.SerpentDefeated = true
        return false
    end
end

function septicBoss:ResetData()
    --  Isaac.DebugString("Is reset")
    Genesis.moddata.SerpentDefeated = false
    SERPENT.phase = -1
    SERPENT.bossEntity = nil
    SERPENT.removedTrapdoor = false
    SERPENT.spawnedKey = false
    local player = Isaac.GetPlayer(0)
    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.RARE_KEY) then
        player:RemoveCollectible(GENESIS_ITEMS.PASSIVE.RARE_KEY)
    end
end

septicBoss:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, septicBoss.ResetData)
--septicBoss:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, septicBoss.bossDmg, GENESIS_ENTITIES.THE_SERPENT)
septicBoss:AddCallback(ModCallbacks.MC_POST_RENDER, septicBoss.render)
septicBoss:AddCallback(ModCallbacks.MC_POST_UPDATE, septicBoss.bossFight)
septicBoss:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, septicBoss.OnRoomEnter)

------------------------------------------
-- THE BOUND
------------------------------------------

function septicBoss:BossBound(npc)
    if npc.Variant ~= GENESIS_ENTITIES.VARIANT.THE_BOUND then return end

    local sprite = npc:GetSprite()
    local data = npc:GetData()
    local player = npc:GetPlayerTarget()
    local room = Game():GetRoom()

    npc.Friction = npc.Friction * 0.5
    local pos = npc.Position
    npc.StateFrame = npc.StateFrame + 1

    if npc.State ~= 3 and (sprite:IsFinished("Appear") or sprite:IsFinished("Attack") or sprite:IsFinished("Spawn")) then
        npc.StateFrame = 0
        npc.State = 3
    elseif sprite:IsFinished("Death") then
        npc:BloodExplode()
        npc:Morph(GENESIS_ENTITIES.THE_BOUND_HEAD, GENESIS_ENTITIES.VARIANT.THE_BOUND_HEAD, 0, -1)
        sprite:Play("Plop", true)
    end

    if npc.State == 3 then
        if sprite:IsPlaying("Idle") == false then
            sprite:Play("Idle", true)
        end
        if npc.StateFrame == 45 then
            npc.State = 30
        end
    elseif npc.State == 30 then
        local ai = math.random(12) % 3 + 1
        if ai == 1 or ai == 2 then
            sprite:Play("Attack", true)
        elseif ai == 3 then
            local roomGurgles = Isaac.CountEntities(nil, EntityType.ENTITY_GURGLE, -1, -1)
            if roomGurgles < 1 then
                sprite:Play("Spawn", true)
            else
                sprite:Play("Attack", true)
            end
        end
        npc.State = 31
    elseif npc.State == 32 then
        sprite:Play("Death", 0)
        npc.State = 33
    end

    if sprite:IsEventTriggered("Charge") then
        local snd = math.random(3)
        if snd == 1 then
            npc:PlaySound(SoundEffect.SOUND_MONSTER_GRUNT_1, 1, 0, false, 1)
        elseif snd == 2 then
            npc:PlaySound(SoundEffect.SOUND_MONSTER_GRUNT_2, 1, 0, false, 1)
        elseif snd == 3 then
            npc:PlaySound(SoundEffect.SOUND_MONSTER_GRUNT_4, 1, 0, false, 1)
        end
    elseif sprite:IsEventTriggered("Spawn") then
        npc:PlaySound(SoundEffect.SOUND_SUMMONSOUND, 1, 0, false, 1)
        local gurg = Isaac.Spawn(EntityType.ENTITY_GURGLE, 0, 0, npc.Position, Vector(1, 0), npc):ToNPC()
        gurg.MaxHitPoints = gurg.MaxHitPoints * 0.6
        gurg.HitPoints = gurg.MaxHitPoints
    elseif sprite:IsEventTriggered("Shoot") then
        local atk = math.random(8) % 2 + 1
        if atk == 1 then
            npc:PlaySound(SoundEffect.SOUND_BOSS_SPIT_BLOB_BARF, 1, 0, false, 1)
            local angle = (player.Position - npc.Position):GetAngleDegrees()
            Genesis.SpawnIpecacProjectile(npc.Position, Vector.FromAngle(1 * angle):Resized(4), npc, 1)
        elseif atk == 2 then
            npc:PlaySound(SoundEffect.SOUND_BOSS_LITE_SLOPPY_ROAR, 1, 0, false, 1)
            local offset = data.LastOffset
            if not offset then
                local bullet = math.random(2)
                offset = 0
                if bullet == 1 then
                    offset = 45
                elseif bullet == 2 then
                    offset = 90
                end
            else
                offset = offset == 45 and 90 or 45
            end
            data.LastOffset = offset

            for i = 0, 3 do
                local tear = Isaac.Spawn(9, 0, 9, pos, Vector.FromAngle(i * 90 + offset):Resized(15), npc)
                tear:SetColor(Color(0, 0, 0, 0, 81, 54, 46), 99999, 1, false, false)
            end
        end
    end

    for _, v in ipairs(Isaac:GetRoomEntities()) do
        if v.Type == 9 and v.SpawnerVariant == GENESIS_ENTITIES.VARIANT.THE_BOUND then
            if v.Variant == 0 and v.SubType == 9 then
                v.Visible = false
                v.CollisionDamage = 0
                v.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
                local creep = Isaac.Spawn(1000, EffectVariant.CREEP_GREEN, 0, v.Position, Vector(0, 0), npc):ToEffect()
                creep.Timeout = 15
            end
        end
    end

    if npc.HitPoints <= 150 and npc.State ~= 33 then
        npc.State = 32
    end
end

function septicBoss:BossBoundHead(npc)
    if npc.Variant ~= GENESIS_ENTITIES.VARIANT.THE_BOUND_HEAD then
        return
    end

    local sprite = npc:GetSprite()
    local player = npc:GetPlayerTarget()
    local room = Game():GetRoom()
    local pos = npc.Position
    npc.StateFrame = npc.StateFrame + 1

    if sprite:IsPlaying("Plop") or sprite:IsPlaying("Appear") then
        npc.Velocity = npc.Velocity * 0
        npc.Friction = 0
    else
        npc.Friction = npc.Friction * 0.5
    end

    if npc.State ~= 3 and (sprite:IsFinished("Plop") or sprite:IsFinished("Appear")
    or sprite:IsFinished("Spit") or sprite:IsFinished("Beam") or sprite:IsFinished("Jump")) then
        npc.StateFrame = 0
        npc.State = 3
    end

    if npc.State == 3 then
        if not sprite:IsPlaying("Idle") then
            sprite:Play("Idle", true)
        end
        if npc.StateFrame == 40 then
            npc.State = 30
        end
    elseif npc.State == 30 then
        local ai = math.random(12) % 3 + 1
        if ai == 1 then
            sprite:Play("Spit", true)
        elseif ai == 2 then
            sprite:Play("Beam", true)
        elseif ai == 3 then
            sprite:Play("Jump", true)
        end
        npc.State = 31
    elseif npc.State == 33 then
        npc.State = 34
    elseif npc.State == 34 then
        local targetVelocity = (player.Position - npc.Position):Resized(22.5)
        npc.Velocity = targetVelocity
    end

    if npc.FrameCount % 100 == 0 then
        npc:PlaySound(SoundEffect.SOUND_SCARED_WHIMPER, 1, 0, false, 1)
    end

    if sprite:IsEventTriggered("Land") then
        npc:PlaySound(SoundEffect.SOUND_MEAT_IMPACTS, 1, 0, false, 1)
    elseif sprite:IsEventTriggered("Land DX") then
        npc.State = 32
        npc:PlaySound(SoundEffect.SOUND_MEAT_IMPACTS, 1, 0, false, 1)
        npc.Velocity = npc.Velocity * 0

        local bullet = math.random(8) % 2 + 1
        local offset = 0
        if bullet == 1 then
            offset = 45
        elseif bullet == 2 then
            offset = 90
        end
        for i = 0, 3 do
            Isaac.Spawn(9, 4, 0, pos, Vector.FromAngle(i * 90 + offset):Resized(7), npc)
        end
    elseif sprite:IsEventTriggered("Hop") then
        npc.State = 33
    elseif sprite:IsEventTriggered("Shoot") then
        npc:PlaySound(SoundEffect.SOUND_BLOODSHOOT, 0.8, 0, false, 1)
        npc:PlaySound(SoundEffect.SOUND_THUMBS_DOWN, 0.8, 0, false, 1)
        for i = 0, 7 do
            Isaac.Spawn(9, 4, 0, pos, Vector.FromAngle(i * 45):Resized(7), npc)
        end
    elseif sprite:IsEventTriggered("Light") then
        npc:PlaySound(SoundEffect.SOUND_WHEEZY_COUGH, 1, 0, false, 1)

        local roomBabies = Isaac.CountEntities(nil, EntityType.ENTITY_BABY, -1, -1)
        if roomBabies < 2 then
            local baby = Isaac.Spawn(EntityType.ENTITY_BABY, 0, 0, room:GetRandomPosition(0), Vector(0, 0), npc):ToNPC()
            baby.MaxHitPoints = baby.MaxHitPoints * 0.4
            baby.HitPoints = baby.MaxHitPoints
        end

        Isaac.Spawn(1000, 19, 0, room:GetRandomPosition(0), Vector(0, 0), npc)
        Isaac.Spawn(1000, 19, 0, room:GetRandomPosition(0), Vector(0, 0), npc)
    end
end

septicBoss:AddCallback(ModCallbacks.MC_NPC_UPDATE, septicBoss.BossBound, GENESIS_ENTITIES.THE_BOUND)
septicBoss:AddCallback(ModCallbacks.MC_NPC_UPDATE, septicBoss.BossBoundHead, GENESIS_ENTITIES.THE_BOUND_HEAD)

Isaac.DebugString("Genesis+: Loaded Septic Boss!")
error({ hack = true })
