--[[
Big G genesis is for static functions and constants, small g genesis is for callbacks and stuff
More generic global functions and tables are at the bottom for ease of navigation (we ain't likely to edit em anyways)

COOL STUFF YOU SHOULD PROBABLY KNOW:
-the getRoomSomething functions right here are much less laggy than doing Genesis.getRoomEntities, for performance reasons that I'm too tired to explain
-You can use Genesis.RenderPickupNumber to render a number in the pickup genesis.font (look below for arguments)
-You can use Genesis.RegisterEntity to add an entity to the entity register, so that when you come back to the room it's still there
-You can use the UNLOCKS table in the characters file to add unlocks for specific characters, no need to make another check "if mom is dead etc", its all already there
-You can use Genesis.moddata.statsup.<stat> to edit stats for the whole run, and lasting even if you exit and reload the game (like if you have an item that when used increases stats, like Xaphan's candle)
-In case you need to get a random stat, there is a Genesis.STATSNAMES table with the 6 names in number indexes, so you can do statname = Genesis.STATSNAMES[math.random(6)]
-to get if the current stage is septic, just do Genesis.IsSeptic()
]]

--UNCOMMENT NEXT 2 LINES IF USING --luadebug
--local modDirectory = string.gsub(debug.getinfo(1).source, "^@(.+)main.lua$", "%1");
--package.path = package.path .. ";" .. modDirectory .. "?.lua"

_G.Genesis = {
	RoomEntities = {}, getRoomEntities = function() return Genesis.RoomEntities end,
	RoomTears = {}, getRoomTears = function() return Genesis.RoomTears end,
	RoomLasers = {}, getRoomLasers = function() return Genesis.RoomLasers end,
	RoomEnemies = {}, getRoomEnemies = function() return Genesis.RoomEnemies end,
	RoomKnives = {}, getRoomKnives = function() return Genesis.RoomKnives end,
	RoomEffects = {}, getRoomEffects = function() return Genesis.RoomEffects end,
	RoomFamiliars = {}, getRoomFamiliars = function() return Genesis.RoomFamiliars end,
  RoomProjectiles = {}, getRoomProjectiles = function() return Genesis.RoomProjectiles end,
  RoomPickups = {}, getRoomPickups = function() return Genesis.RoomPickups end,
  loaderCount = 0,
  moddata = {}
}

Genesis.DEBUG = true

local DevilPool = {8, 34, 35, 51, 67, 79, 80, 81, 82, 83, 84, 97, 113, 114, 118, 122, 126, 133, 134, 145, 159, 163, 172, 187, 212, 215, 216, 225, 230, 237, 241, 259, 262, 269, 268, 275, 278, 292, 311, 412, 408, 399, 391, 360, 409, 433, 431, 420, 417, 441, 498, 477, 475, 462, 442, 468}

function table_val_to_str(v)
	if "string" == type(v) then
		v = string.gsub(v, "\n", "\\n")
		if string.match(string.gsub(v,"[^'\"]",""), '^"+$') then
			return "'" .. v .. "'"
		end
		return '"' .. string.gsub(v,'"', '\\"' ) .. '"'
	else
		return "table" == type(v) and table_tostring(v) or
		tostring(v)
	end
end

function table_key_to_str(k)
	if "string" == type(k) and string.match(k, "^[_%a][_%a%d]*$") then
		return k
	else
		return "[" .. table_val_to_str(k) .. "]"
	end
end

function table_tostring(tbl)
	local result, done = {}, {}
	for k, v in ipairs(tbl) do
		table.insert(result, table_val_to_str(v))
		done[k] = true
	end
	for k, v in pairs(tbl) do
		if not done[k] then
			table.insert(result,
			table_key_to_str(k) .. "=" .. table_val_to_str(v))
		end
	end
	return "{" .. table.concat(result, ",") .. "}"
end

Genesis.VEC_ZERO = Vector(0,0)

Genesis.FONT_H = 9
Genesis.FONT_W = 6
Genesis.DCOLOR = Color(1,1,1,1,0,0,0)
Genesis.CharWidth = {
  ["1"] = 4
}
Genesis.textPos = Vector(0,0)
Genesis.font = Sprite()
Genesis.font:Load("gfx/pickupnums.anm2", true)
Genesis.font:Play("Idle", true)
Genesis.font.PlaybackSpeed = 0

--Renders a number with the pickup Genesis.font
--Why only numbers? Couldn't be bothered to make the anm2 for all chars lol, also it's only used for numbers rn
--AlignX is: 0/nil from left, 1 from center, 2 from right
--AlignY is: 0/nil from top, 1 from center, 2 from bottom
function Genesis.RenderPickupNumber(a, pos, alignX, alignY, size, minChars, color)
  a = tostring(a)
  size = size or 1
  Genesis.font.Color = color or Genesis.DCOLOR
  minChars = minChars or 1

  if a:len() < minChars then
    repeat a = "0"..a
    until a:len() == minChars
  end

  local frame

  Genesis.font.Scale = Vector(size,size)
  Genesis.textPos.X = pos.X Genesis.textPos.Y = pos.Y --doing this as vectors are pointers, so modifiying the position would do so permanently, not only for this function

  if alignX == 1 or alignX == 2 then
    local width = 0

    for char in a:gmatch(".") do
      width = width + (Genesis.CharWidth[char] or Genesis.FONT_W)
    end
    Genesis.textPos.X = Genesis.textPos.X-width*size*alignX/2
  end

  if alignY == 1 or alignY == 2 then
    Genesis.textPos.Y = Genesis.textPos.Y - Genesis.FONT_H/2*alignY
  end

  for char in a:gmatch(".") do
      frame = tonumber(char) or -1
      if frame ~= -1 then
        Genesis.font:SetLayerFrame(0,frame)
        Genesis.font:Render(Genesis.textPos,Vector(0,0),Vector(0,0))
      end
      Genesis.textPos.X = Genesis.textPos.X + (Genesis.CharWidth[char] or Genesis.FONT_W)*size
    char = nil
  end
end

--Set an entity to be respawned on room reenter, keeping data, type, subtype, variant
--Either register an entity not in the room to be respawned, or an entity in the room to be despawned on room enter
function Genesis.RegisterEntity(e, forRemoval)
  local rseed = Game():GetRoom():GetDecorationSeed()
  local data = e:GetData()
  if not Genesis.moddata.entityRegister[rseed] then Genesis.moddata.entityRegister[rseed] = {remove = {}} end

  if not Genesis.IsEntityRegistered(e, forRemoval) then
    if not forRemoval then
      if not data.registerIndex then
        data.registerIndex = e.InitSeed
      end

      Genesis.moddata.entityRegister[rseed][data.registerIndex] = {Type = e.Type, Variant = e.Variant, SubType = e.SubType, X = e.Position.X, Y = e.Position.Y}
      if Genesis.DEBUG then Isaac.DebugString(table_tostring(Genesis.moddata.entityRegister[rseed][data.registerIndex])) end
      if Genesis.DEBUG then Isaac.DebugString(table_tostring(Genesis.moddata.entityRegister)) end
    else
      Genesis.moddata.entityRegister[rseed].remove[e.InitSeed] = true
      Genesis.moddata.entityRegister[rseed][data.registerIndex] = nil
    end
  end
end

--Remove it from being respawned/removed
function Genesis.UnregisterEntity(e, fromRemoval)
  local rseed = Game():GetRoom():GetDecorationSeed()
  local rindex = e:GetData().registerIndex
  local seed = chars.game:GetSeeds():GetStartSeed()
  fromRemoval = fromRemoval or false
  if Genesis.moddata.entityRegister[rseed] then
    if not fromRemoval then
      Genesis.moddata.entityRegister[rseed][rindex] = nil
    else
      Genesis.moddata.entityRegister[rseed].remove[e.InitSeed] = nil
    end
    if #Genesis.moddata.entityRegister[rseed] == 1 and #Genesis.moddata.entityRegister[rseed].remove < 1 then
      Genesis.moddata.entityRegister[rseed] = nil
    end
  else
    Isaac.DebugString("Genesis+: Cannot unregister from not registered room!")
  end
end

function Genesis.IsEntityRegistered(e, forRemoval)
  local rseed = Game():GetRoom():GetDecorationSeed()
  local data = e:GetData()

  return data.registerIndex and Genesis.moddata.entityRegister[rseed] and
    ((Genesis.moddata.entityRegister[rseed][data.registerIndex] and not forRemoval)
  or (Genesis.moddata.entityRegister[rseed].remove[e.InitSeed] and forRemoval))
end

local genesis = RegisterMod("Genesis+", 1)

function genesis.tlen(t) --why do I even have to do this ugh lua jesus christ (btw gives the table length, # only works for number indexes)
  local count = 0
  for _ in pairs(t) do count = count + 1 end
  return count
end

function genesis:ModdataInit()
  if genesis.tlen(Genesis.moddata) < genesis.tlen(genesis.DEFAULT_MODDATA) then
    Genesis.LoadModdata()
  end

  if Game():GetFrameCount() <= 1 then
    local player = Isaac.GetPlayer(0)
		Genesis.moddata.statsup.TEARS = 0
		Genesis.moddata.statsup.SHOTSPEED = 0
		Genesis.moddata.statsup.DAMAGE = 0
		Genesis.moddata.statsup.RANGE = 0
		Genesis.moddata.statsup.SPEED = 0
		Genesis.moddata.statsup.LUCK = 0
    Genesis.moddata.entityRegister = {}
		player:AddCacheFlags(CacheFlag.CACHE_ALL)
		player:EvaluateItems()
    if Genesis.DEBUG then Isaac.DebugString("Genesis+: Reset data for new run") end
  end
end

genesis:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, genesis.ModdataInit) --GAME_START was wonky for some reason


--LOAD STAGESYSTEM NOW
--As the room callbacks get overridden by it (to fix the NEW_ROOM called 2 times on cusotm floors bug), we need it as early as possible, but after moddata so the custom stage is loaded correctly
--[[if StageSystem == nil then
    require "stagesapi"
end]]

function genesis:UpdateEntFunc() --update the entities table
	local e = Isaac.GetRoomEntities()
	Genesis.RoomEntities = e
	Genesis.RoomTears = {}
	Genesis.RoomLasers = {}
	Genesis.RoomEnemies = {}
	Genesis.RoomKnives = {}
	Genesis.RoomEffects = {}
	Genesis.RoomFamiliars = {}
  Genesis.RoomProjectiles = {}
  Genesis.RoomPickups = {}
	for i=1, #e do
		if e[i].Type == EntityType.ENTITY_TEAR then table.insert(Genesis.RoomTears, e[i]:ToTear())
		elseif e[i].Type == EntityType.ENTITY_LASER then table.insert(Genesis.RoomLasers, e[i]:ToLaser())
		elseif e[i]:IsEnemy() then table.insert(Genesis.RoomEnemies, e[i]:ToNPC())
		elseif e[i].Type == EntityType.ENTITY_KNIFE then table.insert(Genesis.RoomKnives, e[i]:ToKnife())
		elseif e[i].Type == EntityType.ENTITY_EFFECT then table.insert(Genesis.RoomEffects, e[i]:ToEffect())
		elseif e[i].Type == EntityType.ENTITY_PROJECTILE then table.insert(Genesis.RoomProjectiles, e[i])
    elseif e[i].Type == EntityType.ENTITY_FAMILIAR then table.insert(Genesis.RoomFamiliars, e[i]:ToFamiliar())
    elseif e[i].Type == EntityType.ENTITY_PICKUP then table.insert(Genesis.RoomPickups, e[i]:ToPickup()) end
	end
--  Isaac.DebugString("a"..#Genesis.RoomEntities)
end

function Genesis.UpdateEntities() --update entities tables, is global so can be done from other lua files
  genesis:UpdateEntFunc()
end



genesis:AddCallback(ModCallbacks.MC_POST_RENDER, genesis.UpdateEntFunc)
genesis:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, genesis.UpdateEntFunc)

function genesis.ChangeCostume(player) --I dunno, sorry
    local    cpool = {}
    local    fcpool = {}
    local    rand = 0

    local    effect
    local    configItem

    for k, v in pairs(itemtable) do
        player:GetEffects():AddCollectibleEffect(v, true)
        effect = player:GetEffects():GetCollectibleEffect(v)
        configItem = effect.Item

        if (configItem.Costume.ID > 0) then
            if (configItem.Costume.IsFlying) then
                table.insert(fcpool, configItem)
            else
                table.insert(cpool, configItem)
            end
        end

        player:GetEffects():RemoveCollectibleEffect(v)
        player:RemoveCostume(configItem)
    end

    if (#fcpool > 0) then
        if (flynullcostume == false)
        or (flynullcostume == true and math.random(1,2) == 1) then
            rand = math.random(1,#fcpool)
            player:AddCostume(fcpool[rand], false)
        end
    end

    for i = 1, math.min(maxCostumes, #cpool) do
        rand = math.random(1,#cpool)
        player:AddCostume(cpool[rand], false)
        table.remove(cpool,rand)
    end
end

function genesis:Cache(pl, flag)
--  Isaac.DebugString(genesis.tlen(Genesis.moddata)..";"..genesis.tlen(genesis.DEFAULT_MODDATA))
  if genesis.tlen(Genesis.moddata) < genesis.tlen(genesis.DEFAULT_MODDATA) then --I put this here as CACHE is called before GAME_START for some reason, and sometimes before room
    Genesis.LoadModdata()
  end

	if flag == CacheFlag.CACHE_DAMAGE then
		pl.Damage = pl.Damage + Genesis.moddata.statsup.DAMAGE
	elseif flag == CacheFlag.CACHE_LUCK then
		pl.Luck = pl.Luck + Genesis.moddata.statsup.LUCK
	elseif flag == CacheFlag.CACHE_RANGE then
		pl.TearHeight = pl.TearHeight + Genesis.moddata.statsup.RANGE
	elseif flag == CacheFlag.CACHE_SHOTSPEED then
		pl.ShotSpeed = pl.ShotSpeed + Genesis.moddata.statsup.SHOTSPEED
	elseif flag == CacheFlag.CACHE_SPEED then
		pl.MoveSpeed = math.min(pl.MoveSpeed + Genesis.moddata.statsup.SPEED, 2)
  elseif flag == CacheFlag.CACHE_FIREDELAY then
		pl.MaxFireDelay = math.max(pl.MaxFireDelay + math.floor(Genesis.moddata.statsup.TEARS), 1)
  end
end

genesis:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, genesis.Cache)

function genesis:ApplyEntityRegister()
--  Isaac.DebugString(genesis.tlen(Genesis.moddata)..";"..genesis.tlen(genesis.DEFAULT_MODDATA))
  if genesis.tlen(Genesis.moddata) < genesis.tlen(genesis.DEFAULT_MODDATA) then --I put this here as ROOM is called before GAME_START for some reason, and sometimes before room
    Genesis.LoadModdata()
  end

  local rseed = Game():GetRoom():GetDecorationSeed()
  if Genesis.moddata.entityRegister[rseed] then
    local roomReg = Genesis.moddata.entityRegister[rseed]
    for i,e in ipairs(Genesis.RoomEntities) do
      if roomReg.remove[e.InitSeed] then
        roomReg.remove[e.InitSeed] = nil
        e:Remove()
      end
    end

    local rem = roomReg.remove
    roomReg.remove = nil
    for i,v in pairs(Genesis.moddata.entityRegister[rseed]) do
        local e = Isaac.Spawn(v.Type, v.Variant, v.SubType, Vector(v.X, v.Y), Genesis.VEC_ZERO, nil)
        if Genesis.DEBUG then Isaac.DebugString("Spawned registered ent "..e.Type.."!") end
        e:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
        e:GetData().registerIndex = i
    end
    roomReg.remove = rem
  end
end

genesis:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, genesis.ApplyEntityRegister)

do --for easier hiding in text editor, items and stuff
-------------
--All Items--
-------------
_G.GENESIS_ITEMS = {
	ACTIVE = {
		ROPE_LADDER = Isaac.GetItemIdByName("Rope Ladder"),
		WHITE_MASK = Isaac.GetItemIdByName("White Mask"),
		BOWLING_BALL = Isaac.GetItemIdByName("Bowling Ball"),
		LIL_LAMBY = Isaac.GetItemIdByName("Lil Lamby"),
		BOOK_OF_LUCIFER = Isaac.GetItemIdByName("Book of Lucifer"),
		TECH_G = Isaac.GetItemIdByName("Tech G"),
		CEREMONIAL_DAGGER = Isaac.GetItemIdByName("Ceremonial Dagger"),
		DAMAGED_CEREMONIAL_DAGGER = Isaac.GetItemIdByName("Damaged Ceremonial Dagger"),
		BROKEN_CEREMONIAL_DAGGER = Isaac.GetItemIdByName("Broken Ceremonial Dagger"),
		BYOUXS_HEAD = Isaac.GetItemIdByName("Byoux's Head"),
		PILLOW = Isaac.GetItemIdByName("Pillow"),
		USED_PILLOW = Isaac.GetItemIdByName("Used Pillow"),
		RAGGED_PILLOW = Isaac.GetItemIdByName("Ragged Pillow"),
		CRAYON = Isaac.GetItemIdByName("The Crayon"),
		TOWEL = Isaac.GetItemIdByName("Towel"),
		DASH = Isaac.GetItemIdByName("Dash"),
		BOOK_OF_GENESIS = Isaac.GetItemIdByName("Book of Genesis"),
		D666 = Isaac.GetItemIdByName("D666"),
		EXTINGUISHED_CANDLE = Isaac.GetItemIdByName("Extinguished Candle"),
		ETERNAL_CANDLE = Isaac.GetItemIdByName("Eternal Candle"),
		COUNTERFEIT_SOUL = Isaac.GetItemIdByName("Counterfeit Soul"),
		EBUY = Isaac.GetItemIdByName("eBuy"),
		TINTED_BLASTER = Isaac.GetItemIdByName("Tinted Blaster"),
		SCRATCH = Isaac.GetItemIdByName("Scratch"),
		DUMMY = Isaac.GetItemIdByName("Practice Dummy"),
		POKERDECK = Isaac.GetItemIdByName("Poker Deck"),
		MAGICDECK = Isaac.GetItemIdByName("Magic Deck"),
		BOOK_OF_RANDOMNESS = Isaac.GetItemIdByName("Book of Randomness"),
		CREEP_BUCKET = Isaac.GetItemIdByName("Bucket O' Creep"),
		WISH = Isaac.GetItemIdByName("Wish for a Day")
	},

	PASSIVE = {
		GLOBIN_GOO = Isaac.GetItemIdByName("Globin Goo"),
		SPLIT_CROSS = Isaac.GetItemIdByName("Split Cross"),
		PONGER = Isaac.GetItemIdByName("Ponger"),
		SALT = Isaac.GetItemIdByName("Salt"),
		RING_OF_FIRE = Isaac.GetItemIdByName("Ring of Fire"),
		SUPERPOWERS = Isaac.GetItemIdByName("SUPER POWERS!"),
		CURSED_BONE = Isaac.GetItemIdByName("Cursed Bone"),
		POISON_RAY = Isaac.GetItemIdByName("Poison Ray"),
		HELLBEAM = Isaac.GetItemIdByName("Hellbeam"),
		BEGINNERS_LUCK = Isaac.GetItemIdByName("Beginner's Luck"),
		CUTE_BOMBS = Isaac.GetItemIdByName("Cute Bombs"),
		LUCIFERS_WALLET = Isaac.GetItemIdByName("Lucifer's Wallet"),
		TECHNOLOGY_3 = Isaac.GetItemIdByName('Technology 3'),
		TECH_BOOM = Isaac.GetItemIdByName("Tech Boom"),
		AZAZELS_LOST_HORN = Isaac.GetItemIdByName('Azazels lost horn'),
		LOST_COURAGE = Isaac.GetItemIdByName("Lost Courage"),
		DYSLEXIA = Isaac.GetItemIdByName("Dyslexia"),
		DEJA_VU = Isaac.GetItemIdByName("Deja Vu"),
		CHAMELEON = Isaac.GetItemIdByName("Chameleon"),
		TETRACHROMACY = Isaac.GetItemIdByName("Tetrachromacy"),
		OVEN_MITT = Isaac.GetItemIdByName("Oven Mitt"),
		CURSED_CONTRACT = Isaac.GetItemIdByName("Cursed Contract"),
		LIGHTSHOT = Isaac.GetItemIdByName("Lightshot"),
		SATANS_CHALICE = Isaac.GetItemIdByName("Satan's Chalice"),
		LOST_POWER = Isaac.GetItemIdByName("Lost Power"),
		EVIL_OPTIONS = Isaac.GetItemIdByName("Evil Options"),
		ZEUS_TOUCH = Isaac.GetItemIdByName("Zeus Touch"),
		EXPLOSIVE_TEMPER = Isaac.GetItemIdByName("Explosive Temper"),
		LOST_WISDOM = Isaac.GetItemIdByName("Lost Wisdom"),
		RARE_KEY = Isaac.GetItemIdByName("Rare Key"),
		BLACK_BELT = Isaac.GetItemIdByName("Black Belt"),
		RAINSTORM = Isaac.GetItemIdByName("The Rainstorm"),
		RAINBOOTS = Isaac.GetItemIdByName("Rain Boots"),
		CUTEDRESS = Isaac.GetItemIdByName("Cute Dress"),
		SAD_HEART = Isaac.GetItemIdByName("Sad Heart"),
		ARMOR_OF_ARES = Isaac.GetItemIdByName("Armor of Ares"),
		BANDANA = Isaac.GetItemIdByName("Bandana"),
		PURPLE_BELT = Isaac.GetItemIdByName("Purple Belt"),
		BALLOON = Isaac.GetItemIdByName("Water Balloon"),
		VIRTUE = Isaac.GetItemIdByName("Virtue of Patience"),
		ETERNAL = Isaac.GetItemIdByName("Eternal Resurrection"),
		EVENMOREOPTIONS = Isaac.GetItemIdByName("Even More Options!"),
		QUAKESHOT = Isaac.GetItemIdByName("Quake Shot"),
		MOUSTACHE = Isaac.GetItemIdByName("Dad's Moustache"),
		PUKE = Isaac.GetItemIdByName("Puke"),
		SPARKLEBOMB = Isaac.GetItemIdByName("Sparkle Bombs"),
		SKIPPYTEARS = Isaac.GetItemIdByName("Skippy Tears"),
		ANGELBLADE = Isaac.GetItemIdByName("Angel Blade"),
		TOO_MUCH_WATER = Isaac.GetItemIdByName("Too Much Water!")
	},

	FAMILIAR = {
		SCHIZOFRENIA = Isaac.GetItemIdByName("Schizofrenia"),
		MERP = Isaac.GetItemIdByName("Merp"),
		RAG_MINI = Isaac.GetItemIdByName("Rag Mini"),
		LIL_BROSKI = Isaac.GetItemIdByName("Lil Broski"),
		LIL_FORSAKEN = Isaac.GetItemIdByName("Little Forsaken"),
		INFERNO = Isaac.GetItemIdByName("Inferno Soul"),
		LIL_GABRIEL = Isaac.GetItemIdByName("Lil' Gabriel")
	},

	TRINKET = {
		SOUL_KEY = Isaac.GetTrinketIdByName("Soul Key"),
		RED_PENDANT = Isaac.GetTrinketIdByName("Red Pendant"),
		HOLY_PENDANT = Isaac.GetTrinketIdByName("Holy Pendant"),
		PENDANT_OF_SHADOWS = Isaac.GetTrinketIdByName("Pendant of Shadows"),
		RITUAL_CANDLE = Isaac.GetTrinketIdByName("Ritual Candle"),
		BLOODY_CARD = Isaac.GetTrinketIdByName("Bloody Card"),
		STAMP = Isaac.GetTrinketIdByName("Stamp"),
		LOCKPICKS = Isaac.GetTrinketIdByName("Lockpicks"),
		GLOWING_ROCK = Isaac.GetTrinketIdByName("Glowing Rock"),
		LOST_CD = Isaac.GetTrinketIdByName("Lost CD"),
		BROKEN_CLOCK = Isaac.GetTrinketIdByName("Broken Clock"),
		BROKEN_MAP = Isaac.GetTrinketIdByName("Broken Map"),
		GOLDEN_CALF = Isaac.GetTrinketIdByName("Broken Golden Calf"),
		LASER_OPTIC = Isaac.GetTrinketIdByName("Laser Optic"),
		MYSTERY_BOX = Isaac.GetTrinketIdByName("Mystery Box"),
		SAFETY_PICKAXE = Isaac.GetTrinketIdByName("Safety Pickaxe"),
		ROTTEN_EGG = Isaac.GetTrinketIdByName("Rotten Egg"),
		UNCERTAIN = Isaac.GetTrinketIdByName("Uncertain"),
		GLOBE = Isaac.GetTrinketIdByName("The Globe"),
		HOPE = Isaac.GetTrinketIdByName("The Hope"),
		REMORSE = Isaac.GetTrinketIdByName("The Remorse"),
		CLOVER = Isaac.GetTrinketIdByName("Rotten Clover"),
		ASH = Isaac.GetTrinketIdByName("Ash of Gold"),
		KNIFE = Isaac.GetTrinketIdByName("Pocket Knife"),
		CHILI = Isaac.GetTrinketIdByName("Chili Con Carne"),
		CHARGEDKEY = Isaac.GetTrinketIdByName("Charged Key"),
		BLOODYKEY = Isaac.GetTrinketIdByName("Bloody Key"),
		BOSSPASS = Isaac.GetTrinketIdByName("Boss Pass"),
		FIREEYE = Isaac.GetTrinketIdByName("Fire Eye"),
		FASTFORWARD = Isaac.GetTrinketIdByName("Fast Forward"),
		SKULL = Isaac.GetTrinketIdByName("Enlightened Skull")
	}
}

GENESIS_PILLS = {
  HELLPILL = Isaac.GetPillEffectByName("Satan Loves You"),
  GODPILL = Isaac.GetPillEffectByName("Jesus Loves You"),
  KEYPILL = Isaac.GetPillEffectByName("Coins are Keys"),
  BOMBPILL = Isaac.GetPillEffectByName("Bombs are Coins"),
  DAMAGEPILL = Isaac.GetPillEffectByName("Damage Up"),
  MINDPILL = Isaac.GetPillEffectByName("I Know Everything"),
  DAMAGEDOWN = Isaac.GetPillEffectByName("Damage Down")
}

GENESIS_CARDS = {
  ACE_OF_RICHES = Isaac.GetCardIdByName("AceofRiches"),
  LUST = Isaac.GetCardIdByName("Lust"),
  RIFFLE = Isaac.GetCardIdByName("Riffle"),
  TEIWAZ = Isaac.GetCardIdByName("Teiwaz"),
  PERPS = Isaac.GetCardIdByName("Perps"),
  DIVERSITY = Isaac.GetCardIdByName("Diversity"),
  KING_OF_HEARTS = Isaac.GetCardIdByName("KingofHearts"),
  SAINT = Isaac.GetCardIdByName("00_TheSaint"),
  HERO = Isaac.GetCardIdByName("01_TheHero"),
  CRUCIFIX = Isaac.GetCardIdByName("02_Crucifix"),
  SALVATION = Isaac.GetCardIdByName("03_RingofSalvation"),
  HUMBLE = Isaac.GetCardIdByName("04_TheHumble"),
  GALAXY = Isaac.GetCardIdByName("05_TheGalaxy"),
  WISE = Isaac.GetCardIdByName("06_WiseOne"),
  RIVER = Isaac.GetCardIdByName("07_TheRiver"),
  PAIN = Isaac.GetCardIdByName("08_ThePainful"),
  DAMAGER = Isaac.GetCardIdByName("09_TheDamager")
}

---------------------------
--EID Text Implementation--
---------------------------

if not __eidItemDescriptions then
	__eidItemDescriptions = {};
end
--Active Items
do
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.ROPE_LADDER] = "!SINGLE USE!#Returns Isaac to the first room of the previous floor and resets all rooms"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.WHITE_MASK] = "Apply a random transformation for the current room"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.BOWLING_BALL] = "Roll a bowling ball that damages enemies"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.LIL_LAMBY] = "Spawns a familiar that slows enemies and projectiles around it"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.BOOK_OF_LUCIFER] = "Spawns a succubus for the current room"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.TECH_G] = "Become Mecha-Guppy for current room, shoot mini Tech-X shots as well as tears"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.CEREMONIAL_DAGGER] = "Spawn a devil deal item, take 1 or 2 red heart containers#3 uses remaining"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.DAMAGED_CEREMONIAL_DAGGER] = "Spawn a devil deal item, take 1 or 2 red heart containers#2 uses remaining"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.BROKEN_CEREMONIAL_DAGGER] = "Spawn a devil deal item, take 1 or 2 red heart containers"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.BYOUXS_HEAD] = "Rainbow gems fall from the sky, enemies hit by them become friendly"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.PILLOW] = "1st use - Fully heals the player as if using a bed#2nd use - Gives player half a red heart for each heart container they have#3rd use - Tear delay -2"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.USED_PILLOW] = "1st use - Gives player half a red heart for each heart container they have#2nd use - Tear delay -2"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.RAGGED_PILLOW] = "Tear delay -2"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.CRAYON] = "Create a drawn turret that fights for you"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.TOWEL] = "Clears all enemy projectiles in the room,sprays a tear from the player in a random direction for each projectile removed"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.DASH] = "Become invincible and break rocks under you for a couple seconds"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.BOOK_OF_GENESIS] = "!SINGLE USE!#Return to Basement 1 while keeping your items and health"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.D666] = "Rerolls devil deal rooms and rerolls red chests into devil deal items#(Spawns Krampus if you reroll multiple red chests at once)"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.EXTINGUISHED_CANDLE] = "Absorb fires in the room for a stat boost, can sometimes spawn flame familiars"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.ETERNAL_CANDLE] = "Set fire to every enemy in the room"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.COUNTERFEIT_SOUL] = "Makes a devil deal in the room free"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.EBUY] = "Sell your newest item#(Will sell itself if it is your most recently picked up item)"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.TINTED_BLASTER] = "Shatters every tinted rock in the room"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.SCRATCH] = "Closest enemy becomes scratched, all other enemies are charmed and target the scratched enemy"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.DUMMY] = "Spawn a dummy, hurting the dummy hurts all enemies in the room"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.POKERDECK] = "Spawns a random playing card"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.MAGICDECK] = "Spawns a random magic card"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.BOOK_OF_RANDOMNESS] = "Activate a random book effect"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.CREEP_BUCKET] = "Spawn creep puddles under enemies in the room"
	__eidItemDescriptions[GENESIS_ITEMS.ACTIVE.WISH] = "Gives you a random item for the current floor#Recharges on new floor"
end

--Passive Items
do
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.GLOBIN_GOO] = "Upon death, Isaac turns into goo. If no damage is taken, Isaac will regenerate with half a heart after a few seconds."
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.SPLIT_CROSS] = "Tears have a chance to split into 4 smaller tears that move diagonally"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.PONGER] = "Shoot tears diagonally, bombs shoot 4 diagonal tears"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.SALT] = "Small damage up when you miss#Miss enough times and you'll explode and lose the damage boost#(Explosion does not damage you)"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.RING_OF_FIRE] = "Get a fire orbital when you clear a room#Fires can be extinguished"
	--this is a long one
	local superpowerText = "Isaac: Fire 8 tears around you every second when low on health - Magdalene: Enemies are charmed on room entry and red hearts are doubled"
	superpowerText = superpowerText .. "#Cain: Enemies sometimes drop coins on death - Judas: Create a shockwave when hurt"
	superpowerText = superpowerText .. "#???: Red hearts are replaced by soul hearts - Eve: Sometimes damage all enemies in room when hurt"
	superpowerText = superpowerText .. "#Samson: Gives Lusty Blood and makes creep trail - Azazel: Doubles brimstone shot range"
	superpowerText = superpowerText .. "#Lazarus: Chance for enemies to spawn creep - Eden: Spawn 2 items"
	superpowerText = superpowerText .. "#The Lost: 50% to avoid damage and gain a shield when hit - Lilith: Gives Succubus and BFFS!"
	superpowerText = superpowerText .. "#The Keeper: Spawns 1-10 Lucky Pennies - Apollyon: Spawns Smelter and Void if you don't already have them"
	superpowerText = superpowerText .. "#The Forgotten: HP up and random bone item - The Soulless: Spawn friendly boney when entering hostile room"
	superpowerText = superpowerText .. "#Lucifer: Spawns 2 devil items - Xaphan: Spawns 4 fireplaces"
	superpowerText = superpowerText .. "#The Drawn: Spawn 80 ink - Barnael: Spawns a Resurrection Heart and a Necro Heart."
	superpowerText = superpowerText .. "#Other: HP up and spawns random coin"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.SUPERPOWERS] = superpowerText
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.CURSED_BONE] = "You shoot bones with a small chance to charm enemies#Damage and Shot speed up"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.POISON_RAY] = "Constantly shoot a beam in front of the player that damages enemies"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.HELLBEAM] = "Fire brimstone beams from the floor#Target enemies like epic fetus"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.BEGINNERS_LUCK] = "Gives more luck the earlier in the run you are"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.CUTE_BOMBS] = "Bombs charm enemies when they explode#+5 bombs"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.LUCIFERS_WALLET] = "Causes devil items to cost money instead of hearts"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.TECHNOLOGY_3] = "Charge up and release bursts of laser beams"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.TECH_BOOM] = "Bombs randomly shoot out lasers until they explode#+5 bombs"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.AZAZELS_LOST_HORN] = "You shoot short range brimstone beams like Azazel"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.LOST_COURAGE] = "The closer you get to enemies the higher your damage gets"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.DYSLEXIA] = "Hearts, bombs, keys and coins are randomised#You can't tell what type they are until you grab them#Nickels, Dimes and Golden versions of pickups don't glitch"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.DEJA_VU] = "Your damage is doubled in rooms you've already visited"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.CHAMELEON] = "After taking damage from an enemy become immune to that type of enemy#Does not apply to bosses"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.TETRACHROMACY] = "Fire rainbow shots in 8 different directions at once#Tear delay +20"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.OVEN_MITT] = "Fireplaces no longer collide with you (or your tears)#(Fires spawned by enemies can still hurt you)"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.CURSED_CONTRACT] = "Spawns a devil deal item but gives you curses"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.LIGHTSHOT] = "Fire pulses of holy light"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.SATANS_CHALICE] = "When player's heart containers are full red hearts picked up become orbitals that block damage."
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.LOST_POWER] = "Gain +1 damage for every 12 rooms cleared"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.EVIL_OPTIONS] = "Devil deal rooms always have 4 items"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.ZEUS_TOUCH] = "Lightning strikes when you take damage"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.EXPLOSIVE_TEMPER] = "After firing consistently for a while you explode and become unable to shoot for a few seconds#(Explosion does not damage you)"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.LOST_WISDOM] = "Replaces curses with wishes#(Wishes are like positive curses)"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.RARE_KEY] = "Allows you to challenge the serpent"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.BLACK_BELT] = "Gives you a random item if you complete a floor without taking damage"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.RAINSTORM] = "Tears rain down from the sky after you take damage"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.RAINBOOTS] = "When tears hit an enemy, it creates a puddle#Stepping in the puddle will splash tears in all directions"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.CUTEDRESS] = "If an enemy hurts you other enemies will attack them"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.SAD_HEART] = "+1 HP, +1 damage"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.ARMOR_OF_ARES] = "2 items spawn in challenge rooms, +1 damage"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.BANDANA] = "When attacked you have a chance to avoid the damage and hurt the enemy#Chance scales with luck"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.PURPLE_BELT] = "When hurt by an enemy all other enemies freeze and become invincible until you kill the enemy that hurt you"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.BALLOON] = "Lob balloons that burst into tears when they hit the ground"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.VIRTUE] = "All stats up when all red heart containers are full#Stat boost amount based on how many red hearts you have"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.ETERNAL] = "Eternal hearts give extra lives instead of HP ups"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.EVENMOREOPTIONS] = "The first time an item pedestal spawns after picking this up or changing floors a second item also spawns"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.QUAKESHOT] = "Fire shockwaves across the ground that break rocks and stone chests"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.MOUSTACHE] = "Sometimes fire 3 shots behind you"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.PUKE] = "Sometimes after firing a tear you fire an extra one"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.SPARKLEBOMB] = "Enemies hit by bombs turn to gold#Break gold enemies for coins"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.SKIPPYTEARS] = "Tears bounce as they travel, enemies near them when they bounce can take AOE damage#+5 tear delay and range down"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.ANGELBLADE] = "Attack with a sword#(Disables tears)"
	__eidItemDescriptions[GENESIS_ITEMS.PASSIVE.TOO_MUCH_WATER] = "Killed enemies burst into tears"
end

--Familiars
do
	__eidItemDescriptions[GENESIS_ITEMS.FAMILIAR.SCHIZOFRENIA] = "Familiar that gives you different stats based on its mood#Mood changes randomly when changing rooms"
	__eidItemDescriptions[GENESIS_ITEMS.FAMILIAR.MERP] = "Familiar that moves diagonally and sometimes attracts pickups, enemies and projectiles"
	__eidItemDescriptions[GENESIS_ITEMS.FAMILIAR.RAG_MINI] = "Familiar that sometimes spawns homing orbs when hit by enemy projectiles"
	__eidItemDescriptions[GENESIS_ITEMS.FAMILIAR.LIL_BROSKI] = "Familiar that targets enemies and explodes on contact"
	__eidItemDescriptions[GENESIS_ITEMS.FAMILIAR.LIL_FORSAKEN] = "Familiar that bounces around, randomly firing short brimstone shots"
	__eidItemDescriptions[GENESIS_ITEMS.FAMILIAR.INFERNO] = "Familiar that sets fire to enemies and tears that pass through it"
	__eidItemDescriptions[GENESIS_ITEMS.FAMILIAR.LIL_GABRIEL] = "Familiar with holy beam attack, needs to charge like Lil Brimstone"
end

--Trinkets
if not __eidTrinketDescriptions then
	__eidTrinketDescriptions = {};
end
do
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.SOUL_KEY] = "Small chance to spawn a soul heart when unlocking something#Lets you use soul hearts to unlock things if you have no keys"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.RED_PENDANT] = "When using ceremonial dagger it always take only 1 heart"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.HOLY_PENDANT] = "Spawn an angel item instead of a devil deal item when using ceremonial dagger"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.PENDANT_OF_SHADOWS] = "When using ceremonial dagger it always gives a 2 heart devil deal item, but always takes 2 hearts"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.RITUAL_CANDLE] = "Doubles the chance of getting ritual rooms"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.BLOODY_CARD] = "Don't consume cards when you use them if you have 2 hearts or more, but it costs 1 heart#(Works without damaging you during invincibility frames)"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.STAMP] = "Small chance to slow all enemies in the room when you take damage"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.LOCKPICKS] = "The first time you would unlock something each floor you unlock it for free"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.GLOWING_ROCK] = "Higher chance for tinted rocks and crawlspaces to spawn"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.LOST_CD] = "Sometimes fire a Tetrachromacy tear burst"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.BROKEN_CLOCK] = "Opens boss rush door regardless of time taken"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.BROKEN_MAP] = "Reveals mapping while enemies are in the room#G ives curse of the lost in cleared rooms"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.GOLDEN_CALF] = "100% angel room chance#(Does not work for Lucifer)"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.LASER_OPTIC] = "Sometimes fire tech lasers instead of tears"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.MYSTERY_BOX] = "Spawns random pickups when you start a new floor"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.SAFETY_PICKAXE] = "Destroys bomb rocks when you enter a room"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.ROTTEN_EGG] = "Sometimes fire explosive tears"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.UNCERTAIN] = "Provides a random trinket effect#Effect changes when you change rooms"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.GLOBE] = "The first time you die while holding this you survive and warp to a random floor#(Be warned, this can take you all the way to the void)"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.HOPE] = "The first time you die while holding this you survive and warp to the cathedral"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.REMORSE] = "The first time you die while holding this you survive and warp to sheol"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.CLOVER] = "Randomly sets your luck between 10 and -10 when changing rooms"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.ASH] = "Fires spawn coins more often"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.KNIFE] = "Small chance to apply bleed effect to enemies in the room when you enter"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.CHILI] = "1/5 chance to release a burning fart when you take damage"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.CHARGEDKEY] = "If you have no keys you can spend your active item charge to unlock doors"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.BLOODYKEY] = "If you have no keys you can spend your red hearts to unlock doors"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.BOSSPASS] = "Boss trap rooms are always open regardless of your health"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.FIREEYE] = "Turns red fires into regular fires"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.FASTFORWARD] = "Everything becomes faster"
	__eidTrinketDescriptions[GENESIS_ITEMS.TRINKET.SKULL] = "Prevents skulls turning into hosts when they break"
end

--Pills
if not __eidPillDescriptions then
	__eidPillDescriptions = {};
end
do
	__eidPillDescriptions[GENESIS_PILLS.HELLPILL] = "Sets angel room chance to 0%#(HUD does not show update)";
	__eidPillDescriptions[GENESIS_PILLS.GODPILL] = "Sets angel room chance to 100%"
	__eidPillDescriptions[GENESIS_PILLS.KEYPILL] = "Swap key and coin amounts"
	__eidPillDescriptions[GENESIS_PILLS.BOMBPILL] = "Swap bomb and coin amounts"
	__eidPillDescriptions[GENESIS_PILLS.DAMAGEPILL] = "Damage up"
	__eidPillDescriptions[GENESIS_PILLS.MINDPILL] = "Map is revealed"
	__eidPillDescriptions[GENESIS_PILLS.DAMAGEDOWN] = "Damage down"
end

--Cards
if not __eidCardDescriptions then
	__eidCardDescriptions = {};
end
do
	__eidCardDescriptions[GENESIS_CARDS.ACE_OF_RICHES] = "Changes all enemies in the room into pickups"
	__eidCardDescriptions[GENESIS_CARDS.LUST] = "Charms all enemies in the room"
	__eidCardDescriptions[GENESIS_CARDS.RIFFLE] = "Switches your amounts of bombs, coins and keys"
	__eidCardDescriptions[GENESIS_CARDS.TEIWAZ] = "Gives you a normal heart, a black heart and soul heart"
	__eidCardDescriptions[GENESIS_CARDS.PERPS] = "Spawns a number of coins based on your luck"
	__eidCardDescriptions[GENESIS_CARDS.DIVERSITY] = "Spawns a Reroll Machine"
	__eidCardDescriptions[GENESIS_CARDS.KING_OF_HEARTS] = "Turns Champions into normal enemies"
	__eidCardDescriptions[GENESIS_CARDS.SAINT] = "Teleports you to an angel room"
	__eidCardDescriptions[GENESIS_CARDS.HERO] = "Spawn a key, a golden chest and a statue of a king"
	__eidCardDescriptions[GENESIS_CARDS.CRUCIFIX] = "You can fly for the current room"
	__eidCardDescriptions[GENESIS_CARDS.SALVATION] = "Turns red hearts in room into double the amount of soul hearts"
	__eidCardDescriptions[GENESIS_CARDS.HUMBLE] = "When used in a cleared room it spawns an additional random reward for clearing the room"
	__eidCardDescriptions[GENESIS_CARDS.GALAXY] = "Spawn ladder to crawlspace, open way to black market if you are in crawlspace"
	__eidCardDescriptions[GENESIS_CARDS.WISE] = "Activates random book effect"
	__eidCardDescriptions[GENESIS_CARDS.RIVER] = "Flush effect, kills poop enemies and floods room"
	__eidCardDescriptions[GENESIS_CARDS.PAIN] = "Take 1 red heart, double your damage for the current floor#Effect can be stacked"
	__eidCardDescriptions[GENESIS_CARDS.DAMAGER] = "For this room if you take damage deal your damage to enemies in the room"
end


--------------------
--All Achievements--
--------------------

GENESIS_ACHIEVEMENTS = {
  --Lucifer
  HELLBEAM_UNLOCKED = false,
  BOOK_OF_LUCIFER_UNLOCKED = false,
  LIL_LAMBY_UNLOCKED = false,
  LUCIFER_WALLET_UNLOCKED = false,
  CURSED_CONTRACT_UNLOCKED = false,
  SATANS_CHALICE_UNLOCKED = false,

  --Soulless
  CURSED_BONE_UNLOCKED = false,
  LOST_COURAGE_UNLOCKED = false,
  RAG_MINI_UNLOCKED = false,
  LOST_POWER_UNLOCKED = false,
  LOST_WISDOM_UNLOCKED = false,

  --Barnael
  AZAZELS_LOST_HORN_UNLOCKED = false,
  TECHNOLOGY_3_UNLOCKED = false,

  --Xaphan
  RING_OF_FIRE_UNLOCKED = false,
  ETERNAL_CANDLE_UNLOCKED = false,
  COUNTERFEIT_SOUL_UNLOCKED = false,
  ZEUS_TOUCH_UNLOCKED = false,

  --Drawn
  CRAYON_UNLOCKED = false
}


----------------
--All Costumes--
----------------

_G.GENESIS_COSTUMES = {
  HELLBEAM_COSTUME = Isaac.GetCostumeIdByPath("gfx/characters/costume_hellBeam.anm2"),
	EXTINGUISHED_CANDLE_COSTUME = Isaac.GetCostumeIdByPath("gfx/characters/extinguishedcandle.anm2"),
	FIRE_HEAD_COSTUME = Isaac.GetCostumeIdByPath("gfx/characters/firehead.anm2"),
	TECH_G_COSTUME = Isaac.GetCostumeIdByPath("gfx/characters/cyberguppy.anm2"),
	SPLIT_CROSS_COSTUME = Isaac.GetCostumeIdByPath("gfx/characters/4cross.anm2"),
	SALT_COSTUME = Isaac.GetCostumeIdByPath("gfx/characters/salt.anm2"),
	HIDDEN_BOX = Isaac.GetCostumeIdByPath("gfx/characters/hiddenbox.anm2"),
	LIL_DEMON = Isaac.GetCostumeIdByPath("gfx/characters/lildemon.anm2"),
	LIL_DEMON_EVIL = Isaac.GetCostumeIdByPath("gfx/characters/lildemon evil.anm2"),
	LIL_DEMON_GOOD = Isaac.GetCostumeIdByPath("gfx/characters/lildemon holy.anm2"),
	HORN = Isaac.GetCostumeIdByPath("gfx/characters/horn.anm2"),
	TECH_3 = Isaac.GetCostumeIdByPath("gfx/characters/tech3.anm2"),
  ROBOSACC = Isaac.GetCostumeIdByPath("gfx/characters/robosacc.anm2"),
  ENIGMA = Isaac.GetCostumeIdByPath("gfx/characters/transformation_enigma.anm2"),
	LUCIFERS_WALLET = Isaac.GetCostumeIdByPath("gfx/characters/costume_lucifersWallet.anm2"),
	DASH = Isaac.GetCostumeIdByPath("gfx/characters/spinning.anm2"),
	LIGHTSHOT = Isaac.GetCostumeIdByPath("gfx/characters/lightshot_costume.anm2"),
	LOST_POWER = Isaac.GetCostumeIdByPath("gfx/characters/lpower_costume.anm2"),
	BLACK_BELT = Isaac.GetCostumeIdByPath("gfx/characters/black_belt.anm2"),
	RAINBOOTS = Isaac.GetCostumeIdByPath("gfx/characters/rainboots.anm2"),
	DRESS = Isaac.GetCostumeIdByPath("gfx/characters/dresscostume.anm2"),
	SAD_HEART = Isaac.GetCostumeIdByPath("gfx/characters/hearteyes.anm2"),
  ARMOR_OF_ARES = Isaac.GetCostumeIdByPath("gfx/characters/armorofares.anm2"),
  BANDANA = Isaac.GetCostumeIdByPath("gfx/characters/bandana_costume.anm2"),
  BELT = Isaac.GetCostumeIdByPath("gfx/characters/purple_belt.anm2"),
  POISONRAY_COSTUME = Isaac.GetCostumeIdByPath("gfx/characters/poisonray.anm2")
}

----------------
--All Entities--
----------------

_G.GENESIS_ENTITIES = {
	VARIANT = {
		SPIKED_GOLDEN_CHEST = Isaac.GetEntityVariantByName("Spiked Golden Chest"),
		MUSIC_MACHINE = Isaac.GetEntityVariantByName("Music Machine"),
		CHARGE_MACHINE = Isaac.GetEntityVariantByName("Charge Machine"),
		RAG_MINI = Isaac.GetEntityVariantByName("Rag Mini"),
		SMALL_PURPLE_BALL = Isaac.GetEntityVariantByName("Small Purple Ball"),
		MERP = Isaac.GetEntityVariantByName("Merp"),
		SOUND_EFFECT = Isaac.GetEntityVariantByName("sound effect"),
		SCHIZOFRENIA = Isaac.GetEntityVariantByName("Schizofrenia"),
		SOY_WORM = Isaac.GetEntityVariantByName("Soy Worm"),
		ROUND_WORM = Isaac.GetEntityTypeByName("Round Worm"),
		HANGING_SPIDER = Isaac.GetEntityVariantByName("Hanging Spider"),
		BIG_BONES = Isaac.GetEntityVariantByName("Big Bones"),
		MYOTE = Isaac.GetEntityVariantByName("Myote"),
		THE_BOUND = Isaac.GetEntityVariantByName("The Bound"),
		DIP_DOUBLE = Isaac.GetEntityVariantByName("Double Dip"),
		BLOODY_SUCKER = Isaac.GetEntityVariantByName("Bloody Sucker"),
		DROWNED_SPITTY = Isaac.GetEntityVariantByName("Drowned Spitty"),
		PUKIE = Isaac.GetEntityVariantByName("Pukie"),
		BONE_CRUSHER = Isaac.GetEntityVariantByName("Bone Crusher"),
		RESURRECTION_HEART = Isaac.GetEntityVariantByName("Resurrection Heart"),
		NECRO_HEART = Isaac.GetEntityVariantByName("Necro Heart"),
    BARNAEL_GLOW = Isaac.GetEntityVariantByName("Barnael Glow"),
		GREEN_GHOST = Isaac.GetEntityVariantByName("Green Ghost"),
		SATANIC_CANDLE = Isaac.GetEntityVariantByName("Satanic Candle"),
		CRAYON_TURRET = Isaac.GetEntityVariantByName("Drawn Turret"),
		DYSLEXIA_PICKUP = Isaac.GetEntityVariantByName("Dyslexia Pickup"),
		DYSLEXIA_HEART = Isaac.GetEntityVariantByName("Dyslexia Heart"),
		MONEY_DEAL = Isaac.GetEntityVariantByName("Money Deal"),
		DEATH_LIST_SOUL = Isaac.GetEntityVariantByName("Red Soul"),
		BLOOD_ORBITAL = Isaac.GetEntityVariantByName("Blood Orbital"),
		BOMB_DIP = Isaac.GetEntityVariantByName("Bomb Dip"),
		DANK_DIP = Isaac.GetEntityVariantByName("Dank Dip"),
		SEPTIC_DIP = Isaac.GetEntityVariantByName("Septic Dip"),
    INK_SMALL = Isaac.GetEntityVariantByName("Small Ink Drop"),
    INK_MED = Isaac.GetEntityVariantByName("Medium Ink Drop"),
    INK_LARGE = Isaac.GetEntityVariantByName("Large Ink Drop"),
    FANGS = Isaac.GetEntityVariantByName("Fangs"),
    BOMB_TOOTH = Isaac.GetEntityVariantByName("Bomb Tooth"),
    IPECAC_CREEP = Isaac.GetEntityVariantByName("Ipecac Wall Creep"),
    TINTED_STONEY = Isaac.GetEntityVariantByName("Tinted Stoney"),
    BOMB_STONEY = Isaac.GetEntityVariantByName("Bomb Stoney"),
    PIMPLE_MAN = Isaac.GetEntityVariantByName("Pimple Man"),
    NECROMANCER = Isaac.GetEntityVariantByName("Necromancer"),
    SEPTIC_ENTRANCE = Isaac.GetEntityVariantByName("Septic Trapdoor"),
    HOLY_BABY = Isaac.GetEntityVariantByName("Holy Baby"),
    HOLY_CLOTTY = Isaac.GetEntityVariantByName("Holy Clotty"),
    BREEDER_FLY = Isaac.GetEntityVariantByName("Breeder Fly"),
    SHITTY_CHARGER = Isaac.GetEntityVariantByName("Shitty Charger"),
    ROCK_FLY = Isaac.GetEntityVariantByName("Rock Fly"),
    GREEN_FIRE = Isaac.GetEntityVariantByName("Green Fire Place"),
    INFAMOUS = Isaac.GetEntityVariantByName("Infamous"),
    ANGRY_INFAMOUS = Isaac.GetEntityVariantByName("Angry Infamous"),
    NERVELESS = Isaac.GetEntityVariantByName("Nerveless"),
    SEPTIC_GAPER = Isaac.GetEntityVariantByName("Septic Gaper"), --the various septic enemies have all the same variant, actually, except for pacers which have it increased by 1
    SEPTIC_FATTY = Isaac.GetEntityVariantByName("Septic Fatty"),
    SEPTIC_GUSHER = Isaac.GetEntityVariantByName("Septic Gusher"),
    SEPTIC_PACER = Isaac.GetEntityVariantByName("Septic Pacer"),
    SEPTIC_HOST = Isaac.GetEntityVariantByName("Septic Host"),
    BLOODY_CHEST = Isaac.GetEntityVariantByName("Bloody Chest"),
    THE_SERPENT = Isaac.GetEntityVariantByName("The Serpent"),
    THE_BOUND = Isaac.GetEntityVariantByName("The Bound"),
    THE_BOUND_HEAD = Isaac.GetEntityVariantByName("Bounded Head"),
    BEANIE = Isaac.GetEntityVariantByName("Beanie"),
    SEPTIC_PLATFORM = Isaac.GetEntityVariantByName("SepticPlatform"),
    SEPTIC_SPLASH = Isaac.GetEntityVariantByName("Septic Splash"),
    TRAPDOOR_EFFECT = Isaac.GetEntityVariantByName("SepticTrapdoorEffect"),
    SUCK_SPIDER = Isaac.GetEntityVariantByName("Suckspider"),
    SPLIT_SPIDER = Isaac.GetEntityVariantByName("splitspider"),
    LIL_BROSKI = Isaac.GetEntityVariantByName("Lil Broski"),
    PUDDLE_TEAR_VARIANT = Isaac.GetEntityVariantByName("puddleTear"),
    PUDDLE_LASER_VARIANT = Isaac.GetEntityVariantByName("puddleLaser"),
    PUDDLE_BRIMSTONE_VARIANT = Isaac.GetEntityVariantByName("puddleBrimstone"),
    PUDDLE_KNIFE_VARIANT = Isaac.GetEntityVariantByName("puddleKnife"),
    CUTE_BUBBLE_VARIANT = Isaac.GetEntityVariantByName("Cute Bubble"),
    SCRATCH_MARKS_VARIANT = Isaac.GetEntityVariantByName("Scratch Marks"),
    STATUS_VARIANT = Isaac.GetEntityVariantByName("status"),
    DRAWN_CONTROLS = Isaac.GetEntityVariantByName("Drawn Controls")
	},

  CUSTOM_SPIDERS = Isaac.GetEntityTypeByName("Suckspider"),
  THE_SERPENT = Isaac.GetEntityTypeByName("The Serpent"),
	MUSIC_MACHINE = Isaac.GetEntityTypeByName("Music Machine"),
	CHARGE_MACHINE = Isaac.GetEntityTypeByName("Charge Machine"),
	HELLBEAM_TARGET = Isaac.GetEntityTypeByName("Hellbeam Target"),
	RAG_MINI = Isaac.GetEntityTypeByName("Rag Mini"),
	SMALL_PURPLE_BALL = Isaac.GetEntityTypeByName("Small Purple Ball"),
	LIL_LAMBY = Isaac.GetEntityTypeByName("Lil Lamby"),
	CENSER_HALO = Isaac.GetEntityTypeByName("Censer Halo"),
	MERP = Isaac.GetEntityTypeByName("Merp"),
	SOUND_EFFECT = Isaac.GetEntityTypeByName("sound effect"),
	SCHIZOFRENIA = Isaac.GetEntityTypeByName("Schizofrenia"),
	BOWLING_BALL = Isaac.GetEntityTypeByName("Rolling Bowling Ball"),
	GOO = Isaac.GetEntityTypeByName("Goo"),
  BONE_CRUSHER = Isaac.GetEntityTypeByName("Bone Crusher"),
	BURN_CREEP = Isaac.GetEntityTypeByName("Burn Creep"),
	SPIDER = Isaac.GetEntityTypeByName("Spider"),
	CHARGER = Isaac.GetEntityTypeByName("Charger"),
	ROUND_WORM = Isaac.GetEntityTypeByName("Round Worm"),
	VIS = Isaac.GetEntityTypeByName("Vis"),
	DUKIE = Isaac.GetEntityTypeByName("Dukie"),
	MONEY_DEAL = Isaac.GetEntityTypeByName("Money Deal"),
	SATANIC_CANDLE = Isaac.GetEntityTypeByName("Satanic Candle"),
	FANGS = Isaac.GetEntityTypeByName("Fangs"),
  HANGING_SPIDER = Isaac.GetEntityTypeByName("Hanging Spider"),
  BIG_BONES = Isaac.GetEntityTypeByName("Big Bones"),
  MYOTE = Isaac.GetEntityTypeByName("Myote"),
  BLOODY_SUCKER = Isaac.GetEntityTypeByName("Bloody Sucker"),
  NECROMANCER = Isaac.GetEntityTypeByName("Necromancer"),
  PIMPLE_MAN = Isaac.GetEntityTypeByName("Pimple Man"),
  DR_EMBRYO = Isaac.GetEntityTypeByName("Dr. Embryo"),
  INFAMOUS = Isaac.GetEntityTypeByName("Infamous"),
  ANGRY_INFAMOUS = Isaac.GetEntityTypeByName("Angry Infamous"),
  NERVELESS = Isaac.GetEntityTypeByName("Nerveless"),
  PUDDLE_TEAR_TYPE = Isaac.GetEntityTypeByName("puddleTear"),
  PUDDLE_LASER_TYPE = Isaac.GetEntityTypeByName("puddleLaser"),
  PUDDLE_BRIMSTONE_TYPE = Isaac.GetEntityTypeByName("puddleBrimstone"),
  PUDDLE_KNIFE_TYPE = Isaac.GetEntityTypeByName("puddleKnife"),
  CUTE_BUBBLE_TYPE = Isaac.GetEntityTypeByName("Cute Bubble"),
  SCRATCH_MARKS_TYPE = Isaac.GetEntityTypeByName("Scratch Marks"),
  STATUS_TYPE = Isaac.GetEntityTypeByName("status"),
  BEANIE = Isaac.GetEntityTypeByName("Beanie"),
  THE_BOUND = Isaac.GetEntityTypeByName("The Bound"),
  THE_BOUND_HEAD = Isaac.GetEntityTypeByName("Bounded Head"),

}

-------------
--ALL MUSIC--
-------------

_G.GENESIS_MUSIC = {
	SATANIC_RITUAL_ROOM = Isaac.GetMusicIdByName("Satanic Ritual"),
	SATANIC_RITUAL_PUZZLE = Isaac.GetMusicIdByName("Satanic Puzzle"),
	SATANIC_RITUAL_COMPLETE = Isaac.GetMusicIdByName("Puzzle Completion"),
  SEPTIC = Isaac.GetMusicIdByName("Venemum Septic"),
  THE_SERPENT = Isaac.GetMusicIdByName("The Serpent Boss"),
  ALT_BOSS_JINGLE = Isaac.GetMusicIdByName("Boss Jingle 2"),
  CATACOMBS = Isaac.GetMusicIdByName("Catacombs2")
}

GENESIS_SFX = {
  DYSLEXIA_DROP = Isaac.GetSoundIdByName("Dyslexia Drop"),
  DYSLEXIA_PICKUP = Isaac.GetSoundIdByName("Dyslexia Pickup"),
  CANDLE_LIGHT = Isaac.GetSoundIdByName("Candle Light"),
  DYSLEXIA_HEART_DROP = Isaac.GetSoundIdByName("Dyslexia Heart Drop"),
  DYSLEXIA_HEART_PICKUP = Isaac.GetSoundIdByName("Dyslexia Heart Pickup"),
  DEATHLIST_LOST = Isaac.GetSoundIdByName("Death's List Lost"),
  DEATHLIST_REWARD = Isaac.GetSoundIdByName("Death's List Reward"),
  DEATHLIST_STREAK = Isaac.GetSoundIdByName("Death's List Streak"),
  SOUL_USE = Isaac.GetSoundIdByName("Souls Use"),
  LIGHTSHOT = Isaac.GetSoundIdByName("Lightshot"),
  SERPENT_ANGRY = Isaac.GetSoundIdByName("serpent_angry"),
  SERPENT_ANGRY2 = Isaac.GetSoundIdByName("serpent_angry2"),
  SERPENT_ATTACK = Isaac.GetSoundIdByName("serpent_attack"),
  FAMILIAR_SWEAR = Isaac.GetSoundIdByName("FUCK"),
  CUTE = Isaac.GetSoundIdByName("cuteSFX")
}

--------------
--ALL CURSES--
--------------

GENESIS_CURSES = {
	CURSE_REWIND = Isaac.GetCurseIdByName("Curse of Rewind") -1,
	CURSE_CHEST = Isaac.GetCurseIdByName("Curse of the Chest") -1,
	CURSE_PIT = Isaac.GetCurseIdByName("What is That?!") -1,
	CURSE_SEP = Isaac.GetCurseIdByName("Curse of Separation") -1,
	ENL_BUNDLE =  Isaac.GetCurseIdByName("You Feel Bundled..") - 1,
	ENL_SOUL =  Isaac.GetCurseIdByName("I Hear Lost Souls...") - 1,
	ENL_POOR = Isaac.GetCurseIdByName("You Feel Rich..") - 1,
	ENL_DELIGHT = Isaac.GetCurseIdByName("You Feel Delighted..") - 1,
	ENL_GOLDEN = Isaac.GetCurseIdByName("You Are Golden..") - 1,
	ENL_FRIEND = Isaac.GetCurseIdByName("Man's Best Friend") - 1,
	ENL_GIFT = Isaac.GetCurseIdByName("You Feel Gifted..") - 1
}
------------------
--ALL CHALLENGES--
------------------
GENESIS_CHALLENGES = {
  CHALLENGE_OLDSCHOOL = Isaac.GetChallengeIdByName("Old School Gamer"),
  CHALLENGE_HOLYMERP = Isaac.GetChallengeIdByName("HOLY MERP?!"),
  CHALLENGE_LAMBYSHADOW = Isaac.GetChallengeIdByName("Lamby's Shadow"),
  CHALLENGE_TECHG = Isaac.GetChallengeIdByName("Tech Guppy"),
  CHALLENGE_REWIND = Isaac.GetChallengeIdByName("Dniwer Emit"),
  CHALLENGE_SEPTIC = Isaac.GetChallengeIdByName("Lost in Septic")
}

end

do  --unlock animation stuff
    local sprite
    local idleDelay
    local idleMaxDelay = 60

    PlayUnlockAnimation = function(filename)
        filename = "gfx/ui/achievement/"..filename
        sprite = Sprite()
        sprite:Load("gfx/ui/achievement/achievements.anm2", true)
        sprite:ReplaceSpritesheet(3, filename)
        sprite:LoadGraphics()
        sprite:Play("Appear", true)
        Isaac.GetPlayer(0).ControlsEnabled = false
    end

    function genesis:RenderSpriteModdata()
        if not sprite then return end
        local room = Game():GetRoom()
        local pos = getScreenCenterPosition()
        sprite:Render(pos, Vector(0, 0), Vector(0, 0))
    end

    genesis:AddCallback(ModCallbacks.MC_POST_RENDER, genesis.RenderSpriteModdata)

    function genesis:UpdateSpriteModdata()
        if not sprite then return end
		local e = Genesis.getRoomEntities()
		for i=1, #e do
			if e[i]:IsEnemy() then
				e[i].Velocity =Genesis.VEC_ZERO
			end
		end
        sprite:Update()
        if sprite:IsFinished("Appear") then
            idleDelay = idleMaxDelay
            sprite:Play("Idle", true)
        end
        if sprite:IsPlaying("Idle") then
            idleDelay = idleDelay - 1
            if idleDelay < 0 then
                sprite:Play("Dissapear", true)
            end
        end
        if sprite:IsFinished("Dissapear") then
            sprite = nil
            Isaac.GetPlayer(0).ControlsEnabled = true
        end
    end

    genesis:AddCallback(ModCallbacks.MC_POST_UPDATE, genesis.UpdateSpriteModdata)

    function genesis:playerDamageModdata()
        if sprite then
            return false
        end
    end

    genesis:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, genesis.playerDamageModdata, EntityType.ENTITY_PLAYER)
end


genesis.STATS = {
	TEARS = 0,
	SHOTSPEED = 0,
	DAMAGE = 0,
	RANGE = 0,
	SPEED = 0,
	LUCK = 0
}

Genesis.STATSNAMES = {
	"TEARS",
	"SHOTSPEED",
	"DAMAGE",
	"RANGE",
	"SPEED",
	"LUCK"
}


----------------------
--ACHIEVEMENT SYSTEM--
----------------------

-- if Genesis.moddata.unlocks.<Achievement below> then -- to test if you have an achievement

-- to unlock an achievement
-- if not Genesis.moddata.unlocks.<Achievement below> then
-- 	   Genesis.moddata.unlocks.<Achievement below> = true
--     PlayUnlockAnimation("<Filename Unlock Animation>")
-- end

-- for adding Achievement add
-- <Achievement name> = false(,)

-- for adding Item Unlocks to an Achievement add
-- {ITEM = GENESIS_ITEMS.<Item Type>.<Item Name>, ACHIEVEMENT = "<Achievement name>"}(,)

Genesis.UNLOCKABLES = {
  --Lucifer
  {ITEM = GENESIS_ITEMS.ACTIVE.BOOK_OF_LUCIFER, ACHIEVEMENT = "BOOK_OF_LUCIFER_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.PASSIVE.LOST_COURAGE, ACHIEVEMENT = "HELLBEAM_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.ACTIVE.LIL_LAMBY, ACHIEVEMENT = "LIL_LAMBY_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.PASSIVE.LUCIFERS_WALLET, ACHIEVEMENT = "LUCIFER_WALLET_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.PASSIVE.CURSED_CONTRACT, ACHIEVEMENT = "CURSED_CONTRACT_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.PASSIVE.SATANS_CHALICE, ACHIEVEMENT = "SATANS_CHALICE_UNLOCKED"},

  --Soulless
  {ITEM = GENESIS_ITEMS.PASSIVE.CURSED_BONE, ACHIEVEMENT = "CURSED_BONE_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.PASSIVE.LOST_COURAGE, ACHIEVEMENT = "LOST_COURAGE_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.FAMILIAR.RAG_MINI, ACHIEVEMENT = "RAG_MINI_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.PASSIVE.LOST_POWER, ACHIEVEMENT = "LOST_POWER_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.PASSIVE.LOST_WISDOM, ACHIEVEMENT = "LOST_WISDOM_UNLOCKED"},

  --Barnael
  {ITEM = GENESIS_ITEMS.PASSIVE.AZAZELS_LOST_HORN, ACHIEVEMENT = "AZAZELS_LOST_HORN_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.PASSIVE.TECHNOLOGY_3, ACHIEVEMENT = "TECHNOLOGY_3_UNLOCKED"},

  --Xaphan
  {ITEM = GENESIS_ITEMS.PASSIVE.RING_OF_FIRE, ACHIEVEMENT = "RING_OF_FIRE_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.ACTIVE.ETERNAL_CANDLE, ACHIEVEMENT = "ETERNAL_CANDLE_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.ACTIVE.COUNTERFEIT_SOUL, ACHIEVEMENT = "COUNTERFEIT_SOUL_UNLOCKED"},
  {ITEM = GENESIS_ITEMS.ACTIVE.ZEUS_TOUCH, ACHIEVEMENT = "ZEUS_TOUCH_UNLOCKED"},

  --Drawn
  {ITEM = GENESIS_ITEMS.ACTIVE.CRAYON, ACHIEVEMENT = "CRAYON_UNLOCKED"}
}



--The Entity Register contains tables with this format: [RoomDecorationSeed]={[DataIndex (an int saved in the entity data)]={Type=the ent's type, Variant=the ent's variant, SubType=the ent's subtype,X,Y}}

genesis.DEFAULT_MODDATA = {
  unlocks = GENESIS_ACHIEVEMENTS,
  statsup = genesis.STATS,
  entityRegister = {}, --entities in this table will be respawned on room enter, look above
  merp = 0,
  firesouls = 0,
  fireysoullv = 0,
  currentstage = 1,
  barnael = 0,
  resetData = true,
  isSatanicRitualThisFloor = false,
  isSatanicRitualEnd = false,
  NumOfSatanicRitualsOnRun = 0,
  lastLevel = nil,
  Satanic_State = -1,
  Curse_Room_Id = -1,
  Payed_For_Ritual = false,
  Ritual_Puzzle = nil,
  ink = 0,
  spawnedCurseGift = false,
  curseRemovedTrapDoor = false,
  bloodOrbitals = { },
  walletItems = { },
  septicEntranceId = -1,
  septicEntranceGrid = -1,
  customCurrentStage = 0,
  customCurrentBoss = {},
  customBossdefeated = false,
	customBosssettedup = false,
  unlockedSepticEntrance = false,
  SerpentDefeated = false,
  SOUL_ARRAY = {
		Red = {Num = 0, Timer = nil, TimerFrames = nil, Selected = true, SType = 1, Cache = CacheFlag.CACHE_DAMAGE},
		Blue = {Num = 0, Timer = nil, TimerFrames = nil, Selected = false, SType = 4, Cache = CacheFlag.CACHE_FIREDELAY},
		Green = {Num = 0, Timer = nil, TimerFrames =  nil,  Selected = false, SType = 3, Cache = CacheFlag.CACHE_LUCK},
		Yellow = {Num = 0, Timer = nil, TimerFrames = nil, Selected = false, SType = 2, Cache = CacheFlag.CACHE_SPEED},
		Black = {Num = 0, Selected = false, SType = 5, Sound = SoundEffect.SOUND_DEVILROOM_DEAL},
		White = {Num = 0, Selected = false, SType = 6, Sound = SoundEffect.SOUND_HOLY}}
}

-- this code is used for resetting your achievements
--Isaac.RemoveModData(data)

--Yes, there is a json lua. It ran out of memory when saving entityRegister, plus it counted towards the local cap. So, table_tostring it is.

function Genesis.LoadString(s)
  local val = load("return "..s:gsub("\n",""))
--  if Genesis.DEBUG then Isaac.DebugString('Converted string "'..s..'" to '..type(val())) end
  if val then
    return val()
  else
    Isaac.DebugString("Genesis+: Failed to load string \""..s.."\"! Is there userdata in there?")
    return {}
  end
end

function Genesis.TableToString(tbl)
	local result, done = {}, {}
	for k, v in ipairs(tbl) do
		table.insert(result, table_val_to_str(v))
		done[k] = true
	end
	for k, v in pairs(tbl) do
		if not done[k] then
			table.insert(result,
			table_key_to_str(k) .. "=" .. table_val_to_str(v))
		end
	end
	return "{" .. table.concat(result, ",\n") .. "}"
end

function Genesis.LoadModdata()
	if Isaac.HasModData(genesis) then
		local data = Isaac.LoadModData(genesis)
		if data then
			Genesis.moddata = Genesis.CopyTable(Genesis.LoadString(data),genesis.DEFAULT_MODDATA)
		else
      Genesis.moddata = Genesis.CopyTable({},genesis.DEFAULT_MODDATA)
    end
	else
    Genesis.moddata = Genesis.CopyTable({},genesis.DEFAULT_MODDATA)
  end

--  Set the stageapi custom stage
--[[
  StageSystem.currentstage = Genesis.moddata.customCurrentStage
  StageSystem.currentboss = Genesis.moddata.customCurrentBoss
  StageSystem.bossdefeated = Genesis.moddata.customBossdefeated
	StageSystem.bosssettedup = Genesis.moddata.customBosssettedup

if Game() then
    local level = Game():GetLevel()
    if level:GetStage() == LevelStage.STAGE2_1 and Genesis.IsSeptic() then
        StageSystem.nextstage = StageSystem.currentstage
    end
  end
  ]]

  Isaac.DebugString("Genesis+: Loaded saved Data!")
  if Genesis.DEBUG then Isaac.DebugString("  ^-Loaded: "..table_tostring(Genesis.moddata)) end
end
function Genesis.SaveModdata()
  if Genesis.DEBUG then Isaac.DebugString("Genesis+: Saving...") end
  --[[Genesis.moddata.customCurrentStage = StageSystem.currentstage
  Genesis.moddata.customCurrentBoss = StageSystem.currentboss
  Genesis.moddata.customBossdefeated = StageSystem.bossdefeated
	Genesis.moddata.customBosssettedup = StageSystem.bosssettedup]]
  Isaac.SaveModData(genesis, Genesis.TableToString(Genesis.moddata))
  if type(Genesis.moddata) ~= "table" then
    Isaac.DebugString("Genesis.moddata is not a table")
    Isaac.DebugString(tostring(Genesis.moddata))
  end
  if Genesis.DEBUG then Isaac.DebugString("   Saved: "..table_tostring(Genesis.moddata)) end
end

--[[Return a table with, for keys that exist in both the source and target table, the values of the source table, and for values that only exist in the target, the values of the target table
We need to do this as simply doing, for example, table={a=1} and newtable = table, and then editing newtable.a like newtable.a=2, would also edit the variable in the old table.]]--
function Genesis.CopyTable(source,target)
  local output = {}

  if not target then target = source end --This needs to be done, to prevent errors when using a nil as a table

  for k,v in pairs(target) do --For every variable in target (not in source as it might not have variables added in a new version)
    if source[k] ~= nil then --If the source contains the variable

      if type(source[k]) == "table" then --If the value of the variable is also a table, we need to do this again. (Actually, I'm not sure, but to be safe, do this)
        output[k] = Genesis.CopyTable(source[k],target[k]);
      else
        output[k] = source[k]; --If the value of k isn't a table, just copy it over to the output.
      end

    else   --If the variable is only in target

      if type(target[k]) == "table" then --If the value of the variable is also a table, we need to do this again. (Actually, I'm not sure, but to be safe, do this)
        output[k] = Genesis.CopyTable({},target[k]);
      else
        output[k] = target[k]; --If the value of k isn't a table, just copy it over to the output.
      end

    end
  end

  return output;
end


function genesis:UpdateModdata()
  local player = Isaac.GetPlayer(0)
  local game = Game()
  local level = game:GetLevel()
	local room = game:GetRoom()
  local e = Genesis.getRoomEntities()

  math.randomseed(Isaac.GetTime())

	-- saving
  if game:GetFrameCount() % 200 == 0 then
    Genesis.SaveModdata()
  end
	-- unlockables
	for i=1, #e do
		if e[i].Type == EntityType.ENTITY_PICKUP then
			if e[i].Variant == PickupVariant.PICKUP_COLLECTIBLE or e[i].Variant == PickupVariant.PICKUP_SHOPITEM then
				for q=1, #Genesis.UNLOCKABLES do
					if e[i].SubType == Genesis.UNLOCKABLES[q].ITEM and not Genesis.moddata.unlocks[Genesis.UNLOCKABLES[q].ACHIEVEMENT] and not (e[i].SubType == GENESIS_ITEMS.PASSIVE.LOST_COURAGE and player:GetName() == "The Soulless") then
						player:UseActiveItem(CollectibleType.COLLECTIBLE_D6, false, false, true, false)
					end
				end
			end
		end
	end
end

genesis:AddCallback(ModCallbacks.MC_POST_UPDATE, genesis.UpdateModdata)

function genesis:UpdateModdataExit()
  Genesis.SaveModdata()
  Genesis.moddata = {}
end

genesis:AddCallback(ModCallbacks.MC_PRE_GAME_EXIT, genesis.UpdateModdataExit)

--the data is loaded on new save above, in genesis:Init

----------
--Septic--
----------

--[[
The check for the boss room in SettingUpStage2 in stagesapi.lua should be changed to this if it isn't like this already
			if room:GetType() == RoomType.ROOM_BOSS and room:GetFrameCount() < 1 and not (StageSystem.currentstage == StageSystem.GetStageIdByName("new_Septic") and level:GetStage() == LevelStage.STAGE2_2 or (level:GetCurseName() == "Curse of the Labyrinth" and level:GetStage() == LevelStage.STAGE2_1) and level:GetCurrentRoomDesc().ListIndex == level:GetLastBossRoomListIndex()) and not bossinroom and not StageSystem.bossdefeated then
Also, in NewRoom (still stagesapi.lua)
      if room:IsFirstVisit() and firstvisit ~= 1 and  then
should be changed to
			if room:IsFirstVisit() and firstvisit ~= 1 and not (StageSystem.currentstage == StageSystem.GetStageIdByName("new_Septic") and level:GetStage() == LevelStage.STAGE2_2 or ( level:GetCurseName() == "Curse of the Labyrinth" and level:GetStage() == LevelStage.STAGE2_1) and level:GetCurrentRoomDesc().ListIndex == level:GetLastBossRoomListIndex() )  then
--]]

local septic = RegisterMod("Septic",1)

Genesis.SepticBosses = {
    {
        Name = "Frail",
        Portrait = "gfx/ui/boss/portrait_thefrail.png",
        Bossname = "gfx/ui/boss/bossname_thefrail.png",
        Entity = {
            Type = EntityType.ENTITY_PIN,
            Variant = 2,
            SubType = 0,
            FallbackRoomName = 'FrailPlaceholder',
            Name = 'Frail'
        }
    },
    {
        Name = "Peep",
        Portrait = "gfx/ui/boss/portrait_68.0_peep.png",
        Bossname = "gfx/ui/boss/bossname_68.0_peep.png",
        Entity = {
            Type = EntityType.ENTITY_PEEP,
            Variant = 0,
            SubType = 0,
            FallbackRoomName = 'PeepPlaceholder',
            Name = 'Peep'
        }
    },
    {
        Name = "CarrionQueen",
        Portrait = "gfx/ui/boss/portrait_28.2_carrionqueen.png",
        Bossname = "gfx/ui/boss/bossname_28.2_carrionqueen.png",
        Entity = {
            Type = EntityType.ENTITY_CHUB,
            Variant = 2,
            SubType = 0,
            FallbackRoomName = 'CarrionQueenPlaceholder',
            Name = 'CarrionQueen'
        }
    },
    {
        Name = "Pestilence",
        Portrait = "gfx/ui/boss/portrait_64.0_pestilence.png",
        Bossname = "gfx/ui/boss/bossname_64.0_pestilence.png",
        Entity = {
            Type = EntityType.ENTITY_PESTILENCE,
            Variant = 0,
            SubType = 0,
            FallbackRoomName = 'PestilencePlaceholder',
            Name = 'Pestilence'
        }
    },
    {
        Name = "The Bound",
        Portrait = "gfx/bosses/the bound.png",
        Bossname = "gfx/bosses/bossname_the bound.png",
        Entity = {
            Type = GENESIS_ENTITIES.THE_BOUND,
            Variant = GENESIS_ENTITIES.VARIANT.THE_BOUND,
            SubType = 0,
            FallbackRoomName = 'TheBoundPlaceholder',
            Name = 'The Bound'
        }
    },
    {
        Name = "Serpent",
        Portrait = "gfx/septic_boss/portrait_serpent.png",
        Bossname = "gfx/septic_boss/bossname_serpent.png",
        Entity = {
            Type = GENESIS_ENTITIES.THE_SERPENT,
            Variant = GENESIS_ENTITIES.VARIANT.THE_SERPENT,
            SubType = 0,
            FallbackRoomName = 'SerpentPlaceholder',
            Name = 'Serpent'
        }
    },
}

for _, boss in ipairs(Genesis.SepticBosses) do
    boss.Rooms = StageAPI.CreateSingleEntityRoomList(boss.Entity.Type, boss.Entity.Variant, boss.Entity.SubType, boss.Entity.Name or "Placeholder", RoomType.ROOM_BOSS, boss.Entity.Name or "Placeholder")
    StageAPI.AddBossData(boss.Name, boss)
end

Genesis.SepticOverlays = {
    StageAPI.Overlay("gfx/backdrop/septic_overlay.anm2", Vector( 0.5,  0.5)),
    StageAPI.Overlay("gfx/backdrop/septic_overlay.anm2", Vector(-0.4, -0.5)),
    StageAPI.Overlay("gfx/backdrop/septic_overlay.anm2", Vector(-0.5,  0.5)),
    StageAPI.Overlay("gfx/backdrop/septic_overlay.anm2", Vector( 0.4, -0.5)),
}

Genesis.SepticGrid = StageAPI.GridGfx()
Genesis.SepticGrid:AddDoors("gfx/grid/septicdoor.png", StageAPI.DefaultDoorSpawn)
Genesis.SepticGrid:SetRocks("gfx/grid/rocks_septic.png")
Genesis.SepticGrid:SetPits("gfx/grid/septicpit.png")
Genesis.SepticGrid:SetBridges("gfx/grid/septic_bridge.png")
Genesis.SepticGrid:SetDecorations("stageapi/none.png")

local SepticBackdrop = StageAPI.BackdropHelper({
    Walls = {"1", "2", "3"},
    NFloors = {"nfloor"},
    LFloors = {"lfloor"},
    Corners = {"corner"}
}, "gfx/backdrop/new_Septic1_", ".png")

local SepticBigBackdrop = StageAPI.BackdropHelper({
    Walls = {"big_1", "big_2", "big_3"},
    NFloors = {"nfloor"},
    LFloors = {"lfloor"},
    Corners = {"corner"}
}, "gfx/backdrop/new_Septic1_", ".png")

local SepticBackdrop2 = StageAPI.BackdropHelper({
    Walls = {"1", "2", "3"},
    NFloors = {"nfloor"},
    LFloors = {"lfloor"},
    Corners = {"corner"}
}, "gfx/backdrop/new_Septic2_", ".png")

local SepticBigBackdrop2 = StageAPI.BackdropHelper({
    Walls = {"big_1", "big_2", "big_3"},
    NFloors = {"nfloor"},
    LFloors = {"lfloor"},
    Corners = {"corner"}
}, "gfx/backdrop/new_Septic2_", ".png")

local SepticBackdropBoss = StageAPI.BackdropHelper({
    Walls = {"boss"},
    NFloors = {"nfloor"},
    LFloors = {"lfloor"},
    Corners = {"corner"}
}, "gfx/backdrop/new_Septic1_", ".png")

local SepticBigBackdropBoss = StageAPI.BackdropHelper({
    Walls = {"big_boss"},
    NFloors = {"nfloor"},
    LFloors = {"lfloor"},
    Corners = {"corner"}
}, "gfx/backdrop/new_Septic1_", ".png")

local SepticRoomGfx = StageAPI.RoomGfx(SepticBackdrop, Genesis.SepticGrid, "_default", "stageapi/shading/shading")
local SepticRoomGfx2 = StageAPI.RoomGfx(SepticBackdrop2, Genesis.SepticGrid, "_default", "stageapi/shading/shading")
local SepticBossRoomGfx = StageAPI.RoomGfx(SepticBackdropBoss, Genesis.SepticGrid, "_default", "stageapi/shading/shading")

local SepticBigRoomGfx = StageAPI.RoomGfx(SepticBigBackdrop, Genesis.SepticGrid, "_default", "stageapi/shading/shading")
local SepticBigRoomGfx2 = StageAPI.RoomGfx(SepticBigBackdrop2, Genesis.SepticGrid, "_default", "stageapi/shading/shading")
local SepticBigBossRoomGfx = StageAPI.RoomGfx(Septic2BigBackdrop, Genesis.SepticGrid, "_default", "stageapi/shading/shading")

local SepticRooms = StageAPI.RoomsList('SepticDefault', {
    Name = 'SepticGenesis+',
    Rooms = require('resources.luarooms.septic')
})
SepticRoomCompat = SepticRoomCompat or {}
if SepticRoomCompat.Default then
    SepticRooms:AddRooms(table.unpack(SepticRoomCompat.Default))
end

Genesis.SepticStage = StageAPI.CustomStage("Septic")
Genesis.SepticStage:SetRooms({
    [RoomType.ROOM_DEFAULT] = SepticRooms
})

Genesis.SepticGfxRooms = {RoomType.ROOM_DEFAULT, RoomType.ROOM_TREASURE, RoomType.ROOM_MINIBOSS, RoomType.ROOM_BOSS }
Genesis.SepticStage:SetRoomGfx(SepticRoomGfx, {RoomType.ROOM_DEFAULT, RoomType.ROOM_TREASURE, RoomType.ROOM_MINIBOSS})
Genesis.SepticStage:SetRoomGfx(SepticBossRoomGfx, {RoomType.ROOM_BOSS})

Genesis.SepticStage:SetBosses({
    "Frail",
    "Peep",
    "CarrionQueen",
    "Pestilence",
    "The Bound"
})

Genesis.SepticStage:SetTransitionIcon("gfx/ui/stage/septic_icon.png")

Genesis.SepticStage:SetMusic(GENESIS_MUSIC.SEPTIC, {RoomType.ROOM_DEFAULT, RoomType.ROOM_TREASURE})
Genesis.SepticStage:SetMusic(GENESIS_MUSIC.THE_SERPENT, {RoomType.ROOM_BOSS})

Genesis.SepticStage:SetSpots("gfx/ui/stage/septic_boss_spot.png", "gfx/ui/stage/septic_player_spot.png")

Genesis.SepticStage:SetDisplayName('Septic I')
Genesis.SepticStage:SetReplace(StageAPI.StageOverride.CatacombsOne) -- og override was necro, but xl will trigger mom fight if necro
Genesis.SepticStage.IsSecondStage = false

Genesis.SepticStageXL = Genesis.SepticStage("Septic XL")
Genesis.SepticStageXL:SetDisplayName("Septic XL")
Genesis.SepticStageXL:SetNextStage({
    NormalStage = true,
    Stage = LevelStage.STAGE3_2
})
Genesis.SepticStageXL.IsSecondStage = true

Genesis.SepticStage:SetXLStage(Genesis.SepticStageXL)

Genesis.SepticStage2 = Genesis.SepticStage("Septic 2")

Genesis.SepticStage2:SetRoomGfx(SepticRoomGfx2, {RoomType.ROOM_DEFAULT, RoomType.ROOM_TREASURE, RoomType.ROOM_MINIBOSS})

Genesis.SepticStage2:SetReplace(StageAPI.StageOverride.CatacombsTwo) -- overriding necro 2 leads to mom effects in the boss
Genesis.SepticStage2:SetDisplayName("Septic II")
Genesis.SepticStage:SetNextStage(Genesis.SepticStage2)
Genesis.SepticStage2:SetNextStage({
    NormalStage = true,
    Stage = LevelStage.STAGE3_2
})
Genesis.SepticStage2.IsSecondStage = true

StageAPI.AddCallback("Genesis+", "PRE_CHANGE_ROOM_GFX", 2, function(stageRoom, gfx)
    if not Genesis.IsSeptic() then return end

    local room = Game():GetRoom()
    local type = room:GetType()
    local isGfxRoom = false
    for _, t in ipairs(Genesis.SepticGfxRooms) do
        if t == type then
            isGfxRoom = true
            break
        end
    end
    if not isGfxRoom then return end

    if room:GetRoomShape() ~= 1 then
        if gfx == SepticRoomGfx     then return SepticBigRoomGfx end
        if gfx == SepticRoomGfx2    then return SepticBigRoomGfx2 end
        if gfx == SepticBossRoomGfx then return SepticBigBossRoomGfx end
        return SepticBigRoomGfx
    end
end)

    --septic.Septic1 = StageSystem.GetOverlay(StageSystem.AddOverlay("septic_overlay.anm2", Vector(0.5,0.5)))
    --septic.Septic2 = StageSystem.GetOverlay(StageSystem.AddOverlay("septic_overlay.anm2", Vector(-0.4,-0.5)))
    --septic.Septic3 = StageSystem.GetOverlay(StageSystem.AddOverlay("septic_overlay.anm2", Vector(-0.5,0.5)))
    --septic.Septic4 = StageSystem.GetOverlay(StageSystem.AddOverlay("septic_overlay.anm2", Vector(0.4,-0.5)))
    --local Septic = StageSystem.GetStage(StageSystem.AddStage("new_Septic"))

    --StageSystem.AddCustomRoomsLua("septic")
    --Septic.AddBackdrop("new_Septic1", 3)
    --Septic.AddBackdrop("new_Septic2", 3)
    --Septic.SetBossBackdrop("new_Septicboss")
    --Septic.SetRoomFile("septic")
    --Septic.SetPits("septicpit.png")
    --Septic.SetRocks("rocks_septic.png")
--Septic.AddBoss("thefrail", EntityType.ENTITY_PIN, 2)
--Septic.AddBoss("68.0_peep", EntityType.ENTITY_PEEP)
--Septic.AddBoss("28.2_carrionqueen", EntityType.ENTITY_CHUB, 2)
--Septic.AddBoss("64.0_pestilence", EntityType.ENTITY_PESTILENCE)
    --Septic.SetNameSprite("septic_text.png","septic_text2.png")
    --Septic.SetMusic(GENESIS_MUSIC.SEPTIC)

function septic:NewFloor()
  local level = Game():GetLevel()
  local stage = level:GetStage()

  -- generate the septic entrance
  if (stage == LevelStage.STAGE2_2 or stage == LevelStage.STAGE2_1)
  and not Genesis.IsSeptic() then
    local rooms = level:GetRooms()
    local roomIds = {}
    local seed = Game():GetSeeds():GetStageSeed(stage)

    for i = 0, rooms.Size - 1 do
        local room = rooms:Get(i)
      local rType = room.Data.Type
      if (rType == RoomType.ROOM_DEFAULT or rType == RoomType.ROOM_BARREN or rType == RoomType.ROOM_MINIBOSS) and room.GridIndex ~= level:GetCurrentRoomIndex() then
        table.insert(roomIds, room.ListIndex)
      end
    end

    Genesis.moddata.septicEntranceId = roomIds[math.ceil((seed%100+1)/100*#roomIds)]

--    Isaac.DebugString("Septic entrance:"..tostring(Genesis.moddata.septicEntranceId).." "..tostring((seed%100+1)/100*#roomIds))
  else
    Genesis.moddata.septicEntranceId = -1
  end
  Genesis.moddata.septicEntranceGrid = -1
  Genesis.moddata.unlockedSepticEntrance = false

  PedestalSpawned = false
end

septic:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, septic.NewFloor)

function septic:Render()
  local room = Game():GetRoom()
  --local tab = Input.IsButtonPressed(Keyboard.KEY_TAB,0)
  if Genesis.IsSeptic() then
    if not (room:GetType() == RoomType.ROOM_BOSS and room:GetFrameCount() < 1 and room:IsFirstVisit()) then
      for _, overlay in ipairs(Genesis.SepticOverlays) do
        --septic["Septic"..i].Render()
        overlay:Render()
      end
  end
end
end

septic:AddCallback(ModCallbacks.MC_POST_RENDER, septic.Render)

function septic:NewRoom()
  local level = Game():GetLevel()
  local room = level:GetCurrentRoom()
  --local player = Isaac.GetPlayer(0)
  --stop enemies instantly attacking when you enter the room
    --local ents = Isaac.GetRoomEntities()
    --for i=1,#ents do
	--	if ents[i]:IsEnemy() then
	--		ents[i]:AddFreeze(EntityRef(player), 9)
	--		ents[i]:SetColor(Color(1,1,1,1,1,1,1), 9, 9, false, false)
	--	end
	--end


  --spawn multiple items in treasure rooms

  septic.entrance = nil
  if Genesis.DEBUG and (level:GetStage() == LevelStage.STAGE2_1 or level:GetStage() == LevelStage.STAGE2_2 and not Genesis.IsSeptic()) then
    Isaac.DebugString("Room: "..tostring(level:GetCurrentRoomDesc().ListIndex).."; Septic Entrance Room: "..tostring(Genesis.moddata.septicEntranceId))
end
  if level:GetCurrentRoomDesc().ListIndex == Genesis.moddata.septicEntranceId and Genesis.moddata.septicEntranceId >= 0 and not Genesis.IsSeptic() then
    septic.entranceFrame = 0
    septic.entering = false

    if Genesis.moddata.septicEntranceGrid < 0 then
      local rocks = {}
      for i=1, room:GetGridSize() do
        local grid = room:GetGridEntity(i)
        if grid ~= nil then
          if grid:ToRock() and grid.State == 1 then
            table.insert(rocks, i)
          end
        end
      end
      if #rocks>1 then
        Genesis.moddata.septicEntranceGrid = rocks[math.ceil((room:GetDecorationSeed()%100+1)/100*#rocks)]
      else
        local pos,k = nil, 0

        repeat
          local nearDoor
          pos = room:FindFreeTilePosition(room:GetCenterPos(), 32)
          for i=0,7 do
            if (room:GetDoorSlotPosition(i)-pos).X+(room:GetDoorSlotPosition(i)-pos).Y<=64 then --yes, this is a weird way of getting dist, but it's intended, I perfectly know about :Length()
              nearDoor = true
            end
          end
          k = k+1
        until not nearDoor or k >= 500

        Genesis.moddata.septicEntranceGrid = room:GetGridIndex(pos)
      end

      local grid = room:GetGridEntity(Genesis.moddata.septicEntranceGrid)
      if not grid then
        room:SpawnGridEntity(Genesis.moddata.septicEntranceGrid, GridEntityType.GRID_ROCKT, 1, RNG():GetSeed(), 1)
      else
        grid:SetType(GridEntityType.GRID_ROCKT)
        grid:Update()
        grid:Init(RNG():GetSeed())
      end
    end

    local grid = room:GetGridEntity(Genesis.moddata.septicEntranceGrid)
--    Isaac.DebugString("Replaced "..Genesis.moddata.septicEntranceGrid)
    if grid.State == 1 then
      local sprite = Sprite()
      sprite:Load("gfx/grid/Rocks.anm2", false)
      sprite:ReplaceSpritesheet(0, "gfx/grid/rocks_septic_entrance.png")
      sprite:LoadGraphics()
      sprite:SetFrame("tinted", 10)
      sprite:LoadGraphics()
      grid.Sprite = sprite
    else
      septic.entrance = Isaac.Spawn(EntityType.ENTITY_EFFECT, GENESIS_ENTITIES.VARIANT.SEPTIC_ENTRANCE, 0, grid.Position,Genesis.VEC_ZERO, nil)
      septic.entrance.DepthOffset = -200
      septic.entrance:GetSprite():Play("Spawn (Locked)", true)

      local spritesheet
      local h = room:GetGridHeight()
      local w = room:GetGridWidth()
      if level:GetStageType() == StageType.STAGETYPE_AFTERBIRTH then
        spritesheet = "gfx/grid/rocks_drownedcaves_noshade.png"
      elseif level:GetStageType() == StageType.STAGETYPE_WOTL then
        spritesheet = "gfx/grid/rocks_catacombs_noshade.png"
      else
        spritesheet = "gfx/grid/rocks_caves_noshade.png"
      end
      for i=1,h-math.ceil(Genesis.moddata.septicEntranceGrid/w) do --that second number is the grid y of the rock
        for k=-1,1 do
          local gr = room:GetGridEntity(Genesis.moddata.septicEntranceGrid+w*i+k)
          if gr and gr:ToRock() and gr.State == 1 then
            local e = Isaac.Spawn(EntityType.ENTITY_EFFECT, 8, 0, gr.Position,Genesis.VEC_ZERO, nil)
            e:GetSprite():Load("gfx/grid/Rocks.anm2", false)
            e:GetSprite():ReplaceSpritesheet(0, spritesheet)
            local rockanimations = {"normal", "black", "tinted", "rubble", "alt", "rubble_alt", "bombrock", "big", "superspecial", "ss_broken"}
            for rock=1, #rockanimations do
              if gr.Sprite:IsPlaying(rockanimations[rock]) or gr.Sprite:IsFinished(rockanimations[rock]) then
                e:GetSprite():SetFrame(rockanimations[rock], gr.Sprite:GetFrame())
              end
            end
            e:ToEffect():AddEntityFlags(EntityFlag.FLAG_NO_REMOVE_ON_TEX_RENDER)
            e:GetSprite():LoadGraphics()
          end
        end
      end
    end
  end
  end

septic:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, septic.NewRoom)

--if debug ~= nil then --if --luadebug is enabled
--  local vanillaSpawn = Isaac.Spawn

--  function Isaac.Spawn(t,v,s,p,vel,sp)
--    if t == EntityType.ENTITY_PIN then
--      Isaac.DebugString(debug.traceback())
--    end
--    local npc = vanillaSpawn(t,v,s,p,vel,sp)
--    return npc
--  end
--end
SepticPool = {}
PedestalsInTreasureRoom = 0
function septic:Update()
  local level = Game():GetLevel()
  local room = level:GetCurrentRoom()
  if room:IsFirstVisit() == false or room:GetType() ~= RoomType.ROOM_TREASURE then
	PedestalsInTreasureRoom = 0
  end
  local player = Isaac.GetPlayer(0)
  --get pool of all genesis items
  if #SepticPool < 1 then
	for key, value in pairs(GENESIS_ITEMS.ACTIVE) do
		table.insert(SepticPool, value)
	end
	for key, value in pairs(GENESIS_ITEMS.PASSIVE) do
		table.insert(SepticPool, value)
	end
  end


	--warp directly to septic debug command, press U I and O together
  if Genesis.DEBUG and not Genesis.IsSeptic() and level:GetCurrentRoomIndex() == level:GetStartingRoomIndex() then
    if Input.IsButtonPressed(Keyboard.KEY_U, player.ControllerIndex) and Input.IsButtonPressed(Keyboard.KEY_O, player.ControllerIndex) and Input.IsButtonPressed(Keyboard.KEY_I, player.ControllerIndex) then
      --StageSystem.nextstage = StageSystem.GetStageIdByName("new_Septic")
      Isaac.ExecuteCommand('cstage Septic')
      --Isaac.ExecuteCommand("stage 3a")
    end
  end


  if septic.entrance then
    local sprite = septic.entrance:GetSprite()
    if not Genesis.moddata.unlockedSepticEntrance then
      if sprite:IsFinished("Spawn (Locked)") and not sprite:IsPlaying("Unlock") and (player.Position-septic.entrance.Position):Length() <= player.Size+septic.entrance.Size and (player:GetNumKeys() > 0 or player:HasGoldenKey()) then
        Genesis.moddata.unlockedSepticEntrance = true
        sprite:Play("Unlock", true)
		if not player:HasGoldenKey() then
			player:AddKeys(-1)
		end
        SFXManager():Play(SoundEffect.SOUND_UNLOCK00, 1, 0, false, 1)
      end

    else
      if sprite:IsFinished("Spawn (Unlocked)") and (player.Position-septic.entrance.Position):Length() <= player.Size+septic.entrance.Size then
        player.Position = septic.entrance.Position
        if septic.entering and player:GetSprite():GetFrame() >= 15 then
            --StageSystem.GotoNewStage(StageSystem.GetStageIdByName("new_Septic"))
            StageAPI.GotoCustomStage(Genesis.SepticStage, true)
        elseif not septic.entering then
          player:AnimateTrapdoor()
          septic.entering = true
        end
      elseif (sprite:IsFinished("Unlock") or sprite:IsFinished("Spawn (Locked)")) and (player.Position-septic.entrance.Position):Length() > player.Size+septic.entrance.Size+32 and room:GetFrameCount() >= 33 then
        sprite:Play("Spawn (Unlocked)", true)
      end
    end

    for i,e in ipairs(Genesis.RoomPickups) do
      if e.Variant == PickupVariant.PICKUP_COLLECTIBLE and e.SubType == CollectibleType.COLLECTIBLE_SMALL_ROCK and (e.Position-septic.entrance.Position):Length() < 3 then
        e:Remove()
        break
      end
    end

    for i = #Genesis.RoomEffects,1,-1 do --loop through genesis.roomeffects in reverse
      local e = Genesis.RoomEffects[i]
      if e.Variant == 8 then
        local grid = room:GetGridEntity(room:GetGridIndex(e.Position))
        if not grid or grid.State ~= 1 then
          e:Remove()
        end
      end
    end
  elseif level:GetCurrentRoomDesc().ListIndex == Genesis.moddata.septicEntranceId then
    local grid = room:GetGridEntity(Genesis.moddata.septicEntranceGrid)
    if grid.State ~= 1 then
      septic.entrance = Isaac.Spawn(EntityType.ENTITY_EFFECT, GENESIS_ENTITIES.VARIANT.SEPTIC_ENTRANCE, 0, grid.Position,Genesis.VEC_ZERO, nil)
      septic.entrance.DepthOffset = -200
      septic.entrance:GetSprite():Play("Spawn (Locked)", true)

      local spritesheet
      local h = room:GetGridHeight()
      local w = room:GetGridWidth()
      if level:GetStageType() == StageType.STAGETYPE_AFTERBIRTH then
        spritesheet = "gfx/grid/rocks_drownedcaves_noshade.png"
      elseif level:GetStageType() == StageType.STAGETYPE_WOTL then
        spritesheet = "gfx/grid/rocks_catacombs_noshade.png"
      else
        spritesheet = "gfx/grid/rocks_caves_noshade.png"
      end
      for i=1,h-math.ceil(Genesis.moddata.septicEntranceGrid/w) do --that second number is the grid y of the rock
        for k=-1,1 do
          local gr = room:GetGridEntity(Genesis.moddata.septicEntranceGrid+w*i+k)
          if gr and gr:ToRock() and gr.State == 1 then
            local e = Isaac.Spawn(EntityType.ENTITY_EFFECT, 8, 0, gr.Position,Genesis.VEC_ZERO, nil)
            e:GetSprite():Load("gfx/grid/Rocks.anm2", false)
            e:GetSprite():ReplaceSpritesheet(0, spritesheet)
            local rockanimations = {"normal", "black", "tinted", "rubble", "alt", "rubble_alt", "bombrock", "big", "superspecial", "ss_broken"}
            for rock=1, #rockanimations do
              if gr.Sprite:IsPlaying(rockanimations[rock]) or gr.Sprite:IsFinished(rockanimations[rock]) then
                e:GetSprite():SetFrame(rockanimations[rock], gr.Sprite:GetFrame())
              end
            end
            e:ToEffect():AddEntityFlags(EntityFlag.FLAG_NO_REMOVE_ON_TEX_RENDER)
            e:GetSprite():LoadGraphics()
          end
        end
      end
    end
  end
end

function septic:use_dadskey()
	if septic.entrance then
		local sprite = septic.entrance:GetSprite()
		if not Genesis.moddata.unlockedSepticEntrance then
			  if sprite:IsFinished("Spawn (Locked)") and not sprite:IsPlaying("Unlock") then
				Genesis.moddata.unlockedSepticEntrance = true
				sprite:Play("Unlock", true)
				SFXManager():Play(SoundEffect.SOUND_UNLOCK00, 1, 0, false, 1)
			  end
		end
	end
end

---spawn 3 items in septic treasure rooms, with 2 being genesis items---
--(pickup update since pickup init fails to get item position for some reason)--
function septic:onSpawnPickup(pickup)
	if Genesis.IsSeptic() and pickup.Variant == PickupVariant.PICKUP_COLLECTIBLE and pickup.FrameCount > 2 and pickup.FrameCount < 5 then
		local room = Game():GetRoom()
		if room:GetType() == RoomType.ROOM_TREASURE and room:IsFirstVisit() then
			PedestalsInTreasureRoom = PedestalsInTreasureRoom + 1
			if PedestalsInTreasureRoom == 1 then
				Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_COLLECTIBLE,SepticPool[math.random(1,#SepticPool)],Vector(pickup.Position.X+50,pickup.Position.Y),Vector(0,0),nil)
				Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_COLLECTIBLE,SepticPool[math.random(1,#SepticPool)],Vector(pickup.Position.X-50,pickup.Position.Y),Vector(0,0),nil)
			end
		end
	end
end
septic:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, septic.onSpawnPickup);
septic:AddCallback(ModCallbacks.MC_POST_UPDATE, septic.Update)
septic:AddCallback(ModCallbacks.MC_USE_ITEM, septic.use_dadskey, 175);

function Genesis.IsSeptic()
  --local stage = Game():GetLevel():GetStage()
  --return StageSystem.currentstage == StageSystem.GetStageIdByName("new_Septic") and (stage == LevelStage.STAGE2_1 or stage == LevelStage.STAGE2_2) and Game():GetLevel():GetStageType() == StageType.STAGETYPE_WOTL
  return Genesis.SepticStage:IsStage()
  --return false
end

function Genesis.IsSeptic2()
    return Genesis.IsSeptic() and StageAPI.GetCurrentStage().IsSecondStage
end

--------------
--Ab++ stuff--
--------------

--[[function EntityPlayer.inst:HasCollectible(id)
  if id < 0 then
    return false
  else
    return Genesis.vanillaHasCollectible(self, id)
  end
end


--It's a mod (included with stagesapi) that allows you to edit vanilla things

Genesis.vanillaHasCollectible = EntityPlayer.inst.HasCollectible

this will fix many possible bugs, trust me on this
basically the game always returns true if you call HasCollectible(-1)
and -1 is waht you get with GetItemIdByName if there is no item with that name
so, asssigning stuff to an item that isnt loaded will make the game think you always have it
if things get weird with HasCollectible, comment this code out]]--




---------------
--SHOPKEEPERS--
---------------

local shopkeepers = RegisterMod('Genesis Shopkeepers', 1)

function shopkeepers:NewRoom()
  local room = Game():GetRoom()
  local entities = Genesis.getRoomEntities()
  for i,e in ipairs(entities) do
    if 10 > (e.InitSeed * room:GetDecorationSeed()) % 100 and e.Type == EntityType.ENTITY_SHOPKEEPER then
      local spr = e:GetSprite();

      if e.Variant == 0 then
        spr:ReplaceSpritesheet(0, "gfx/effects/genesis shopkeepers.png");
        e:GetData().IsGenesis = true
      elseif e.Variant == 3 then
        spr:ReplaceSpritesheet(0, "gfx/effects/genesis special shopkeepers.png");
        e:GetData().IsGenesisSpecial = true
      end
      spr:LoadGraphics();
    end
  end
end

shopkeepers:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, shopkeepers.NewRoom);

function shopkeepers:OnUpdate()
  local entities = Genesis.getRoomEntities()
  for i, e in ipairs(entities) do
    if e:GetData().IsGenesis == true then
      if e:IsDead() then
--                Isaac.DebugString("It died, genesis normal shopkeeper")
        if math.random(5) == 2 then
            for i=1, math.random(3) + 4, 1 do
                Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, CoinSubType.COIN_PENNY , e.Position, Vector(math.random(-10,10), math.random(-10,10)), nil)
            end
        end
      end
    elseif e:GetData().IsGenesisSpecial == true then
      if e:IsDead() then
--                Isaac.DebugString("It died, genesis special shopkeeper")
        if math.random(5) == 2 then
          for i=1, math.random(3) + 2, 1 do
              Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, CoinSubType.COIN_NICKEL , e.Position, Vector(math.random(-10,10), math.random(-10,10)), nil)
          end
        end
      end
    end
  end
end

shopkeepers:AddCallback(ModCallbacks.MC_POST_UPDATE, shopkeepers.OnUpdate);

--#############################
---REQUIRE OTHER STUFF
--#############################

local requires = {
"main_enemies",
"main_characters",
"main_items",
"main_items2",
"main_items3",
"main_cards",
"main_curses",
"main_pills",
"main_challenges",
"main_satanic",

"main_septicboss"
}

for _, req in ipairs(requires) do
    status, res = pcall(require, req)
    if not status and not res.hack then
        Isaac.ConsoleOutput(res .. '\n')
        Isaac.DebugString(res)
    else
        Genesis.loaderCount = Genesis.loaderCount + 1
    end
end

function genesis:Render()
    if Genesis.loaderCount < #requires then
      Isaac.RenderText("WARNING: Genesis not loaded correctly!", 60, 50, 255, 50, 50, 255)
      Isaac.RenderText("Did you disable and enable it on the mod menu or use luamod?", 60, 80, 255, 50, 50, 255)
    end
  end

  genesis:AddCallback(ModCallbacks.MC_POST_RENDER, genesis.Render)

--#############################
---GENERIC FUNCTIONS AND TABLES
--#############################

do

function VectorFromDirection(dir)
  if dir == Direction.LEFT then
    return Vector(-1,0)
  elseif dir == Direction.RIGHT then
    return Vector(1,0)
  elseif dir == Direction.UP then
    return Vector(-1,0)
  elseif dir == Direction.DOWN then
    return Vector(1,0)
  else
    return Vector(0,0)
  end
end

function Bit(p)
    return 2 ^ (p - 1)
end

function HasBit(x, p)
	return (x & p) ~= 0
end

function SetBit(x, p)
	return x | p
end

function ClearBit(x, p)
	return (~p) & x
end

function GetDirectionFromVelocity(velocity)
	if math.abs(velocity.X) < math.abs(velocity.Y) then
		if velocity.Y >= 0 then
			return Direction.DOWN
		else
			return Direction.UP
		end
	else
		if velocity.X >= 0 then
			return Direction.RIGHT
		else
			return Direction.LEFT
		end
	end
end

function GetOrbitPosition(entity, angle, distance)
  return Vector(entity.Position.X + (distance * math.cos(angle)),entity.Position.Y + (distance * math.sin(angle)))
end

function GetConfigItem(id)
	local player = Isaac.GetPlayer(0)

	if id > 0 then
		player:GetEffects():AddCollectibleEffect(id, true)
		local effect = player:GetEffects():GetCollectibleEffect(id)
		player:GetEffects():RemoveCollectibleEffect(id)
		return effect.Item
	end
end

function getScreenCenterPosition()
local room = Game():GetRoom()
local centerOffset = (room:GetCenterPos()) - room:GetTopLeftPos()
local pos = room:GetCenterPos()
if centerOffset.X > 260 then
  pos.X = pos.X - 260
end
if centerOffset.Y > 140 then
	pos.Y = pos.Y - 140
end
return Isaac.WorldToRenderPosition(pos, false)
end

DirectionName = {
  [Direction.LEFT] = "Left",
  [Direction.RIGHT] = "Right",
  [Direction.UP] = "Up",
  [Direction.DOWN] = "Down"
}

DirectionVector = {
  [Direction.LEFT] = Vector(-1, 0),
  [Direction.RIGHT] = Vector(1, 0),
  [Direction.UP] = Vector(0, -1),
  [Direction.DOWN] = Vector(0, 1)
}
end

if Genesis.loaderCount == #requires then
  Isaac.DebugString("Genesis+: Completed Loading!")
else
  Isaac.DebugString("Genesis+: ERROR! Couldn't load! (Remember that luamod doesn't work for mods with multiple lua files!)")
end