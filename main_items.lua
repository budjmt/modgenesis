---------------
--ROPE LADDER--
---------------

local RopeLadder = RegisterMod("Rope Ladder", 1);
RopeLadder.ItemId = GENESIS_ITEMS.ACTIVE.ROPE_LADDER
RopeLadder.StatAlter = {
	1,
	-0.2,
	-0.5,
	2.5,
	-0.2,
	-1
}

function RopeLadder:OnItemUse(item, rng)
	local player = Isaac.GetPlayer(0)
	local game = Game()
    local level = game:GetLevel()
    local stage = level:GetAbsoluteStage()
    player:AnimateCollectible(item, "UseItem", "Idle")
    player:RemoveCollectible(item)
    if stage > 1 then
        level:SetStage(stage - 1, rng:RandomInt(4))
        Game():StartStageTransition(true, 1)
		local RandomInt = math.random(6)
		Genesis.moddata.statsup[Genesis.STATSNAMES[RandomInt]] = Genesis.moddata.statsup[Genesis.STATSNAMES[RandomInt]] + RopeLadder.StatAlter[RandomInt]
        player:AddCacheFlags(CacheFlag.CACHE_ALL)
        player:EvaluateItems()
    else
        Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.MOM_FOOT_STOMP , 2, player.Position, Vector(0, 0), player)
        local room = game:GetRoom()
        for i=0, DoorSlot.NUM_DOOR_SLOTS - 1 do
            local door = room:GetDoor(i)
            if door ~= nil then
                door:Close()
            end
        end
    end
end

RopeLadder:AddCallback(ModCallbacks.MC_USE_ITEM, RopeLadder.OnItemUse, RopeLadder.ItemId)

--------------
--GLOBIN GOO--
--------------

--I will rewrite it later, when I have more time, this code needs a remake(GoldFeniks)--

local goblinGoo = RegisterMod("Globin Goo",1)

function goblinGoo:Item()
	local game = Game()
	local room = game:GetRoom()
	local level = game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local gooexists = false
	goblinGoo.globingooactive = false
	goblinGoo.pos = player.Position
	if level:GetAbsoluteStage() == 1 and level.EnterDoor == -1 and player.FrameCount == 1 then
		goblinGoo.playerisgoo = false
		goblinGoo.noglobingoo = true
	end
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.GLOBIN_GOO) and goblinGoo.noglobingoo == true then
		Isaac.DebugString("HASITEM")
		goblinGoo.globingooactive = true
	end
	local e = Genesis.getRoomEntities()
	if player:IsDead() and goblinGoo.globingooactive == true and player:HasCollectible(GENESIS_ITEMS.PASSIVE.GLOBIN_GOO) then
		goblinGoo.globingooactive = false
		Isaac.DebugString("DED")
		player:Revive()
		Isaac.Spawn(GENESIS_ENTITIES.GOO, 0, 0, player.Position, Vector(0,0), player)
		player.FireDelay = 999
		player:SetMinDamageCooldown(60)
	end
end

function goblinGoo:Entity(npc)
	local game = Game()
	local room = game:GetRoom()
	local level = game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local sprite = npc:GetSprite()
	local dir = player:GetMovementDirection()
	if npc.FrameCount > 30 then
		npc.Position = player.Position
	else
		player.Position = npc.Position
	end
	player.Visible = false
	if npc.FrameCount % 130 == 0 then
		sprite:Play("Transform",false)
	end
	if npc.FrameCount % 150 == 0 then
		goblinGoo.globingooactive = true
		npc:Remove()
		player.Visible = true
	end
	if player:IsDead() then
		npc:Remove()
		player.Visible = true
	end
	if goblinGoo.globingooactive == false then
		player:AddSlowing(EntityRef(player), 1, 0.3, Color(0,0,0,0,0,0,0))
	end
end

goblinGoo:AddCallback(ModCallbacks.MC_POST_UPDATE, goblinGoo.Item)
goblinGoo:AddCallback(ModCallbacks.MC_NPC_UPDATE, goblinGoo.Entity, GENESIS_ENTITIES.GOO)

--------------
--WHITE MASK--
--------------

local WhiteMask = RegisterMod("White Mask", 1)
WhiteMask.ItemId = GENESIS_ITEMS.ACTIVE.WHITE_MASK

WhiteMask.Effects = {
	{
		Anim = CollectibleType.COLLECTIBLE_BOBS_HEAD,
		NullEffects = { NullItemID.ID_BOB },
		CollectibleEffects = { CollectibleType.COLLECTIBLE_BALL_OF_TAR },
	},
	{
		Anim = CollectibleType.COLLECTIBLE_BBF,
		NullEffects = { NullItemID.ID_LORD_OF_THE_FLIES },
		CollectibleEffects = { CollectibleType.COLLECTIBLE_TRANSCENDENCE, CollectibleType.COLLECTIBLE_SKATOLE }
	},
	{
		Anim = CollectibleType.COLLECTIBLE_HARLEQUIN_BABY,
		NullEffects = { NullItemID.ID_BABY },
		CollectibleEffects = { CollectibleType.COLLECTIBLE_THE_WIZ }
	},
	{
		Anim = CollectibleType.COLLECTIBLE_MAGIC_MUSHROOM,
		NullEffects = { NullItemID.ID_MUSHROOM },
		CollectibleEffects = {},
		Special = function(player) player:AddEternalHearts(1) end
	},
	{
		Anim = CollectibleType.COLLECTIBLE_CEREMONIAL_ROBES,
		NullEffects = { NullItemID.ID_EVIL_ANGEL },
		CollectibleEffects = { CollectibleType.COLLECTIBLE_TRANSCENDENCE },
		Special = function(player) player:AddBlackHearts(2) end
	},
	{
		Anim = CollectibleType.COLLECTIBLE_SWORN_PROTECTOR,
		NullEffects = { NullItemID.ID_ANGEL },
		CollectibleEffects = { CollectibleType.COLLECTIBLE_TRANSCENDENCE },
		Special = function(player) player:AddSoulHearts(2) end
	},
	{
		Anim = CollectibleType.COLLECTIBLE_POOP,
		NullEffects = { NullItemID.ID_POOP },
		CollectibleEffects = {},
		Special = function(player) player:UseActiveItem(CollectibleType.COLLECTIBLE_POOP, false, false, false, false); player:GetEffects():AddTrinketEffect(TrinketType.TRINKET_PETRIFIED_POOP, false) end
	},
	{
		Anim = CollectibleType.COLLECTIBLE_VIRUS,
		NullEffects = { NullItemID.ID_DRUGS },
		CollectibleEffects = {},
		Special = function(player) player:GetEffects():AddTrinketEffect(TrinketType.TRINKET_CURVED_HORN, false); player:AddPill(PillColor.PILL_WHITE_YELLOW); end
	},
	{
		Anim = CollectibleType.COLLECTIBLE_MOMS_PURSE,
		NullEffects = { NullItemID.ID_MOM },
		CollectibleEffects = { CollectibleType.COLLECTIBLE_SACRIFICIAL_DAGGER }
	},
	{
		Anim = CollectibleType.COLLECTIBLE_TELEPATHY_BOOK,
		NullEffects = { NullItemID.ID_BOOKWORM },
		CollectibleEffects = { CollectibleType.COLLECTIBLE_20_20 }
	},
	{
		NullEffects = { NullItemID.ID_ADULTHOOD },
		CollectibleEffects = {},
		Special = function(player) player:UseCard(Card.CARD_HIGH_PRIESTESS) end
	},
	{
		Anim = CollectibleType.COLLECTIBLE_SPIDER_BITE,
		NullEffects = { NullItemID.ID_SPIDERBABY },
		CollectibleEffects = { CollectibleType.COLLECTIBLE_SPIDER_MOD }
	},
	{
		NullEffects = {},
		CollectibleEffects = { CollectibleType.COLLECTIBLE_BUM_FRIEND, CollectibleType.COLLECTIBLE_DARK_BUM, CollectibleType.COLLECTIBLE_KEY_BUM }
	}
}

function WhiteMask:OnItemUse(item, rng)
	local player = Isaac.GetPlayer(0)
	local random = rng:RandomInt(#(self.Effects)) + 1
	Isaac.DebugString(tostring(random))
	local effects = self.Effects[random]
	if effects.Anim ~= nil then
		player:AnimateCollectible(effects.Anim, "UseItem", "Idle")
	end
	for i=1, #(effects.NullEffects) do
		player:GetEffects():AddNullEffect(effects.NullEffects[i], true)
	end
	if effects ~= self.Effects[13] then
		for i=1, #(effects.CollectibleEffects) do
			player:GetEffects():AddCollectibleEffect(effects.CollectibleEffects[i], false)
		end
	else
		Isaac.Spawn(3, 102, 0, player.Position, Vector(0,0), player)
	end
	if effects.Special ~= nil then
		effects.Special(player)
	end
end

WhiteMask:AddCallback(ModCallbacks.MC_USE_ITEM, WhiteMask.OnItemUse, WhiteMask.ItemId)

----------------
--BOWLING BALL--
----------------

--[[ I don't know who rewrote Bowling Ball but the code is broken
local BowlingBall = RegisterMod("Bowling Ball", 1)
BowlingBall.ItemId = GENESIS_ITEMS.ACTIVE.BOWLING_BALL
BowlingBall.EntityType = GENESIS_ENTITIES.BOWLING_BALL

BowlingBall.CanThrowBall = false
BowlingBall.BaseSpeed = 10
BowlingBall.DamageDelay = 2

function BowlingBall:OnPeffectUpdate(player)
	if player:HasCollectible(self.ItemId) then
		if self.CanThrowBall then
			local direction = player:GetFireDirection()
			if direction ~= Direction.NO_DIRECTION then
				self.CanThrowBall = false
				local velocity = player:GetShootingJoystick() * self.BaseSpeed
				local ball = Isaac.Spawn(self.EntityType, 0, 0, player.Position + velocity * 5, velocity, player)
				ball:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
				ball.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
				ball.GridCollisionClass = GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER
				ball.CollisionDamage = player.Damage
				local entities = Genesis.getRoomEntities()
				local data = ball:GetData()
				data.Count = 0
				for i=1, #entities do
					if entities[i]:IsVulnerableEnemy() then
						entities[i]:GetData().Counts = true
						data.Count = data.Count + 1
					end
				end
			end
		end
	else
		self.CanThrowBall = false
	end
end

function BowlingBall:OnItemUse(...)
	self.CanThrowBall = not self.CanThrowBall
	return self.CanThrowBall
end

function BowlingBall:OnNPCUpdate(ball)
	local player = Isaac.GetPlayer(0)
	if ball.Velocity:Length() <= 1 then
		Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.IMPACT, 0, ball.Position, Vector(0,0), player)
		if ball:GetData().Count == 0 and player:HasCollectible(self.ItemId) then
			player:FullCharge()
		else
			self.Count = 0
		end
		ball:Remove()
	else
		local animName = "Roll"..DirectionName[GetDirectionFromVelocity(ball.Velocity)]
		local sprite = ball:GetSprite()
		if not sprite:IsPlaying(animName) then
			sprite:Play(animName, true)
		end
		local entities = Genesis.getRoomEntities()
		for i=1, #entities do
			if entities[i]:IsVulnerableEnemy() and (entities[i].Position - ball.Position):Length() <= ball.Size + entities[i].Size then
				entities[i]:TakeDamage(ball.CollisionDamage, 0, EntityRef(player), self.DamageDelay)
				local data = entities[i]:GetData()
				if data.Counts then
					data.Counts = nil
					self.Count = self.Count - 1
				end
			end
		end
	end
end

BowlingBall:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, BowlingBall.OnPeffectUpdate)
BowlingBall:AddCallback(ModCallbacks.MC_USE_ITEM, BowlingBall.OnItemUse, BowlingBall.ItemId)
BowlingBall:AddCallback(ModCallbacks.MC_NPC_UPDATE, BowlingBall.OnNPCUpdate, BowlingBall.EntityType)
--]]

local boBall = RegisterMod("Bowling Ball",1)
boBall.canthrowball = false
boBall.oldx = 0
boBall.oldy = 0
boBall.enemies = {""}
boBall.ehit = {""}

function boBall:bowlingballUpdate()
	local game = Game()
	local room = game:GetRoom()
	local level = game:GetLevel()
	local player = Isaac.GetPlayer(0)
	if boBall.canthrowball == true and not player:HasCollectible(GENESIS_ITEMS.ACTIVE.BOWLING_BALL) then
		boBall.canthrowball = false
	end
	if boBall.canthrowball == true then
		if player:GetFireDirection() == Direction.LEFT then
			boBall.vel = Vector(-10,0)
		elseif player:GetFireDirection() == Direction.UP then
			boBall.vel = Vector(0,-10)
		elseif player:GetFireDirection() == Direction.RIGHT then
			boBall.vel = Vector(10,0)
		elseif player:GetFireDirection() == Direction.DOWN then
			boBall.vel = Vector(0,10)
		end
		if player:GetFireDirection() ~= Direction.NO_DIRECTION then
			boBall.canthrowball = false
			boBall.dir = player:GetFireDirection()
			boBall.ehit = {""}
			boBall.enemies = {""}
			e = Genesis.getRoomEntities()
			for i=1, #e do
				if e[i]:IsVulnerableEnemy() then
					table.insert(boBall.enemies, e[i])
				end
			end
			local npc = Isaac.Spawn(GENESIS_ENTITIES.BOWLING_BALL, 0, 0, boBall.vel:__mul(5):__add(player.Position), boBall.vel, player)
			npc:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
		end
	end
end

function boBall:use_bowling()
	boBall.canthrowball = true
	return true
end

function boBall:bowlingball(npc)
	local game = Game()
	local room = game:GetRoom()
	local level = game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local sprite = npc:GetSprite()
	npc.Velocity = boBall.vel
	if npc.FrameCount % 2 == 0 then
		if boBall.dir == Direction.LEFT then
			sprite:Play("RollLeft")
		elseif boBall.dir == Direction.UP then
			sprite:Play("RollUp")
		elseif boBall.dir == Direction.RIGHT then
			sprite:Play("RollRight")
		elseif boBall.dir == Direction.DOWN then
			sprite:Play("RollDown")
		end
		if npc.Position.X == boBall.oldx and npc.Position.Y == boBall.oldy then
			Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.IMPACT, 0, npc.Position, Vector(0,0), player)
			if #boBall.ehit == #boBall.enemies then
				player:SetActiveCharge(3)
			end
			npc:Remove()
		end
		boBall.oldx = npc.Position.X
		boBall.oldy = npc.Position.Y
		local e = Genesis.getRoomEntities()
		for i=1, #e do
			if e[i]:IsVulnerableEnemy() then
				if e[i].Position:__sub(npc.Position):Length() <= 50 then
					e[i]:TakeDamage(player.Damage, EntityFlag.FLAG_POISON, EntityRef(player), 0)
					local nohitentry = true
					for q=1, #boBall.ehit do
						if e[i].Index == boBall.ehit[q] then
							nohitentry = false
						end
					end
					if nohitentry == true then
						table.insert(boBall.ehit, e[i].Index)
					end
				end
			end
		end
	end
end

boBall:AddCallback(ModCallbacks.MC_POST_UPDATE, boBall.bowlingballUpdate)
boBall:AddCallback(ModCallbacks.MC_USE_ITEM, boBall.use_bowling, GENESIS_ITEMS.ACTIVE.BOWLING_BALL)
boBall:AddCallback(ModCallbacks.MC_NPC_UPDATE, boBall.bowlingball, GENESIS_ENTITIES.BOWLING_BALL)

----------------
--SCHIZOFRENIA--
----------------

local Schizofrenia = RegisterMod("Schizofrenia", 1)

Schizofrenia.ItemId = GENESIS_ITEMS.FAMILIAR.SCHIZOFRENIA
Schizofrenia.EntityTypeId = GENESIS_ENTITIES.SCHIZOFRENIA
Schizofrenia.EntityVariantId = GENESIS_ENTITIES.VARIANT.SCHIZOFRENIA

Schizofrenia.EntitiesSpawned = 0
Schizofrenia.LastRoomIndex = nil
Schizofrenia.CurrentStage = nil
Schizofrenia.SpeedBoost = 0.2
Schizofrenia.DamageBoost = 2
Schizofrenia.TearsBoost = -1
Schizofrenia.CharmedTearChance = 15

Schizofrenia.State = {
	NULL = 0,
	HAPPY = 1,
	SAD = 2,
	ANGRY = 3,
	CHARMED = 4,
	ENEMY = 5
}

Schizofrenia.EvaluateIncrease = nil

Schizofrenia.ImagePath = {
	"gfx/familiar/schizofrenia_happy.png",
	"gfx/familiar/schizofrenia_sad.png",
	"gfx/familiar/schizofrenia_angry.png",
	"gfx/familiar/schizofrenia_charm.png",
	"gfx/familiar/schizofrenia_happy.png"
}

function Schizofrenia:OnPlayerInit(player)
	self.EntitiesSpawned = 0
	self.LastRoomIndex = nil
	self.EvaluateIncrease = nil
	self.CurrentState = nil
end

function Schizofrenia:Spawn(from, to, parent)
	if to < from then
		local entities = Genesis.getRoomEntities()
		local familiars = {}
		for i=1, #entities do
			if entities[i].Type == self.EntityTypeId and entities[i].Variant == self.EntityVariantId then
				table.insert(familiars, entities[i])
			end
		end
		for i=to, from - 1 do
			self.EntitiesSpawned = self.EntitiesSpawned - 1
			familiars[1]:Die()
			table.remove(familiars, 1)
		end
	else
		for i=from, (to - 1) do
			Isaac.Spawn(self.EntityTypeId, self.EntityVariantId, 0, parent.Position, Vector(0, 0), parent)
		end
	end
end

--Seriously. This part needs to die--
function Schizofrenia:OnEvaluateCache(player, cacheFlag)
	if cacheFlag == CacheFlag.CACHE_FAMILIARS then
		self:Spawn(self.EntitiesSpawned, player:GetCollectibleNum(self.ItemId), player)
	end
	if player:HasCollectible(self.ItemId) then
		if cacheFlag == CacheFlag.CACHE_SPEED and self.EvaluateIncrease == self.State.HAPPY then
			player.MoveSpeed = player.MoveSpeed + self.SpeedBoost * self.EntitiesSpawned
			self.EvaluateIncrease = nil
		elseif cacheFlag == CacheFlag.CACHE_DAMAGE and self.EvaluateIncrease == self.State.ANGRY then
			player.Damage = player.Damage + self.DamageBoost * self.EntitiesSpawned
			self.EvaluateIncrease = nil
		elseif cacheFlag == CacheFlag.CACHE_FIREDELAY and self.EvaluateIncrease == self.State.SAD then
			player.MaxFireDelay = player.MaxFireDelay + self.TearsBoost * self.EntitiesSpawned
			self.EvaluateIncrease = nil
		end
	end
end

function Schizofrenia:OnGameUpdate()
	local level = Game():GetLevel()
	local player = Isaac.GetPlayer(0)
	local rng = player:GetCollectibleRNG(self.ItemId)
	if level:GetCurrentRoomIndex() ~= self.LastRoomIndex then
		self.LastRoomIndex = nil
	end
	if level:GetCurrentRoomIndex() ~= self.LastRoomIndex or level:GetAbsoluteStage() ~= self.CurrentStage then
		self.LastRoomIndex = level:GetCurrentRoomIndex()
		self.CurrentStage = level:GetAbsoluteStage()
		if self.CurrentState == self.State.ENEMY then
			self.EntitiesSpawned = 0
			player:RespawnFamiliars()
		end
		self.CurrentState = player:GetCollectibleRNG(self.ItemId):RandomInt(5) + 1
		self.EvaluateIncrease = self.CurrentState
		player:GetEffects():AddCollectibleEffect(self.ItemId, false)
	end
	if self.CurrentState == self.State.CHARMED and player:HasCollectible(self.ItemId) then
		local entities = Genesis.getRoomEntities()
		for i=1, #entities do
			if entities[i].Type == EntityType.ENTITY_TEAR and entities[i].FrameCount == 1 and rng:RandomInt(100) <= self.CharmedTearChance * self.EntitiesSpawned then
				local tear = entities[i]:ToTear()
				tear.TearFlags = SetBit(tear.TearFlags, 1<<13)
				tear.Color = Color(50, 0, 50, 1, 1, 0, 0)
			end
		end
	end
end

function Schizofrenia:OnFamiliarInit(familiar)
	familiar:GetSprite():Play("FloatDown", true)
	familiar.State = self.State.HAPPY
	self.EntitiesSpawned = self.EntitiesSpawned + 1
end

function Schizofrenia:OnFamiliarUpdate(familiar)
	familiar:FollowParent()
	local Player = Isaac.GetPlayer(0)
	local sprite = familiar:GetSprite()
	if familiar.State ~= self.CurrentState then
		Isaac.DebugString(tostring(self.CurrentState))
		familiar.State = self.CurrentState
		sprite:ReplaceSpritesheet(0, self.ImagePath[self.CurrentState])
		sprite:LoadGraphics()
		sprite:Play("Change", true)
		if familiar.State == self.State.ENEMY then
			local entities = Genesis.getRoomEntities()
			local enemies = {}
			for i=1, #entities do
				if entities[i]:IsVulnerableEnemy() and not entities[i]:IsBoss() then
					table.insert(enemies, entities[i])
				end
			end
			if #enemies > 0 then
				local enemy = enemies[familiar:GetDropRNG():RandomInt(#enemies) + 1]
				Isaac.Spawn(enemy.Type, enemy.Variant, enemy.SubType, enemy.Position, Vector(0, 0), nil)
			else
				Isaac.Spawn(EntityType.ENTITY_FLY, 0, 0, familiar.Position, Vector(0, 0), nil)
			end
			familiar:Remove()
		end
	end
	if sprite:IsFinished("Change") then
		sprite:Play("FloatDown", true)
	end
end

Schizofrenia:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, Schizofrenia.OnEvaluateCache)
Schizofrenia:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, Schizofrenia.OnFamiliarInit, Schizofrenia.EntityVariantId)
Schizofrenia:AddCallback(ModCallbacks.MC_POST_UPDATE, Schizofrenia.OnGameUpdate)
Schizofrenia:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, Schizofrenia.OnFamiliarUpdate, Schizofrenia.EntityVariantId)
Schizofrenia:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, Schizofrenia.OnPlayerInit)

----------
--Ponger--
----------

local PongerMod = RegisterMod("Ponger mod",1)
PongerMod.r = 0
PongerMod.xy = 0
PongerMod.tearDir = "left"
PongerMod.CostumeId = Isaac.GetCostumeIdByPath("gfx/characters/ponger.anm2")
PongerMod.HasCostume = false

function PongerMod:Update()
    math.randomseed(PongerMod.r + 1)
    PongerMod.r = PongerMod.r + 1
    local Game = Game()
    local Room = Game:GetRoom()
    local Level = Game:GetLevel()
    local Player = Isaac.GetPlayer(0)
    local e = Genesis.getRoomEntities()
    if Player:HasCollectible(GENESIS_ITEMS.PASSIVE.PONGER) then
        Player:GetEffects():AddCollectibleEffect(306,false)
		if SFXManager():IsPlaying(SoundEffect.SOUND_TEARS_FIRE) then
			SFXManager():Stop(SoundEffect.SOUND_TEARS_FIRE)
			SFXManager():Play(268, 0.25, 0, false, 0.8)
		end
        for i=1, #e do
            if e[i].Type == EntityType.ENTITY_TEAR and e[i].Parent and e[i].Parent.Type == EntityType.ENTITY_PLAYER then
				local sprite = e[i]:GetSprite()
				sprite:Load("gfx/pongertears.anm2", true)
				--Sould be a better way to write it--
				if Player.Damage <= 0.5 then
					sprite:Play("pongertear1", true)
				elseif Player.Damage <= 1 then
					sprite:Play("pongertear2", true)
				elseif Player.Damage <= 1.5 then
					sprite:Play("pongertear3", true)
				elseif Player.Damage <= 2 then
					sprite:Play("pongertear4", true)
				elseif Player.Damage <= 3 then
					sprite:Play("pongertear5", true)
				elseif Player.Damage <= 4.5 then
					sprite:Play("pongertear6", true)
				elseif Player.Damage <= 6 then
					sprite:Play("pongertear7", true)
				elseif Player.Damage <= 7.5 then
					sprite:Play("pongertear8", true)
				elseif Player.Damage <= 9 then
					sprite:Play("pongertear9", true)
				elseif Player.Damage <= 10.5 then
					sprite:Play("pongertear10", true)
				elseif Player.Damage <= 15 then
					sprite:Play("pongertear11", true)
				elseif Player.Damage < 20 then
					sprite:Play("pongertear12", true)
				elseif Player.Damage >= 20 then
					sprite:Play("pongertear13", true)
				end
	            if e[i].FrameCount == 1 and e[i].Velocity.X ~= e[i].Velocity.Y and e[i].Velocity.X ~= -e[i].Velocity.Y then
	            	--Make a function to get rid of copypasted code, also please use spaces before and after operators--
		            if Player:GetHeadDirection() == Direction.LEFT then
		                PongerMod.xy = e[i].Velocity.X * 1.5 + e[i].Velocity.Y
			            if PongerMod.tearDir == "left" then
		                    e[i].Velocity = Vector(PongerMod.xy/2,PongerMod.xy/2)
			            elseif PongerMod.tearDir == "right" then
							e[i].Velocity = Vector(PongerMod.xy/2,-PongerMod.xy/2)
						end
					elseif Player:GetHeadDirection() == Direction.RIGHT then
						PongerMod.xy = e[i].Velocity.X * 1.5 - e[i].Velocity.Y
						if PongerMod.tearDir == "left" then
							e[i].Velocity = Vector(PongerMod.xy/2,PongerMod.xy/2)
						elseif PongerMod.tearDir == "right" then
							e[i].Velocity = Vector(PongerMod.xy/2,-PongerMod.xy/2)
						end
					elseif Player:GetHeadDirection() == Direction.UP then
						PongerMod.xy = e[i].Velocity.X - e[i].Velocity.Y * 1.5
						if PongerMod.tearDir == "left" then
							e[i].Velocity = Vector(PongerMod.xy/2,-PongerMod.xy/2)
						elseif PongerMod.tearDir == "right" then
							e[i].Velocity = Vector(-PongerMod.xy/2,-PongerMod.xy/2)
						end
					elseif Player:GetHeadDirection() == Direction.DOWN then
						PongerMod.xy = e[i].Velocity.X + e[i].Velocity.Y * 1.5
						if PongerMod.tearDir == "left" then
							e[i].Velocity = Vector(-PongerMod.xy/2,PongerMod.xy/2)
						elseif PongerMod.tearDir == "right" then
							e[i].Velocity = Vector(PongerMod.xy/2,PongerMod.xy/2)
						end
					end
					if PongerMod.tearDir == "left" then
					PongerMod.tearDir = "right"
					elseif PongerMod.tearDir == "right" then
					PongerMod.tearDir = "left"
					end
				end
			end
		end
		for i=1, #e do
			if e[i].Parent and e[i].Parent.Type == EntityType.ENTITY_PLAYER then
				if e[i].Type == EntityType.ENTITY_LASER then
					sprite = e[i]:GetSprite()
					sprite.Color = Color(0,0,0,1,0,255,0)
				end
				if e[i].Type == EntityType.ENTITY_EFFECT then
					if e[i].Variant == EffectVariant.TEAR_POOF_A or e[i].Variant == EffectVariant.TEAR_POOF_B or e[i].Variant == EffectVariant.TEAR_POOF_SMALL or e[i].Variant == EffectVariant.TEAR_POOF_VERYSMALL or e[i].Variant == EffectVariant.BRIMSTONE_SWIRL or e[i].Variant == EffectVariant.BRIMSTONE_SWIRL or e[i].Variant == EffectVariant.LASER_IMPACT then
						sprite = e[i]:GetSprite()
						sprite.Color = Color(0,0,0,1,0,255,0)
					end
				end
				if e[i].Type == EntityType.ENTITY_TEAR then
					if e[i].Variant == TearVariant.BLOOD then
						sprite = e[i]:GetSprite()
						sprite.Color = Color(0,0,0,1,255,0,0)
					end
				end
				if e[i].Type == EntityType.ENTITY_BOMBDROP and e[i].FrameCount == 1 then
					local sprite = e[i]:GetSprite()
					if e[i].Variant == BombVariant.BOMB_NORMAL then
						sprite:Load("gfx/Pongbomb.anm2", true)
						sprite:Play("Normal", true)
					elseif e[i].Variant == BombVariant.BOMB_BIG then
						sprite:Load("gfx/Pongbomb.anm2", true)
						sprite:Play("Big", true)
					elseif e[i].Variant == BombVariant.BOMB_DECOY then
						sprite:Load("gfx/Pongbomb.anm2", true)
						sprite:Play("Decoy", true)
					elseif e[i].Variant == BombVariant.BOMB_POISON then
						sprite:Load("gfx/Pongbomb.anm2", true)
						sprite:Play("Poison", true)
					elseif e[i].Variant == BombVariant.BOMB_POISON_BIG then
						sprite:Load("gfx/Pongbomb.anm2", true)
						sprite:Play("PoisonBig", true)
					elseif e[i].Variant == BombVariant.BOMB_SAD then
						sprite:Load("gfx/Pongbomb.anm2", true)
						sprite:Play("Sad", true)
					elseif e[i].Variant == BombVariant.BOMB_HOT then
						sprite:Load("gfx/Pongbomb.anm2", true)
						sprite:Play("Hot", true)
					elseif e[i].Variant == BombVariant.BOMB_BUTT then
						sprite:Load("gfx/Pongbomb.anm2", true)
						sprite:Play("Butt", true)
					elseif e[i].Variant == BombVariant.BOMB_GLITTER then
						sprite:Load("gfx/Pongbomb.anm2", true)
						sprite:Play("Glitter", true)
					end
				end
				if e[i].Type == EntityType.ENTITY_BOMBDROP then
					if e[i].FrameCount == 45 and e[i].Variant ~= BombVariant.BOMB_TROLL and e[i].Variant ~= BombVariant.BOMB_SUPERTROLL and e[i].Variant ~= BombVariant.BOMB_MR_MEGA and e[i].Variant ~= BombVariant.BOMB_BOBBY then
						if Player:HasWeaponType(WeaponType.WEAPON_BRIMSTONE) then
							local brim = Player:FireBrimstone(Vector(Player.ShotSpeed,Player.ShotSpeed))
							brim.ParentOffset = (e[i].Position - Player.Position)
							local brim = Player:FireBrimstone(Vector(-Player.ShotSpeed,Player.ShotSpeed))
							brim.ParentOffset = (e[i].Position - Player.Position)
							local brim = Player:FireBrimstone(Vector(Player.ShotSpeed,-Player.ShotSpeed))
							brim.ParentOffset = (e[i].Position - Player.Position)
							local brim = Player:FireBrimstone(Vector(-Player.ShotSpeed,-Player.ShotSpeed))
							brim.ParentOffset = (e[i].Position - Player.Position)
						elseif Player:HasWeaponType(WeaponType.WEAPON_LASER) then
							Player:FireTechLaser(e[i].Position, LaserOffset.LASER_TECH1_OFFSET, Vector(Player.ShotSpeed,Player.ShotSpeed), false, true)
							Player:FireTechLaser(e[i].Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-Player.ShotSpeed,Player.ShotSpeed), false, true)
							Player:FireTechLaser(e[i].Position, LaserOffset.LASER_TECH1_OFFSET, Vector(Player.ShotSpeed,-Player.ShotSpeed), false, true)
							Player:FireTechLaser(e[i].Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-Player.ShotSpeed,-Player.ShotSpeed), false, true)
						else
							--SPACES--
							Player:FireTear(e[i].Position, Vector(Player.ShotSpeed*9,Player.ShotSpeed*9), false, true, false)
							Player:FireTear(e[i].Position, Vector(-Player.ShotSpeed*9,Player.ShotSpeed*9), false, true, false)
							Player:FireTear(e[i].Position, Vector(Player.ShotSpeed*9,-Player.ShotSpeed*9), false, true, false)
							Player:FireTear(e[i].Position, Vector(-Player.ShotSpeed*9,-Player.ShotSpeed*9), false, true, false)
						end
					end
				end
			end
		end
	end
end

function PongerMod:CacheUpdate(Player, cacheFlag)
	local Player = Isaac.GetPlayer(0)
	if Player:HasCollectible(GENESIS_ITEMS.PASSIVE.PONGER) and PongerMod.HasCostume ~= true then
    Player:AddNullCostume(self.CostumeId)
    PongerMod.HasCostume = true
	    if cacheFlag == CacheFlag.CACHE_TEARFLAG then
			Player.TearFlags = SetBit(Player.TearFlags, Bit(20))
		end
    if not Player:HasCollectible(GENESIS_ITEMS.PASSIVE.PONGER) then
    Player:TryRemoveNullCostume(self.CostumeId)
    PongerMod.HasCostume = false
    end
	end
  end

PongerMod:AddCallback(ModCallbacks.MC_POST_UPDATE, PongerMod.Update)
PongerMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, PongerMod.CacheUpdate)

-------------
--MERP ITEM--
-------------

local MerpMod = RegisterMod("Merp", 1)

--Flap duration--
MerpMod.FlapFrames = 300

--Pull  duration--
MerpMod.PullFrames = 120

--How hard should Merp pull--
MerpMod.PullingForce = 1

--Merp speed--
MerpMod.FlyingSpeed = 1

--Nuff said--
MerpMod.SoundVolume = 0.5

--What types of entities should be pulled--
MerpMod.PullWhiteList = {
    EntityType.ENTITY_TEAR,
    EntityType.ENTITY_FAMILIAR,
    EntityType.ENTITY_BOMBDROP,
    EntityType.ENTITY_PICKUP,
    EntityType.ENTITY_PROJECTILE,
    EntityType.ENTITY_KNIFE,
    EntityType.ENTITY_LASER
}

--What types of entities shouldn't be pulled--
MerpMod.PullBlackList = {
    EntityType.ENTITY_SHOPKEEPER
}

--What types of entities should be annoyed--
MerpMod.AnnoyedEntities = {
    {
        Type=EntityType.ENTITY_EFFECT,
        Variant=EffectVariant.DEVIL,
        AnnoyedSprite="gfx/annoyed_satan.anm2",
        Sprite="gfx/084.000_Satan.anm2",
        AnimationAnnoy="Idle",
        Animation="SmallIdle"
    },
    {
        Type=EntityType.ENTITY_EFFECT,
        Variant=EffectVariant.ANGEL,
        AnnoyedSprite="gfx/annoyed_angel.anm2",
        Sprite="gfx/1000.009_angelstatue.anm2",
        AnimationAnnoy="Idle",
        Animation="Idle"
    }
}

--Smerp will spawn with 1/SmerpChance chance--
MerpMod.SmerpChance = 256

--OrBit distance. X, Y should be possitive and equal--
MerpMod.MerpOrBitDistance = Vector(150, 150)

--OrBit speed--
MerpMod.MerpOrBitSpeed = 0.03

--Nuff said--
MerpMod.MerpStates = {
    NULL = 0,
    OnTape = 1,
    OnLeash = 2
}

MerpMod.SmerpSpawns = 0
MerpMod.MerpsActive = false
MerpMod.MerpsCount = 0
MerpMod.MerpsFrames = 1
MerpMod.SoundEffectEntity = nil
MerpMod.Position = nil
MerpMod.Start = true

local function playAnimation(sprite, animation)
    if (not sprite:IsPlaying(animation)) then
        sprite:Play(animation, true)
    end
end

--Initialize values--
function MerpMod:OnPlayerInit(Player)
    MerpMod.MerpsFrames = 1
    MerpMod.MerpsActive = false
    MerpMod.Position = nil
    MerpMod.MerpsCount = 0
    MerpMod.SmerpSpawns = 0
    MerpMod.Start = true
end

--Initialize Merp--
function MerpMod:MerpInit(Familiar)
    local count = Isaac.GetPlayer(0):GetCollectibleNum(GENESIS_ITEMS.FAMILIAR.MERP)
    if tonumber(Genesis.moddata.merp) > MerpMod.SmerpSpawns then
        MerpMod.SmerpSpawns = MerpMod.SmerpSpawns + 1
        Familiar:GetSprite():Load("gfx/smerp.anm2", true)
    end
    if count > MerpMod.MerpsCount then
        MerpMod.MerpsCount = MerpMod.MerpsCount + 1
    end
    if MerpMod.MerpsFrames == nil then
        MerpMod.MerpsFrames = Isaac.GetFrameCount()
    end
    Familiar.State = MerpMod.MerpStates.NULL
end

--Spawn Merps when needed--
function MerpMod:Spawn(Player, Flag)
    if Flag == CacheFlag.CACHE_FAMILIARS then
        local player = Isaac.GetPlayer(0)
        local count = player:GetCollectibleNum(GENESIS_ITEMS.FAMILIAR.MERP)
        if count == 0 then
            --Genesis.moddata.merp = 0
        end
        if count > MerpMod.MerpsCount then
            for i=MerpMod.MerpsCount + 1, count do
                if Random() % MerpMod.SmerpChance == 0 then
                    --Genesis.moddata.merp = Genesis.moddata.merp + 1
                end
                MerpMod.MerpsCount = MerpMod.MerpsCount + 1
                Isaac.Spawn(GENESIS_ENTITIES.MERP, GENESIS_ENTITIES.VARIANT.MERP, 0, player.Position, Vector(0, 0), player)
            end
        end
    end
end

local function CheckInList(List, Value)
    for i=1, #List do
        if List[i] == Value then
            return true
        end
    end
    return false
end

local function Fly(Familiar, Speed)
    local Player = Isaac.GetPlayer(0)
    if Player:HasTrinket(TrinketType.TRINKET_DUCT_TAPE) then
        if Familiar.State ~= MerpMod.MerpStates.OnTape then
            Familiar.State = MerpMod.MerpStates.OnTape
            Familiar.OrbitDistance = Player.Position - Familiar.Position
        end
        Familiar.Velocity = Player.Position - Familiar.OrbitDistance - Familiar.Position
    else
        if Player:HasTrinket(TrinketType.TRINKET_CHILD_LEASH) then
            if (Familiar.State ~= MerpMod.MerpStates.OnLeash) or MerpMod.Start then
                Familiar.State = MerpMod.MerpStates.OnLeash
                local entities = Isaac.GetRoomEntities()
                local Merps = {}
                for i=1, #entities do
                    if (entities[i].Type == GENESIS_ENTITIES.MERP) and (entities[i].Variant == GENESIS_ENTITIES.VARIANT.MERP) then
                        table.insert(Merps, entities[i])
                    end
                end
                local angle = (2 * math.pi) / #Merps
                for i=1, #Merps do
                    Merps[i]:ToFamiliar().OrbitAngleOffset = angle * (i - 1)
                end
                MerpMod.Start = false
            end
            Familiar.OrbitLayer = 42
            Familiar.OrbitDistance = MerpMod.MerpOrBitDistance
            Familiar.OrbitSpeed = MerpMod.MerpOrBitSpeed
            Familiar.Velocity = Familiar:GetOrbitPosition(Player.Position) - Familiar.Position
        else
            Familiar.State = MerpMod.MerpStates.NULL
            Familiar:MoveDiagonally(Speed)
        end
    end
end

--Merp action--
function MerpMod:MerpUpdate(Familiar)
    local frames = Isaac.GetFrameCount()
    local sprite = Familiar:GetSprite()
    local entities = Isaac.GetRoomEntities()
    if ((frames - MerpMod.MerpsFrames) > MerpMod.FlapFrames) or MerpMod.MerpsActive then

        --That's not the code you're looking for--
        if MerpMod.SoundEffectEntity == nil then
            MerpMod.SoundEffectEntity = Isaac.Spawn(GENESIS_ENTITIES.SOUND_EFFECT, GENESIS_ENTITIES.VARIANT.SOUND_EFFECT, 0, Isaac.GetPlayer(0).Position, Vector(0, 0), nil)
            MerpMod.SoundEffectEntity:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            MerpMod.SoundEffectEntity:ToNPC():PlaySound(SoundEffect.SOUND_DERP, MerpMod.SoundVolume, 0, false, 1)
            MerpMod.SoundEffectEntity:ToNPC().CanShutDoors = false
        end
        ------------------------------------------

        --Start pulling--
        MerpMod.MerpsActive = true
        Fly(Familiar, 0)
        playAnimation(sprite, "Magnetize")
        Familiar.CollisionDamage = 5
        --Action--
        for i=1, #entities do

            --Annoying--
            for j=1, #MerpMod.AnnoyedEntities do
                local annoyed = MerpMod.AnnoyedEntities[j]
                if (entities[i].Type == annoyed.Type) and (entities[i].Variant == annoyed.Variant) then
                    local spr = entities[i]:GetSprite()
                    if spr:GetFilename() ~= annoyed.AnnoyedSprite then
                        spr:Load(annoyed.AnnoyedSprite, true)
                        playAnimation(spr, annoyed.AnimationAnnoy)
                    end
                end
            end

            --Pulling--
            if entities[i].Variant ~= GENESIS_ENTITIES.VARIANT.MERP then
                if (entities[i]:IsEnemy() or CheckInList(MerpMod.PullWhiteList, entities[i].Type)) and (not CheckInList(MerpMod.PullBlackList, entities[i].Type)) then
                    --PULL!!!--
                    entities[i]:AddVelocity((Familiar.Position - entities[i].Position):Normalized() * MerpMod.PullingForce)
                end
            end

        end
        if MerpMod.MerpsFrames + MerpMod.PullFrames + MerpMod.FlapFrames <= frames then
            --Stop pulling--
            MerpMod.MerpsFrames = frames
            MerpMod.MerpsActive = false
            Familiar.CollisionDamage = 1
            MerpMod.SoundEffectEntity:Remove()
            MerpMod.SoundEffectEntity = nil
            for i=1, #entities do
                for j=1, #MerpMod.AnnoyedEntities do
                    if entities[i].Type == MerpMod.AnnoyedEntities[j].Type and entities[i].Variant == MerpMod.AnnoyedEntities[j].Variant then
                        entities[i]:GetSprite():Load(MerpMod.AnnoyedEntities[j].Sprite, true)
                        playAnimation(entities[i]:GetSprite(), MerpMod.AnnoyedEntities[j].Animation)
                    end
                end
            end
        end
    else
        --Flap like you've never flapped before--
        Fly(Familiar, MerpMod.FlyingSpeed)
        playAnimation(sprite, "Flap")
    end
    for i=1, #entities do
        if ((Familiar.Position - entities[i].Position):Length() < 13 + (entities[i].SizeMulti * entities[i].Size):Length() / 2)
            and (entities[i].Type == EntityType.ENTITY_PROJECTILE) then
            entities[i]:Die()
        end
    end
end

MerpMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, MerpMod.Spawn)
MerpMod:AddCallback(ModCallbacks.MC_FAMILIAR_INIT , MerpMod.MerpInit, GENESIS_ENTITIES.VARIANT.MERP)
MerpMod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE , MerpMod.MerpUpdate, GENESIS_ENTITIES.VARIANT.MERP)
MerpMod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, MerpMod.OnPlayerInit)

-------------
--Lil Lamby--
-------------

local LambyMod = RegisterMod("mod",1)

function LambyMod:use_LilLamby()
	local player = Isaac.GetPlayer(0)
	local npc = Isaac.Spawn(GENESIS_ENTITIES.LIL_LAMBY, 0, 0, player.Position, Vector(0,0), player)
	npc:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
	local npc = Isaac.Spawn(GENESIS_ENTITIES.CENSER_HALO, 0, 0, player.Position, Vector(0,0), player)
	npc:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
	return true
end

function LambyMod:LilLamby(npc)
	local player = Isaac.GetPlayer(0)
	local sprite = npc:GetSprite()
	if npc.FrameCount == 1 then
		sprite:Play("Idle", true)
	end
	local e = Genesis.getRoomEntities()
	for i=1, #e do
		if e[i]:IsEnemy() then
			if (npc.Position - e[i].Position):Length() <= 140 then
				e[i]:AddSlowing(EntityRef(npc), 1, 3, e[i]:GetColor())
				if player:HasCollectible(247) and player.FrameCount % 5 == 0 then
					e[i]:TakeDamage(1,EntityFlag.FLAG_POISON,EntityRef(player),0)
				end
			end
		elseif e[i].Type == EntityType.ENTITY_PROJECTILE then
			if (npc.Position - e[i].Position):Length() <= 140 and e[i]:GetData().IsSlowed ~= true then
				e[i]:GetData().IsSlowed = true
				e[i].Velocity = e[i].Velocity / 2
			end
		end
	end
end

LambyMod:AddCallback(ModCallbacks.MC_USE_ITEM, LambyMod.use_LilLamby, GENESIS_ITEMS.ACTIVE.LIL_LAMBY)
LambyMod:AddCallback(ModCallbacks.MC_NPC_UPDATE, LambyMod.LilLamby, GENESIS_ENTITIES.LIL_LAMBY)

--------------
--SUPERPOWERS--
--------------

local SuperPowers = RegisterMod("Super Powers", 1)
SuperPowers.JudasUsed = false
SuperPowers.JudasEnemiesKilled = 0
SuperPowers.IsEve = false
SuperPowers.IsLost = false
SuperPowers.AddedLilithItems = false
SuperPowers.AddedKeeperCoins = false
SuperPowers.AddedEdenItems = false
SuperPowers.AddedApollyonItems = false
SuperPowers.AddedForgottenItems = false
SuperPowers.AddedSoullessBonies = false
SuperPowers.AddedLuciferItems = false
SuperPowers.AddedSamsonBonus = false
SuperPowers.AddedAzazelRange = false
SuperPowers.AddedDrawnInkDrops = false
SuperPowers.AddedBarnaelHearts = false
SuperPowers.AddedXaphanFire = false
SuperPowers.HasItem = false

function SuperPowers:onplayerinit()
	SuperPowers.JudasUsed = false
	SuperPowers.JudasEnemiesKilled = 0
	SuperPowers.IsEve = false
	SuperPowers.IsLost = false
	SuperPowers.AddedLilithItems = false
	SuperPowers.AddedKeeperCoins = false
	SuperPowers.AddedEdenItems = false
	SuperPowers.AddedApollyonItems = false
	SuperPowers.AddedForgottenItems = false
	SuperPowers.AddedSoullessBonies = false
	SuperPowers.AddedLuciferItems = false
	SuperPowers.AddedSamsonBonus = false
	SuperPowers.AddedAzazelRange = false
	SuperPowers.AddedDrawnInkDrops = false
	SuperPowers.AddedBarnaelHearts = false
	SuperPowers.AddedXaphanFire = false
	SuperPowers.HasItem = false
end

SuperPowers.dirVecs = { --#performance
  Vector(0,10),
  Vector(10,0),
  Vector(0,-10),
  Vector(-10,0),
  Vector(10,10),
  Vector(10,-10),
  Vector(-10,-10),
  Vector(-10,10)
}

function SuperPowers:update()
  local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.SUPERPOWERS) then
		SuperPowers.IsEve = false
		SuperPowers.IsLost = false
        -- Isaac: When at half a heart shoots tears in 8 directions in a 1 second interval (Working)
		if player:GetName() == "Isaac" then
			local room = Game():GetRoom()
			if player:GetHearts() == 1 or (player:GetHearts() == 0 and player:GetSoulHearts() == 1) then
				if Game():GetFrameCount() % 30 == 0 and not room:IsClear() then
					for i=1,8 do
						player:FireTear(player.Position, SuperPowers.dirVecs[i], false, true, false)
					end
				end
			end
		--Maggy: Charms every enemy in the room for 3 seconds when going in the room, and every red heart becomes a better one (Working)
        elseif player:GetName() == "Magdalene" then
			local entities = Genesis.getRoomEntities()
			if Game():GetRoom():GetFrameCount() == 5 then
				for i = 1, #entities do
					if entities[i]:IsEnemy() then
						local enemydata = entities[i]:GetData()
						if entities[i]:IsVulnerableEnemy() and enemydata.Charmed ~= 1 then
							entities[i]:AddCharmed(120)
							enemydata.Charmed = 1
							player:AnimateHappy()
						end
					elseif entities[i].Type == EntityType.ENTITY_PICKUP then
						if entities[i].Variant == PickupVariant.PICKUP_HEART then
							local pickup = entities[i]:ToPickup()
							if entities[i].SubType == HeartSubType.HEART_HALF then
								entities[i]:Remove()
								local newheart = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_FULL, entities[i].Position, Vector(0,0), nil)
								newheart:GetData().changed = 1
							elseif entities[i].SubType == HeartSubType.HEART_FULL or entities[i].SubType == HeartSubType.HEART_SCARED and entities[i]:GetData().changed ~= 1 then
								pickup:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_DOUBLEPACK, true)
							end
						end
					end
				end
			end
		-- Cain: Enemies have a chance of dropping a coin, and it having a 10% to be a dime and 20% to be a nickel (Not tested)
		elseif player:GetName() == "Cain" then
			local entities = Genesis.getRoomEntities()
			for i = 1, #entities do
				if entities[i]:IsEnemy() and entities[i]:HasMortalDamage() and math.random(1,30) == 1 then
					player:AnimateHappy()
					local upgradechance = math.random(1,100)
					if upgradechance > 90 then
						Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, CoinSubType.COIN_DIME, entities[i].Position, Vector(0,0), player)
					elseif upgradechance > 70 then
						Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, CoinSubType.COIN_NICKEL, entities[i].Position, Vector(0,0), player)
					else
						Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, CoinSubType.COIN_PENNY, entities[i].Position, Vector(0,0), player)
					end
				end
			end
		-- Judas: When you have invincibility frames you shoot out a shockwave that you can send out again after killing 5 enemies (Working)
		elseif player:GetName() == "Judas" or player:GetName() == "Black Judas" then
			if SuperPowers.JudasUsed == true then
				local entities = Genesis.getRoomEntities()
				for i = 1, #entities do
					if entities[i]:IsEnemy() and entities[i]:IsVulnerableEnemy() and entities[i]:HasMortalDamage() then
						SuperPowers.JudasEnemiesKilled = SuperPowers.JudasEnemiesKilled + 1
					end
				end
				if SuperPowers.JudasEnemiesKilled > 5 then
					player:AnimateHappy()
					SuperPowers.JudasUsed = false
					SuperPowers.JudasEnemiesKilled = 0
				end
			end
		-- Blue Baby: Every red heart turns into a soul heart of its kind - a half red heart turns into a half soul heart (Working)
		elseif player:GetName() == "???" then
			entities = Genesis.getRoomEntities()
			for i = 1, #entities do
                local entitydata = entities[i]:GetData()
				if entities[i].Type == EntityType.ENTITY_PICKUP and entitydata.changed ~= 1 then
                    if entities[i].Variant == PickupVariant.PICKUP_HEART then
                        local pickup = entities[i]:ToPickup()
						if entities[i].SubType == HeartSubType.HEART_HALF then
							pickup:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_HALF_SOUL, true)
							entitydata.changed = 1
						elseif entities[i].SubType == HeartSubType.HEART_FULL or entities[i].SubType == HeartSubType.HEART_SCARED then
							pickup:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_SOUL, true)
							entitydata.changed = 1
						elseif entities[i].SubType == HeartSubType.HEART_DOUBLEPACK then
							pickup:Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_SOUL, true)
							entitydata.changed = 1
							local extrasoulheart = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_SOUL, entities[i].Position+Vector(0.1,0), Vector(0,0), player)
							extrasoulheart:GetData().changed = 1
						end
					end
				end
			end
		-- Eve: When you take damage you have a chance to activate a Necronomicon effect (Working)
		elseif player:GetName() == "Eve" then
			SuperPowers.IsEve = true
		-- Samson: If you don't have lusty blood you get it, if you have it then you leave a red creep trail (Working)
		elseif player:GetName() == "Samson" then
			if SuperPowers.AddedSamsonBonus == false then
				SuperPowers.AddedSamsonBonus = true
				if player:HasCollectible(CollectibleType.COLLECTIBLE_LUSTY_BLOOD) == false then
					player:AddCollectible(CollectibleType.COLLECTIBLE_LUSTY_BLOOD, 0, false)
				end
			end
			if Game():GetFrameCount() % 5 == 0 then
				Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.PLAYER_CREEP_RED, 0, player.Position, Vector(0,0), player)
			end
		-- Azazel: Huge range up (Working)
		elseif player:GetName() == "Azazel"	then
			if SuperPowers.AddedAzazelRange == false then
				Genesis.moddata.statsup.RANGE = Genesis.moddata.statsup.RANGE - 60
				SuperPowers.AddedAzazelRange = true
				player:AddCacheFlags(CacheFlag.CACHE_RANGE)
				player:EvaluateItems()
			end
		-- Lazarus: Upon entering the room, random enemies are chosen to have a blood trail. After that, the marked enemies leave creep behind them every 20 frames (Working)
		elseif player:GetName() == "Lazarus" or player:GetName() == "Lazarus II" then
			local rng = player:GetCollectibleRNG(GENESIS_ITEMS.PASSIVE.SUPERPOWERS)
			local entities = Genesis.getRoomEntities()
			for i = 1, #entities do
				local enemydata = entities[i]:GetData()
				if entities[i]:IsVulnerableEnemy() and rng:RandomInt(100) <= ((100 - 30) * player.Luck / 10) + 30 and enemydata.Creep ~= 1 and entities[i].FrameCount == 1 then
					enemydata.Creep = 1
				end
				if entities[i]:IsVulnerableEnemy() and enemydata.Creep == 1 and entities[i].FrameCount % 20 == 0 then
					Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.PLAYER_CREEP_RED, 0, entities[i].Position, Vector(0,0), player)
				end
			end
		-- Eden: Spawns 2 random items (Working)
		elseif player:GetName() == "Eden" then
			if SuperPowers.AddedEdenItems == false then
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CollectibleType.COLLECTIBLE_NULL, Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 10, true), Vector(0,0), player)
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CollectibleType.COLLECTIBLE_NULL, Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 10, true), Vector(0,0), player)
				SuperPowers.AddedEdenItems = true
			end
		-- The Lost: 50% chance not to take damage! (Working)
		elseif player:GetName() == "The Lost" then
			SuperPowers.IsLost = true
		-- Lilith: You get BBFS and Succubus (Working)
		elseif player:GetName() == "Lilith" then
			if SuperPowers.AddedLilithItems == false then
				player:AddCollectible(CollectibleType.COLLECTIBLE_SUCCUBUS, 0, false)
				player:AddCollectible(CollectibleType.COLLECTIBLE_BFFS, 0, false)
				SuperPowers.AddedLilithItems = true
			end
		-- Keeper: You get 1-10 lucky coins (Working)
		elseif player:GetName() == "Keeper" then
			if SuperPowers.AddedKeeperCoins == false then
				for var = 1, math.random(1,10), 1 do
					Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, CoinSubType.COIN_LUCKYPENNY, Isaac.GetFreeNearPosition(player.Position, 1), Vector(0,0), player)
				end
				SuperPowers.AddedKeeperCoins = true
			end
		-- Apollyon: If he has the Void, Smelter spawns. If he has Smelter, Void spawns. If he has neither, both spawn. (Working)
		elseif player:GetName() == "Apollyon" and SuperPowers.AddedApollyonItems == false then
			SuperPowers.AddedApollyonItems = true
			if player:HasCollectible(CollectibleType.COLLECTIBLE_VOID) then
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CollectibleType.COLLECTIBLE_SMELTER, Isaac.GetFreeNearPosition(player.Position, 1), Vector(0,0), player)
			elseif player:HasCollectible(CollectibleType.COLLECTIBLE_SMELTER) then
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CollectibleType.COLLECTIBLE_VOID, Isaac.GetFreeNearPosition(player.Position, 1), Vector(0,0), player)
			elseif not (player:HasCollectible(CollectibleType.COLLECTIBLE_SMELTER) and player:HasCollectible(CollectibleType.COLLECTIBLE_VOID)) then
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CollectibleType.COLLECTIBLE_VOID, Isaac.GetFreeNearPosition(player.Position, 1), Vector(0,0), player)
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CollectibleType.COLLECTIBLE_SMELTER, Isaac.GetFreeNearPosition(player.Position, 1), Vector(0,0), player)
			end
		-- The Forgotten: +1 bone heart and a random bone item
		elseif player:GetName() == "The Forgotten" or player:GetName() == "The Soul" then
			if SuperPowers.AddedForgottenItems == false then
				player:AddBoneHearts(1)
				player:AddHearts(2)
				local BonePool = {549,548,544,542,541,453,375}
				math.randomseed = math.random(1,99)
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, BonePool[math.random(#BonePool)], Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 10, true), Vector(0,0), player)
				player:AnimateHappy()
				SuperPowers.AddedForgottenItems = true
			end
		-- The Soulless: Spawns bonies that are loyal to you until they die (Working, I think)
		elseif player:GetName() == "The Soulless" then
			if SuperPowers.AddedSoullessBonies == false then
				SuperPowers:SpawnBony()
				player:AnimateHappy()
				SuperPowers.AddedSoullessBonies = true
			end
			if Game():GetRoom():GetFrameCount() <= 1 and Game():GetRoom():IsClear() == false then
				SuperPowers:SpawnBony()
			end
		-- Lucifer: Spawns 2 Devil items (Not tested)
		elseif player:GetName() == "Lucifer" then
			if SuperPowers.AddedLuciferItems == false then
				local DevilPool = {8, 34, 35, 51, 67, 79, 80, 81, 82, 83, 84, 97, 113, 114, 118, 122, 126, 133, 134, 145, 159, 163, 172, 187, 212, 215, 216, 225, 230, 237, 241, 259, 262, 269, 268, 275, 278, 292, 311, 412, 408, 399, 391, 360, 409, 433, 431, 420, 417, 441, 498, 477, 475, 462, 442, 468}
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, DevilPool[math.random(#DevilPool)], Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 10, true), Vector(0,0), player)
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, DevilPool[math.random(#DevilPool)], Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 10, true), Vector(0,0), player)
				SuperPowers.AddedLuciferItems = true
			end
		-- Xaphan: spawn fires in corners of the room
      	elseif player:GetName() == "Xaphan" then
			if SuperPowers.AddedXaphanFire == false then
				local room = Game():GetRoom()
				local topleftcorner = room:GetTopLeftPos()
				local bottomrightcorner = room:GetBottomRightPos()
				local bottomleftcorner = Vector(topleftcorner.X,bottomrightcorner.Y)
				local toprightcorner = Vector(bottomrightcorner.X,topleftcorner.Y)
				Isaac.Spawn(EntityType.ENTITY_FIREPLACE, 0, 0, room:FindFreePickupSpawnPosition(topleftcorner, 20, true), Vector(0,0), player)
				Isaac.Spawn(EntityType.ENTITY_FIREPLACE, 0, 0, room:FindFreePickupSpawnPosition(toprightcorner, 20, true), Vector(0,0), player)
				Isaac.Spawn(EntityType.ENTITY_FIREPLACE, 0, 0, room:FindFreePickupSpawnPosition(bottomrightcorner, 20, true), Vector(0,0), player)
				Isaac.Spawn(EntityType.ENTITY_FIREPLACE, 0, 0, room:FindFreePickupSpawnPosition(bottomleftcorner, 20, true), Vector(0,0), player)
				SuperPowers.AddedXaphanFire = true
			end
		-- The Drawn: spawn ink blobs
        elseif player:GetName() == "The Drawn" then
			if SuperPowers.AddedDrawnInkDrops == false then
				Isaac.Spawn(EntityType.ENTITY_PICKUP, 10492, 0, Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 10, true), Vector(0,0), player)
				Isaac.Spawn(EntityType.ENTITY_PICKUP, 10492, 0, Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 10, true), Vector(0,0), player)
				SuperPowers.AddedDrawnInkDrops = true
			end
		-- Barnael: spawn resurrection heart
        elseif player:GetName() == "Barnael" then
			if SuperPowers.AddedBarnaelHearts == false then
				Isaac.Spawn(EntityType.ENTITY_PICKUP, 1490, 0, Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 10, true), Vector(0,0), player)
				Isaac.Spawn(EntityType.ENTITY_PICKUP, 1591, 0, Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 10, true), Vector(0,0), player)
				SuperPowers.AddedBarnaelHearts = true
			end
		-- Any other character: HP up and spawn coin
		elseif SuperPowers.HasItem == false then
			SuperPowers.IsLost = false
			SuperPowers.IsEve = false
			player:AddMaxHearts(2, false)
			player:AddHearts(2)
			Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, 0, player.Position, Vector(0,0), player)
		end
		SuperPowers.HasItem = true
	end
end

function SuperPowers:SpawnBony()
	local Bony = Isaac.Spawn(EntityType.ENTITY_BONY, 0, 0, Isaac.GetFreeNearPosition(player.Position, 5), player.Velocity, player)
	Bony:AddCharmed(-1)
end

function SuperPowers:ontakedamage(target, amount, flag, source, num)
	local player = Isaac.GetPlayer(0)
	if SuperPowers.IsEve == true and math.random(1,6) == 1 then
		player:UseActiveItem(CollectibleType.COLLECTIBLE_NECRONOMICON, true, false, true, false)
	elseif SuperPowers.IsLost == true and math.random(1,2) == 1 then
		player:AnimateHappy()
		player:UseActiveItem(CollectibleType.COLLECTIBLE_BOOK_OF_SHADOWS, false, false, true, false)
		return false
	elseif (player:GetName() == "Judas" or player:GetName() == "Black Judas") and player:HasCollectible(GENESIS_ITEMS.PASSIVE.SUPERPOWERS) and SuperPowers.JudasUsed == false then
		local shockwave = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.SHOCKWAVE , 0, player.Position, Vector(0,0), player)
		shockwave:ToEffect():SetRadii(0,75)
		SuperPowers.JudasUsed = true
	end
end
SuperPowers:AddCallback(ModCallbacks.MC_POST_UPDATE,SuperPowers.update)
SuperPowers:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, SuperPowers.onplayerinit)
SuperPowers:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, SuperPowers.ontakedamage, EntityType.ENTITY_PLAYER)

-------------
--Salt Item--
-------------

local SaltMod = RegisterMod("salt",1);

function SaltMod:PlaySoundAtPos(sound, volume, pos) -- make a function to play a sound when tears split
	local soundDummyNPC = Isaac.Spawn(EntityType.ENTITY_FLY, 0, 0, pos, Vector(0,0), Isaac.GetPlayer(0)):ToNPC();
	soundDummyNPC:PlaySound(sound, volume, 0, false, 1.0);
	soundDummyNPC:Remove();
	soundDummyNPC.CanShutDoors = false
end

	-- Amplifies Isaac's damage
function SaltMod:cacheUpdate(player, cacheFlag)
    if cacheFlag == CacheFlag.CACHE_DAMAGE then
        local count = player:GetCollectibleNum(GENESIS_ITEMS.PASSIVE.SALT)
        if count > 0 then
            local data = player:GetData()
			player.Damage = player.Damage + 0.25 * data.SaltMod.miss * count
			if data.SaltMod.angry == 1 then
				player.Damage = player.Damage + (90 / data.SaltMod.angerFrames) * count
			end
		end
	end
end
	-- Updates the stats
function SaltMod:addDmg(player)
    local data = player:GetData()
    if not player:HasCollectible(GENESIS_ITEMS.PASSIVE.SALT) then
        if data.SaltMod then
            player:GetSprite().Color = Color(1,1,1,1,0,0,0)
        end
        data.SaltMod = nil
        return
    end

    if not data.SaltMod then
        data.SaltMod = {
            miss = 0,
	        angry = 0,
	        angerFrames = 1
        }
        player:AddNullCostume(GENESIS_COSTUMES.SALT_COSTUME)
    end

    local saltUpdate = false

    if data.SaltMod.angry == 1 then
        saltUpdate = true
        data.SaltMod.angerFrames = data.SaltMod.angerFrames + 1
		if data.SaltMod.angerFrames > 360 then
			data.SaltMod.angry = 0
			data.SaltMod.angerFrames = 1
		end
    elseif data.SaltMod.angry == 0 and Game():GetRoom():GetAliveEnemiesCount() > 0 then
        local entities = Isaac.FindByType(EntityType.ENTITY_TEAR, -1, -1)
	    for _, entity in pairs(entities) do
		    local TearData = entity:GetData()
            if TearData.Calculated ~= 1 and (entity:ToTear().Height >= -5 or entity:CollidesWithGrid()) then
                saltUpdate = true
			    data.SaltMod.miss = data.SaltMod.miss + (0.8 ^ data.SaltMod.miss)
                TearData.Calculated = 1
            end
		end
    end

    if saltUpdate then
    	if data.SaltMod.miss >= 8 then
	    	data.SaltMod.miss = 0
		    Isaac.Explode(player.Position, player, player.Damage*10)
		    player:AnimatePitfallOut()
		    SaltMod:PlaySoundAtPos(SoundEffect.SOUND_MONSTER_YELL_A , 1, player.Position)
		    data.SaltMod.angry = 1
        end

	    player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
        player:EvaluateItems()

        local BOff = math.ceil(255 * data.SaltMod.miss / 8)
        player:GetSprite().Color = Color(1,1,1,1,BOff,0,0)
    end
end

SaltMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, SaltMod.cacheUpdate)
SaltMod:AddCallback(ModCallbacks.MC_POST_PLAYER_UPDATE, SaltMod.addDmg)

--Code below is quite complex, parts of it were wrote in haste(Broski wanted to make a video) so I'll check and rewrite it later (GoldFeniks)

---------------
--SPLIT CROSS--
---------------

local SplitMod = RegisterMod("Split Cross",1) -- how do we refer to our mod?
SplitMod.ItemEffectGained = false
SplitMod.HasBlood = false

SplitMod.EntitySplitDescriptor = {}

SplitMod.EntitySplitDescriptor[EntityType.ENTITY_TEAR] = {
	CastFunction = "ToTear", --Method name to cast entity to right type
	Chance = 10, -- Chanse for entity to be able to split
	SplitField = {
		FieldName = "Height", -- Field name on which splitting depends
		Function = function (entity, rng) return rng:RandomInt(math.ceil(math.abs(Isaac.GetPlayer(0).TearHeight - 5))) + 5 end -- Function to calculate value on which entity will split
	},
	Spawn = function (entity, direction, player)
				local result = Isaac.Spawn(entity.Type, entity.Variant, 0, entity.Position, direction * entity.Velocity:Length(), player):ToTear()
				if player:HasWeaponType(WeaponType.WEAPON_LUDOVICO_TECHNIQUE) then
					result.TearFlags = ClearBit(entity.TearFlags, 1<<55)
					result.Velocity = direction * player.ShotSpeed * 5
				end
				for i=0, 63 do
					if HasBit(result.TearFlags, 1<<i) then
						Isaac.DebugString(tostring(i))
					end
				end
				return result
			end,
	Params = { -- Field that should be assigned after splitting
		{
			Field = "CollisionDamage",
			Multiplier = 0.5
		},
		{
			Field = "Height",
			Multiplier = 0.5
		},
		{
			Field = "Scale",
			Multiplier = 0.5
		}
	},
	DamageMultiplier = 2, -- Nuff said
	ChangeSpriteFunction = function (entity, applyNew)
					local player = Isaac.GetPlayer(0)
					local TearData = entity:GetData()
					local Sprite = entity:GetSprite()
					local Name = ""
					local anm2 = ""
					for j=1, 13 do
						if Sprite:IsPlaying("RegularTear"..j) and not player:HasCollectible(CollectibleType.COLLECTIBLE_HEAD_OF_THE_KEEPER) and not player:HasCollectible(CollectibleType.COLLECTIBLE_COMPOUND_FRACTURE) then
							Name = "RegularTear"..j
							if applyNew then
								TearData.FileName = Sprite:GetFilename()
								anm2 = "gfx/split.anm2"
							else
								anm2 = TearData.FileName
							end
							SplitMod.HasBlood = false
							break
						elseif Sprite:IsPlaying("BloodTear"..j) and not player:HasCollectible(CollectibleType.COLLECTIBLE_HEAD_OF_THE_KEEPER) and not player:HasCollectible(CollectibleType.COLLECTIBLE_COMPOUND_FRACTURE) then
							Name = "BloodTear"..j
							if applyNew then
								TearData.FileName = Sprite:GetFilename()
								anm2 = "gfx/splitblood.anm2"
							else
								anm2 = TearData.FileName
							end
							SplitMod.HasBlood = true
							break
						end
					end
					if Name ~= "" then
						Sprite:Load(anm2, true)
						Sprite:Play(Name, true)
						TearData.Changed = true
					end
					return nil
				end, -- function
	SplitCondition = function (entity) return entity.FrameCount == 1 and entity:GetData().Split == nil and not HasBit(entity.TearFlags, 1<<55) end,
	DoSplit = function (a, b, rng, entity, player)
					return (entity:GetData().Split == 1 and math.abs(a) < b) or
						(player:HasWeaponType(WeaponType.WEAPON_LUDOVICO_TECHNIQUE) and rng:RandomInt(200) <= player.Luck and HasBit(entity.TearFlags, 1<<55))
					end,
	SpawnCreep = true--EffectVariant.PLAYER_CREEP_HOLYWATER_TRAIL
}

SplitMod.EntitySplitDescriptor[EntityType.ENTITY_BOMBDROP] = {
	CastFunction = "ToBomb", --Method name to cast entity to right type
	Chance = 10, -- Chanse for entity to be able to split
	SplitField = {
		FieldName = "FrameCount", -- Field name on which splitting depends
		Function = function (entity, rng) return rng:RandomInt(10) + 10 end -- Function to calculate value on which entity will split
	},
	Spawn = function (entity, direction, player)
				local result = Isaac.Spawn(entity.Type, entity.Variant, 0, entity.Position, direction * entity.Velocity:Length(), player):ToBomb()
				local sprite = result:GetSprite()
				sprite.Scale = sprite.Scale * 0.5
				return result
			end,
	Params = { -- Field that should be assigned after splitting
		{
			Field = "Flags",
			Multiplier = 1
		},
		{
			Field = "ExplosionDamage",
			Multiplier = 0.5
		},
		{
			Field = "RadiusMultiplier",
			Multiplier = 0.5
		}
	},
	DamageMultiplier = 2, -- Nuff said
	ChangeSpriteFunction = function (entity, applyNew) return nil end, -- function
	SplitCondition = function (entity) return entity.IsFetus and entity:GetData().Split == nil end,
	DoSplit = function (a, b, rng, entity) return entity:GetData().Split == 1 and a >= b end,
	SpawnCreep = nil
}

SplitMod.EntitySplitDescriptor[EntityType.ENTITY_LASER] = {
	CastFunction = "ToLaser", --Method name to cast entity to right type
	Chance = 10, -- Chanse for entity to be able to split
	SplitField = {
		FieldName = "FrameCount", -- Field name on which splitting depends
		Function = function (entity, rng) return 0 end -- Function to calculate value on which entity will split
	},
	Spawn = function (entity, direction, player)
				local pos = entity.Position
				if not entity:IsCircleLaser() then
					pos = player.Position + (entity.EndPoint - player.Position) * 0.5
				end
				local result = EntityLaser.ShootAngle(entity.Variant, pos + Vector(0, -20), 90 * direction.X - 45 * direction.Y, math.max(entity.Timeout, 5), Vector(0, 0), player):ToLaser()
				if entity:IsCircleLaser() then
					result:SetActiveRotation(0, 360, 2, false)
					result:SetTimeout(50)
					result:GetData().Follow = true
					result.DisableFollowParent = true
				else
					result.DisableFollowParent = false
				end
				result.TearFlags = entity.TearFlags
				--result.TearFlags = ClearBit(result.TearFlags, 1<<19) -- removes synergy
				return result
			end,
	Params = { -- Field that should be assigned after splitting
		{
			Field = "MaxDistance",
			Multiplier = 1
		},
	},
	DamageMultiplier = 1, -- Nuff said
	ChangeSpriteFunction = function (entity, applyNew) return nil end, -- function
	SplitCondition = function (entity) return false end,
	DoSplit = function (a, b, rng, entity, player) return entity.Parent.Type == EntityType.ENTITY_PLAYER and
						((entity.FrameCount == 1 and entity:GetData().Split ~= 0 and rng:RandomInt(20) <= player.Luck) or
							(entity:IsCircleLaser() and player.FrameCount % (200 - player.Luck) == 0))
			 	end,
	SpawnCreep = nil
}


-- SplitMod.EntitySplitDescriptor[EntityType.ENTITY_KNIFE] = {
-- 	CastFunction = "ToKnife", --Method name to cast entity to right type
-- 	Chance = 2, -- Chanse for entity to be able to split
-- 	SplitField = {
-- 		FieldName = "FrameCount", -- Field name on which splitting depends
-- 		Function = function (entity, rng) return nil end -- Function to calculate value on which entity will split
-- 	},
-- 	Spawn = function (entity, direction, player)
-- 				local result = Isaac.Spawn(entity.Type, entity.Variant, 0, entity.Position, direction * entity.Velocity:Length(), nil):ToKnife()
-- 				result:SetPathFollowSpeed(5)
-- 				result:Shoot(1, 500)
-- 				result.HellbeamMod.Rotation = 90 * direction.X - 45 * direction.Y
-- 				result.Velocity = direction * 20
-- 				return result
-- 			end,
-- 	Params = { -- Field that should be assigned after splitting
-- 		-- {
-- 		-- 	Field = "TearFlags",
-- 		-- 	Multiplier = 1
-- 		-- },
-- 		{
-- 			Field = "CollisionDamage",
-- 			Multiplier = 0.5
-- 		},
-- 		-- {
-- 		-- 	Field = "MaxDistance",
-- 		-- 	Multiplier = 50
-- 		-- },
-- 		{
-- 			Field = "Scale",
-- 			Multiplier = 0.5
-- 		}
-- 	},
-- 	PostCreate = function (knife) knife:Shoot(100, 100) end,
-- 	DamageMultiplier = 2, -- Nuff said
-- 	ChangeSpriteFunction = function (entity, applyNew) return nil end, -- function
-- 	SplitCondition = function (entity) return false end,
-- 	DoSplit = function (a, b, rng, entity, player) return entity:IsFlying() and rng:RandomInt(200) <= player.Luck end,
-- 	SpawnCreep = nil
-- }

function SplitMod:onplayerinit(...)
	self.ItemEffectGained = false
end

function SplitMod:givecostume(player, cacheFlag)
    local player = Isaac.GetPlayer(0)
    if cacheFlag == CacheFlag.CACHE_TEARFLAG then
    	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.SPLIT_CROSS) then
    		if not self.ItemEffectGained then
        		self.ItemEffectGained = true
        		player:AddNullCostume(GENESIS_COSTUMES.SPLIT_CROSS_COSTUME)
        	end
        end
    end
end

function SplitMod:SpawnSplitEntity(entity, entityParams, direction, spawnCreep, player, spawnFunction)
	local newEntity = spawnFunction(entity, direction, player)
	for i=1, #entityParams do
		newEntity[entityParams[i].Field] = entity[entityParams[i].Field] * entityParams[i].Multiplier
	end
	if spawnCreep then
		if self.HasBlood then
			Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.PLAYER_CREEP_RED, 0, entity.Position, Vector(0,0), player)
		else
			Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CREEP_WHITE , 0, entity.Position, Vector(0,0), player)
		end
	end
	newEntity.Color = entity.Color
	newEntity:GetData().Split = 0
end

function SplitMod:EvaluateSplit(entity)
	if entity == nil then
		return nil
	end
	local d = self.EntitySplitDescriptor[entity.Type]
	if d == nil then
		return nil
	end
	entity = entity[d.CastFunction](entity)
	local player = Isaac.GetPlayer(0)
	local entityData = entity:GetData()
	local rng = player:GetCollectibleRNG(GENESIS_ITEMS.PASSIVE.SPLIT_CROSS)
	if d.SplitCondition(entity) then
		if rng:RandomInt(100) <= ((100 - d.Chance) * player.Luck / 10) + d.Chance then
			entityData.Split = 1
			entityData[d.SplitField.FieldName] = d.SplitField.Function(entity, rng)
			entity.CollisionDamage = entity.CollisionDamage * d.DamageMultiplier
			d.ChangeSpriteFunction(entity, true)
		end
	elseif d.DoSplit(entity[d.SplitField.FieldName], entityData[d.SplitField.FieldName], rng, entity, player) then
		if entityData.Changed ~= nil then
			d.ChangeSpriteFunction(entity, false)
		end
		entityData.Split = 0
		entity.CollisionDamage = entity.CollisionDamage / d.DamageMultiplier
		self:SpawnSplitEntity(entity, d.Params, Vector(1, 1), d.SpawnCreep, player, d.Spawn)
		self:SpawnSplitEntity(entity, d.Params, Vector(1, -1), d.SpawnCreep, player, d.Spawn)
		self:SpawnSplitEntity(entity, d.Params, Vector(-1, 1), d.SpawnCreep, player, d.Spawn)
		self:SpawnSplitEntity(entity, d.Params, Vector(-1, -1), d.SpawnCreep, player, d.Spawn)
	end

end

function SplitMod:evaluatetears()
    local player = Isaac.GetPlayer(0) -- Gets the player
    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.SPLIT_CROSS) then -- If the player has our item...
		local entities = Genesis.getRoomEntities() -- every entity in the room
        for i=1, #entities do -- for every entity in the room
        	self:EvaluateSplit(entities[i])
        	if entities[i].Type == EntityType.ENTITY_LASER then
        		entities[i]:ToLaser().TearFlags = SetBit(entities[i]:ToLaser().TearFlags, 1)
        		-- Move laser -- needs fix
        		-- if entities[i]:GetData().Follow == true then
        		-- 	entities[i].ParentOffset = player:GetActiveWeaponEntity().Position - entities[i].Position
        		-- end
        	end
        end
    end
end

SplitMod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, SplitMod.onplayerinit) -- when the game begins, call the mod.onplayerinit function
SplitMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, SplitMod.givecostume) -- when the cache is evaluated, call the mod.givecostume function
SplitMod:AddCallback(ModCallbacks.MC_POST_UPDATE, SplitMod.evaluatetears) -- Every time the game updates, call the mod.tears function

----------------
--Ring of Fire--
----------------

local RoFMod = RegisterMod("mod",1)
RoFMod.r = 0
RoFMod.roomcleared = true
RoFMod.amountflames = 0
RoFMod.oldamountflames = 0
RoFMod.flames = {}
RoFMod.Rotation = {}
RoFMod.hasitem = false

function RoFMod:Update()
	math.randomseed(RoFMod.r + 1)
	RoFMod.r = RoFMod.r + 1
    local game = Game()
    local room = game:GetRoom()
    local level = game:GetLevel()
    local player = Isaac.GetPlayer(0)
	if level:GetAbsoluteStage() == 1 and level.EnterDoor == -1 and player.FrameCount == 1 then
		RoFMod.oldamountflames = 0
		RoFMod.flames = {}
		RoFMod.Rotation = {}
	end
	if room:GetFrameCount() == 1 then
		for i=1, RoFMod.oldamountflames do
			local flame = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.RED_CANDLE_FLAME, 3, player.Position:__add(Vector(75,0)), Vector(0,0), player)
			table.insert(RoFMod.flames, flame)
			table.insert(RoFMod.Rotation, math.random() * 360)
		end
	end
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.RING_OF_FIRE) then
		RoFMod.hasitem = true
		if room:IsClear() == false then
			RoFMod.roomcleared = false
		elseif RoFMod.roomcleared == false then
			RoFMod.roomcleared = true
			if RoFMod.oldamountflames < 6 then
				local flame = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.RED_CANDLE_FLAME, 0, player.Position:__add(Vector(75,0)), Vector(0,0), player)
				table.insert(RoFMod.flames, flame)
				table.insert(RoFMod.Rotation, math.random() * 360)
				RoFMod.oldamountflames = RoFMod.oldamountflames + 1
			end
		end
		RoFMod.amountflames = 0
		local e = Genesis.getRoomEntities()
		for i=1, #e do
			if e[i].Type == EntityType.ENTITY_EFFECT and e[i].Variant == EffectVariant.RED_CANDLE_FLAME then
				RoFMod.amountflames = RoFMod.amountflames + 1
			end
		end
		for i=1, #RoFMod.flames do
			RoFMod.Rotation[i] = RoFMod.Rotation[i] + 0.1
			if RoFMod.Rotation[i] >= 360 then RoFMod.Rotation[i] = RoFMod.Rotation[i] - 360 end
			RoFMod.flames[i].Position = player.Position:__add(Vector(math.cos(RoFMod.Rotation[i]),math.sin(RoFMod.Rotation[i])):__mul(75))
			RoFMod.flames[i].Velocity = Vector(0,0)
			if room:GetGridCollisionAtPos(RoFMod.flames[i].Position) == GridCollisionClass.COLLISION_WALL then RoFMod.flames[i].Visible = false else RoFMod.flames[i].Visible = true end
		end
		if RoFMod.amountflames < RoFMod.oldamountflames then
			RoFMod.oldamountflames = RoFMod.amountflames
		end
	end
	if not player:HasCollectible(GENESIS_ITEMS.PASSIVE.RING_OF_FIRE) and RoFMod.flames ~= {} then
		if RoFMod.hasitem then
			RoFMod.roomcleared = true
			RoFMod.amountflames = 0
			RoFMod.oldamountflames = 0
			RoFMod.flames = {}
			RoFMod.Rotation = {}
			local e = Genesis.getRoomEntities()
			for i=1, #e do
				if e[i].Type == EntityType.ENTITY_EFFECT and e[i].Variant == EffectVariant.RED_CANDLE_FLAME then
					e[i]:Remove()
				end
			end
		end
		RoFMod.hasitem = false
	end
end

RoFMod:AddCallback(ModCallbacks.MC_POST_UPDATE, RoFMod.Update)


------------
--Rag Mini--
------------

local RagMini = RegisterMod("ragmini", 1)

RagMini.ItemId = GENESIS_ITEMS.FAMILIAR.RAG_MINI
RagMini.RagMiniEntityTypeId = GENESIS_ENTITIES.RAG_MINI
RagMini.RagMiniEntityVariantId = GENESIS_ENTITIES.VARIANT.RAG_MINI

RagMini.BallEntityTypeId = GENESIS_ENTITIES.SMALL_PURPLE_BALL
RagMini.BallEntityVariantId = GENESIS_ENTITIES.VARIANT.SMALL_PURPLE_BALL

RagMini.RagsSpawned = 0
RagMini.BallsSpawned = 0

--Balls constants--
RagMini.MaxBalls = 3
RagMini.BallState = {
    NULL = 0,
    FOLLOW_ORBit_POSITION = 1,
    ORBit = 2,
    REGROUP = 3,
    ATTACK = 4
}
RagMini.PurpleSpritePath = "gfx/effects/effect_088_purpleball.png"
RagMini.PurplePoofPath = "gfx/effects/purple_poof.png"
RagMini.MinDistance = 3
RagMini.BallInitSpeed = 3
RagMini.BallOrBitLayer = 42
RagMini.BallOrBitDistance = Vector(50, 50)
RagMini.BallOrBitSpeed = 0.03
RagMini.BallDamageMultipler = 2

--Rag Mini constants--
RagMini.SpawnBalls = 3
RagMini.SpawnChance = 10
RagMini.RagFloatAnimName = "FloatDown"
RagMini.RagSpawnAnimName = "Spawn"
RagMini.RagSpawnEventName = "Spawn"
RagMini.CollisionRadius = 15

function RagMini:OnPlayerInit(player)
    self.RagsSpawned = 0
    self.BallsSpawned = 0
end

function RagMini:OnEvaluateCache(player, cacheFlag)
  if cacheFlag == CacheFlag.CACHE_FAMILIARS then
    player:CheckFamiliar(self.RagMiniEntityVariantId, player:GetCollectibleNum(GENESIS_ITEMS.FAMILIAR.RAG_MINI), RNG())
  end
end

function RagMini:OnRagMiniFamiliarInit(familiar)
  self.RagsSpawned = self.RagsSpawned + 1
  familiar:GetSprite():Play(self.RagFloatAnimName, true)
  familiar.State = 0
  familiar.IsFollower = true
end

function RagMini:OnRagMiniFamiliarUpdate(familiar)
    familiar:FollowParent()
    local sprite = familiar:GetSprite()
    local entities = Isaac.GetRoomEntities()
    for i=1, #entities do
        if entities[i].Type == EntityType.ENTITY_PROJECTILE and (familiar.Position - entities[i].Position):Length() < self.CollisionRadius then
            entities[i]:Die()
            familiar.State = familiar.State + 1
            if familiar:GetDropRNG():RandomInt(100) < self.SpawnChance * familiar.State then
                familiar.State = 0
                sprite:Play(self.RagSpawnAnimName, true)
            end
        end
    end
    if sprite:IsFinished(self.RagSpawnAnimName) then
        sprite:Play(self.RagFloatAnimName, true)
    end
    if sprite:IsEventTriggered(self.RagSpawnEventName) then
        for i=1, self.SpawnBalls do
            Isaac.Spawn(self.BallEntityTypeId, self.BallEntityVariantId, 0, familiar.Position, RandomVector() * self.BallInitSpeed, Isaac.GetPlayer(0))
        end
    end
end

function RagMini:EavluateAngles(familiar)
    local entities = Isaac.GetRoomEntities()
    local Balls = {}
    if familiar ~= nil then
        table.insert(Balls, familiar)
    end
    for i=1, #entities do
        if (entities[i].Type == self.BallEntityTypeId) and (entities[i].Variant == self.BallEntityVariantId) and entities[i]:ToFamiliar().State ~= self.BallState.ATTACK then
            table.insert(Balls, entities[i])
        end
    end
    local angle = (2 * math.pi) / #Balls
    Isaac.DebugString(tostring(#Balls))
    for i=1, #Balls do
        local ball = Balls[i]:ToFamiliar()
        ball.State = self.BallState.REGROUP
        ball.OrbitAngleOffset = angle * (i - 1)
    end
    if familiar ~= nil then
        familiar.State = self.BallState.FOLLOW_ORBit_POSITION
    end
end

function RagMini:OnBallFamiliarInit(familiar)
    self.BallsSpawned = self.BallsSpawned + 1
    familiar.State = self.BallState.FOLLOW_ORBit_POSITION
    self:EavluateAngles(familiar)
    familiar.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ENEMIES
    familiar.CollisionDamage = Isaac.GetPlayer(0).Damage * self.BallDamageMultipler
end

function RagMini:PickTarget(familiar)
    local entities = Isaac.GetRoomEntities()
    local result = nil
    local length = nil
    for i=1, #entities do
        local len = (entities[i].Position - familiar.Position):Length()
        if entities[i]:IsVulnerableEnemy() and (result == nil or len < length) then
            result = entities[i]
            length = len
        end
    end
    return result
end

function RagMini:OnBallFamiliarUpdate(familiar)
    local player = Isaac.GetPlayer(0)
    if familiar.State ~= self.BallState.ATTACK then
        familiar.OrbitDistance = self.BallOrBitDistance
        familiar.OrbitSpeed = self.BallOrBitSpeed
        familiar.OrbitLayer = self.BallOrBitLayer
        local vec = familiar:GetOrbitPosition(player.Position) - familiar.Position
        if familiar.State == self.BallState.FOLLOW_ORBit_POSITION then
            familiar.Velocity = vec:Normalized() * math.max(1, math.min(vec:Length(), 5))
        elseif familiar.State == self.BallState.ORBit then
            familiar.Velocity = vec
        elseif familiar.State == self.BallState.REGROUP then
            familiar.Velocity = vec:Normalized() * math.max(2, math.min(vec:Length(), 5)) + player.Velocity
        end
        if vec:Length() < self.MinDistance then
            familiar.State = self.BallState.ORBit
        end
    else
        if familiar.Target == nil or familiar.Target:IsDead() then
            familiar.Target = self:PickTarget(familiar)
        end
        if familiar.Target ~= nil then
            local vec = familiar.Target.Position - familiar.Position
            familiar.Velocity = vec:Normalized() * math.max(1, math.min(vec:Length(), 5))
        else
            self:EavluateAngles(familiar)
        end
    end
    if familiar.FrameCount % (math.floor(self.BallsSpawned / 7) + 1) == 0 then
        local sprite = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.DARK_BALL_SMOKE_PARTICLE, 0, familiar.Position, Vector(0, 0), familiar):GetSprite()
        sprite:ReplaceSpritesheet(0, self.PurpleSpritePath)
        sprite:LoadGraphics()
    end
end

function RagMini:BallDeath(ball)
    if ball.State ~= self.BallState.ATTACK then
        self.BallsSpawned = self.BallsSpawned - 1
        self:EavluateAngles(nil)
    end
    local sprite = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BLOOD_EXPLOSION, 0, ball.Position, Vector(0, 0), nil):GetSprite()
    sprite:ReplaceSpritesheet(0, self.PurplePoofPath)
    sprite:LoadGraphics()
    ball:Die()
end

function RagMini:OnEntityTakeDamage(entity, _, _, source, _)
    if source.Type == self.BallEntityTypeId and source.Variant == self.BallEntityVariantId then
        local entities = Isaac.GetRoomEntities()
        for i=1, #entities do
            if entities[i].Type == self.BallEntityTypeId and entities[i].Variant == self.BallEntityVariantId and (entities[i].Position - source.Position):Length() < 1 then
                self:BallDeath(entities[i]:ToFamiliar())
            end
        end
    end
end

function RagMini:OnPlayerTakeDamage(player, _, _, _, _)
    local entities = Isaac.GetRoomEntities()
    for i=1, #entities do
        if entities[i].Type == self.BallEntityTypeId and entities[i].Variant == self.BallEntityVariantId then
            entities[i]:ToFamiliar().State = self.BallState.ATTACK
        end
    end
end

RagMini:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, RagMini.OnEvaluateCache)
RagMini:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, RagMini.OnRagMiniFamiliarInit, RagMini.RagMiniEntityVariantId)
RagMini:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, RagMini.OnRagMiniFamiliarUpdate, RagMini.RagMiniEntityVariantId)
RagMini:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, RagMini.OnPlayerInit)
RagMini:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, RagMini.OnBallFamiliarInit, RagMini.BallEntityVariantId)
RagMini:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, RagMini.OnBallFamiliarUpdate, RagMini.BallEntityVariantId)
RagMini:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, RagMini.OnEntityTakeDamage)
RagMini:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, RagMini.OnPlayerTakeDamage, EntityType.ENTITY_PLAYER)

---------------------
--	IAL DAGGER--
---------------------

--Setting up general variables--
local CeremonialDagger = RegisterMod("Ceremonial Dagger", 1)
CeremonialDagger.chance = 0
CeremonialDagger.HasTrinketSpawned = false
CeremonialDagger.PendantChance = 200
CeremonialDagger.CurrentRoomSeed = nil

--Devil Pool Items Array--
CeremonialDagger.DevilItems = {
CollectibleType.COLLECTIBLE_GUPPYS_HEAD,
CollectibleType.COLLECTIBLE_GUPPYS_PAW,
CollectibleType.COLLECTIBLE_MEGA_SATANS_BREATH,
CollectibleType.COLLECTIBLE_PLAN_C,
CollectibleType.COLLECTIBLE_RAZOR_BLADE,
CollectibleType.COLLECTIBLE_BOOK_OF_BELIAL,
CollectibleType.COLLECTIBLE_BOOK_OF_SIN,
CollectibleType.COLLECTIBLE_NECRONOMICON,
CollectibleType.COLLECTIBLE_VOID,
CollectibleType.COLLECTIBLE_WE_NEED_GO_DEEPER,
CollectibleType.COLLECTIBLE_SATANIC_BIBLE,
CollectibleType.COLLECTIBLE_THE_NAIL,
CollectibleType.COLLECTIBLE_ATHAME,
CollectibleType.COLLECTIBLE_BETRAYAL,
CollectibleType.COLLECTIBLE_BLACK_POWDER,
CollectibleType.COLLECTIBLE_BROTHER_BOBBY,
CollectibleType.COLLECTIBLE_CAMBION_CONCEPTION,
CollectibleType.COLLECTIBLE_CEREMONIAL_ROBES,
CollectibleType.COLLECTIBLE_CONTRACT_FROM_BELOW,
CollectibleType.COLLECTIBLE_DARK_BUM,
CollectibleType.COLLECTIBLE_DARK_MATTER,
CollectibleType.COLLECTIBLE_DEAD_CAT,
CollectibleType.COLLECTIBLE_DEMON_BABY,
CollectibleType.COLLECTIBLE_DUALITY,
CollectibleType.COLLECTIBLE_EMPTY_VESSEL,
CollectibleType.COLLECTIBLE_GHOST_BABY,
CollectibleType.COLLECTIBLE_GIMPY,
CollectibleType.COLLECTIBLE_GOAT_HEAD,
CollectibleType.COLLECTIBLE_GUPPYS_COLLAR,
CollectibleType.COLLECTIBLE_GUPPYS_HAIRBALL,
CollectibleType.COLLECTIBLE_GUPPYS_TAIL,
CollectibleType.COLLECTIBLE_HEADLESS_BABY,
CollectibleType.COLLECTIBLE_MISSING_PAGE_2,
CollectibleType.COLLECTIBLE_MULTIDIMENSIONAL_BABY,
CollectibleType.COLLECTIBLE_MY_SHADOW,
CollectibleType.COLLECTIBLE_PENTAGRAM,
CollectibleType.COLLECTIBLE_ROTTEN_BABY,
CollectibleType.COLLECTIBLE_SACRIFICIAL_DAGGER,
CollectibleType.COLLECTIBLE_SHADE,
CollectibleType.COLLECTIBLE_SISTER_MAGGY,
CollectibleType.COLLECTIBLE_SUCCUBUS,
CollectibleType.COLLECTIBLE_MARK,
CollectibleType.COLLECTIBLE_WHORE_OF_BABYLON,
CollectibleType.COLLECTIBLE_ABADDON,
CollectibleType.COLLECTIBLE_BRIMSTONE,
CollectibleType.COLLECTIBLE_DARK_PRINCESS_CROWN,
CollectibleType.COLLECTIBLE_DEATHS_TOUCH,
CollectibleType.COLLECTIBLE_EYE_OF_BELIAL,
CollectibleType.COLLECTIBLE_INCUBUS,
CollectibleType.COLLECTIBLE_JUDAS_SHADOW,
CollectibleType.COLLECTIBLE_LIL_BRIMSTONE,
CollectibleType.COLLECTIBLE_LORD_OF_THE_PIT,
CollectibleType.COLLECTIBLE_MAW_OF_VOID,
CollectibleType.COLLECTIBLE_MOMS_KNIFE,
CollectibleType.COLLECTIBLE_SPIRIT_NIGHT,
CollectibleType.COLLECTIBLE_PACT
}

--Double Hearts Devil Pool Items Array--
CeremonialDagger.DoubleDevilItems = {
CollectibleType.COLLECTIBLE_SATANIC_BIBLE,
CollectibleType.COLLECTIBLE_THE_NAIL,
CollectibleType.COLLECTIBLE_ABADDON,
CollectibleType.COLLECTIBLE_BRIMSTONE,
CollectibleType.COLLECTIBLE_DARK_PRINCESS_CROWN,
CollectibleType.COLLECTIBLE_DEATHS_TOUCH,
CollectibleType.COLLECTIBLE_EYE_OF_BELIAL,
CollectibleType.COLLECTIBLE_INCUBUS,
CollectibleType.COLLECTIBLE_JUDAS_SHADOW,
CollectibleType.COLLECTIBLE_LIL_BRIMSTONE,
CollectibleType.COLLECTIBLE_LORD_OF_THE_PIT,
CollectibleType.COLLECTIBLE_MAW_OF_VOID,
CollectibleType.COLLECTIBLE_MOMS_KNIFE,
CollectibleType.COLLECTIBLE_SPIRIT_NIGHT,
CollectibleType.COLLECTIBLE_PACT
}

--Angel Pool Items Array--
CeremonialDagger.AngelItems = {
CollectibleType.COLLECTIBLE_BREATH_OF_LIFE,
CollectibleType.COLLECTIBLE_DEAD_SEA_SCROLLS,
CollectibleType.COLLECTIBLE_DELIRIOUS,
CollectibleType.COLLECTIBLE_EDENS_SOUL,
CollectibleType.COLLECTIBLE_PRAYER_CARD,
CollectibleType.COLLECTIBLE_BIBLE,
CollectibleType.COLLECTIBLE_VOID,
CollectibleType.COLLECTIBLE_CELTIC_CROSS,
CollectibleType.COLLECTIBLE_CENSER,
CollectibleType.COLLECTIBLE_CIRCLE_OF_PROTECTION,
CollectibleType.COLLECTIBLE_CROWN_OF_LIGHT,
CollectibleType.COLLECTIBLE_DEAD_DOVE,
CollectibleType.COLLECTIBLE_DUALITY,
CollectibleType.COLLECTIBLE_EUCHARIST,
CollectibleType.COLLECTIBLE_GODHEAD,
CollectibleType.COLLECTIBLE_GUARDIAN_ANGEL,
CollectibleType.COLLECTIBLE_HABit,
CollectibleType.COLLECTIBLE_HOLY_GRAIL,
CollectibleType.COLLECTIBLE_HOLY_LIGHT,
CollectibleType.COLLECTIBLE_HOLY_MANTLE,
CollectibleType.COLLECTIBLE_COLLECTIBLE_HOLY_WATER,
CollectibleType.COLLECTIBLE_IMMACULATE_CONCEPTION,
CollectibleType.COLLECTIBLE_MITRE,
CollectibleType.COLLECTIBLE_ROSARY,
CollectibleType.COLLECTIBLE_SACRED_HEART,
CollectibleType.COLLECTIBLE_SCAPULAR,
CollectibleType.COLLECTIBLE_SERAPHIM,
CollectibleType.COLLECTIBLE_SPEAR_OF_DESTINY,
CollectibleType.COLLECTIBLE_SWORN_PROTECTOR,
CollectibleType.COLLECTIBLE_BODY,
CollectibleType.COLLECTIBLE_HALO,
CollectibleType.COLLECTIBLE_MIND,
CollectibleType.COLLECTIBLE_RELIC,
CollectibleType.COLLECTIBLE_SOUL,
CollectibleType.COLLECTIBLE_WAFER,
CollectibleType.COLLECTIBLE_TRINITY_SHIELD
}

--Custom Trinkets Array--
CeremonialDagger.CustomTrinkets = {
GENESIS_ITEMS.TRINKET.RED_PENDANT,
GENESIS_ITEMS.TRINKET.HOLY_PENDANT,
GENESIS_ITEMS.TRINKET.PENDANT_OF_SHADOWS,
GENESIS_ITEMS.TRINKET.RITUAL_CANDLE,
GENESIS_ITEMS.TRINKET.BLOODY_CARD,
GENESIS_ITEMS.TRINKET.STAMP,
GENESIS_ITEMS.TRINKET.LOCKPICKS,
GENESIS_ITEMS.TRINKET.GLOWING_ROCK,
GENESIS_ITEMS.TRINKET.LOST_CD,
GENESIS_ITEMS.TRINKET.BROKEN_CLOCK,
GENESIS_ITEMS.TRINKET.BROKEN_MAP,
GENESIS_ITEMS.TRINKET.GOLDEN_CALF,
GENESIS_ITEMS.TRINKET.LASER_OPTIC,
GENESIS_ITEMS.TRINKET.MYSTERY_BOX,
GENESIS_ITEMS.TRINKET.SAFETY_PICKAXE,
GENESIS_ITEMS.TRINKET.ROTTEN_EGG,
GENESIS_ITEMS.TRINKET.UNCERTAIN,
GENESIS_ITEMS.TRINKET.GLOBE,
GENESIS_ITEMS.TRINKET.HOPE,
GENESIS_ITEMS.TRINKET.REMORSE,
GENESIS_ITEMS.TRINKET.CLOVER,
GENESIS_ITEMS.TRINKET.ASH,
GENESIS_ITEMS.TRINKET.KNIFE,
GENESIS_ITEMS.TRINKET.CHILI,
GENESIS_ITEMS.TRINKET.CHARGEDKEY,
GENESIS_ITEMS.TRINKET.BLOODYKEY,
GENESIS_ITEMS.TRINKET.BOSSPASS,
GENESIS_ITEMS.TRINKET.FIREEYE,
GENESIS_ITEMS.TRINKET.FASTFORWARD,
GENESIS_ITEMS.TRINKET.SKULL
}

--Dagger Logic--
function CeremonialDagger:DaggerLogic()
	local player = Isaac.GetPlayer(0)
	--Setting up the chance to remove one or two hearts for each item and the red pendant--
	if player:HasCollectible(GENESIS_ITEMS.ACTIVE.CEREMONIAL_DAGGER) or player:HasCollectible(GENESIS_ITEMS.ACTIVE.DAMAGED_CEREMONIAL_DAGGER) or player:HasCollectible(GENESIS_ITEMS.ACTIVE.BROKEN_CEREMONIAL_DAGGER) then
		if player:HasTrinket(GENESIS_ITEMS.TRINKET.RED_PENDANT) then
			CeremonialDagger.chance = 9999999999999
		elseif player:HasTrinket(GENESIS_ITEMS.TRINKET.PENDANT_OF_SHADOWS) then
			CeremonialDagger.chance = 1
		elseif player:HasCollectible(GENESIS_ITEMS.ACTIVE.CEREMONIAL_DAGGER) then
			CeremonialDagger.chance = 20
		elseif player:HasCollectible(GENESIS_ITEMS.ACTIVE.DAMAGED_CEREMONIAL_DAGGER) then
			CeremonialDagger.chance = 8
		elseif player:HasCollectible(GENESIS_ITEMS.ACTIVE.BROKEN_CEREMONIAL_DAGGER) then
			CeremonialDagger.chance = 3
		end
	end
	--Removing 3 soul hearts, if the player doesn't have any removing one or two red hearts, always one if the player has the red pendant--
	if player:HasCollectible(GENESIS_ITEMS.ACTIVE.CEREMONIAL_DAGGER) or player:HasCollectible(GENESIS_ITEMS.ACTIVE.DAMAGED_CEREMONIAL_DAGGER) or player:HasCollectible(GENESIS_ITEMS.ACTIVE.BROKEN_CEREMONIAL_DAGGER) then
		if player:GetSoulHearts() == 0 and player:GetHearts() <= 2 and player:GetMaxHearts () <= 2 then
			player:AddMaxHearts(-2, true)
			player:Kill()
		elseif player:GetSoulHearts() >= 1 and player:GetHearts() <= 2 then
				player:AddSoulHearts(-6)
			elseif math.random(1,CeremonialDagger.chance) == 1 then
				if player:GetSoulHearts() == 0 and player:GetHearts() <= 4 and player:GetMaxHearts () <= 4 then
					player:AddMaxHearts(-4, true)
					player:Kill()
					else
					player:AddMaxHearts(-4, true)
				end
			else
				player:AddMaxHearts(-2, true)
		end
		--Spawning the collectible, random devil one if the player doesn't have pendants, always two hearts one if the player has the shadow pendant and always angel ones if player has the holy pendant and playing sound--
		if player:HasTrinket(GENESIS_ITEMS.TRINKET.HOLY_PENDANT) then
			Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CeremonialDagger.AngelItems [math.random(1,#CeremonialDagger.AngelItems)], Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 1, true), Vector(0, 0), nil)
			local sound_entity = Isaac.Spawn(EntityType.ENTITY_FLY, 0, 0, Vector(320,300), Vector(0,0), nil):ToNPC()
			sound_entity:PlaySound(SoundEffect.SOUND_HOLY, 100, 0, false, 1)
			sound_entity:Remove()

		elseif player:HasTrinket(GENESIS_ITEMS.TRINKET.PENDANT_OF_SHADOWS) then
			Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CeremonialDagger.DoubleDevilItems [math.random(1,#CeremonialDagger.DoubleDevilItems)], Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 1, true), Vector(0, 0), nil)
			local sound_entity = Isaac.Spawn(EntityType.ENTITY_FLY, 0, 0, Vector(320,300), Vector(0,0), nil):ToNPC()
			sound_entity:PlaySound(SoundEffect.SOUND_DEVILROOM_DEAL, 100, 0, false, 1)
			sound_entity:Remove()
		else
			Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CeremonialDagger.DevilItems [math.random(1,#CeremonialDagger.DevilItems)], Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 1, true), Vector(0, 0), nil)
			local sound_entity = Isaac.Spawn(EntityType.ENTITY_FLY, 0, 0, Vector(320,300), Vector(0,0), nil):ToNPC()
			sound_entity:PlaySound(SoundEffect.SOUND_DEVILROOM_DEAL, 100, 0, false, 1)
			sound_entity:Remove()
		end
		--Removing current active item and changing it to the next state, Ceremonial->Damaged->Broken--
		if player:HasCollectible(GENESIS_ITEMS.ACTIVE.CEREMONIAL_DAGGER)	then
			player:RemoveCollectible(GENESIS_ITEMS.ACTIVE.CEREMONIAL_DAGGER)
			player:AddCollectible(GENESIS_ITEMS.ACTIVE.DAMAGED_CEREMONIAL_DAGGER, 0, false)

		elseif player:HasCollectible(GENESIS_ITEMS.ACTIVE.DAMAGED_CEREMONIAL_DAGGER) then
		player:RemoveCollectible(GENESIS_ITEMS.ACTIVE.DAMAGED_CEREMONIAL_DAGGER)
		player:AddCollectible(GENESIS_ITEMS.ACTIVE.BROKEN_CEREMONIAL_DAGGER, 0, false)

		else
		player:RemoveCollectible(GENESIS_ITEMS.ACTIVE.BROKEN_CEREMONIAL_DAGGER)
		end
	end
end

--Function to spawn the trinkets into the game--
function CeremonialDagger:SpawningFunction()
	local entities = Genesis.getRoomEntities()
	for i = 1, #entities do
		local e = entities[i]
	--Change Exitsiting Trinkets On Entering a room
		if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_TRINKET and e.SubType ~= CeremonialDagger.CustomTrinkets[1] and e.SubType ~= CeremonialDagger.CustomTrinkets[2] and e.SubType ~= CeremonialDagger.CustomTrinkets[3] and Game():GetRoom():IsFirstVisit() and Game():GetRoom():GetFrameCount() == 1 and math.random(1,CeremonialDagger.PendantChance) == 1 then
			e:Remove()
			Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TRINKET, CeremonialDagger.CustomTrinkets[math.random(1,#CeremonialDagger.CustomTrinkets)], e.Position, Vector(0, 0), nil)
		end
	--Spawn Trinkets from Mom's Box--
	local player = Isaac.GetPlayer(0)
		if player:GetLastActionTriggers() == ActionTriggers.ACTIONTRIGGER_ITEMACTIVATED and player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_BOX) then
			if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_TRINKET and e.SubType ~= CeremonialDagger.CustomTrinkets[1] and e.SubType ~= CeremonialDagger.CustomTrinkets[2] and e.SubType ~= CeremonialDagger.CustomTrinkets[3] and math.random(1,CeremonialDagger.PendantChance) == 1 then
				e:Remove()
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TRINKET, CeremonialDagger.CustomTrinkets[math.random(1,#CeremonialDagger.CustomTrinkets)], e.Position, Vector(0, 0), nil)
			end
		end
	end
	--On Entering room--
	if Game():GetFrameCount() == 1  then
    CeremonialDagger.HasTrinketSpawned = true
    CeremonialDagger.CurrentRoomSeed = Game():GetRoom():GetSpawnSeed()
	end
	if Game():GetRoom():GetSpawnSeed() ~= CeremonialDagger.CurrentRoomSeed then
    CeremonialDagger.HasTrinketSpawned = false
    CeremonialDagger.CurrentRoomSeed = Game():GetRoom():GetSpawnSeed()
	end
	--Spawn Trinkets in new completed rooms--
	if Game():GetRoom():IsFirstVisit() and Game():GetRoom():IsClear() and not CeremonialDagger.HasTrinketSpawned then
		CeremonialDagger.HasTrinketSpawned = true
		if math.random(1,CeremonialDagger.PendantChance) == 1 then
			Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TRINKET, CeremonialDagger.CustomTrinkets[math.random(1,#CeremonialDagger.CustomTrinkets)], Game():GetRoom():FindFreePickupSpawnPosition(Game():GetRoom():GetCenterPos(), 1, true), Vector(0, 0), nil)
		end
	end
end

-- Laser Optic --

function CeremonialDagger:onOptic()
  local player = Isaac.GetPlayer(0)
  local chance = math.random(1,40)
  local FireDir = player:GetFireDirection()
  if player:HasTrinket(Isaac.GetTrinketIdByName("Laser Optic")) and chance == 40 and FireDir > -1 then
	local entities = Isaac.GetRoomEntities()
	for i=1, #entities do
		if entities[i].Type == EntityType.ENTITY_TEAR and entities[i].Variant == TearVariant.BLUE then
			entities[i]:Remove()
			if FireDir == Direction.LEFT then
				player:FireTechLaser(player.Position,LaserOffset.LASER_TECH1_OFFSET,Vector(-10,0),false,false)
			elseif FireDir == Direction.RIGHT then
				player:FireTechLaser(player.Position,LaserOffset.LASER_TECH1_OFFSET,Vector(10,0),false,false)
			elseif FireDir == Direction.UP then
				player:FireTechLaser(player.Position,LaserOffset.LASER_TECH1_OFFSET,Vector(0,-10),false,false)
			elseif FireDir == Direction.DOWN then
				player:FireTechLaser(player.Position,LaserOffset.LASER_TECH1_OFFSET,Vector(0,10),false,false)
			end
		end
	end
  end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onOptic)

-- Mystery Box --

function CeremonialDagger:onMystery()
  local player = Isaac.GetPlayer(0)
  local FireDir = player:GetFireDirection()
  local entities = Isaac.GetRoomEntities()
  if player:HasTrinket(Isaac.GetTrinketIdByName("Mystery Box")) then
	  Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_COIN,0,player.Position,Vector(0,0),nil)
	  Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_HEART,0,player.Position,Vector(0,0),nil)
	  Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_KEY,0,player.Position,Vector(0,0),nil)
	  Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_COIN,0,player.Position,Vector(0,0),nil)
  end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, CeremonialDagger.onMystery)

-- Safety Pickaxe --

function CeremonialDagger:onSafety()
  local player = Isaac.GetPlayer(0)
  if player:HasTrinket(Isaac.GetTrinketIdByName("Safety Pickaxe")) then
		for i = 1, Game():GetRoom():GetGridSize() do
          local gridEntity = Game():GetRoom():GetGridEntity(i)
          if gridEntity ~= nil then
            if gridEntity.Desc.Type == GridEntityType.GRID_ROCK_BOMB then
                gridEntity:Destroy()
              end
            end
          end
        end
    end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, CeremonialDagger.onSafety)

-- Rotten Egg --

function CeremonialDagger:onRotten()
	local player = Isaac.GetPlayer(0)
	local FireDir = player:GetFireDirection()
	math.randomseed(tonumber(math.random(1,99) .. math.random(1,99) .. math.random(1,99)))
	local chance = math.random(1,60)
	if player:HasTrinket(Isaac.GetTrinketIdByName("Rotten Egg")) and chance == 60 and FireDir > -1 then
		local entities = Isaac.GetRoomEntities()
		for i=1, #entities do
			if entities[i].Type == EntityType.ENTITY_TEAR and entities[i].Variant == TearVariant.BLUE then
				entities[i]:Remove()
				if FireDir == Direction.LEFT then
					local tear = player:FireTear(player.Position,Vector(-10,0),false,false,false):ToTear()
					tear.TearFlags = TearFlags.TEAR_EXPLOSIVE + TearFlags.TEAR_FEAR
					tear:ChangeVariant(TearVariant.EGG)
					tear.Scale = Vector(1.2,1.2)
				elseif FireDir == Direction.RIGHT then
					local tear = player:FireTear(player.Position,Vector(10,0),false,false,false):ToTear()
					tear.TearFlags = TearFlags.TEAR_EXPLOSIVE + TearFlags.TEAR_FEAR
					tear:ChangeVariant(TearVariant.EGG)
					tear.Scale = Vector(1.2,1.2)
				elseif FireDir == Direction.UP then
					local tear = player:FireTear(player.Position,Vector(0,-10),false,false,false):ToTear()
					tear.TearFlags = TearFlags.TEAR_EXPLOSIVE + TearFlags.TEAR_FEAR
					tear:ChangeVariant(TearVariant.EGG)
					tear.Scale = Vector(1.2,1.2)
				elseif FireDir == Direction.DOWN then
					local tear = player:FireTear(player.Position,Vector(0,10),false,false,false):ToTear()
					tear.TearFlags = TearFlags.TEAR_EXPLOSIVE + TearFlags.TEAR_FEAR
					tear:ChangeVariant(TearVariant.EGG)
					tear.Scale = Vector(1.2,1.2)
				end
				break
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onRotten)

-- Uncertain --
local CurrentUncertainTrinket = -99
function CeremonialDagger:onUncertain()
	if CurrentUncertainTrinket == -99 then --load value
		if Genesis.moddata.UncertainID ~= nil then
			CurrentUncertainTrinket = Genesis.moddata.UncertainID
		else
			CurrentUncertainTrinket = -9
			Genesis.moddata.UncertainID = CurrentUncertainTrinket
		end
	end
	local player = Isaac.GetPlayer(0)
	if player:HasTrinket(Isaac.GetTrinketIdByName("Uncertain")) then
		CurrentUncertainTrinket = Isaac.GetTrinketIdByName("Uncertain")
    end
	if CurrentUncertainTrinket > 0 and player:HasTrinket(CurrentUncertainTrinket) == false then
		--find dropped trinket
		local entities = Isaac.GetRoomEntities()
		for i=1, #entities do
			local e = entities[i]
			if e.Type == 5 and e.Variant == 350 and e.SubType == CurrentUncertainTrinket then
				e:ToPickup():Morph(5, 350, Isaac.GetTrinketIdByName("Uncertain"), false)
				CurrentUncertainTrinket = -9
				break
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onUncertain)

function CeremonialDagger:onRoomUncertain()
	if Game():GetFrameCount() < 5 then
		CurrentUncertainTrinket = -9
		Genesis.moddata.UncertainID = CurrentUncertainTrinket
	elseif CurrentUncertainTrinket > 0 then
		Isaac.ExecuteCommand("remove t" .. CurrentUncertainTrinket)
		CurrentUncertainTrinket = math.random(1,154)
		Isaac.ExecuteCommand("g t" .. CurrentUncertainTrinket)
		Genesis.moddata.UncertainID = CurrentUncertainTrinket
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, CeremonialDagger.onRoomUncertain)





-- The Globe --
function CeremonialDagger:onGlobe()
	local player = Isaac.GetPlayer(0)
	if Game():GetFrameCount() < 5 or Genesis.moddata.GlobeUnused == nil then
		Genesis.moddata.GlobeUnused = true
	end
	if Genesis.moddata.GlobeUnused and player:HasTrinket(Isaac.GetTrinketIdByName("The Globe")) then
		if player:IsDead() and player.FrameCount > 5 then
			local level = Game():GetLevel()
			player:Revive()
			Genesis.moddata.GlobeUnused = false
			Isaac.ExecuteCommand("remove t" .. Isaac.GetTrinketIdByName("The Globe"))
			level:SetStage(math.random(1,8),math.random(1,3))
			local stage = level:GetStage()
			Game():StartStageTransition(false,stage)
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onGlobe)

-- Hope --
function CeremonialDagger:onHope()
	local player = Isaac.GetPlayer(0)
	if Game():GetFrameCount() < 5 or Genesis.moddata.HopeUnused == nil then
		Genesis.moddata.HopeUnused = true
	end
	if Genesis.moddata.HopeUnused and player:HasTrinket(Isaac.GetTrinketIdByName("The Hope")) then
		if player:IsDead() and player.FrameCount > 30 then
			local level = Game():GetLevel()
			player:Revive()
			Isaac.ExecuteCommand("remove t" .. Isaac.GetTrinketIdByName("The Hope"))
			--level:SetStage(10,0)
			--local stage = level:GetStage()
			--Game():StartStageTransition(true,stage)
			Isaac.ExecuteCommand("stage 10a")
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onHope)

-- Remorse --
function CeremonialDagger:onRemorse()
	local player = Isaac.GetPlayer(0)
	if Game():GetFrameCount() < 5 or Genesis.moddata.RemorseUnused == nil then
		Genesis.moddata.RemorseUnused = true
	end
	if Genesis.moddata.RemorseUnused and player:HasTrinket(Isaac.GetTrinketIdByName("The Remorse")) then
		if player:IsDead() and player.FrameCount > 5 then
			local level = Game():GetLevel()
			player:Revive()
			Isaac.ExecuteCommand("remove t" .. Isaac.GetTrinketIdByName("The Remorse"))
			level:SetStage(10,0)
			local stage = level:GetStage()
			Game():StartStageTransition(true,stage)
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onRemorse)


-- Rotten Clover --
local RottenCloverLuckSetting = 0
function CeremonialDagger:onClover()
	local player = Isaac.GetPlayer(0)
	if player:HasTrinket(Isaac.GetTrinketIdByName("Rotten Clover")) then
		RottenCloverLuckSetting = math.random(-10,10)
		player:AddCacheFlags(CacheFlag.CACHE_LUCK)
		player:EvaluateItems()
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, CeremonialDagger.onClover)

function CeremonialDagger:cloverCacheUpdate(player, cacheFlag)
	if cacheFlag == CacheFlag.CACHE_LUCK and player:HasTrinket(Isaac.GetTrinketIdByName("Rotten Clover")) then
		player.Luck = RottenCloverLuckSetting
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, CeremonialDagger.cloverCacheUpdate)

-- Ash of Gold --
function CeremonialDagger:onAsh()
	local player = Isaac.GetPlayer(0)
	if player:HasTrinket(Isaac.GetTrinketIdByName("Ash of Gold")) then
		local entities = Isaac.GetRoomEntities()
		for i=1, #entities do
			if entities[i].Type == EntityType.ENTITY_FIREPLACE then
				local chance = math.random(1,4)
				if not entities[i]:IsDead() and entities[i].HitPoints == 1.0 and chance == 1 then
					Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_COIN,0,entities[i].Position,Vector(0,0),nil)
					entities[i].HitPoints = 0
				end
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onAsh)


-- Pocket Knife --
function CeremonialDagger:onPocket()
	local player = Isaac.GetPlayer(0)
	if player:HasTrinket(Isaac.GetTrinketIdByName("Pocket Knife")) and Game():GetRoom():GetFrameCount() >= 25 and Game():GetRoom():GetFrameCount() <= 30 then
		local entities = Isaac.GetRoomEntities()
		for i=1, #entities do
			if entities[i]:IsVulnerableEnemy() or entities[i]:IsBoss() and not entities[i].Type == EntityType.ENTITY_FIREPLACE then
				local chance = math.random(1,80)
				if chance == 1 then
					entities[i]:AddEntityFlags(EntityFlag.FLAG_BLEED_OUT)
				end
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onPocket)


-- Fire Eye --

function CeremonialDagger:onFiree()
	local player = Isaac.GetPlayer(0)
	local entities = Isaac.GetRoomEntities()
	if player:HasTrinket(Isaac.GetTrinketIdByName("Fire Eye")) then
		for i=1, #entities do
			if entities[i].Type == EntityType.ENTITY_FIREPLACE and
				entities[i].Variant == 1 then
				entities[i]:Remove()
				Isaac.Spawn(EntityType.ENTITY_FIREPLACE, 0, 0, entities[i].Position, Vector(0,0),nil)
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, CeremonialDagger.onFiree)

-- Chili Con Carne --
chili = false

function CeremonialDagger:onChili(target , amount, flag, source, countdownframes)
   local player = Isaac.GetPlayer(0)
  local FireDir = player:GetFireDirection()
    if player:HasTrinket(Isaac.GetTrinketIdByName("Chili Con Carne")) then
          local chance = math.random(1,5)
        if chance == 5 then
        Isaac.Spawn(1000,EffectVariant.FART,1,player.Position,Vector(0,0),nil)
        chili=true
      end
    end
   end
CeremonialDagger:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, CeremonialDagger.onChili, EntityType.ENTITY_PLAYER)

function CeremonialDagger:onChiliUpdate()
  local player = Isaac.GetPlayer(0)
  if player:HasTrinket(Isaac.GetTrinketIdByName("Chili Con Carne")) then
    local entities = Isaac.GetRoomEntities()
    for i=1, #entities do
    if entities[i]:IsVulnerableEnemy() then
      if chili== true then
        if (entities[i].Position - player.Position):Length() < 50 then
        entities[i]:AddBurn(EntityRef(player),99,1)
      end
    end
  end
end
end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onChiliUpdate)

-- Bloody Key --

function CeremonialDagger:onBlood()
	local player = Isaac.GetPlayer(0)
	if player:HasTrinket(Isaac.GetTrinketIdByName("Bloody Key")) then
		for i = 0, 9 do
			local door = Game():GetRoom():GetDoor(i)
			if door ~= nil then
				if door:IsLocked() and (door.Position - player.Position):Length() < 50 then
					if player:GetKeys() == 0 and player:GetHearts() > 1 then
						player:TakeDamage(1,DamageFlag.DAMAGE_IV_BAG,EntityRef(player),0)
						player:AddKeys(1)
					end
				end
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onBlood)

-- Charged Key --

function CeremonialDagger:onChargeKey()
	local player = Isaac.GetPlayer(0)
	if player:HasTrinket(Isaac.GetTrinketIdByName("Charged Key")) then
		for i = 0, 9 do
			local door = Game():GetRoom():GetDoor(i)
			if door ~= nil then
				if door:IsLocked() and (door.Position - player.Position):Length() < 50 then
					if player:GetKeys() == 0 and player:GetActiveCharge() > 0 then
						player:DischargeActiveItem()
						player:AddKeys(1)
					end
				end
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onChargeKey)


-- Boss Pass --
function CeremonialDagger:onBossPass()
	local player = Isaac.GetPlayer(0)
	local room = Game():GetRoom()
	--annoyingly challenge rooms and boss challenge rooms use the same types, but found a command that should work
	if Game():GetLevel():HasBossChallenge() and player:HasTrinket(Isaac.GetTrinketIdByName("Boss Pass")) then
		for i = 0, 9 do
			local door = Game():GetRoom():GetDoor(i)
			if door ~= nil then
				if door:IsRoomType(RoomType.ROOM_CHALLENGE) and door:IsOpen() == false then
					door:SetLocked(false)
					door:TryUnlock(true)
					door:Open()
				end
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onBossPass)


-- Fast Forward --
function CeremonialDagger:onForward()
  local player = Isaac.GetPlayer(0)
  local game = Game()
if player:HasTrinket(Isaac.GetTrinketIdByName("Fast Forward")) then
MusicManager():PitchSlide(3)
		local seed = game:GetSeeds()
		player.MoveSpeed = 2
		player.ShotSpeed = 1.7
		local entities = Isaac.GetRoomEntities()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type > 8 or e.Type == 2 then
				e.Velocity = e.Velocity * 1.1
			end
		end
  end
  if not player:HasTrinket(Isaac.GetTrinketIdByName("Fast Forward")) then
end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onForward)

-- Enlightened Skull --
local ListOfSkullsInRoom = {}
function CeremonialDagger:onSkull()
	local player = Isaac.GetPlayer(0)
	if player:HasTrinket(Isaac.GetTrinketIdByName("Enlightened Skull")) and #ListOfSkullsInRoom > 1 then
		for i = 1, #ListOfSkullsInRoom do
			if ListOfSkullsInRoom[i] then
				local skull = Game():GetRoom():GetGridEntityFromPos(ListOfSkullsInRoom[i])
				if skull ~= nil and skull.State == 2 then
					local entities = Isaac.GetRoomEntities()
					for j=1, #entities do
						if entities[j] and entities[j].Type == EntityType.ENTITY_HOST and entities[j].Position:Distance(ListOfSkullsInRoom[i]) < 50 then
							entities[j]:Remove()
							table.remove(ListOfSkullsInRoom, i)
							break
						end
					end
				end
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.onSkull)

function CeremonialDagger:onSkullNewRoom()
	ListOfSkullsInRoom = {}
	local player = Isaac.GetPlayer(0)
	if player:HasTrinket(Isaac.GetTrinketIdByName("Enlightened Skull")) then
		for i = 1, Game():GetRoom():GetGridSize() do
			local gridEntity = Game():GetRoom():GetGridEntity(i)
			if gridEntity ~= nil then
				if gridEntity.Desc.Type == GridEntityType.GRID_ROCK_ALT and gridEntity ~= 2 then
					table.insert(ListOfSkullsInRoom, gridEntity.Position)
				end
			end
		end
	end
end
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, CeremonialDagger.onSkullNewRoom)

--Callbacks--
CeremonialDagger:AddCallback(ModCallbacks.MC_USE_ITEM, CeremonialDagger.DaggerLogic)
CeremonialDagger:AddCallback(ModCallbacks.MC_POST_UPDATE, CeremonialDagger.SpawningFunction)

----------
--TECH G--
----------

local techG = RegisterMod("Tech G",1)
techG.ALTERNATE_METHOD = true
techG.TEAR_LUDOVICO = 1<<55
techG.TECH_G_ITEM = GENESIS_ITEMS.ACTIVE.TECH_G
techG.TECH_G_COSTUME = GENESIS_COSTUMES.TECH_G_COSTUME
techG.TECH_G_MAXIMUM_RADIUS = 16 -- How big Tech G laser rings should be
techG.TECH_G_BRIMSTONE_MAXIMUM_RADIUS = 48 -- How big Tech G laser rings should be
techG.TECH_G_TIMEOUT_FRAMES = 30 -- The Tech G laser rings Timeout value
techG.TECH_G_INITIALIZATION_FRAMES = 8 -- How many frames it takes Tech G laser rings to grow to full size
techG.game = Game()
techG.activeRoomIndex = nil
techG.activeStageIndex = nil
techG.isTechGActive = false
techG.HasCostume = false

-- Replace normal tears and hide ludovico tears
if techG.ALTERNATE_METHOD == false then
	function techG.PostUpdate()
		local level = techG.game:GetLevel()
		local stageIndex = level:GetAbsoluteStage()
		local roomIndex = level:GetCurrentRoomIndex()
		if stageIndex ~= activeStageIndex or roomIndex ~= activeRoomIndex then
			techG.isTechGActive = false
			local player = Isaac.GetPlayer(0)
			player:AddCacheFlags(CacheFlag.CACHE_FLYING)
			player:EvaluateItems()
		end
		if techG.isTechGActive == true then
			local player = Isaac.GetPlayer(0)
			for i,entity in ipairs(Genesis.getRoomEntities()) do
				if entity.Type == EntityType.ENTITY_TEAR and entity.Parent and entity.Parent.Type == EntityType.ENTITY_PLAYER then
					local tear = entity:ToTear()
					-- Replace new, non ludo tears with TechX rings
					if entity.FrameCount <= 1 and tear.TearFlags & techG.TEAR_LUDOVICO == 0 then
						local techX = player:FireTechXLaser(entity.Position + entity.Velocity, entity.Velocity, techG.TECH_G_MAXIMUM_RADIUS)
						techX.TearFlags = techX.TearFlags | techG.TEAR_LUDOVICO
						entity:Remove()
					-- Track ludovico tears and create a TechX ring for those that don't have one
					elseif tear.TearFlags & techG.TEAR_LUDOVICO ~= 0 and (tear:GetData().techX == nil or tear:GetData().techX:Exists() == false ) then
						local techX = player:FireTechXLaser(entity.Position + entity.Velocity, entity.Velocity, techG.TECH_G_MAXIMUM_RADIUS)
						--tear.Child = techX
						tear:GetData().techX = techX
						techX.Parent = tear
					end
					-- If a tear has a TechX child referenced in its data, update it
					if tear:GetData().techX ~= nil then
						tear.Visible = false
						local techX = tear:GetData().techX
						techX.Velocity = (tear.Position + tear.Velocity) - techX.Position
						techX.Timeout = TECH_G_TIMEOUT_FRAMES
					end
				end
			end
		end
		activeRoomIndex = roomIndex
		activeStageIndex = stageIndex
	end
	techG:AddCallback(ModCallbacks.MC_POST_UPDATE, techG.PostUpdate)
end

-- Replace normal tears and hide ludovico tears
if techG.ALTERNATE_METHOD == true then
	function techG.PostUpdate()
		local level = techG.game:GetLevel()
		local stageIndex = level:GetAbsoluteStage()
		local roomIndex = level:GetCurrentRoomIndex()
		if stageIndex ~= activeStageIndex or roomIndex ~= activeRoomIndex then
			techG.isTechGActive = false
     local player = Isaac.GetPlayer(0)
     player:TryRemoveNullCostume(techG.TECH_G_COSTUME)
			player:AddCacheFlags(CacheFlag.CACHE_FLYING)
			player:EvaluateItems()
		end
		if techG.isTechGActive == true then
			local player = Isaac.GetPlayer(0)
			for i,entity in ipairs(Genesis.getRoomEntities()) do
				if entity.Type == EntityType.ENTITY_TEAR and entity.Parent and entity.Parent.Type == EntityType.ENTITY_PLAYER then
					local tear = entity:ToTear()
					local tearData = tear:GetData()
					if (tearData.techX == nil or tearData.techX:Exists() == false) then
						local techX = player:FireTechXLaser(entity.Position + entity.Velocity, entity.Velocity, 0)
						tear:GetData().techX = techX
						techX.Parent = tear
					end
					-- If a tear has a TechX child referenced in its data, update it
					if tear:GetData().techX ~= nil then
						local techX = tear:GetData().techX
						local effectiveness = math.min(1,techX.FrameCount/techG.TECH_G_INITIALIZATION_FRAMES)
						techX.Radius = techG.TECH_G_MAXIMUM_RADIUS * effectiveness
						-- Position SHOULD be set here since the Tech X ring automatically interpolates towards this Parent entity
						techX.Position = (tear.Position + tear.Velocity)
						techX.Timeout = techG.TECH_G_TIMEOUT_FRAMES
					end
				elseif entity.Type == EntityType.ENTITY_LASER and entity.Parent and entity.Parent.Type == EntityType.ENTITY_PLAYER then
					local tear = entity:ToLaser()
					local tearData = tear:GetData()
					if (tearData.techX == nil or tearData.techX:Exists() == false ) then
						local velocity = Vector.FromAngle(tear.Angle) * player.ShotSpeed * 10
						local techX = player:FireTechXLaser(entity.Position + entity.Velocity, velocity, techG.TECH_G_MAXIMUM_RADIUS)
						tearData.techX = techX
						techX.Parent = tear
					end
				end
			end
		end
		activeRoomIndex = roomIndex
		activeStageIndex = stageIndex
	end
	techG:AddCallback(ModCallbacks.MC_POST_UPDATE, techG.PostUpdate)
end

function techG:UseTechG()
	local level = techG.game:GetLevel()
	activeRoomIndex = level:GetCurrentRoomIndex()
	activeStageIndex = level:GetAbsoluteStage()
	Isaac.GetPlayer(0):AddNullCostume(techG.TECH_G_COSTUME)
	techG.isTechGActive = true
end
techG:AddCallback( ModCallbacks.MC_USE_ITEM, techG.UseTechG, techG.TECH_G_ITEM)

function techG:EvaluateCache(player, cacheFlag)
	if cacheFlag == CacheFlag.CACHE_FLYING and techG.isTechGActive then
		player.CanFly = true
	end
  end
techG:AddCallback( ModCallbacks.MC_EVALUATE_CACHE, techG.EvaluateCache)

function techG:PostPlayerInit(player)
	techG.isTechGActive = false
end
techG:AddCallback( ModCallbacks.MC_POST_PLAYER_INIT, techG.PostPlayerInit)

------------
--Hellbeam--
------------

local HellbeamMod = RegisterMod("Hellbeam", 1)

HellbeamMod.hellbeamexists = false
HellbeamMod.hellbeamdelay = 60
HellbeamMod.beamsize = 40
HellbeamMod.hellbeamcount = 1
HellbeamMod.PositionOffset = {}
HellbeamMod.Rotation = {}
HellbeamMod.r = 0
HellbeamMod.HasCostume = false

function HellbeamMod:HellbeamTarget(npc)
	math.randomseed(HellbeamMod.r + 1)
	HellbeamMod.r = HellbeamMod.r + 1
	local game = Game()
	local room = game:GetRoom()
	local player = Isaac.GetPlayer(0)
	local e = Genesis.getRoomEntities()
	HellbeamMod.beamsize = 40
	HellbeamMod.hellbeamexists = true
	npc.Velocity = player:GetShootingJoystick()*(8*player.ShotSpeed)
	if npc.FrameCount == HellbeamMod.hellbeamdelay then
		HellbeamMod.PositionOffset = {}
		HellbeamMod.Rotation = {}
		for i=1, HellbeamMod.hellbeamcount do
			local hellbeam = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.HUSH_LASER_UP, 0, npc.Position, npc.Velocity, npc)
			if i ~= 1 then
				table.insert(HellbeamMod.PositionOffset, Vector(math.cos(0),math.sin(0))*40)
				table.insert(HellbeamMod.Rotation, (6/(HellbeamMod.hellbeamcount-1))*(i-1))
			else
				table.insert(HellbeamMod.PositionOffset, Vector(0,0))
				table.insert(HellbeamMod.Rotation, 0)
			end
			hellbeam.Position = npc.Position + Vector(0,1) + HellbeamMod.PositionOffset[i]
			hellbeam.Velocity = npc.Velocity
			local sprite = hellbeam:GetSprite()
			sprite.Color = Color(0,0,0,1,255,0,0)
			if player:HasCollectible(GENESIS_ITEMS.PASSIVE.PONGER) then sprite.Color = Color(0,0,0,1,0,255,0) end
			if player:HasCollectible(CollectibleType.COLLECTIBLE_NUMBER_ONE) then sprite.Color = Color(0,0,0,1,255,255,0) end
			if player:HasCollectible(331) or player:HasCollectible(182) or player:HasCollectible(3) then sprite.Color = Color(0,0,0,1,255,0,255) end
			sprite:Play("Start", true)
			SFXManager():Play(SoundEffect.SOUND_BLOOD_LASER_LARGE, 1, 0, false, 1)
			if player:HasCollectible(CollectibleType.COLLECTIBLE_EPIC_FETUS) then
				Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ROCKET, 0, npc.Position + Vector(0,2), Vector(0,0), player)
			end
			if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) then
				local rand = math.random(0,1)
				if rand == 0 then
					player:FireTechLaser(npc.Position, 0, Vector(0,1), false, true)
					player:FireTechLaser(npc.Position, 0, Vector(0,-1), false, true)
					player:FireTechLaser(npc.Position, 0, Vector(1,0), false, true)
					player:FireTechLaser(npc.Position, 0, Vector(-1,0), false, true)
				end
				if rand == 1 then
					player:FireTechLaser(npc.Position, 0, Vector(1,1), false, true)
					player:FireTechLaser(npc.Position, 0, Vector(1,-1), false, true)
					player:FireTechLaser(npc.Position, 0, Vector(-1,-1), false, true)
					player:FireTechLaser(npc.Position, 0, Vector(-1,1), false, true)
				end
			end
			if player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) and not player:HasCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE) then
				player:FireTechXLaser(npc.Position, Vector(0,0), 50)
			end
		end
	end
	if npc.FrameCount >= HellbeamMod.hellbeamdelay then
		local num = 0
		local numtech = 0
		for i=1, #e do
			if e[i].SpawnerType == GENESIS_ENTITIES.HELLBEAM_TARGET and e[i].Type == EntityType.ENTITY_EFFECT then
				num = num + 1
				if num ~= 1 then
					HellbeamMod.Rotation[num] = HellbeamMod.Rotation[num] + 0.2
					HellbeamMod.PositionOffset[num] = Vector(math.cos(HellbeamMod.Rotation[num]),math.sin(HellbeamMod.Rotation[num]))*40
				end
				local sprite = e[i]:GetSprite()
				sprite.Color = Color(0,0,0,1,255,0,0)
				if player:HasCollectible(GENESIS_ITEMS.PASSIVE.PONGER) then sprite.Color = Color(0,0,0,1,0,255,0) end
				if player:HasCollectible(CollectibleType.COLLECTIBLE_NUMBER_ONE) then sprite.Color = Color(0,0,0,1,255,255,0) end
				if player:HasCollectible(331) or player:HasCollectible(182) or player:HasCollectible(3) then sprite.Color = Color(0,0,0,1,255,0,255) end
				if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) then
					HellbeamMod.beamsize = 20
					sprite.Scale = Vector(0.5,0.5)
					if e[i].FrameCount == 30 then
						local rand = math.random(0,1)
						if rand == 0 then
							player:FireTechLaser(e[i].Position, 0, Vector(0,1), false, true)
							player:FireTechLaser(e[i].Position, 0, Vector(0,-1), false, true)
							player:FireTechLaser(e[i].Position, 0, Vector(1,0), false, true)
							player:FireTechLaser(e[i].Position, 0, Vector(-1,0), false, true)
						end
						if rand == 1 then
							player:FireTechLaser(e[i].Position, 0, Vector(1,1), false, true)
							player:FireTechLaser(e[i].Position, 0, Vector(1,-1), false, true)
							player:FireTechLaser(e[i].Position, 0, Vector(-1,-1), false, true)
							player:FireTechLaser(e[i].Position, 0, Vector(-1,1), false, true)
						end
					end
				end
				if player:HasCollectible(CollectibleType.COLLECTIBLE_ABADDON) then
					HellbeamMod.beamsize = 70
					sprite.Scale = Vector(2,2)
				end
				if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) and player:HasCollectible(CollectibleType.COLLECTIBLE_ABADDON)	then
					HellbeamMod.beamsize = 40
					sprite.Scale = Vector(1,1)
				end
				e[i].Velocity = npc.Velocity
				e[i].Position = npc.Position + Vector(0,1) + HellbeamMod.PositionOffset[num]
				if npc.FrameCount == HellbeamMod.hellbeamdelay+7 then sprite:Play("Loop", true) end
				if npc.FrameCount == HellbeamMod.hellbeamdelay+13 then sprite:Play("End", true) end
				if npc.FrameCount == HellbeamMod.hellbeamdelay+20 then e[i]:Remove() end
				room:DamageGrid(room:GetGridIndex(e[i].Position), 1)
				for q=1, #e do
					if e[q]:IsVulnerableEnemy() and (e[i].Position - e[q].Position):Length() <= HellbeamMod.beamsize then
						e[q]:TakeDamage(player.Damage,EntityFlag.FLAG_POISON,EntityRef(player),0)
					elseif e[q].Type == EntityType.ENTITY_FIREPLACE and (e[i].Position - e[q].Position):Length() <= HellbeamMod.beamsize then
						e[q]:TakeDamage(player.Damage,EntityFlag.FLAG_POISON,EntityRef(player),0)
					elseif e[q].Type == EntityType.ENTITY_MOVABLE_TNT and (e[i].Position - e[q].Position):Length() <= HellbeamMod.beamsize then
						e[q]:TakeDamage(player.Damage,EntityFlag.FLAG_POISON,EntityRef(player),0)
					end
				end
			end
			if player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) and e[i].Type == EntityType.ENTITY_LASER and e[i].Variant == 2 then
				numtech = numtech + 1
				if numtech ~= 1 then
					HellbeamMod.PositionOffset[numtech] = Vector(math.cos(HellbeamMod.Rotation[numtech]),math.sin(HellbeamMod.Rotation[numtech]))*40
				end
				e[i].Position = npc.Position + Vector(0,1) + HellbeamMod.PositionOffset[num]
				if npc.FrameCount == HellbeamMod.hellbeamdelay+20 then e[i]:Remove() end
			end
			if e[i].Type == EntityType.ENTITY_EFFECT and e[i].Variant == EffectVariant.ROCKET then
				if e[i].FrameCount == 1 then
					Isaac.Explode(e[i].Position, npc, player.Damage*2)
					e[i]:Remove()
				end
			end
		end
	end
	local nearestenemy = 40
	for i=1, #e do
		if player:HasCollectible(331) or player:HasCollectible(182) or player:HasCollectible(3) then
			if e[i]:IsVulnerableEnemy() and (npc.Position - e[i].Position):Length() < nearestenemy then
				nearestenemy = (npc.Position - e[i].Position):Length()
				npc.Velocity = npc.Velocity + e[i].Velocity
			end
		end
	end
	if npc.FrameCount == HellbeamMod.hellbeamdelay+30 then npc:Remove() end
end

function HellbeamMod:Update()
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.HELLBEAM) then
		if player:GetShootingJoystick().X + player:GetShootingJoystick().Y ~= 0 and not HellbeamMod.hellbeamexists then
			HellbeamMod.hellbeamexists = true
			HellbeamMod.hellbeamdelay = math.ceil(player.MaxFireDelay/0.10)
			local npc = Isaac.Spawn(GENESIS_ENTITIES.HELLBEAM_TARGET, 0, 0, player.Position, Vector(0,0), player)
			npc:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
		end
		player:SetShootingCooldown(2)
		local sprite = player:GetSprite()
		sprite:SetOverlayFrame("HeadDown", 1)
	end
	HellbeamMod.hellbeamexists = false
	HellbeamMod.hellbeamcount = 1
	for i=1, player:GetCollectibleNum(CollectibleType.COLLECTIBLE_20_20) do HellbeamMod.hellbeamcount = HellbeamMod.hellbeamcount + 1 end
	for i=1, player:GetCollectibleNum(CollectibleType.COLLECTIBLE_BRIMSTONE) do HellbeamMod.hellbeamcount = HellbeamMod.hellbeamcount + 1 end
	for i=1, player:GetCollectibleNum(GENESIS_ITEMS.PASSIVE.PONGER) do HellbeamMod.hellbeamcount = HellbeamMod.hellbeamcount + 1 end
	for i=1, player:GetCollectibleNum(CollectibleType.COLLECTIBLE_THE_WIZ) do HellbeamMod.hellbeamcount = HellbeamMod.hellbeamcount + 1 end
	for i=1, player:GetCollectibleNum(CollectibleType.COLLECTIBLE_INNER_EYE) do HellbeamMod.hellbeamcount = HellbeamMod.hellbeamcount + 2 end
	for i=1, player:GetCollectibleNum(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) do HellbeamMod.hellbeamcount = HellbeamMod.hellbeamcount + 3 end
end

function HellbeamMod:EvalCache(player, CacheFlag)
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.HELLBEAM) and HellbeamMod.HasCostume ~= true then
    player:AddNullCostume(GENESIS_COSTUMES.HELLBEAM_COSTUME)
    player:AddNullCostume(Isaac.GetCostumeIdByPath("gfx/characters/hellbeam_body.anm2"))
    HellbeamMod.HasCostume = true
  end
  if not player:HasCollectible(GENESIS_ITEMS.PASSIVE.HELLBEAM) and HellbeamMod.HasCostume == true then
    player:TryRemoveNullCostume(GENESIS_COSTUMES.HELLBEAM_COSTUME)
    player:TryRemoveNullCostume(Isaac.GetCostumeIdByPath("gfx/characters/hellbeam_body.anm2"))
    HellbeamMod.HasCostume = false
end
end
HellbeamMod:AddCallback(ModCallbacks.MC_NPC_UPDATE, HellbeamMod.HellbeamTarget, GENESIS_ENTITIES.HELLBEAM_TARGET)
HellbeamMod:AddCallback(ModCallbacks.MC_POST_UPDATE, HellbeamMod.Update)
HellbeamMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, HellbeamMod.EvalCache)

--------------
--Poison Ray--
--------------

local PoisonMod = RegisterMod("Poison Ray", 1)
PoisonMod.prsprite = Sprite()
PoisonMod.prsprite:Load("gfx/PoisonRayBeam.anm2", true)
PoisonMod.prsprite:Play("ShootDown", true)
PoisonMod.HasCostume = false

function PoisonMod:Update()
	PoisonMod.prsprite:Update()
	PoisonMod.prsprite:LoadGraphics()
    local player = Isaac.GetPlayer(0)
    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.POISON_RAY) then
        local e = Genesis.getRoomEntities()
        if player:GetHeadDirection() == Direction.RIGHT then
            for i=1, #e do
                if e[i]:IsVulnerableEnemy() and e[i].Position.X >= player.Position.X and e[i].Position.Y <= player.Position.Y + 30 and e[i].Position.Y >= player.Position.Y - 30 then
					e[i]:TakeDamage(0.3,EntityFlag.FLAG_POISON,EntityRef(player),0)
					local sprite = e[i]:GetSprite()
					sprite.Color = Color(1,1,1,1,0,150,0)
                end
            end
			if not PoisonMod.prsprite:IsPlaying("ShootRight") then
				PoisonMod.prsprite:Play("ShootRight", true)
			end
        elseif player:GetHeadDirection() == Direction.UP then
            for i=1, #e do
                if e[i]:IsVulnerableEnemy() and e[i].Position.Y <= player.Position.Y and e[i].Position.X <= player.Position.X + 30 and e[i].Position.X >= player.Position.X - 30 then
                    e[i]:TakeDamage(0.3,EntityFlag.FLAG_POISON,EntityRef(player),0)
					local sprite = e[i]:GetSprite()
					sprite.Color = Color(1,1,1,1,0,150,0)
                end
            end
			if not PoisonMod.prsprite:IsPlaying("ShootUp") then
				PoisonMod.prsprite:Play("ShootUp", true)
			end
        elseif player:GetHeadDirection() == Direction.LEFT then
            for i=1, #e do
                if e[i]:IsVulnerableEnemy() and e[i].Position.X <= player.Position.X and e[i].Position.Y <= player.Position.Y + 30 and e[i].Position.Y >= player.Position.Y - 30 then
                    e[i]:TakeDamage(0.3,EntityFlag.FLAG_POISON,EntityRef(player),0)
					local sprite = e[i]:GetSprite()
					sprite.Color = Color(1,1,1,1,0,150,0)
                end
            end
			if not PoisonMod.prsprite:IsPlaying("ShootLeft") then
				PoisonMod.prsprite:Play("ShootLeft", true)
			end
        elseif player:GetHeadDirection() == Direction.DOWN or player:GetHeadDirection() == Direction.NO_DIRECTION then
            for i=1, #e do
                if e[i]:IsVulnerableEnemy() and e[i].Position.Y >= player.Position.Y and e[i].Position.X <= player.Position.X + 30 and e[i].Position.X >= player.Position.X - 30 then
                    e[i]:TakeDamage(0.3,EntityFlag.FLAG_POISON,EntityRef(player),0)
					local sprite = e[i]:GetSprite()
					sprite.Color = Color(1,1,1,1,0,150,0)
                end
            end
			if not PoisonMod.prsprite:IsPlaying("ShootDown") then
				PoisonMod.prsprite:Play("ShootDown", true)
			end
        end
    end
end

function PoisonMod:Render()
	local player = Isaac.GetPlayer(0)
	local game = Game()
	local room = game:GetRoom()
    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.POISON_RAY) and room:GetFrameCount() >= 1 then
        PoisonMod.prsprite:Render(room:WorldToScreenPosition(player.Position), Vector(0,0), Vector(0,0))
    end
end

function PoisonMod:EvalCache(player, CacheFlag)
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.POISON_RAY) and PoisonMod.HasCostume == false then
    player:AddNullCostume(GENESIS_COSTUMES.POISONRAY_COSTUME)
    PoisonMod.HasCostume = true
end
  if not player:HasCollectible(GENESIS_ITEMS.PASSIVE.POISON_RAY) and PoisonMod.HasCostume == true then
    player:TryRemoveNullCostume(GENESIS_COSTUMES.POISONRAY_COSTUME)
    PoisonMod.HasCostume = false
end
end

PoisonMod:AddCallback(ModCallbacks.MC_POST_UPDATE, PoisonMod.Update)
PoisonMod:AddCallback(ModCallbacks.MC_POST_RENDER, PoisonMod.Render)
PoisonMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, PoisonMod.EvalCache)

------------
--SOUL KEY--
------------

--Setting up general variables--
local SoulKey = RegisterMod("Soul Key", 1)
SoulKey.Current_Keys = 0
SoulKey.Previous_Keys = 0

--Soul Key Trinket Logic--
function SoulKey:KeyLogic()
	local player = Isaac.GetPlayer(0)
	--Check if player has the trinket--
	if player:HasTrinket(GENESIS_ITEMS.TRINKET.SOUL_KEY) then
		SoulKey.Current_Keys = player:GetNumKeys()
		--Check if player's current key number is lower than the numer he had before the update and roll a probability--
		if SoulKey.Current_Keys < SoulKey.Previous_Keys and math.random(1,20) == 1 then
			--Spawn the heart-
			Isaac.Spawn(5, 10, 3, Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 1, true), Vector(0, 0), nil)
		end
		--Get the number of keys the player has at the end of the update--
	SoulKey.Previous_Keys = player:GetNumKeys()
	--Checks if player wants to use a key on a chest, removes a soul heart and gives player a key if player doesn't have keys--
		local entities = Genesis.getRoomEntities()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type == EntityType.ENTITY_PICKUP and (e.Variant == PickupVariant.PICKUP_LOCKEDCHEST or e.Variant == PickupVariant.PICKUP_ETERNALCHEST) and e.SubType ~= ChestSubType.CHEST_OPENED  and  e.Position:Distance(player.Position) < 20 and  player:GetNumKeys() == 0 and player:GetSoulHearts() >= 2 and player:GetBlackHearts() == 0  then
				player:AddKeys(1)
				player:AddSoulHearts(-2)
			end
		end
	--Checks if the player wants to use a key on a room, removes a soul heart and gives player a key if player doesn't have any--
		local doorID = {Game():GetRoom():GetDoor(DoorSlot.RIGHT0), Game():GetRoom():GetDoor(DoorSlot.RIGHT1), Game():GetRoom():GetDoor(DoorSlot.UP0), Game():GetRoom():GetDoor(DoorSlot.UP1), Game():GetRoom():GetDoor(DoorSlot.DOWN0), Game():GetRoom():GetDoor(DoorSlot.DOWN1), Game():GetRoom():GetDoor(DoorSlot.LEFT0), Game():GetRoom():GetDoor(DoorSlot.LEFT1)}
		for i = 1, 8 do
			if doorID[i] ~= nil and player:GetNumKeys() == 0 and player:GetSoulHearts() >= 2 and player:GetBlackHearts() == 0 and doorID[i].Position:Distance(player.Position) < 40 and doorID[i]:IsLocked() then
				player:AddKeys(1)
				player:AddSoulHearts(-2)
			end
		end
	end
end

--Callbacks--
SoulKey:AddCallback(ModCallbacks.MC_POST_RENDER, SoulKey.KeyLogic)

-------------------
--Beginner's Luck--
-------------------

local BeginnersLuckMod = RegisterMod("BeginnersLuck", 1)

function BeginnersLuckMod:CacheUpdate(player, cacheFlag)
    if cacheFlag == CacheFlag.CACHE_LUCK then
        local count = player:GetCollectibleNum(GENESIS_ITEMS.PASSIVE.BEGINNERS_LUCK)
        if count > 0 then
            local game = Game()
            local stage = game:GetLevel():GetStage()

            local Multiplier
            if game:IsGreedMode() then
                Multiplier = 12 - stage * 2
            else
                Multiplier = 11 - stage
            end

            if Multiplier > 0 then
                player.Luck = player.Luck + Multiplier * count
            end
		end
	end
end

function BeginnersLuckMod:OnLevel()
    for i = 0,3 do
        local player = Isaac.GetPlayer(i)
        if player:HasCollectible(GENESIS_ITEMS.PASSIVE.BEGINNERS_LUCK) then
		    player:AddCacheFlags(CacheFlag.CACHE_LUCK)
            player:EvaluateItems()
        end
	end
end

BeginnersLuckMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE,BeginnersLuckMod.CacheUpdate)
BeginnersLuckMod:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, BeginnersLuckMod.OnLevel)

---------------
--BYOUXS HEAD--
---------------

local ByouxMod = RegisterMod("Byoux's Head",1)

function ByouxMod:SpawnDiamondExplosion(position, spawner)
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(CollectibleType.COLLECTIBLE_DR_FETUS) then
		Isaac.Explode(position, player, 50)
	elseif not player:HasCollectible(CollectibleType.COLLECTIBLE_DR_FETUS) then
		local DiamondExplosion = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BOMB_EXPLOSION , 0, position, Vector(0,0), spawner)
		if DiamondExplosion.FrameCount == 0 then
			DiamondExplosion.Color = spawner.Color
		end
	end
end

function ByouxMod:SpawnDiamond(position, velocity)
	local player = Isaac.GetPlayer(0)
	local tear = Isaac.Spawn(2, 6514, 0, position, velocity, player):ToTear()
	tear.Height = -1 * (math.random(500,700))
	tear.FallingAcceleration = 5
	tear.CollisionDamage = 0
	tear.Color = Color(1,1,1,1,math.random(255),math.random(255),math.random(255))
	tear.TearFlags = SetBit(tear.TearFlags, Bit(2))
	tear.TearFlags = SetBit(tear.TearFlags, Bit(1))
	tear:GetSprite():PlayRandom(math.random(1,4))
end

function ByouxMod:SpawnDiamondGib(position, spawner)
	local player = Isaac.GetPlayer(0)
	local DiamondGibs = Isaac.Spawn(EntityType.ENTITY_EFFECT, 6514 , 0, position, Vector(math.random(-15,15),-10), spawner)
	if DiamondGibs.FrameCount == 0 then
		DiamondGibs:GetSprite():PlayRandom(math.random(1,8))
		DiamondGibs:GetSprite():Stop()
		DiamondGibs.Color = spawner.Color
	end
end

function ByouxMod:OnUseByoux()
	Game():ShakeScreen(30)
	if Game():GetRoom():GetAliveEnemiesCount() > 0 then
		local entities = Genesis.getRoomEntities()
		for i = 1, #entities do
			if entities[i]:IsVulnerableEnemy() and not EntityRef(entities[i]).IsFriendly then
				ByouxMod:SpawnDiamond(entities[i].Position, entities[i].Velocity / 2)
			end
		end
	elseif Game():GetRoom():GetAliveEnemiesCount() == 0 then
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
		ByouxMod:SpawnDiamond(Game():GetRoom():GetRandomPosition(0.6), Vector(0,0))
	end
	SFXManager():Play(SoundEffect.SOUND_ULTRA_GREED_COINS_FALLING , 1, 0, false, 3)
	return true
end

function ByouxMod:DiamondLogic()
	local player = Isaac.GetPlayer(0)
	local entities = Genesis.getRoomEntities()
	for i = 1, #entities do
		if entities[i].Type == 2 and entities[i].Variant == 6514 then
			local Diamond = entities[i]:ToTear()
				if Diamond.Height >= -4 then
				ByouxMod:SpawnDiamondGib(entities[i].Position - Vector(math.random(-10,10),math.random(-10,10)), entities[i])
				ByouxMod:SpawnDiamondGib(entities[i].Position - Vector(math.random(-10,10),math.random(-10,10)), entities[i])
				ByouxMod:SpawnDiamondGib(entities[i].Position - Vector(math.random(-10,10),math.random(-10,10)), entities[i])
				ByouxMod:SpawnDiamondGib(entities[i].Position - Vector(math.random(-10,10),math.random(-10,10)), entities[i])
				ByouxMod:SpawnDiamondExplosion(entities[i].Position, entities[i])
				SFXManager():Play(SoundEffect.SOUND_POT_BREAK , 1, 0, false, 1)
				entities[i]:Remove()
			end
			if Diamond.Height >= -50 then
				Diamond.TearFlags = ClearBit(Diamond.TearFlags, Bit(2))
				Diamond.TearFlags = ClearBit(Diamond.TearFlags, Bit(1))
				Diamond.CollisionDamage = 0.1
			end
		end
	end
end

function ByouxMod:DiamondGibsLogic()
	local player = Isaac.GetPlayer(0)
	local entities = Genesis.getRoomEntities()
	for i = 1, #entities do
		if entities[i].Type == 1000 and entities[i].Variant == 6514 then
			if entities[i].Velocity.Y < 10 and entities[i]:GetData().Resting ~= true then
				entities[i].Velocity = entities[i].Velocity + Vector(0, 1)
			end
			if entities[i].Velocity.X < 0 then
				entities[i].Velocity = entities[i].Velocity + Vector(1, 0)
			end
			if entities[i].Velocity.X > 0 then
				entities[i].Velocity = entities[i].Velocity + Vector(-1, 0)
			end
			if entities[i].Velocity.Y >= 10 then
				entities[i].Velocity = Vector(entities[i].Velocity.X, 0)
				entities[i]:GetData().Resting = true
			end
		end
	end
end

function ByouxMod:DiamondLogicEnemy(entity, amt, flag, source, countdown)
	local player = Isaac.GetPlayer(0)
	if source.Type == 2 and source.Variant == 6514 then
		if not entity:IsBoss() and entity:IsVulnerableEnemy() and not player:HasCollectible(CollectibleType.COLLECTIBLE_DR_FETUS) then
			entity:AddCharmed(-1)
		end
		ByouxMod:SpawnDiamondGib(source.Position - Vector(math.random(-10,10),math.random(-10,10)), source.Entity)
		ByouxMod:SpawnDiamondGib(source.Position - Vector(math.random(-10,10),math.random(-10,10)), source.Entity)
		ByouxMod:SpawnDiamondGib(source.Position - Vector(math.random(-10,10),math.random(-10,10)), source.Entity)
		ByouxMod:SpawnDiamondGib(source.Position - Vector(math.random(-10,10),math.random(-10,10)), source.Entity)
		ByouxMod:SpawnDiamondExplosion(source.Position, source.Entity)
		SFXManager():Play(SoundEffect.SOUND_POT_BREAK , 1, 0, false, 1)
	end
end

ByouxMod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, ByouxMod.DiamondLogicEnemy)
ByouxMod:AddCallback(ModCallbacks.MC_USE_ITEM, ByouxMod.OnUseByoux, GENESIS_ITEMS.ACTIVE.BYOUXS_HEAD)
ByouxMod:AddCallback(ModCallbacks.MC_POST_UPDATE, ByouxMod.DiamondLogic)
ByouxMod:AddCallback(ModCallbacks.MC_POST_RENDER, ByouxMod.DiamondGibsLogic)

--------------
--CUTE BOMBS--
--------------

local CuteBombsMod = RegisterMod("Cute Bombs", 1)
CuteBombsMod.AddedBombs = false
CuteBombsMod.CharmDuration = 150

function CuteBombsMod:onInit()
	CuteBombsMod.AddedBombs = false
end

function CharmEnemies(duration)
	local player = Isaac.GetPlayer(0)
	local rng = player:GetCollectibleRNG(GENESIS_ITEMS.PASSIVE.CUTE_BOMBS)
	local entities = Genesis.getRoomEntities()
	for i=1,#entities do
		if entities[i]:IsVulnerableEnemy() then
			if rng:RandomInt(10) == 1 then
				entities[i]:AddCharmed(-1)
			else
				entities[i]:AddCharmed(CuteBombsMod.CharmDuration)
			end
		end
	end
end

function CuteBombsMod:onUpdate()
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.CUTE_BOMBS) == true then
		if CuteBombsMod.AddedBombs == false then
			player:AddBombs(5)
			CuteBombsMod.AddedBombs = true
		end
		local entities = Genesis.getRoomEntities()
		for i=1,#entities do
			if entities[i].Type == EntityType.ENTITY_BOMBDROP and entities[i].SpawnerType == EntityType.ENTITY_PLAYER then
				local sprite = entities[i]:GetSprite()
				if entities[i].FrameCount  == 1 then
					sprite:Load("gfx/cute_bomb.anm2",false)
					sprite:LoadGraphics()
				end
				if sprite:IsPlaying("Explode") then
					CharmEnemies()
				end
			end
		end
	end
end

CuteBombsMod:AddCallback(ModCallbacks.MC_POST_UPDATE, CuteBombsMod.onUpdate)
CuteBombsMod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, CuteBombsMod.onInit)

------------------
--LUCIFER WALLET--
------------------

local lwallet = RegisterMod("LuciferWallet", 1)
lwallet.had = false
lwallet.delay = 0

StageAPI.AddEntityPersistenceData({
    Type = GENESIS_ENTITIES.MONEY_DEAL,
    Variant = GENESIS_ENTITIES.VARIANT.MONEY_DEAL,
    RemoveOnRemove = true
})

function lwallet:MakeWalletDeal(item)
    local player = Isaac.GetPlayer(0)
    local itemId = item.SubType
    player:GetEffects():AddCollectibleEffect(itemId, false)
    local eff = Isaac.GetPlayer(0):GetEffects():GetCollectibleEffect(itemId)
    local file = eff.Item.GfxFileName
    if Game():GetLevel():GetCurses() == 64 then
      file = "gfx/items/collectibles/questionmark.png"
    end
    --Isaac.DebugString("Is curse of blind? : " .. tostring(eff.Item.GfxFileName))
    local devilPrice = eff.Item.DevilPrice
    player:GetEffects():RemoveCollectibleEffect(itemId)
    local deal = Isaac.Spawn(GENESIS_ENTITIES.MONEY_DEAL, GENESIS_ENTITIES.VARIANT.MONEY_DEAL, itemId, item.Position, item.Velocity, item)
    deal:ClearEntityFlags(EntityFlag.FLAG_APPEAR)

    deal:GetSprite():ReplaceSpritesheet(3, file)
    deal:GetSprite():LoadGraphics()
    deal:GetSprite():SetAnimation("NumbersWhite")

    Isaac.DebugString(tostring(deal))
    local steam_sale_num = player:GetCollectibleNum(CollectibleType.COLLECTIBLE_STEAM_SALE)
    if steam_sale_num == 0 then
      if (devilPrice == 1) then
          deal:ToNPC().I1 = 15
          deal:GetSprite():SetLayerFrame(0, 11)
          deal:GetSprite():SetLayerFrame(1, 1)
          deal:GetSprite():SetLayerFrame(2, 5)
      elseif (devilPrice == 2) then
          deal:ToNPC().I1 = 30
          deal:GetSprite():SetLayerFrame(0, 11)
          deal:GetSprite():SetLayerFrame(1, 3)
          deal:GetSprite():SetLayerFrame(2, 0)
      end
    elseif steam_sale_num == 1 then
      if (devilPrice == 1) then
          deal:ToNPC().I1 = 7
          deal:GetSprite():SetLayerFrame(0, 12)
          deal:GetSprite():SetLayerFrame(1, 7)
          deal:GetSprite():SetLayerFrame(2, 10)
      elseif (devilPrice == 2) then
          deal:ToNPC().I1 = 15
          deal:GetSprite():SetLayerFrame(0, 11)
          deal:GetSprite():SetLayerFrame(1, 1)
          deal:GetSprite():SetLayerFrame(2, 5)
      end
    else
      if (devilPrice == 1) then
          deal:ToNPC().I1 = 0
          deal:GetSprite():SetLayerFrame(0, 12)
          deal:GetSprite():SetLayerFrame(1, 0)
          deal:GetSprite():SetLayerFrame(2, 10)
      elseif (devilPrice == 2) then
          deal:ToNPC().I1 = 0
          deal:GetSprite():SetLayerFrame(0, 12)
          deal:GetSprite():SetLayerFrame(1, 0)
          deal:GetSprite():SetLayerFrame(2, 10)
      end
    end
    deal:GetSprite():PlayOverlay("IdleShop", true)
    Genesis.RegisterEntity(deal, false)
    item:Remove()
end

function lwallet:OnUpdate()
  local player = Isaac.GetPlayer(0)
  local room = Game():GetRoom()
  local entities = Genesis.getRoomEntities()
  if Game():GetFrameCount() <= 1 then
    Genesis.moddata.walletItems = { }
  end
  if lwallet.delay > 0 then
    lwallet.delay = lwallet.delay - 1
  end
  -- Add Lucifer's Wallet Costume
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.LUCIFERS_WALLET) == true and not lwallet.had then
    lwallet.had = true
    player:AddNullCostume(GENESIS_COSTUMES.LUCIFERS_WALLET)
  end
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.LUCIFERS_WALLET) == false and lwallet.had then
    lwallet.had = false
  end
  -- Check to change devil pacts
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.LUCIFERS_WALLET) then
    for i=1, #entities do
      if (entities[i].Variant == PickupVariant.PICKUP_COLLECTIBLE and
      (room:GetType() == RoomType.ROOM_DEVIL or room:GetType() == RoomType.ROOM_BLACK_MARKET)) then
        local pickup = entities[i]:ToPickup()
        if (pickup.Price < 0) then
          lwallet:MakeWalletDeal(pickup)
        elseif entities[i].SubType == 0 then
         -- Isaac.DebugString(tostring(entities[i].SubType))
          Genesis.RegisterEntity(entities[i], true)
          Isaac.DebugString("test1")
          entities[i]:Remove()
        end
      end
      if entities[i].Type == EntityType.ENTITY_PICKUP and entities[i].Variant == PickupVariant.PICKUP_HEART then
        if entities[i].SubType == HeartSubType.HEART_SOUL then
          entities[i]:ToPickup():Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_HALF_SOUL, true)
        elseif entities[i].SubType == HeartSubType.HEART_ETERNAL then
          entities[i]:ToPickup():Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_HALF, true)
        end
      end
      if (entities[i].Type == GENESIS_ENTITIES.MONEY_DEAL and entities[i].Variant == GENESIS_ENTITIES.VARIANT.MONEY_DEAL) then
        if (entities[i].Position:Distance(player.Position) < 10) then
          if (player:GetNumCoins() >= entities[i]:ToNPC().I1) and lwallet.delay == 0 then
            local item = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, entities[i].SubType, entities[i].Position, entities[i].Velocity, entities[i])
            if player:GetActiveItem() ~= nil and item:ToPickup().Charge > 0 and not player:GetActiveItem() == 0 then
              local active = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, player:GetActiveItem(), room:FindFreePickupSpawnPosition(entities[i].Position, 0, true), entities[i].Velocity, entities[i])
              active:ToPickup().Charge = player:GetActiveCharge()
              player:RemoveCollectible(active.SubType)
              --item.Position = room:FindFreePickupSpawnPosition(entities[i].Position, 0, true)
            end
            item:ToPickup().Wait = 0
            item:Update()
            player:AddCoins(-entities[i]:ToNPC().I1)
            Game():AddDevilRoomDeal()
            lwallet.delay = 50
            if player:HasCollectible(CollectibleType.COLLECTIBLE_RESTOCK) then
              Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_SHOPITEM, 0, entities[i].Position, entities[i].Velocity, entities[i]):ToPickup().Timeout = 60
            end
            Genesis.RegisterEntity(entities[i], true)
            Isaac.DebugString("test2")
            entities[i]:Remove()
          end
        end
      end
    end
  end
end

function lwallet:reroll()
  local player = Isaac.GetPlayer(0)
  local entities = Genesis.getRoomEntities()
  for i=1, #entities do
    if (entities[i].Type == GENESIS_ENTITIES.MONEY_DEAL and entities[i].Variant == GENESIS_ENTITIES.VARIANT.MONEY_DEAL) then
      Genesis.RegisterEntity(entities[i], true)
      Isaac.DebugString("test3")
      local item = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_SHOPITEM, 0, entities[i].Position, entities[i].Velocity, entities[i])
      item:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
      entities[i]:Remove()
      local itemId = item.SubType
      Isaac.GetPlayer(0):GetEffects():AddCollectibleEffect(itemId, false)
      local eff = Isaac.GetPlayer(0):GetEffects():GetCollectibleEffect(itemId)
      local file = eff.Item.GfxFileName
      if Game():GetLevel():GetCurses() == 64 then
        file = "gfx/items/collectibles/questionmark.png"
      end
      local devilPrice = eff.Item.DevilPrice
      Isaac.GetPlayer(0):GetEffects():RemoveCollectibleEffect(itemId)
      local deal = Isaac.Spawn(GENESIS_ENTITIES.MONEY_DEAL, GENESIS_ENTITIES.VARIANT.MONEY_DEAL, itemId, item.Position, item.Velocity, item)
      deal:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
      deal:GetSprite():ReplaceSpritesheet(3, file)
      deal:GetSprite():LoadGraphics()
      deal:GetSprite():SetAnimation("NumbersWhite")
      local steam_sale_num = player:GetCollectibleNum(CollectibleType.COLLECTIBLE_STEAM_SALE)
      if steam_sale_num == 0 then
        if (devilPrice == 1) then
            deal:ToNPC().I1 = 15
            deal:GetSprite():SetLayerFrame(0, 11)
            deal:GetSprite():SetLayerFrame(1, 1)
            deal:GetSprite():SetLayerFrame(2, 5)
        elseif (devilPrice == 2) then
            deal:ToNPC().I1 = 30
            deal:GetSprite():SetLayerFrame(0, 11)
            deal:GetSprite():SetLayerFrame(1, 3)
            deal:GetSprite():SetLayerFrame(2, 0)
        end
      elseif steam_sale_num == 1 then
        if (devilPrice == 1) then
            deal:ToNPC().I1 = 7
            deal:GetSprite():SetLayerFrame(0, 12)
            deal:GetSprite():SetLayerFrame(1, 7)
            deal:GetSprite():SetLayerFrame(2, 10)
        elseif (devilPrice == 2) then
            deal:ToNPC().I1 = 15
            deal:GetSprite():SetLayerFrame(0, 11)
            deal:GetSprite():SetLayerFrame(1, 1)
            deal:GetSprite():SetLayerFrame(2, 5)
        end
      else
        if (devilPrice == 1) then
            deal:ToNPC().I1 = 0
            deal:GetSprite():SetLayerFrame(0, 12)
            deal:GetSprite():SetLayerFrame(1, 0)
            deal:GetSprite():SetLayerFrame(2, 10)
        elseif (devilPrice == 2) then
            deal:ToNPC().I1 = 0
            deal:GetSprite():SetLayerFrame(0, 12)
            deal:GetSprite():SetLayerFrame(1, 0)
            deal:GetSprite():SetLayerFrame(2, 10)
        end
      end
      deal:GetSprite():SetLayerFrame(2, 10)
      deal:GetSprite():PlayOverlay("IdleShop", true)
      Genesis.RegisterEntity(deal, true)
      Isaac.DebugString("test4")
      item:Remove()
    end
  end
end

function lwallet:duplicate()
  local player = Isaac.GetPlayer(0)
  local entities = Genesis.getRoomEntities()
  for i=1, #entities do
    if (entities[i].Type == GENESIS_ENTITIES.MONEY_DEAL) then
      local deal = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, entities[i].SubType, Game():GetRoom():FindFreePickupSpawnPosition(entities[i].Position, 0, false), Vector(0,0), entities[i])
    end
  end
end

function lwallet:remove()
  local player = Isaac.GetPlayer(0)
  local entities = Genesis.getRoomEntities()
  for i=1, #entities do
    if (entities[i].Type == GENESIS_ENTITIES.MONEY_DEAL) then
      Genesis.RegisterEntity(entities[i], true)
      Isaac.DebugString("test5")
      entities[i]:Remove()
    end
  end
end
function lwallet:UseCard(cardid)
  local player = Isaac.GetPlayer(0)
  if cardid == Card.RUNE_PERTHRO then
    lwallet:reroll()
  end
  if cardid == Card.CARD_DICE_SHARD then
    lwallet:reroll()
  end
end
function lwallet:UseActive()
  local player = Isaac.GetPlayer(0)
  local room = Game():GetRoom()
  if room:GetType() == RoomType.ROOM_DEVIL or room:GetType() == RoomType.ROOM_BLACK_MARKET then
    if player:HasCollectible(CollectibleType.COLLECTIBLE_D6) then
      lwallet:reroll()
    elseif player:HasCollectible(CollectibleType.COLLECTIBLE_DIPLOPIA) then
      lwallet:duplicate()
    elseif player:HasCollectible(CollectibleType.COLLECTIBLE_CROOKED_PENNY) then
      if math.random(0,1) == 1 then
        lwallet:duplicate()
      else
        lwallet:remove()
      end
    end
  end
end
lwallet:AddCallback(ModCallbacks.MC_POST_UPDATE, lwallet.OnUpdate)
lwallet:AddCallback(ModCallbacks.MC_USE_ITEM, lwallet.UseActive)
lwallet:AddCallback(ModCallbacks.MC_USE_CARD, lwallet.UseCard)

--------------------
--MICROMARIOS SHIT-- Filloax's note: he probably didn't mean this as an offense
--------------------

-- These codes could be written way cleaner, i'll rewrite it someday when i have some time for this (Sentinel)

local MicroMarioMods = RegisterMod("micromario", 1)
MicroMarioMods.Charge = 0
MicroMarioMods.stop = false
MicroMarioMods.stop1 = false

function MicroMarioMods:horn()
	local player = Isaac.GetPlayer(0)
	if player.FrameCount == 1 then
		MicroMarioMods.charge2 = 0
		MicroMarioMods.Charge = 0
		MicroMarioMods.stop = false
		MicroMarioMods.stop1 = false
	end
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.AZAZELS_LOST_HORN) then
		local shootnow = false
		local meme = false
		local meme3 = false
		local game = Game()
		local room = game:GetRoom()
		local level = game:GetLevel()
		local entities = Genesis.getRoomEntities()
		local direction = player:GetFireDirection()
		if MicroMarioMods.stop == false then
			player:AddCollectible(CollectibleType.COLLECTIBLE_CHOCOLATE_MILK, 0, false)
		local costume = player:AddNullCostume(GENESIS_COSTUMES.HORN)
			player:RemoveCollectible(CollectibleType.COLLECTIBLE_CHOCOLATE_MILK, 0, false)
			MicroMarioMods.stop = true
		end
		if MicroMarioMods.charge2 ~= player.MaxFireDelay*6 then
			MicroMarioMods.charge2 = MicroMarioMods.charge2 + 1
			player.FireDelay = 1
		else
			if math.ceil(player.FrameCount/8) % 2 == 0 then
				player:SetColor(Color(1, 0.5, 0.5, 1, 0, 0, 0), 1, 1, false, false)
			end
			if direction ~= Direction.NO_DIRECTION then
				MicroMarioMods.charge2 = 0
				player.Velocity = Vector(0,0)
				player.FireDelay = 0
			end
		end
		local ents = Genesis.getRoomEntities()
		for k, v in pairs(ents) do
			if v.Type == EntityType.ENTITY_TEAR and v.FrameCount <= 1 then
				if player:HasCollectible(CollectibleType.COLLECTIBLE_LUDOVICO_TECHNIQUE) then return end
				player.FireDelay = player.MaxFireDelay * 3
				v.Position = v.Position + Vector(0,-10)
				local laser = EntityLaser.ShootAngle(1, v.Position, v.Velocity:GetAngleDegrees(), 10, Vector(0,0), player)
				--laser.Parent = player
				--laser.Radius = 0.1
				laser.ParentOffset = Vector(0,-20)
				laser.MaxDistance = 100 + (player.ShotSpeed*10)
				laser.CollisionDamage = player.Damage * 0.8
				laser.Color = player.TearColor
				laser.Radius = v:ToTear().Scale

				v.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
				v.Color = Color(0,0,0,0,0,0,0)
				v.Visible = false

				--v.SplatColor = Color(0,0,0,0,0,0,0)
				--laser.TearFlags = player.TearFlags
				--laser.Velocity = v.Velocity
				--laser.DisableFollowParent = true
				v:ToTear().TearFlags = 0
				v:Die()
			end
		end
	end
end

MicroMarioMods:AddCallback(ModCallbacks.MC_POST_UPDATE, MicroMarioMods.horn)

MicroMarioMods.charge2 = 0
MicroMarioMods.transformed = false
MicroMarioMods.stop = false
MicroMarioMods.stop1 = false

MicroMarioMods.transformationItems = {68,95,152,244,267,395, GENESIS_ITEMS.PASSIVE.TECHNOLOGY_3, GENESIS_ITEMS.PASSIVE.TECH_BOOM }

function checkForTransformation()
  local player = Isaac.GetPlayer(0)
	local c = 0
	for i=0, #MicroMarioMods.transformationItems do
    if MicroMarioMods.transformationItems[i] ~= nil then
      if player:HasCollectible(MicroMarioMods.transformationItems[i]) then
        c = c + 1
      end
    end
	end
	if c >= 3 then return true else return false end
end

function MicroMarioMods:Update()
	local shootnow = false
	local meme = false
	local meme3 = false
	local game = Game()
	local room = game:GetRoom()
	local level = game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local entities = Genesis.getRoomEntities()
	local direction = player:GetFireDirection()
	if player.FrameCount == 1 then
		MicroMarioMods.Charge = 0
		MicroMarioMods.stop = false
		MicroMarioMods.stop1 = false
		MicroMarioMods.transformed = false
	end

	if checkForTransformation() then
    MicroMarioMods.transformed = true
  end
	if MicroMarioMods.transformed == true then
		for i = 1, #entities do
			if entities[i]:IsVulnerableEnemy() and math.random(player.MaxFireDelay*3) == 1 then
				local dir = entities[i].Position:__sub(player.Position)
				player:FireTechLaser(player.Position, 2, dir, false, true)
			end
		end
	end
	if MicroMarioMods.transformed == true and MicroMarioMods.stop1 == false then
		MicroMarioMods.stop1 = true
	end
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.TECHNOLOGY_3) then
		if MicroMarioMods.stop == false then
			player:AddNullCostume(GENESIS_COSTUMES.TECH_3)
			player:AddCollectible(115, 0, false)
			player:TryRemoveCollectibleCostume(115, false)
			MicroMarioMods.stop = true
		end
		player.FireDelay = 99
		if direction == Direction.UP then
			aimDirection = Vector(0,-1)
			MicroMarioMods.Charge = MicroMarioMods.Charge + 0.5
		elseif direction == Direction.LEFT then
			aimDirection = Vector(-1,0)
			MicroMarioMods.Charge = MicroMarioMods.Charge + 0.5
		elseif direction == Direction.DOWN then
			aimDirection = Vector(0,1)
			MicroMarioMods.Charge = MicroMarioMods.Charge + 0.5
		elseif direction == Direction.RIGHT then
			aimDirection = Vector(1,0)
			MicroMarioMods.Charge = MicroMarioMods.Charge + 0.5
		else
			shootnow = true
		end
    if player.MaxFireDelay < 3 then player.MaxFireDelay = 3 end
    local percentual = math.floor(player.MaxFireDelay / 3)
    Isaac.DebugString(tostring(percentual))
		if MicroMarioMods.Charge > player.MaxFireDelay then
			MicroMarioMods.Charge = player.MaxFireDelay
		end
   -- Isaac.DebugString(tostring(MicroMarioMods.Charge))
		if MicroMarioMods.Charge > 0 then
			--player:SetColor(Color(2, 2, 1, 1, MicroMarioMods.Charge * 155 / player.MaxFireDelay, MicroMarioMods.Charge * 155 / player.MaxFireDelay, MicroMarioMods.Charge * 155 / player.MaxFireDelay), 1, 1, false, false)
      --player:SetColor(Color(1,1,1,1,(MicroMarioMods.Charge * 155 / player.MaxFireDelay),(MicroMarioMods.Charge * 155 / player.MaxFireDelay,MicroMarioMods.Charge * 155 / player.MaxFireDelay),1,1,false,false)
      player.Color = Color(1+(MicroMarioMods.Charge / player.MaxFireDelay), 1+(MicroMarioMods.Charge / player.MaxFireDelay), 1+(MicroMarioMods.Charge / player.MaxFireDelay), 1, 0,0,0)
		end
		if MicroMarioMods.Charge > 0 and shootnow == true then
			MicroMarioMods.Charge = MicroMarioMods.Charge - 1
		end
    if MicroMarioMods.Charge <= 0 then
      MicroMarioMods.Charge = 0
      shootnow = false
    end
    if MicroMarioMods.Charge > player.MaxFireDelay then
      MicroMarioMods.Charge = player.MaxFireDelay
    end
		if MicroMarioMods.Charge % percentual == 0 and shootnow == true then
			for i=0, math.floor(MicroMarioMods.Charge / percentual) do

        player:FireTechLaser(player.Position, 2, aimDirection, false, true)

        if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
        end
      end
		end
	end
end

MicroMarioMods:AddCallback(ModCallbacks.MC_POST_UPDATE, MicroMarioMods.Update)

local item = GENESIS_ITEMS.PASSIVE.TECH_BOMB
local techxlaser
local bombs = {}
local entBombType = EntityType.ENTITY_BOMBDROP
local AddedBombs = false

function MicroMarioMods:onInit()
	AddedBombs = false
end

function MicroMarioMods:onUpdate()
    local player = Isaac.GetPlayer(0)
    local ents = Genesis.getRoomEntities()
    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.TECH_BOOM) then
        if AddedBombs == false then
			player:AddBombs(5)
			AddedBombs = true
		end
		for k, v in pairs(ents) do
            if v.Type == entBombType and not bombs[v.Index] and v.SpawnerType == 1 and v:ToBomb().IsFetus == false then
				if v.FrameCount == 1 then
					v:GetSprite():Load("gfx/technology_bomb.anm2", true)
					v:GetSprite():Play("Pulse", true)
				end
				if v.FrameCount % 5 == 0 then
					local laser = EntityLaser.ShootAngle(2, v.Position, math.random(360), 10, Vector(0,0), player)
					laser.MaxDistance = math.random(100,150)
					laser.CollisionDamage = player.Damage
					laser.DisableFollowParent = true
					laser.Color = player.TearColor
					laser.TearFlags = player.TearFlags
				end
				if v.FrameCount == 20  then
					techxlaser = player:FireTechXLaser(v.Position, v.Velocity, 1)
					techxlaser:GetData().Exists = true
					techxlaser.Timeout = 30
					techxlaser.GridHit = false
					techxlaser.Color = player.TearColor
					techxlaser.TearFlags = player.TearFlags
					techxlaser.DisableFollowParent = false
				elseif techxlaser and techxlaser:GetData().Exists == true then
					if techxlaser.FrameCount <= 15 then
						techxlaser.Radius = techxlaser.FrameCount * 3
					elseif techxlaser.FrameCount > 15 then
						techxlaser.Radius = 45 - ((techxlaser.FrameCount - 15) * 3)
					end
				end
            elseif v.Type == entBombType and not bombs[v.Index] and v.SpawnerType == 1 and v:ToBomb().IsFetus == true then
				if v.FrameCount == 1 then
					v:GetSprite():Load("gfx/technology_bomb.anm2", true)
					v:GetSprite():Play("Pulse", true)
				end
				local laser = EntityLaser.ShootAngle(2, v.Position, math.random(360), 5, Vector(0,0), player)
				laser.MaxDistance = math.random(30,50)
				laser.CollisionDamage = player.Damage * 2
				laser.DisableFollowParent = true
				laser.Color = player.TearColor
				laser.TearFlags = player.TearFlags
			end
        end
	end
end

MicroMarioMods:AddCallback(ModCallbacks.MC_POST_UPDATE, MicroMarioMods.onUpdate)
MicroMarioMods:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, MicroMarioMods.onInit)

----------
--Pillow--
----------

local PillowMod = RegisterMod("Pillow", 1)
PillowMod.UsedRaggedPillowCount = 0

function PillowMod:OnPlayerInit()
	PillowMod.UsedRaggedPillowCount = 0
end

function PillowMod:OnUse()
	local player = Isaac.GetPlayer(0)
	if player:GetMaxHearts() > 0 and player:GetHearts() ~= player:GetMaxHearts() then
		player:SetFullHearts()
	elseif player:GetMaxHearts() == 0 then
		player:AddSoulHearts(6)
	end
	player:RemoveCollectible(GENESIS_ITEMS.ACTIVE.PILLOW)
	player:AddCollectible(GENESIS_ITEMS.ACTIVE.USED_PILLOW, 0, false)
	Game():AddPixelation(30)
	SFXManager():Play(SoundEffect.SOUND_DEVIL_CARD, 1, 0, false, 1)
	return true
end

function PillowMod:OnUseUsed()
	local player = Isaac.GetPlayer(0)
	if player:GetMaxHearts() > 0 and player:GetHearts() ~= player:GetMaxHearts() then
		player:AddHearts(player:GetMaxHearts() / 2)
	elseif player:GetMaxHearts() == 0 then
		player:AddSoulHearts(4)
	end
	player:RemoveCollectible(GENESIS_ITEMS.ACTIVE.USED_PILLOW)
	player:AddCollectible(GENESIS_ITEMS.ACTIVE.RAGGED_PILLOW, 0, false)
	Game():AddPixelation(30)
	SFXManager():Play(SoundEffect.SOUND_DEVIL_CARD, 1, 0, false, 0.5)
	return true
end

function PillowMod:OnUseRagged()
	local player = Isaac.GetPlayer(0)
	player:RemoveCollectible(GENESIS_ITEMS.ACTIVE.RAGGED_PILLOW)
	PillowMod.UsedRaggedPillowCount = PillowMod.UsedRaggedPillowCount + 1
	Game():AddPixelation(30)
	player:AnimateSad()
	player:EvaluateItems()
	player:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
	SFXManager():Play(SoundEffect.SOUND_DEVIL_CARD, 1, 0, false, 0.25)
	return true
end

function PillowMod:OnCacheUpdate(player, cacheFlag)
	if cacheFlag == CacheFlag.CACHE_FIREDELAY then
		if player.MaxFireDelay >= 5 + 2 * PillowMod.UsedRaggedPillowCount then
			player.MaxFireDelay = player.MaxFireDelay - 2 * PillowMod.UsedRaggedPillowCount
		elseif player.MaxFireDelay >= 5 then
			player.MaxFireDelay = 5
		end
	end
end

PillowMod:AddCallback(ModCallbacks.MC_USE_ITEM, PillowMod.OnUse, GENESIS_ITEMS.ACTIVE.PILLOW)
PillowMod:AddCallback(ModCallbacks.MC_USE_ITEM, PillowMod.OnUseUsed, GENESIS_ITEMS.ACTIVE.USED_PILLOW)
PillowMod:AddCallback(ModCallbacks.MC_USE_ITEM, PillowMod.OnUseRagged, GENESIS_ITEMS.ACTIVE.RAGGED_PILLOW)
PillowMod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, PillowMod.OnPlayerInit)
PillowMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, PillowMod.OnCacheUpdate)

Isaac.DebugString("Genesis+: Loaded Items(1/3)!")
error({ hack = true })