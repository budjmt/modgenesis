---------------------------
-- CURSES & ENLIGHTMENTS --
---------------------------

local curse = RegisterMod("Genesis+ Curses",1)
curse.game = Game()
curse.spawn = false

curse.DICEEFFECTS = {
--      CollectibleType.COLLECTIBLE_D4,
      CollectibleType.COLLECTIBLE_D20,
      CollectibleType.COLLECTIBLE_D10,
--      CollectibleType.COLLECTIBLE_D100,
      CollectibleType.COLLECTIBLE_D12,
      CollectibleType.COLLECTIBLE_D6,
--      CollectibleType.COLLECTIBLE_D7,
--      CollectibleType.COLLECTIBLE_D8
}

curse.Curses = {
	1 << GENESIS_CURSES.CURSE_REWIND,
	1 << GENESIS_CURSES.CURSE_CHEST,
	1 << GENESIS_CURSES.CURSE_PIT,
	1 << GENESIS_CURSES.CURSE_SEP,
 }

curse.Enlig = {
	1 << GENESIS_CURSES.ENL_BUNDLE,
	1 << GENESIS_CURSES.ENL_SOUL,
	1 << GENESIS_CURSES.ENL_POOR,
	1 << GENESIS_CURSES.ENL_DELIGHT,
	1 << GENESIS_CURSES.ENL_GOLDEN,
	1 << GENESIS_CURSES.ENL_FRIEND,
	1 << GENESIS_CURSES.ENL_GIFT
  }

function curse:init()
	curse.RemovedTrapDoor = false
end

curse:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT,curse.init)

function curse:postCurseEval(_curses)
	-- return curse.Curses[1] if you wanna try rewind 100%
	if _curses then
		local level = curse.game:GetLevel()
		local player = Isaac.GetPlayer(0)
		local curse_rng = RNG()
		curse_rng:SetSeed(level:GetDevilAngelRoomRNG():GetSeed(), 0)

		if player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_WISDOM) then
			return curse.Enlig[1 + curse_rng:RandomInt(1+#curse.Enlig)]
		elseif curse_rng:RandomInt(4) == 1 then
			if curse_rng:RandomInt(7) <= 3 then
				return curse.Enlig[1 + curse_rng:RandomInt(1+#curse.Enlig)]
			else
				return curse.Curses[1 + curse_rng:RandomInt(1+#curse.Curses)]
			end
		else
			return _curses
		end
	end
end

curse:AddCallback(ModCallbacks.MC_POST_CURSE_EVAL, curse.postCurseEval)

function curse:CurseUpdate()
	local level = curse.game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local seed = curse.game:GetSeeds()
	local music = MusicManager()
	local room = curse.game:GetRoom()

	if level:GetCurseName () == "Curse of Rewind" then
		local gridSize = room:GetGridSize()
		if room:GetType() == RoomType.ROOM_BOSS then
			--SFXManager():Stop(SoundEffect.SOUND_HELL_PORTAL1 )
			for i = 0, gridSize-1 do
				if room:GetGridEntity(i) ~= nil then
					local trapdoor = room:GetGridEntity(i)
					if trapdoor:GetType() == GridEntityType.GRID_TRAPDOOR then
						room:RemoveGridEntity(i,1,false)
					end
				end
			end
		end
	elseif level:GetCurseName () == "Curse of the Chest" then
		local entities = Genesis.getRoomPickups()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type == 5 then
				if e.Variant == PickupVariant.PICKUP_CHEST or e.Variant == PickupVariant.PICKUP_BOMBCHEST or e.Variant == PickupVariant.PICKUP_LOCKEDCHEST or e.Variant == PickupVariant.PICKUP_REDCHEST or e.Variant == PickupVariant.PICKUP_ETERNALCHEST then
					e:ToPickup():Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_SPIKEDCHEST, 0, true)
				end
			end
		end
	elseif level:GetCurseName () == "I Hear Lost Souls..." then
		local entities = Genesis.getRoomPickups()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type == 5 then
				if e.Variant == PickupVariant.PICKUP_HEART and e.SubType ~= 3 then
					e:ToPickup():Morph(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_HEART,HeartSubType.HEART_SOUL,false)
				end
			end
		end
	elseif level:GetCurseName () == "You Feel Delighted.." then
		local entities = Genesis.getRoomPickups()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type == 5 then
				if e.Variant == PickupVariant.PICKUP_HEART and e.SubType ~= 4 and e.InitSeed%100+1 > 75 then
					e:ToPickup():Morph(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_HEART,HeartSubType.HEART_ETERNAL,false)
				end
			end
		end
	elseif level:GetCurseName () == "Curse of Separation" then
		local entities = Genesis.getRoomPickups()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type == 5 and e.Variant == PickupVariant.PICKUP_HEART then
				if e.SubType ~= 2 then
					e:ToPickup():Morph(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_HEART,HeartSubType.HEART_HALF,true)
				end
			end
		end
	elseif level:GetCurseName () == "You Are Golden.." then
		local entities = Genesis.getRoomPickups()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type == 5 then
				if e.Variant == PickupVariant.PICKUP_HEART and e.SubType ~= 7 then
					e:ToPickup():Morph(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_HEART,HeartSubType.HEART_GOLDEN,true)
				elseif e.Variant == PickupVariant.PICKUP_BOMB and e.SubType ~= 4 then
					e:ToPickup():Morph(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_BOMB,BombSubType.BOMB_GOLDEN,true)
				elseif e.Variant == PickupVariant.PICKUP_KEY and e.SubType ~= 2 then
					e:ToPickup():Morph(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_KEY,KeySubType.KEY_GOLDEN,true)
				end
			end
		end
	elseif level:GetCurseName () == "Man's Best Friend" then
		local entities = Genesis.getRoomEnemies()
		for i = 1, #entities do
			local e = entities[i]
			if e:IsVulnerableEnemy() and e.FrameCount == 1 and e.InitSeed%100+1>70 and not e:IsBoss() then
				e:AddCharmed(-1)
			end
		end
  elseif level:GetCurseName () == "You Feel Bundled.." then
		local entities = Genesis.getRoomPickups()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type == 5 and e.FrameCount == 2 then
				if e.Variant == PickupVariant.PICKUP_HEART and e.SubType == 1 then
					e:Remove()
					Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_HEART,HeartSubType.HEART_DOUBLEPACK,e.Position,Vector(0,0),nil)
				elseif e.Variant == PickupVariant.PICKUP_COIN and e.SubType == 1 then
					e:Remove()
					Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_COIN,CoinSubType.COIN_DOUBLEPACK,e.Position,Vector(0,0),nil)
				elseif e.Variant == PickupVariant.PICKUP_BOMB and e.SubType == 1 then
					e:Remove()
					Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_BOMB,BombSubType.BOMB_DOUBLEPACK,e.Position,Vector(0,0),nil)
				end
				local COLLECTIBLE_DYSLEXIA = GENESIS_ITEMS.PASSIVE.DYSLEXIA
				local DislexiaPickupEntity = 5
				local DislexiaPickupVariant = Isaac.GetEntityVariantByName("Dyslexia Double Pickup")
				local DislexiaHeartEntity = 5
				local DislexiaHeartVariant = Isaac.GetEntityVariantByName("Dyslexia Double Pickup")
				if player:HasCollectible(COLLECTIBLE_DYSLEXIA) and e:ToPickup():IsShopItem() == false then
					e:ToPickup():Morph(DislexiaPickupEntity, 200, 2, true)
				end
			end
		end
	elseif level:GetCurseName () == "You Feel Gifted.." and player:IsHoldingItem() and not Genesis.moddata.spawnedCurseGift then
		Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_COLLECTIBLE,0,player.Position,Vector(0,0),nil)
		Genesis.moddata.spawnedCurseGift = true
	end
end

curse:AddCallback(ModCallbacks.MC_POST_UPDATE, curse.CurseUpdate)

curse.pressSpaceCounter = -1

function curse:OnEnter()
	Genesis.moddata.spawnedCurseGift = false
	Genesis.moddata.curseRemovedTrapDoor = false
	local level = curse.game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local seed = curse.game:GetSeeds()
	local index = level:GetCurrentRoomIndex()
	local room = Game():GetRoom()
	if level:GetCurseName () == "Curse of Rewind" then
		player:UseCard(Card.CARD_EMPEROR)
		curse.pressSpaceCounter = 6
		if SFXManager():IsPlaying(SoundEffect.SOUND_THE_EMPEROR) then
			SFXManager():Stop(SoundEffect.SOUND_THE_EMPEROR)
		end
		SFXManager():Stop(SoundEffect.SOUND_HELL_PORTAL1 )
		SFXManager():Stop(SoundEffect.SOUND_HELL_PORTAL2 )
	end
end

curse:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL,curse.OnEnter)

curse.lastHookFrame = 0

--Press space to skip vs screen
function curse:InputHook(ent, hook, action)
	if hook == InputHook.IS_ACTION_PRESSED and action == ButtonAction.ACTION_MENUCONFIRM then
		if curse.pressSpaceCounter == 0 or Input.IsActionPressed(ButtonAction.ACTION_MENUCONFIRM, Isaac.GetPlayer(0).ControllerIndex) then
			curse.pressSpaceCounter = -1
			return true
		end
	end
	if curse.lastHookFrame ~= curse.game:GetFrameCount() then
		curse.pressSpaceCounter = curse.pressSpaceCounter - 1
		curse.lastHookFrame = curse.game:GetFrameCount()
	end
end

curse:AddCallback(ModCallbacks.MC_INPUT_ACTION,curse.InputHook)

function curse:OnRoom()
	local level = curse.game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local seed = curse.game:GetSeeds()
	local room = curse.game:GetRoom()
	if level:GetCurseName() == "What is That?!" then
		local entities = Genesis.getRoomPickups()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type == 5 and e.Variant == 100 then
				e:SetColor(Color(0,0,0,255,0,0,0), 99999999999, 1, false, false)
			end
		end
	elseif level:GetCurseName() == "Curse of Rewind" and level:GetCurrentRoomIndex() == level:GetStartingRoomIndex() and not (level:GetStage() == LevelStage.STAGE2_2 and Genesis.IsSeptic()) then
		local hasTrapdoor = false
		for i = 0, room:GetGridSize()-1 do
			if room:GetGridEntity(i) ~= nil then
				if room:GetGridEntity(i):GetType() == GridEntityType.GRID_TRAPDOOR then
					hasTrapdoor = true
				end
			end
		end
		if not hasTrapdoor then
			room:SpawnGridEntity(room:GetGridIndex(room:FindFreeTilePosition(room:GetCenterPos(), 32)), GridEntityType.GRID_TRAPDOOR, 0, 0, math.random(100000))
		end
	elseif level:GetCurseName () == "You Feel Rich.." then
		player:GetEffects():AddCollectibleEffect(CollectibleType.COLLECTIBLE_STEAM_SALE, false)
	end
end
curse:AddCallback(ModCallbacks.MC_POST_NEW_ROOM,curse.OnRoom)

function curse:onSpawnPickup(pickup)
	local level = curse.game:GetLevel()
	if pickup.Variant == 100 and level:GetCurseName() == "What is That?!" then
		pickup:SetColor(Color(0,0,0,255,0,0,0), 99999999999, 1, false, false)
	end
end
curse:AddCallback(ModCallbacks.MC_POST_PICKUP_INIT, curse.onSpawnPickup);

Isaac.DebugString("Genesis+: Loaded Curses and Enlightenments!")
error({ hack = true })