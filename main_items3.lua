--------------------
--COUNTERFEIT SOUL--
--------------------

local mod = RegisterMod("Counterfeit Soul", 1)

function mod:use_CounterfeitSoul()
	local player = Isaac.GetPlayer(0)
	local game = Game()
	local room = game:GetRoom()
	-- Are we in a Devil/Black Market Room?
	if room:GetType() == RoomType.ROOM_DEVIL or room:GetType() == RoomType.ROOM_BLACK_MARKET then
		local e = Genesis.getRoomEntities()
		local devildeals = {}
		-- Adds every devil- and lucifers wallet- deal to the devildeals array
		for i=1, #e do
			if e[i].Type == EntityType.ENTITY_PICKUP and e[i].Variant == PickupVariant.PICKUP_COLLECTIBLE and e[i].SubType ~= 0 or e[i].Type == 1000 and e[i].Variant == 1901 then
				table.insert(devildeals, i)
			end
		end
		-- Picks a random devildeal and replaces it with a Collectible
    if #devildeals ~= 0 and not player:HasCollectible(GENESIS_ITEMS.PASSIVE.LUCIFERS_WALLET) then
      local freedeal = devildeals[math.random(#devildeals)]
      Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, e[freedeal].SubType, e[freedeal].Position, Vector(0,0), nil)
      e[freedeal]:Remove()
    -- Picks a random devildeal for when it's a lucifers money deal
    elseif #devildeals ~= 0 then
      local freedeal = devildeals[math.random(#devildeals)]
      local shopitem = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_SHOPITEM, e[freedeal].SubType, e[freedeal].Position, Vector(0,0), nil)
      shopitem.Price = 0
      e[freedeal]:Remove()
    end
	end
	return true
end

mod:AddCallback(ModCallbacks.MC_USE_ITEM, mod.use_CounterfeitSoul, GENESIS_ITEMS.ACTIVE.COUNTERFEIT_SOUL)

--------
--EBUY--
--------

local ebuy = RegisterMod("eBuy", 1)
ebuy.PlayerItems = {}
ebuy.TotalItemCount = -9
--return the total number of items loaded in the game including modded
function ebuy:GetMaxItemCount()
	local totalItems = 552
	local temp = Isaac:GetItemConfig():GetCollectible(totalItems)
	while temp ~= nil do
		totalItems = totalItems + 1
		temp = Isaac:GetItemConfig():GetCollectible(totalItems)
	end
	totalItems = totalItems - 1
	return totalItems
end

-- gives a list with all collectibles that the player has
function ebuy:GetCurrentCollectibles(player)
	local tbl = {}
	for i=1, ebuy.TotalItemCount do
		if player:HasCollectible(i) then
			--need to check if the player has multiples of an item
			for j=1, player:GetCollectibleNum(i) do
				local toadd = i
				if player:GetCollectibleNum(i) > 1 then
					toadd = i .. "-multi"
				end
				table.insert(tbl, toadd)
			end
		end
		--stop checking for items if you've found as many items as the player has
		if #tbl >= player:GetCollectibleCount() then
			break
		end
	end
    Isaac.DebugString(table_tostring(tbl))
	return tbl
end

--get difference in number of an item the player has and the amount in stored data
function ebuy:getDuplicateItemDifference(player, itemID)
	local actualCount = player:GetCollectibleNum(itemID)
	local savedCount = 0
	for k=1, #ebuy.PlayerItems do
		if ebuy.PlayerItems[k] == itemID then
			savedCount = savedCount + 1
		end
	end
	return savedCount - actualCount
end

function ebuy:playerUpdate(player)
	if ebuy.TotalItemCount < 0 then
		ebuy.TotalItemCount = ebuy:GetMaxItemCount()
	end
	if player:HasCollectible(GENESIS_ITEMS.ACTIVE.EBUY) then
		-- if the player gets a new item
		if #ebuy.PlayerItems < player:GetCollectibleCount() then
			local currentitems = ebuy:GetCurrentCollectibles(player)
			if #ebuy.PlayerItems == 0 then
				table.insert(ebuy.PlayerItems, currentitems[1])
			else
				local addedItem = false
				for i = 1, #currentitems do
					if addedItem then
						break --stop searching for items after updating list
					end
					local foundItemInBoth = false
					for j = 1, #ebuy.PlayerItems do
						if currentitems[i] == ebuy.PlayerItems[j] then
							foundItemInBoth = true
						elseif currentitems[i] == ebuy.PlayerItems[j] .. "-multi" then
							foundItemInBoth = true
							if ebuy:getDuplicateItemDifference(player, ebuy.PlayerItems[j]) < 0 then
								table.insert(ebuy.PlayerItems, ebuy.PlayerItems[j])
								addedItem = true --break both for loops
							end
						end
					end
					-- adds the item as last item
					if not foundItemInBoth then
						table.insert(ebuy.PlayerItems, currentitems[i])
						break
					end
				end
			end
		end
		--if the player loses an item
		if #ebuy.PlayerItems > player:GetCollectibleCount() then
			local currentitems = ebuy:GetCurrentCollectibles(player)
			local removedItem = false
			for i = 1, #ebuy.PlayerItems do
				if removedItem then
					break --stop searching for items after updating list
				end
				local foundItemInBoth = false
				-- the game searches for the lost item
				for j = 1, #currentitems do
					if currentitems[j] == ebuy.PlayerItems[i] then
						foundItemInBoth = true
					elseif currentitems[j] == ebuy.PlayerItems[i] .. "-multi" then
						--if there are duplicates in the list check if the amount differs
						foundItemInBoth = true
						if ebuy:getDuplicateItemDifference(player, ebuy.PlayerItems[i]) > 0 then
							--remove last duplicate item from list
							for l = #ebuy.PlayerItems, 1, -1 do
								if ebuy.PlayerItems[i] == ebuy.PlayerItems[l] then
									table.remove(ebuy.PlayerItems, l)
									removedItem = true --break both for loops
								end
							end
						end
					end
				end
				--removes missing item
				if not foundItemInBoth then
					table.remove(ebuy.PlayerItems, i)
					break
				end
			end
		end
	end
end
ebuy:AddCallback(ModCallbacks.MC_POST_PLAYER_UPDATE , ebuy.playerUpdate);

function ebuy:use_eBuy()
	local player = Isaac.GetPlayer(0)
	if #ebuy.PlayerItems > 0 then
		-- when eBuy is used remove the last item and gives coins for it
		local lastitem = ebuy.PlayerItems[#ebuy.PlayerItems]
		player:RemoveCollectible(lastitem)
		table.remove(ebuy.PlayerItems, #ebuy.PlayerItems)
		if math.random(5) == 1 then
			player:AddCoins(7)
		else
			player:AddCoins(15)
		end
		return true
	else
		SFXManager():Play(SoundEffect.SOUND_THUMBS_DOWN, 1, 0, false, 1)
		player:SetActiveCharge(6)
	end
end

ebuy:AddCallback(ModCallbacks.MC_USE_ITEM, ebuy.use_eBuy, GENESIS_ITEMS.ACTIVE.EBUY)


----------------
--EVIL OPTIONS--
----------------

local mod = RegisterMod("Evil Options", 1)
mod.roomsetup = false

-- turns things like x = 1, y = 5 into a grid position
function VectorToGrid(x,y)
	local room = Game():GetRoom()
	return room:GetGridPosition(room:GetGridIndex(room:GetTopLeftPos()+Vector(x*40+20,y*40+20)))
end

function mod:Render()
    local player = Isaac.GetPlayer(0)
    local game = Game()
    local room = game:GetRoom()
	-- if the player comes into a devil room for the first time
    if room:GetFrameCount() < 1 and room:GetType() == RoomType.ROOM_DEVIL and not mod.roomsetup and player:HasCollectible(GENESIS_ITEMS.PASSIVE.EVIL_OPTIONS) then
    Isaac.DebugString("INDEVIL")
		mod.roomsetup = true
        local e = Genesis.getRoomEntities()
		-- removes devil deals
        for i=1, #e do
            if e[i].Type == EntityType.ENTITY_PICKUP then
                e[i]:Remove()
                Isaac.DebugString("DEVILREMOVED")
            end
        end
		-- adds 4 devil deals
		local tbl = {VectorToGrid(3,3), VectorToGrid(5,4), VectorToGrid(7,4), VectorToGrid(9,3)}
		for num=1, 4 do
			local npc = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_SHOPITEM, 0, tbl[num], Vector(0,0), nil)
      Isaac.DebugString("4NEWONES")
		end
    end
end

function mod:Update()
	-- after the room is set up makes mod.roomsetup ready for the next room setup
    if mod.roomsetup and Game():GetRoom():GetFrameCount() == 1 then
        mod.roomsetup = false
    end
end

mod:AddCallback(ModCallbacks.MC_POST_RENDER, mod.Render)
mod:AddCallback(ModCallbacks.MC_POST_UPDATE, mod.Update)

------------------
--Tinted Blaster--
------------------

local TBlast = RegisterMod("TintedBlaster", 1)

function TBlast:OnUse()
  for i = 1, Game():GetRoom():GetGridSize() do
    local grid =  Game():GetRoom():GetGridEntity(i)
    if grid ~= nil then
      if grid.Desc.Type == GridEntityType.GRID_ROCKT then
        grid:Destroy(true)
      end
    end
  end
end

TBlast:AddCallback(ModCallbacks.MC_USE_ITEM, TBlast.OnUse, GENESIS_ITEMS.ACTIVE.TINTED_BLASTER)

--------------
--Zeus Touch--
--------------

local ZeusMod = RegisterMod("ZeusTouch", 1)

function ZeusMod:onUpdate()
  local player = Game():GetPlayer(0)
  local ent = Genesis.getRoomEntities()

  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.ZEUS_TOUCH) then
    for i = 1, #ent do
      if ent[i]:IsActiveEnemy(false) and player.Position:Distance(ent[i].Position) <= player.Size + ent[i].Size and ent[i]:GetData().Laser ~= false then
        local lightning = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CRACK_THE_SKY, 0, ent[i].Position, Vector(0, 0), player)
        lightning:GetSprite():Load("gfx/zeus_beam.anm2", true)
        ent[i]:GetData().Laser = false
      end
    end
  end

end

ZeusMod:AddCallback(ModCallbacks.MC_POST_UPDATE, ZeusMod.onUpdate)

--------------------
--Explosive Temper--
--------------------

local Temper = RegisterMod("Explosive Temper", 1)

local ExplosiveTemp = {
  id = GENESIS_ITEMS.PASSIVE.EXPLOSIVE_TEMPER,
  costume = Isaac.GetCostumeIdByPath("gfx/characters/ExplTempCostume.anm2"),
  has = false,
  seconds = 0,
  shooting = true,
  shootingframe = 0
}

function Temper:onUpdate()
  local player = Game():GetPlayer(0)

  -- Isaac.RenderText(tostring(ExplosiveTemp.seconds), 100, 100, 1, 1, 1, 255)

  if Game():GetFrameCount() == 1 then
    ExplosiveTemp.has = false
    ExplosiveTemp.seconds = 0
    ExplosiveTemp.shooting = true
    ExplosiveTemp.shootingframe = 0
  end

  if player:HasCollectible(ExplosiveTemp.id) then
    if ExplosiveTemp.has == false then
      player:AddNullCostume(ExplosiveTemp.costume)
      ExplosiveTemp.has = true
    end

    player:SetColor(Color(1, 1, 1, 1, ExplosiveTemp.seconds, 0, 0), 1, 0, true, false)



    if Input.IsActionPressed(ButtonAction.ACTION_SHOOTDOWN, player.ControllerIndex) or Input.IsActionPressed(ButtonAction.ACTION_SHOOTUP, player.ControllerIndex) or Input.IsActionPressed(ButtonAction.ACTION_SHOOTLEFT, player.ControllerIndex) or Input.IsActionPressed(ButtonAction.ACTION_SHOOTRIGHT, player.ControllerIndex) then
      ExplosiveTemp.seconds = ExplosiveTemp.seconds + 1
    elseif Input.IsActionTriggered(ButtonAction.ACTION_SHOOTDOWN, player.ControllerIndex) == false or Input.IsActionTriggered(ButtonAction.ACTION_SHOOTUP, player.ControllerIndex) == false or Input.IsActionTriggered(ButtonAction.ACTION_SHOOTLEFT, player.ControllerIndex) == false or Input.IsActionTriggered(ButtonAction.ACTION_SHOOTRIGHT, player.ControllerIndex) == false then
      ExplosiveTemp.seconds = 0
    end

    if ExplosiveTemp.seconds >= 300 then
      Isaac.Explode(player.Position, player, (player.Damage * (30 / player.MaxFireDelay)) * 4)
      player:SetColor(Color(1, 1, 1, 1, 255, 0, 0), 60, 0, true, false)
      ExplosiveTemp.seconds = 0
      ExplosiveTemp.shooting = false
      ExplosiveTemp.shootingframe = Game():GetFrameCount()
    end

    if ExplosiveTemp.shooting == false then
      ExplosiveTemp.seconds = 0
      player.FireDelay = player.MaxFireDelay
      if Game():GetFrameCount() >= ExplosiveTemp.shootingframe + 60 then
        ExplosiveTemp.shooting = true
      end
    end
  end
end

Temper:AddCallback(ModCallbacks.MC_POST_RENDER, Temper.onUpdate)

-----------------
-- LOST WISDOM --
-----------------

local filloaxItems = RegisterMod("FilloaxItems", 1)

filloaxItems.hadWisdom = false

function filloaxItems:Init()
  filloaxItems.hadWisdom = Isaac.GetPlayer(0):HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_WISDOM)
end

filloaxItems:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, filloaxItems.Init)

function filloaxItems:Update()
  local player = Isaac.GetPlayer(0)
  if not filloaxItems.hadWisdom and player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_WISDOM) then
    filloaxItems.hadWisdom = true
    player:AddSoulHearts(6)
  end
end

filloaxItems:AddCallback(ModCallbacks.MC_POST_UPDATE, filloaxItems.Update)

---------------------
-- BOOK OF LUCIFER --
---------------------

local LuciferBookMod = RegisterMod("Book of Lucifer",1)
LuciferBookMod.ItemId = GENESIS_ITEMS.ACTIVE.BOOK_OF_LUCIFER
LuciferBookMod.ActiveSuccubi = {}
LuciferBookMod.SuccubusConvert = 0

function LuciferBookMod:Reset()
	LuciferBookMod.ActiveSuccubi = {}
	LuciferBookMod.SuccubusConvert = 0
end

function LuciferBookMod:ResetOnNewRoom()
	LuciferBookMod.DamageMultiplier = 0
	for i, e in pairs(LuciferBookMod.ActiveSuccubi) do
		e:Remove()
		player:RemoveCollectible(417)
	end
	LuciferBookMod.ActiveSuccubi = {}
end

function LuciferBookMod:OnUseLBook()
	local player = Isaac.GetPlayer(0)
	Isaac.ExecuteCommand("g c417")
	LuciferBookMod.SuccubusConvert = LuciferBookMod.SuccubusConvert + 1
	return true
end

function LuciferBookMod:succubusUpdate(familiar)
	if LuciferBookMod.SuccubusConvert > 0 and familiar.FrameCount > 5 and familiar:GetData().IsBookSpawned == nil then
		familiar:GetData().IsBookSpawned = true
		familiar:GetSprite():ReplaceSpritesheet(0, "gfx/lucifer_succubus.png")
		familiar:GetSprite():LoadGraphics()
		table.insert(LuciferBookMod.ActiveSuccubi, familiar)
		LuciferBookMod.SuccubusConvert = LuciferBookMod.SuccubusConvert - 1
	end
end

LuciferBookMod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, LuciferBookMod.ResetOnNewRoom)
LuciferBookMod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, LuciferBookMod.Reset)
LuciferBookMod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, LuciferBookMod.succubusUpdate, 96);
LuciferBookMod:AddCallback(ModCallbacks.MC_USE_ITEM, LuciferBookMod.OnUseLBook, LuciferBookMod.ItemId)

----------------
-- LIL BROSKI --
----------------

local broski = RegisterMod("Lil Broski",1)

broski.respawnCountdowns = {}

function broski:onFamiliarUpdate11(familiar11)
  local sprite = familiar11:GetSprite();
  local player = Isaac.GetPlayer(0)
  local FireDir = player:GetFireDirection()
  local sound = SFXManager()
  familiar11:FollowParent()
  familiar11.SpriteOffset = Vector(0,math.sin(familiar11.FrameCount/10)*2)

  local en = Genesis.getRoomEnemies()
  for i = 1, #en do
    local e = en[i]
    if e:IsVulnerableEnemy() then
      familiar11.Velocity = (e.Position - familiar11.Position):Resized(4)
		if (familiar11.Position - e.Position):Length() < 25 then
		  Isaac.Explode(familiar11.Position,e,30)
		  familiar11:Die()
		  sound:Play(GENESIS_SFX.FAMILIAR_SWEAR,1,0,false,1)
		  table.insert(broski.respawnCountdowns, 250) --we have more countdowns in case the player has more of each familiar
		end
	end
  end
end

function broski:FamInit(fam)
  fam.IsFollower = true
end

function broski:EvalCache(pl, flag)
  if flag == CacheFlag.CACHE_FAMILIARS then
    pl:CheckFamiliar(GENESIS_ENTITIES.VARIANT.LIL_BROSKI, pl:GetCollectibleNum(GENESIS_ITEMS.FAMILIAR.LIL_BROSKI)-#broski.respawnCountdowns, RNG())
  end
end

function broski:Update()
  for i,v in ipairs(broski.respawnCountdowns) do
    local pl = Isaac.GetPlayer(0)
    if i > pl:GetCollectibleNum(GENESIS_ITEMS.FAMILIAR.LIL_BROSKI) then
      table.remove(broski.respawnCountdowns, i)
    else
      if v > 0 then
        broski.respawnCountdowns[i] = broski.respawnCountdowns[i]-1
      elseif v <= 0 then
        pl.Child = Isaac.Spawn(3, GENESIS_ENTITIES.VARIANT.LIL_BROSKI, 0, pl.Position, Genesis.VEC_ZERO, pl)
        table.remove(broski.respawnCountdowns, i)
      end
    end
  end
end

broski:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, broski.EvalCache)
broski:AddCallback( ModCallbacks.MC_POST_UPDATE, broski.Update)
broski:AddCallback( ModCallbacks.MC_FAMILIAR_INIT, broski.FamInit, GENESIS_ENTITIES.VARIANT.LIL_BROSKI)
broski:AddCallback( ModCallbacks.MC_FAMILIAR_UPDATE, broski.onFamiliarUpdate11, GENESIS_ENTITIES.VARIANT.LIL_BROSKI)

--------------
--BLACK BELT--
--------------

local BlackBelt = RegisterMod("Black Belt", 1)

BlackBelt.DamageTaken = true
BlackBelt.HasCostume = false
BlackBelt.DataKey = {
	NOT_DAMAGED = 0,
	DAMAGED = 1
}

local TreasurePool = {1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 15, 19, 36, 37, 39, 40, 41, 42, 44, 45, 46, 47, 48, 49, 52, 53, 55, 56, 57, 58, 62, 65, 66, 67, 68, 69, 71, 72, 74, 75, 76, 77, 78, 85, 86, 87, 88, 89, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 113, 114, 115, 117, 120, 121, 123, 124, 125, 127, 128, 129, 131, 136, 137, 138, 140, 142, 143, 14, 13, 144, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 157, 158, 160, 161, 162, 163, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 178, 180, 186, 188, 189, 190, 191, 192, 200, 201, 202, 206, 209, 210, 211, 213, 214, 217, 220, 221, 222, 223, 224, 225, 227, 228, 229, 231, 233, 234, 236, 237, 240, 242, 244, 245, 256, 257, 261, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 291, 292, 294, 295, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 329, 330, 332, 333, 334, 335, 336, 411, 410, 407, 406, 404, 401, 398, 397, 395, 394, 393, 392, 391, 389, 388, 386, 385, 384, 382, 381, 379, 378, 377, 375, 373, 371, 369, 368, 367, 366, 365, 364, 362, 361, 359, 358, 352, 350, 351, 353, 405, 374, 390, 418, 419, 421, 422, 426, 427, 430, 431, 432, 435, 436, 437, 440, 443, 444, 445, 446, 447, 448, 449, 452, 453, 457, 459, 460, 461, 463, 465, 466, 467, 469, 470, 471, 473, 476, 478, 481, 482, 484, 485, 488, 489, 491, 492, 493, 494, 495, 496, 497, 502, 506, 504, 508, 509, 507,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,537, 540, 543, 544, 545, 546, 547, 548, 549, 552, 553, 557, 559, 560, 561, 563, 565, 566, 567, 569, 570, 571, 573, 576, 578, 581, 582, 584, 585, 588, 589, 591, 592, 593, 594, 595, 596, 597}

function BlackBelt:OnPlayerInit(...)
	if Game():GetFrameCount() <= 2 then
		BlackBelt.DamageTaken = false
		Genesis.moddata.BlackBeltDamaged = BlackBelt.DataKey.NOT_DAMAGED
	else
		BlackBelt.DamageTaken = Genesis.moddata.BlackBeltDamaged == BlackBelt.DataKey.DAMAGED
	end
	BlackBelt.HasCostume = false
end

function BlackBelt:OnTakeDamage(entity)
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.BLACK_BELT) and entity.Type == EntityType.ENTITY_PLAYER then
		BlackBelt.DamageTaken = true
		Genesis.moddata.BlackBeltDamaged = BlackBelt.DataKey.DAMAGED
	end
end

function BlackBelt:Update()
  local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.BLACK_BELT) then
		if not BlackBelt.HasCostume and not BlackBelt.DamageTaken then
			player:AddNullCostume(GENESIS_COSTUMES.BLACK_BELT)
			BlackBelt.HasCostume = true
		elseif BlackBelt.DamageTaken and BlackBelt.HasCostume then
			player:TryRemoveNullCostume(GENESIS_COSTUMES.BLACK_BELT)
			BlackBelt.HasCostume = false
		end
	end
end

function BlackBelt:OnPeffectUpdate(player)
  local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.BLACK_BELT) then
		local level = Game():GetLevel()
		if level:GetCurrentRoomIndex() == level:GetStartingRoomIndex() then
			if not BlackBelt.DamageTaken then
				local item = nil
				if player:HasCollectible(CollectibleType.COLLECTIBLE_CHAOS) then
					item = player:GetCollectibleRNG():RandomInt(510)
				else
					item = TreasurePool[math.random(1,#TreasurePool)]
				end
				if player:HasCollectible(item) then
					item = CollectibleType.COLLECTIBLE_BREAKFAST
				end
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, item, Isaac.GetFreeNearPosition(player.Position, 40), Vector(0, 0), player)
			end
			Genesis.moddata.BlackBeltDamaged = BlackBelt.DataKey.NOT_DAMAGED
			BlackBelt.DamageTaken = false
		end
	end
end

BlackBelt:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, BlackBelt.OnPlayerInit)
BlackBelt:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, BlackBelt.OnPeffectUpdate)
BlackBelt:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, BlackBelt.OnTakeDamage)
BlackBelt:AddCallback(ModCallbacks.MC_POST_UPDATE, BlackBelt.Update)

--------------
--RAIN BOOTS--
--------------

local RainMod = RegisterMod("RainMod", 1)
RainMod.Id = GENESIS_ITEMS.PASSIVE.RAINSTORM
RainMod.RainMultiplier = 0
RainMod.RainStorm = false

function RainMod:onInit()
	RainMod.RainMultiplier = 0
	RainMod.RainStorm = false
end

function RainMod:onUpdate()
    local player = Isaac.GetPlayer(0)
	if player:HasCollectible(RainMod.Id) and Game():GetRoom():GetFrameCount() % (20 + player.MaxFireDelay - (RainMod.RainMultiplier * 5)) == 0 and RainMod.RainMultiplier > 0 and Game():GetRoom():IsClear() == false and not (player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) or player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2) or player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_5) or player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X)) then
		if RainMod.RainStorm == true and math.random(4) == 1 then
			local holybeam = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CRACK_THE_SKY, 0, Game():GetRoom():GetRandomPosition(0.8), Vector(0,0), player)
			holybeam.CollisionDamage = player.Damage
		end
		local tear = player:FireTear(Game():GetRoom():GetRandomPosition(0.8), Vector(0,0), false, false, false)
		tear.Height = -500
		tear.FallingAcceleration = 10
		tear.CollisionDamage = player.Damage * 1.2
	elseif player:HasCollectible(RainMod.Id) and Game():GetRoom():GetFrameCount() % (40 + player.MaxFireDelay - (RainMod.RainMultiplier * 5)) == 0 and RainMod.RainMultiplier > 0 and Game():GetRoom():IsClear() == false and (player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) or player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2) or player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_5) or player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X)) then
		local holybeam = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CRACK_THE_SKY, 0, Game():GetRoom():GetRandomPosition(0.8), Vector(0,0), player)
		if RainMod.RainStorm == true then
			Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.PLAYER_CREEP_HOLYWATER, 0, holybeam.Position, Vector(0,0), player)
		end
	end
	if Game():GetRoom():GetFrameCount() == 1 then
		RainMod.RainMultiplier = 0
		RainMod.RainStorm = false
	end
end

function RainMod:onTakeDamage(target, amount, flag, source, num)
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(RainMod.Id) then
		if target.Type == EntityType.ENTITY_PLAYER and RainMod.RainMultiplier < 4 then
			RainMod.RainMultiplier = RainMod.RainMultiplier + 1
			Game():ShakeScreen(5)
			if RainMod.RainMultiplier == 4 and RainMod.RainStorm == false then
				RainMod.RainStorm = true
				SFXManager():Play(SoundEffect.SOUND_HOLY, 1, 0, false, 0.75)
			end
		end
	end
end

RainMod:AddCallback(ModCallbacks.MC_POST_UPDATE, RainMod.onUpdate)
RainMod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, RainMod.onInit)
RainMod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, RainMod.onTakeDamage)

local rain = RegisterMod("rain", 1)
rain.splashTears = {}
rain.teststring = 0
rain.lastRoom = 0
rain.lastSplash = 0
rain.laserSplash = false
rain.hasBootCostume = false

function rain:init()
  rain.splashTears = {}
  rain.lastRoom = 0
  rain.lastSplash = 0
  rain.hasBootCostume = false
end

rain:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, rain.init)

function rain:splash(tearPosition, puddleType)
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.RAINBOOTS) then
    if puddleType == "tear" or puddleType == "knife" or puddleType == "brimstone" then
      local lungTearIndex = #rain.splashTears
      for i=lungTearIndex, lungTearIndex+3+math.random(3) do
        rain.splashTears[i] = {tear = false, speed = 0, height = 0, splashtear = true}
        rain.splashTears[i].tear = player:FireTear(tearPosition, Vector(5*(math.random()+.5)*(math.abs(player.Velocity.X)+math.abs(player.Velocity.Y))/3,0):Rotated(math.random(360)), false, true, false) --tear
        local randomTearBonus = math.random()+.5
        if player:HasCollectible(CollectibleType.COLLECTIBLE_DEATHS_TOUCH) then
          rain.splashTears[i].tear.Scale = rain.splashTears[i].tear.Scale*randomTearBonus*.45
        else
          rain.splashTears[i].tear.Scale = rain.splashTears[i].tear.Scale*randomTearBonus*.9
        end
        if puddleType == "tear" then
          rain.splashTears[i].tear.CollisionDamage = rain.splashTears[i].tear.CollisionDamage*randomTearBonus
        else
          rain.splashTears[i].tear.CollisionDamage = rain.splashTears[i].tear.CollisionDamage*randomTearBonus*2
          rain.splashTears[i].tear:SetColor(Color(1,0,0,1,0,0,0),999999,1,false,true)
        end
        rain.splashTears[i].speed = (5+math.random()*2)*.75*(math.abs(player.Velocity.X)+math.abs(player.Velocity.Y))/8 --speed - Lower numbers are faster
        rain.splashTears[i].height = 70-(math.abs(player.Velocity.X)+math.abs(player.Velocity.Y))*12.5/1.4 --height
        rain.splashTears[i].splashtear = true
      end
    --elseif puddleType == "brimstone" then
      --for i=1, 4+math.random(3) do
        --local brimshot = player:FireDelayedBrimstone(math.random(360),player)
        --brimshot:GetData().splash = true
      --end
    elseif puddleType == "laser" then
      for i=1, 4+math.random(3) do
        local lasershot = player:FireTechLaser(tearPosition+Vector(math.random(20)-10,math.random(20)-10), 1, Vector(1,0):Rotated(math.random(360)), false, false)
        rain.laserSplash = true
      end
    end
  end
end

function rain:splashMovement()
  rain.lastRoomRemove = false
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.RAINBOOTS) then
    if rain.hasBootCostume == false then
      rain.hasBootCostume = true
      player:AddNullCostume(GENESIS_COSTUMES.RAINBOOTS)
    end
    if rain.lastRoom ~= Game():GetRoom():GetDecorationSeed() then
      rain.lastRoom = Game():GetRoom():GetDecorationSeed()
      rain.splashTears = {}
      rain.lastRoomRemove = true
    end
    for i, Entity in pairs(Isaac.GetRoomEntities()) do
      if rain.lastRoomRemove and ((Entity.Type == GENESIS_ENTITIES.PUDDLE_TEAR_TYPE and Entity.Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_TEAR_VARIANT and Entity.SubType == 1) or (Entity.Type == GENESIS_ENTITIES.PUDDLE_LASER_TYPE and Entity.Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_LASER_VARIANT and Entity.SubType == 4) or (Entity.Type == GENESIS_ENTITIES.PUDDLE_KNIFE_TYPE and Entity.Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_KNIFE_VARIANT and Entity.SubType == 3) or (Entity.Type == GENESIS_ENTITIES.PUDDLE_BRIMSTONE_TYPE and Entity.Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_BRIMSTONE_VARIANT and Entity.SubType == 2)) then
        Entity:Remove()
      end
      if Entity.Type == GENESIS_ENTITIES.PUDDLE_LASER_TYPE and Entity.Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_LASER_VARIANT and Entity.SubType == 4 and Entity.FrameCount == 12 then
        local puddleSprite = Entity:GetSprite()
        if puddleSprite:IsPlaying("Puddle1") then
          puddleSprite:Play("Puddle1_Still")
          --Isaac.DebugString("code4")
        elseif puddleSprite:IsPlaying("Puddle2") then
          puddleSprite:Play("Puddle2_Still")
          --Isaac.DebugString("code5")
        elseif puddleSprite:IsPlaying("Puddle3") then
          puddleSprite:Play("Puddle3_Still")
          --Isaac.DebugString("code6")
        elseif puddleSprite:IsPlaying("Puddle4") then
          puddleSprite:Play("Puddle4_Still")
          --Isaac.DebugString("code7")
        end
      end
      if Game():GetFrameCount() > (rain.lastSplash + 2) and (player.Position - Entity.Position):Length() < 30 and math.abs(player.Velocity.X)+math.abs(player.Velocity.Y) > .2 then
        local tempPosition = Entity.Position
        local isPuddle = false
        if Entity.Type == GENESIS_ENTITIES.PUDDLE_TEAR_TYPE and Entity.Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_TEAR_VARIANT and Entity.SubType == 1 then
          rain:splash(tempPosition, "tear")
          isPuddle = true
        elseif Entity.Type == GENESIS_ENTITIES.PUDDLE_BRIMSTONE_TYPE and Entity.Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_BRIMSTONE_VARIANT and Entity.SubType == 2 then
          rain:splash(tempPosition, "brimstone")
          isPuddle = true
        elseif Entity.Type == GENESIS_ENTITIES.PUDDLE_KNIFE_TYPE and Entity.Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_KNIFE_VARIANT and Entity.SubType == 3 then
          rain:splash(tempPosition, "knife")
          isPuddle = true
        elseif Entity.Type == GENESIS_ENTITIES.PUDDLE_LASER_TYPE and Entity.Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_LASER_VARIANT and Entity.SubType == 4 then
          rain:splash(tempPosition, "laser")
          isPuddle = true
        end
        if isPuddle == true then
          rain.lastSplash = Game():GetFrameCount()
          Entity:Remove()
        end
      end
    end
    if #rain.splashTears > 1 then
    for i=0, #rain.splashTears do
      if rain.splashTears[i].tear ~= false then
        if rain.splashTears[i].tear:IsDead() then
          rain.splashTears[i].splashtear = false
        end
      end
      if rain.splashTears[i].splashtear == true then
        rain.splashTears[i].tear.FallingAcceleration = 0
        rain.splashTears[i].tear.FallingSpeed = 0
        local splashTearHeight = ((rain.splashTears[i].tear.FrameCount/rain.splashTears[i].speed-2)^2-4)/4*(75-rain.splashTears[i].height)+player.TearHeight
        if splashTearHeight > -4.5 then
          rain.splashTears[i].tear.Height = -4.5
          rain.splashTears[i].tear:Die()
        elseif splashTearHeight ~= -4.5 then
          rain.splashTears[i].tear.Height = splashTearHeight
        end
      end
    end
    end
  elseif rain.hasBootCostume == true then
    rain.hasBootCostume = false
    player:TryRemoveNullCostume(GENESIS_COSTUMES.RAINBOOTS)
  end
end
rain:AddCallback(ModCallbacks.MC_POST_RENDER, rain.splashMovement)

function rain:onHit(entity, amount, damageflag, source, countdownframes)
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.RAINBOOTS) then
  local isSplash = false
  if #rain.splashTears > 1 then
    for i=1,#rain.splashTears do
      if source.Entity and source.Entity.Index == rain.splashTears[i].tear.Index then
        if rain.splashTears[i].splashtear == true then
        isSplash = true
        end
      end
    end
  end
  if entity:IsVulnerableEnemy() and (source.Type == EntityType.ENTITY_TEAR or (source.Type == 1 and rain.laserSplash == false) or source.Type == EntityType.ENTITY_KNIFE) and isSplash == false then
    local spawnPuddle = true
    local ents = Isaac.GetRoomEntities()
    for i=1,#ents do
      if ((ents[i].Type == GENESIS_ENTITIES.PUDDLE_TEAR_TYPE and ents[i].Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_TEAR_VARIANT and ents[i].SubType == 1) or
        (ents[i].Type == GENESIS_ENTITIES.PUDDLE_KNIFE_TYPE and ents[i].Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_KNIFE_VARIANT and ents[i].SubType == 3) or
        (ents[i].Type == GENESIS_ENTITIES.PUDDLE_BRIMSTONE_TYPE and ents[i].Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_BRIMSTONE_VARIANT and ents[i].SubType == 2) or
        (ents[i].Type == GENESIS_ENTITIES.PUDDLE_LASER_TYPE and ents[i].Variant == GENESIS_ENTITIES.VARIANT.PUDDLE_LASER_VARIANT and ents[i].SubType == 4)) and
        (ents[i].Position - entity.Position):Length() < player.MaxFireDelay*2.5 then
        spawnPuddle = false
      end
    end
    if spawnPuddle == true then
      local puddleEntity
      if source.Type == EntityType.ENTITY_TEAR then
        puddleEntity = Isaac.Spawn(GENESIS_ENTITIES.PUDDLE_TEAR_TYPE, GENESIS_ENTITIES.VARIANT.PUDDLE_TEAR_VARIANT, 1, entity.Position, Vector(0,0), player)
      elseif source.Type == 1 then
        if player:HasCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE) then
          puddleEntity = Isaac.Spawn(GENESIS_ENTITIES.PUDDLE_BRIMSTONE_TYPE, GENESIS_ENTITIES.VARIANT.PUDDLE_BRIMSTONE_VARIANT, 2, entity.Position, Vector(0,0), player)
        elseif player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) or player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2) or player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_5) or player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) then
          puddleEntity = Isaac.Spawn(GENESIS_ENTITIES.PUDDLE_LASER_TYPE, GENESIS_ENTITIES.VARIANT.PUDDLE_LASER_VARIANT, 4, entity.Position, Vector(0,0), player)
        end
      elseif source.Type == EntityType.ENTITY_KNIFE then
        puddleEntity = Isaac.Spawn(GENESIS_ENTITIES.PUDDLE_KNIFE_TYPE, GENESIS_ENTITIES.VARIANT.PUDDLE_KNIFE_VARIANT, 3, entity.Position, Vector(0,0), player)
      end
      local puddleSprite = puddleEntity:GetSprite()
      if math.random() > .75 then
        puddleSprite:Play("Puddle2", true)
        --Isaac.DebugString("code1")
      elseif math.random() > .66 then
        puddleSprite:Play("Puddle3", true)
        --Isaac.DebugString("code2")
      elseif math.random() > .5 then
        puddleSprite:Play("Puddle4", true)
        --Isaac.DebugString("code3")
      end
    end
  end
  rain.laserSplash = false
end
end
rain:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, rain.onHit)

function rain:testing()
  local player = Isaac.GetPlayer(0)
  Isaac.RenderText("x: " .. tostring(rain.hasBootCostume), 300, 15, 255, 0, 0, 255)
end
--rain:AddCallback(ModCallbacks.MC_POST_RENDER, rain.testing);

function rain:chronosClockFix()
  rain.lastSplash = 0
end

rain:AddCallback(ModCallbacks.MC_USE_ITEM, rain.chronosClockFix, Isaac.GetItemIdByName("Chronos Clock"));

--------------
--CANNIBAL--
--------------

--------------------REGESTRATION----------------------
local cannibal = RegisterMod("cannabalism", 1);
--------------------VARIABLES------------------------
cannibal.shamedEnemyID = {} --Array of all shamed enemies
cannibal.numberOfShamed = 1 --How many monsters are shamed. Used to find point in cannibal.shamedEnemyID matrix
cannibal.shamedEnemies = nil --true when enemies are shamed.
cannibal.tD = .05 --Distance to search around tear for entities
cannibal.dressShame = false --true when enemies are being shamed due to the "Cute Dress" item. False when they are shamed due to "Scratch"
cannibal.numberofenemies = 0 --number of active enemies in the room
cannibal.numberofenemiespre = 0 --number of active enemies in the room one frame ago. Used to detect change.
-------------------ANIMATION VARIABLES-----------------------
cannibal.cuteBubble = GENESIS_ENTITIES.CUTE_BUBBLE_TYPE
cannibal.cuteBubbleVariant = GENESIS_ENTITIES.VARIANT.CUTE_BUBBLE_VARIANT
cannibal.cuteBubbleEntity = nil
cannibal.scratchMarks = GENESIS_ENTITIES.SCRATCH_MARKS_TYPE
cannibal.scratchMarksVariant = GENESIS_ENTITIES.VARIANT.SCRATCH_MARKS_VARIANT
cannibal.scratchMarksEntity = nil
cannibal.cuteSound = GENESIS_SFX.CUTE
cannibal.dressCostume = GENESIS_COSTUMES.DRESS
cannibal.status = GENESIS_ENTITIES.STATUS_TYPE
cannibal.statusVariant = GENESIS_ENTITIES.VARIANT.STATUS_VARIANT
cannibal.statusEntity = {}
cannibal.hasDress = false
--------------------RUN INIT--------------------------------
function cannibal:runInit()
  cannibal.numberOfShamed = 1
  cannibal.shamedEnemies = false
  cannibal.hasDress = false
end
cannibal:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, cannibal.runInit)
------------------TEAR ASSIGNMENT--------------------------- Used to assign tears to entities in a array
function cannibal:assignParent()
  local tearTemp
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.CUTEDRESS) then
    for j, Entity in pairs(Isaac.GetRoomEntities()) do
      if (Entity.Type == 9 or Entity.Type == 276) and Entity.Parent == nil then
        tearTemp = Entity
        local tearXPosition = Entity.Position.X
        local tearYPosition = Entity.Position.Y
        for i, Entity in pairs(Isaac.GetRoomEntities()) do
          if (Entity.Position.X >= (tearTemp.Position.X-cannibal.tD)) and (Entity.Position.X <= (tearTemp.Position.X+cannibal.tD)) and (Entity.Position.Y >= (tearTemp.Position.Y-cannibal.tD)) and (Entity.Position.Y <= (tearTemp.Position.Y+cannibal.tD)) and Entity.Type ~= 9 and Entity.Type ~= 276 then
            tearTemp.Parent = Entity
            break
          end
          if tearTemp.Parent == nil then
            for i, Entity in pairs(Isaac.GetRoomEntities()) do
              if Entity:IsBoss() then
                tearTemp.Parent = Entity
              end
            end
          end
          if tearTemp.Parent == nil then
            for i, Entity in pairs(Isaac.GetRoomEntities()) do
              if Entity:IsActiveEnemy() then
                tearTemp.Parent = Entity
              end
            end
          end
        end
      end
    end
  end
end
cannibal:AddCallback(ModCallbacks.MC_NPC_UPDATE, cannibal.assignParent);
----------------------ENTITY GRAB----------------------
function cannibal:useItemScratch() --Scratch
  local player = Isaac.GetPlayer(0)
  local toBeShamed = cannibal:findNearestEnemy(player)
  cannibal.dressShame = false
  cannibal:shameEnemy(toBeShamed)
  Isaac.DebugString("Used Scratch")
 return true
end
cannibal:AddCallback(ModCallbacks.MC_USE_ITEM, cannibal.useItemScratch, GENESIS_ITEMS.ACTIVE.SCRATCH);
----------------------------------------------------
function cannibal:findNearestEnemy(startObject)
  local closestEntity
  local currentEnemyDist = 999
  local closestEnemyDistance = 999
  for i, entity in pairs(Isaac.GetRoomEntities()) do
    if entity:IsVulnerableEnemy() and entity:IsActiveEnemy() and entity.Type ~= 306 then --entity 306 is portal
      currentEnemyDist = (startObject.Position - entity.Position):Length()
      if closestEnemyDistance > currentEnemyDist then
        closestEntity = entity
        closestEnemyDistance = currentEnemyDist
      end
    end
  end
  return closestEntity
end
-----------------------------------------------------------
function cannibal:matchEntityPosition(matcher1, matcher2)
  if matcher1.Position.X == matcher2.Position.X and matcher1.Position.Y == matcher2.Position.Y then
    return true
  else
    return false
  end
end
------------------------------------------------------
function cannibal:useItemDress(dmg_target, dmg_amount, dmg_flags, dmg_source, dmg_frames) --dress
  local player = Isaac.GetPlayer(0)
  local toBeShamed
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.CUTEDRESS) then
    toBeShamed = cannibal:findDamageParent(dmg_source)
    if toBeShamed ~= false then
      cannibal.dressShame = true
      cannibal:shameEnemy(toBeShamed)
    end
  end
end
cannibal:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, cannibal.useItemDress, EntityType.ENTITY_PLAYER);
--------------------------------------------------------------------
function cannibal:findDamageParent(damageSource)
  local damageParent = false
  local roomEntities = Isaac.GetRoomEntities()
  for i=1, #roomEntities do
    if cannibal:matchEntityPosition(roomEntities[i], damageSource.Entity) then
      if damageSource.Type == 9 then
        damageParent = roomEntities[i].Parent
        break
      else
        damageParent = roomEntities[i]
        break
      end
    end
  end
  return damageParent
end
------------------------SHAMING------------------------
function cannibal:shameEnemy(shamedEnemy)
  local player = Isaac.GetPlayer(0)
  local spriteStatus
  if shamedEnemy:IsVulnerableEnemy() and shamedEnemy:IsActiveEnemy() then
    cannibal.shamedEnemies = true
    local bubbleType = 1
    cannibal.shamedEnemyID[cannibal.numberOfShamed] = shamedEnemy
    local soundFX = shamedEnemy:ToNPC();
    if cannibal.dressShame == true then
      cannibal.statusEntity[cannibal.numberOfShamed] = Isaac.Spawn(cannibal.status, cannibal.statusVariant, 0, shamedEnemy.Position-Vector(0,shamedEnemy.Size*shamedEnemy.SizeMulti.Y+48), Vector(0,0), shamedEnemy)
      cannibal.statusEntity[cannibal.numberOfShamed]:ToEffect():FollowParent(shamedEnemy)
      cannibal.statusEntity[cannibal.numberOfShamed].RenderZOffset = 999999
      spriteStatus = cannibal.statusEntity[cannibal.numberOfShamed]:GetSprite();
      spriteStatus:Play("cuteStatus", true)
      local sfx = SFXManager()
      cannibal:EnemySpawnCallback()
      if cannibal.numberofenemies == 1 then
      elseif cannibal.numberofenemies == 2 then
        SFXManager():Play(25, 1, 0, false, (math.random()*.25+.875))
      else
        SFXManager():Play(cannibal.cuteSound, 1, 0, false, (math.random()*.25+.875))
      end
    else
      cannibal.scratchMarksEntity = Isaac.Spawn(cannibal.scratchMarks, cannibal.scratchMarksVariant, 0, shamedEnemy.Position+Vector(25,10), Vector(0,0), shamedEnemy)
      cannibal.scratchMarksEntity:ToEffect():FollowParent(shamedEnemy)
      cannibal.scratchMarksEntity.RenderZOffset = 999999
      cannibal.statusEntity[cannibal.numberOfShamed] = Isaac.Spawn(cannibal.status, cannibal.statusVariant, 0, shamedEnemy.Position-Vector(0,shamedEnemy.Size*shamedEnemy.SizeMulti.Y+48), Vector(0,0), shamedEnemy)
      cannibal.statusEntity[cannibal.numberOfShamed]:ToEffect():FollowParent(shamedEnemy)
      cannibal.statusEntity[cannibal.numberOfShamed].RenderZOffset = 999999
      spriteStatus = cannibal.statusEntity[cannibal.numberOfShamed]:GetSprite();
      spriteStatus:Play("scratchStatus", true)
      soundFX:PlaySound(30,1, 0, false, 1)
    end
    cannibal.numberOfShamed = cannibal.numberOfShamed + 1
    for i, entity in pairs(Isaac.GetRoomEntities()) do
      if entity:IsActiveEnemy() and ((entity.Position.X ~= shamedEnemy.Position.X) or (entity.Position.Y ~= shamedEnemy.Position.Y)) then
        if entity:IsVulnerableEnemy() then
          entity:AddEntityFlags(EntityFlag.FLAG_CHARM)
          entity.Target = shamedEnemy
          if cannibal.dressShame == true and math.random() > .65 then
            cannibal.cuteBubbleEntity = Isaac.Spawn(cannibal.cuteBubble, cannibal.cuteBubbleVariant, 0, entity.Position+Vector(0,1), Vector(0,0), entity):ToEffect()
            cannibal.cuteBubbleEntity.RenderZOffset = 999999
            local sprite = cannibal.cuteBubbleEntity:GetSprite();
            if bubbleType == 1 then
              sprite:Play("Bubble_01", true)
              bubbleType = 2
            elseif bubbleType == 2 then
              sprite:Play("Bubble_02", true)
              bubbleType = 3
            else
              sprite:Play("Bubble_03", true)
              bubbleType = 1
            end
            cannibal.cuteBubbleEntity:ToEffect():FollowParent(entity)
          end
        end
      end
    end
    for i=1, (cannibal.numberOfShamed) do
      if cannibal.shamedEnemyID[i] ~= nil then
        cannibal.shamedEnemyID[i]:ClearEntityFlags(EntityFlag.FLAG_CHARM)
        cannibal.shamedEnemyID[i].Target = player
      end
    end
  end
end
--------------------------------------------------------------------------------
function cannibal:EnemySpawnCallback()
  if cannibal.shamedEnemies == true then
    cannibal.numberofenemies = 0
    for i, entity in pairs(Isaac.GetRoomEntities()) do
      if entity:IsActiveEnemy() and entity:IsVulnerableEnemy() and entity.Type ~= 306 then
        cannibal.numberofenemies = cannibal.numberofenemies + 1
      end
    end
    if cannibal.numberofenemies > cannibal.numberofenemiespre and cannibal.shamedEnemies == true then
      cannibal:shameUpdate()
    else
      cannibal.numberofenemiespre = cannibal.numberofenemies
    end
  end
end
cannibal:AddCallback(ModCallbacks.MC_NPC_UPDATE, cannibal.EnemySpawnCallback);
-------------------------------------------------------------------
function cannibal:shameUpdate() --used to add charm to newly spawned enemies
  local player = Isaac.GetPlayer(0)
  for i, entity in pairs(Isaac.GetRoomEntities()) do
    if entity:IsVulnerableEnemy() and entity:IsActiveEnemy() then
      entity:AddEntityFlags(EntityFlag.FLAG_CHARM)
      entity.Target = cannibal.shamedEnemyID[cannibal.numberOfShamed-1]
    end
  end
  for i=1, (cannibal.numberOfShamed-1) do
   cannibal.shamedEnemyID[i]:ClearEntityFlags(EntityFlag.FLAG_CHARM)
   cannibal.shamedEnemyID[i].Target = player
  end
end
-------------------------ENDSHAMING------------------------
function cannibal:endShaming()
  local player = Isaac.GetPlayer(0)
  local stillShamed
  if cannibal.shamedEnemies == true then
    for n=1, cannibal.numberOfShamed do
      if cannibal.shamedEnemyID[n-1] ~= nil then
        if cannibal.shamedEnemyID[n-1]:IsActiveEnemy() == false then
          if cannibal.statusEntity[n-1] ~= nil then
            cannibal.statusEntity[n-1]:Remove()
          end
        end
      end
    end
    repeat
      stillShamed = false
      if cannibal.shamedEnemyID[cannibal.numberOfShamed-1] ~= nil then
        if cannibal.shamedEnemyID[cannibal.numberOfShamed-1]:IsActiveEnemy() == false then
          cannibal.numberOfShamed = cannibal.numberOfShamed - 1
          stillShamed = true
        end
      end
    until stillShamed == false
    if cannibal.numberOfShamed == 1 then
      for i, entity in pairs(Isaac.GetRoomEntities()) do
        entity:ClearEntityFlags(EntityFlag.FLAG_CHARM)
        entity.Target = player
      end
      cannibal.shamedEnemies = false
    end
  end
end
cannibal:AddCallback(ModCallbacks.MC_NPC_UPDATE, cannibal.endShaming);
--------------------Graphics----------------------------
function cannibal:graphics()
  local player = Isaac.GetPlayer(0)
  if cannibal.dressShame == false and cannibal.shamedEnemies == true and Isaac.GetFrameCount() % 38 == 0 then
    for i=1, cannibal.numberOfShamed-1 do
      if cannibal.shamedEnemyID[i]:IsActiveEnemy() then
        local scratchMarksBlood = Isaac.Spawn(1000, 2, 0, cannibal.shamedEnemyID[i].Position, Vector(0,0), cannibal.shamedEnemyID[i]):ToEffect()
        scratchMarksBlood.RenderZOffset = 999999
      end
    end
  end
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.CUTEDRESS) then
    if cannibal.hasDress == false then
      cannibal.hasDress = true
      player:AddNullCostume(cannibal.dressCostume)
    end
  elseif cannibal.hasDress == true then
    player:TryRemoveNullCostume(cannibal.dressCostume)
    cannibal.hasDress = false
  end
end
cannibal:AddCallback(ModCallbacks.MC_POST_RENDER, cannibal.graphics);
-----------------------Testing-------------------------------
function cannibal:testing()
  Isaac.RenderText("cannibal.shamedEnemies: " .. tostring(0), 300, 5, 255, 0, 0, 255)
end
--cannibal:AddCallback(ModCallbacks.MC_POST_RENDER, cannibal.testing);

--------------
--SAD HEART--
--------------

local SadHeart = RegisterMod("Sad Heart", 1)

function SadHeart:onUpdate(player)
    local data = player:GetData()
    if not player:HasCollectible(GENESIS_ITEMS.PASSIVE.SAD_HEART) then
        data.SadHeart = nil
        return
    end

    if not data.SadHeart then
        data.SadHeart = { prevMaxHP = player:GetMaxHearts() }
		player:AddNullCostume(GENESIS_COSTUMES.SAD_HEART)
    end

    if player:GetMaxHearts() ~= data.SadHeart.prevMaxHP then
		player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
		player:EvaluateItems()
		data.SadHeart.prevMaxHP = player:GetMaxHearts()
	end
end

function SadHeart:onCache(player, cacheFlag)
    if cacheFlag == CacheFlag.CACHE_DAMAGE then
        local count = player:GetCollectibleNum(GENESIS_ITEMS.PASSIVE.SAD_HEART)
		if count > 0 then
			local damageIncrease = 0.125 * player:GetMaxHearts()
			player.Damage = player.Damage + damageIncrease * count
		end
	end
end

SadHeart:AddCallback(ModCallbacks.MC_POST_PLAYER_UPDATE, SadHeart.onUpdate)
SadHeart:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, SadHeart.onCache)

-----------------
--Armor of Ares--
-----------------

local AresItemMod = RegisterMod('ArmorOfAres',1)

function AresItemMod:CacheUpdate(player,cacheFlag)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.ARMOR_OF_ARES) then
		if cacheFlag == CacheFlag.CACHE_DAMAGE then
			player.Damage = player.Damage + 1
		end
	end
end

function AresItemMod:OpenDoors(player)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.ARMOR_OF_ARES) and Game():GetRoom():IsClear() then
		local room = Game():GetRoom()
		local num = room:GetGridSize()
			for i=0, DoorSlot.NUM_DOOR_SLOTS - 1 do
				local door = room:GetDoor(i)
				if door ~= nil and door:IsRoomType(RoomType.ROOM_CHALLENGE) and room:IsClear() == true and room:GetType() ~= RoomType.ROOM_CHALLENGE then
					if door:IsOpen() == false then
						door:SpawnDust()
					end
					door:Open()
					door:GetSaveState().State = DoorState.STATE_OPEN
				end
			end
		if room:GetType() == RoomType.ROOM_CHALLENGE and room:IsFirstVisit() == true and room:GetFrameCount() == 1 then
			local challengePool = {209, 220, 140, 131, 125, 137, 106, 37, 19, 483, 51, 79, 80, 81, 133, 134, 145, 212, 215, 216, 225, 241, 260, 408, 371, 508, 503, 496, 475, 468, 451, 442}
			Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, challengePool[math.random(#challengePool)], Vector(450,275), Vector(0,0), player)
			Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, challengePool[math.random(#challengePool)], Vector(190,275), Vector(0,0), player)
			Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CRACK_THE_SKY, 0, Vector(450,267), Vector(0,0), player)
			Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CRACK_THE_SKY, 0, Vector(190,267), Vector(0,0), player)
			player:AnimateHappy()
		end
	end
end

AresItemMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, AresItemMod.CacheUpdate)
AresItemMod:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, AresItemMod.OpenDoors)

--------------------REGESTRATION----------------------
local dojo = RegisterMod("Dojo", 1);
--------------------VARIABLES------------------------
dojo.tD = .05 --Distance to search around tear for entities
dojo.dojoTime = false
dojo.teststring = 0
dojo.dojoTarget = nil
dojo.practiceDummyEntity = Isaac.GetEntityTypeByName("Practice Dummy")
dojo.practiceDummyVariant = Isaac.GetEntityVariantByName("Practice Dummy")
dojo.band_targetEntity = Isaac.GetEntityTypeByName("bandanimation_target")
dojo.band_targetVariant = Isaac.GetEntityVariantByName("bandanimation_target")
dojo.versesAnimationEntity = Isaac.GetEntityTypeByName("versesAnimation")
dojo.versesAnimationVariant = Isaac.GetEntityVariantByName("versesAnimation")
dojo.band_charaEntity = Isaac.GetEntityTypeByName("bandanimation_character")
dojo.band_charaVariant = Isaac.GetEntityVariantByName("bandanimation_character")
dojo.band_costume = Isaac.GetCostumeIdByPath("gfx/bandana_costume.anm2")
dojo.belt_costume = Isaac.GetCostumeIdByPath("gfx/purple_belt.anm2")
dojo.lastBandana = 0
dojo.bandanaCost = false
dojo.beltCost = false
dojo.sound = SFXManager()

function dojo:init()
  dojo.dojoTime = false
  dojo.lastBandana = 0
  dojo.bandanaCost = false
  dojo.beltCost = false
end
dojo:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, dojo.init);


function dojo:practiceDummyUse()
  local room = Game():GetRoom()
  local player = Isaac.GetPlayer(0)
  local freePosition = Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 1.0, true)
  local practiceSpawn = Game():Spawn(dojo.practiceDummyEntity,dojo.practiceDummyVariant,freePosition, Vector(0,0), player,1,1)
end
dojo:AddCallback(ModCallbacks.MC_USE_ITEM, dojo.practiceDummyUse, GENESIS_ITEMS.ACTIVE.DUMMY);

function dojo:practiceDummyHit(dmg_target, dmg_amount, dmg_flags, dmg_source, dmg_frames)
  local sprite = dmg_target:GetSprite()
  sprite:SetAnimation ("Damaged")
  sprite:Play("Damaged", true)
  local player = Isaac.GetPlayer(0)
  for i, Entity in pairs(Isaac.GetRoomEntities()) do
    if Entity:IsVulnerableEnemy() and Entity.Type ~= dojo.practiceDummyEntity then
      Entity:TakeDamage(dmg_amount,0,EntityRef(player),0)
    end
  end
end

dojo:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, dojo.practiceDummyHit, dojo.practiceDummyEntity);

function dojo:bandana(dmg_target, dmg_amount, dmg_flags, dmg_source, dmg_frames)
  local player = Isaac.GetPlayer(1)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.BANDANA) then
    local randomValue
    if (dojo.lastBandana+6) < Game():GetFrameCount() then
      if player.Luck >= 12.5 then
        randomValue = math.random()-.25
      else
        randomValue = math.random()-(player.Luck/50)
      end
      if randomValue < .25 then
        local toBeCountered = dojo:findDamageParent(dmg_source)
        local bandanimation_target = Isaac.Spawn(dojo.band_targetEntity, dojo.band_targetVariant, 0, toBeCountered.Position, Vector(0,0), toBeCountered)
        bandanimation_target:ToEffect():FollowParent(toBeCountered)
        bandanimation_target.RenderZOffset = 999999
        local bandanimation_chara = Isaac.Spawn(dojo.band_charaEntity, dojo.band_charaVariant, 0, player.Position, Vector(0,0), player)
        bandanimation_chara:ToEffect():FollowParent(player)
        player:SetColor(Color(1,1,1,0,0,0,0),10,1, false, false)
        dojo.lastBandana = Game():GetFrameCount()
        if toBeCountered:IsBoss() then
          local soundFX = toBeCountered:ToNPC();
          toBeCountered:TakeDamage(40,0,EntityRef(player),0)
          soundFX:PlaySound(30,1, 0, false, 1)
        else
          toBeCountered:Kill()
        end
      return false
      end
    else
      return false
    end
  end
end

dojo:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, dojo.bandana, EntityType.ENTITY_PLAYER);

function dojo:chronosClockFix()
  dojo.lastBandana = 0
end

dojo:AddCallback(ModCallbacks.MC_USE_ITEM, dojo.chronosClockFix, Isaac.GetItemIdByName("Chronos Clock"));

function dojo:dojoCostumes()
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.BANDANA) then
		if dojo.bandanaCost == false then
			player:AddNullCostume(GENESIS_COSTUMES.BANDANA)
			dojo.bandanaCost = true
		end
	elseif bandanaCost == true then
    player:RemoveCostume(GENESIS_COSTUMES.BANDANA)
    dojo.bandanaCost = false
  end
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.PURPLE_BELT) then
    if dojo.beltCost == false then
      player:AddNullCostume(GENESIS_COSTUMES.BELT)
      dojo.beltCost = true
    end
  elseif beltCost == true then
    player:RemoveCostume(GENESIS_COSTUMES.BELT)
    dojo.beltCost = false
  end
end

dojo:AddCallback(ModCallbacks.MC_POST_RENDER, dojo.dojoCostumes)
function dojo:assignParent()
  local tearTemp
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.PURPLE_BELT) or player:HasCollectible(GENESIS_ITEMS.PASSIVE.BANDANA) then
    for j, Entity in pairs(Isaac.GetRoomEntities()) do
      if (Entity.Type == 9 or Entity.Type == 276) and Entity.Parent == nil then
        tearTemp = Entity
        local tearXPosition = Entity.Position.X
        local tearYPosition = Entity.Position.Y
        for i, Entity in pairs(Isaac.GetRoomEntities()) do
          if (Entity.Position.X >= (tearTemp.Position.X-dojo.tD)) and (Entity.Position.X <= (tearTemp.Position.X+dojo.tD)) and (Entity.Position.Y >= (tearTemp.Position.Y-dojo.tD)) and (Entity.Position.Y <= (tearTemp.Position.Y+dojo.tD)) and Entity.Type ~= 9 and Entity.Type ~= 276 then
            tearTemp.Parent = Entity
            break
          end
          if tearTemp.Parent == nil then
            for i, Entity in pairs(Isaac.GetRoomEntities()) do
              if Entity:IsBoss() then
                tearTemp.Parent = Entity
              end
            end
          end
          if tearTemp.Parent == nil then
            for i, Entity in pairs(Isaac.GetRoomEntities()) do
              if Entity:IsActiveEnemy() then
                tearTemp.Parent = Entity
              end
            end
          end
        end
      end
    end
  end
end
dojo:AddCallback(ModCallbacks.MC_NPC_UPDATE, dojo.assignParent);

function dojo:dojoTime(dmg_target, dmg_amount, dmg_flags, dmg_source, dmg_frames) --dress
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.PURPLE_BELT) then
		if dojo.dojoTime == false then
			if Game():GetRoom():GetAliveEnemiesCount() > 1 then
				dojo.dojoTarget = dojo:findDamageParent(dmg_source)
				if dojo.dojoTarget then
					dojo.teststring = dojo.dojoTarget.Type
					if dmg_target.Type == 1 and dojo.dojoTarget ~= false and dojo.dojoTarget.Type ~= 78 then
						for i, Entity in pairs(Isaac.GetRoomEntities()) do
							if Entity:IsVulnerableEnemy() and dojo:matchEntityPosition(dojo.dojoTarget, Entity) == false then
								Entity:AddEntityFlags(EntityFlag.FLAG_FREEZE)
								Entity:SetColor(Color(.25,.25,.25,1,0,0,0), 99999, 0, false, false)
							end
						end
						local versesTarget = Isaac.Spawn(dojo.versesAnimationEntity, dojo.versesAnimationVariant, 0, dojo.dojoTarget.Position+Vector(-20,-80), Vector(0,0), dojo.dojoTarget)
						dojo.sound:Play(190,0.6,0,false,1)
						--versesTarget:ToEffect():FollowParent(dojoTarget)
						versesTarget.RenderZOffset = 999999
						dojo.dojoTime = true
					end
				end
			end
		elseif dmg_target.Type == 1 then
			if dojo:matchEntityPosition(dojo:findDamageParent(dmg_source),dojo.dojoTarget) == false then
				return false
			end
		elseif dojo:matchEntityPosition(dojo.dojoTarget, dmg_target) == false and dmg_target.Type ~= 1 then
			return false
		end
	end
end
dojo:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, dojo.dojoTime);

function dojo:endDojoTime()
  if dojo.dojoTime == true then
    if dojo.dojoTarget:IsDead() then
      for i, Entity in pairs(Isaac.GetRoomEntities()) do
        Entity:ClearEntityFlags(EntityFlag.FLAG_FREEZE)
        Entity:SetColor(Color(1,1,1,1,0,0,0), 99999, 0, false, false)
        dojo.dojoTime = false
      end
    end
  end
end
dojo:AddCallback(ModCallbacks.MC_NPC_UPDATE, dojo.endDojoTime);


function dojo:findDamageParent(damageSource)
  local damageParent = false
  local roomEntities = Isaac.GetRoomEntities()
  for i=1, #roomEntities do
    if dojo:matchEntityPosition(roomEntities[i], damageSource.Entity) then
      if damageSource.Type == 9 or damageSource.Type == 276 then
        damageParent = roomEntities[i].Parent
      elseif roomEntities[i]:IsVulnerableEnemy() then
        damageParent = roomEntities[i]
      end
      break
    end
  end
  return damageParent
end

function dojo:matchEntityPosition(matcher1, matcher2)
  if matcher1 and matcher2 and matcher1.Position.X == matcher2.Position.X and matcher1.Position.Y == matcher2.Position.Y then
    return true
  else
    return false
  end
end

function dojo:testing()
  Isaac.RenderText("teststring: " .. tostring(dojo.teststring), 300, 5, 255, 0, 0, 255)
end
--dojo:AddCallback(ModCallbacks.MC_POST_RENDER, dojo.testing);

function dojo:audiotest()
  dojo.teststring = dojo.teststring + 1
  dojo.sound:Play(dojo.teststring,0.6,0,false,1)
end

--dojo:AddCallback(ModCallbacks.MC_USE_ITEM, dojo.audiotest);

-- Little Forsaken

local Forsaken = RegisterMod('Forsaken', 1)
local ForsakenVariant = Isaac.GetEntityVariantByName("Little Forsaken")
local hasitem = false
local vectorconstant = nil

function Forsaken:Update(e)
local room = Game():GetRoom()
local player = Isaac.GetPlayer(0)
local angle = math.random(360)
local laser = {}
local sprite = e:GetSprite()
sprite.Scale = sprite.Scale * 0.33
  if Game():GetFrameCount() == 1 then
    vectorconstant = Vector(math.random(70,-70),math.random(70,-70))
  end
  if room:GetAliveEnemiesCount() > 0 then
    e:MoveDiagonally(2)
    e.GridCollisionClass = 10
    e:GetSprite():Play("FadeOut", true)
    if player:HasTrinket(89) and player.Position:Distance(e.Position) > 75 then
      e.Velocity = (player.Position - e.Position)/10
    end
else
    e.OrbitDistance = Vector(80, 80)
    e.OrbitLayer = 98
    e.OrbitSpeed = 0.02
    e.Velocity = e:GetOrbitPosition(player.Position + player.Velocity) - e.Position
    e.GridCollisionClass = 0
  end
  if player:HasTrinket(109) then
      e.Position = player.Position + vectorconstant
      e.Velocity = Vector(0,0)
    end
  if e.FrameCount % 180 == 0 and room:GetAliveEnemiesCount() > 0 then
      laser[1] = EntityLaser.ShootAngle(1, e.Position, angle, 30, Vector(0,0), e)
			laser[1].MaxDistance = 100
      laser[1].IsActiveRotating = true
      laser[1].RotationDegrees = 360
      laser[1].RotationDelay = 0
      laser[1].RotationSpd = 4
      laser[1].GridHit = false
      laser[1].Position = e.Position
      laser[2] = EntityLaser.ShootAngle(1, e.Position, angle+120, 30, Vector(0,0), e)
			laser[2].MaxDistance = 100
      laser[2].IsActiveRotating = true
      laser[2].RotationDegrees = 360
      laser[2].RotationDelay = 0
      laser[2].RotationSpd = 4
      laser[2].GridHit = false
      laser[2].Position = e.Position
      laser[3] = EntityLaser.ShootAngle(1, e.Position, angle+240, 30, Vector(0,0), e)
			laser[3].MaxDistance = 100
      laser[3].IsActiveRotating = true
      laser[3].RotationDegrees = 360
      laser[3].RotationDelay = 0
      laser[3].RotationSpd = 4
      laser[3].GridHit = false
      laser[3].Position = e.Position
      if player:HasCollectible(247) then
        laser[1].CollisionDamage = 4
        laser[2].CollisionDamage = 4
        laser[3].CollisionDamage = 4
      else
        laser[1].CollisionDamage = 2
        laser[2].CollisionDamage = 2
        laser[3].CollisionDamage = 2
      end
  end
  if e.FrameCount % 180 <= 20 and room:GetAliveEnemiesCount() > 0 then
    e:GetSprite():Play("Blasting", true)
  end
  if e.FrameCount % 180 <= 30 and e.FrameCount % 180 > 20 and room:GetAliveEnemiesCount() > 0 then
    e:GetSprite():Play("BlastEnd", true)
  end
end

Forsaken:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE , Forsaken.Update, ForsakenVariant)

function Forsaken:FamilliarInit()
  local player = Isaac.GetPlayer(0)
  local entities = Isaac.GetRoomEntities()
    if Game():GetFrameCount() == 1 then
      hasitem = false
    end
    if player:HasCollectible(GENESIS_ITEMS.FAMILIAR.LIL_FORSAKEN) and hasitem == false then
        Isaac.Spawn(3, ForsakenVariant, 0, player.Position, Vector(0,0), nil)
        hasitem = true
    end
    if not player:HasCollectible(GENESIS_ITEMS.FAMILIAR.LIL_FORSAKEN) and hasitem == true then
        for i = 1, #entities do
            if entities[i].Type == EntityType.ENTITY_FAMILIAR and entities[i].Variant == ForsakenVariant then
                entities[i]:Remove()
                hasitem = false
            end
        end
    end
end

Forsaken:AddCallback(ModCallbacks.MC_POST_UPDATE, Forsaken.FamilliarInit)

-- Water Balloon

local balloon = RegisterMod("Water Balloon", 1)
balloon.testString = 0
balloon.tearMass = 0
balloon.collisionHeight = -70
balloon.cacheUpdate = false
balloon.BalloonGFX = "gfx/projectiles/balloon_tears.png"
balloon.bombBalloonGFX = "gfx/projectiles/balloon_tears_bomb.png"
balloon.laserBalloonGFX = "gfx/projectiles/balloon_tears_laser.png"
balloon.brimstoneBalloonGFX = "gfx/projectiles/balloon_tears_Brimstone.png"
balloon.game = Game()
balloon.technology = false
balloon.technology2 = false
balloon.brimstone = false
balloon.drfetus = false
balloon.numOfTears = 6
balloon.numOfCollectibles = 0
balloon.incubusVariant = Isaac.GetEntityVariantByName("Incubus")
balloon.ember = Isaac.GetEntityVariantByName("Ember Particle")

function balloon:init()
  balloon.technology = false
  balloon.technology2 = false
  balloon.brimstone = false
  balloon.drfetus = false
  balloon.collisionHeight = -70
end
balloon:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, balloon.init)
function balloon:postrender()
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.BALLOON) then
    if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY)
      balloon.technology = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2)
      balloon.technology2 = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE)
      balloon.brimstone = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_DR_FETUS) and player:HasCollectible(CollectibleType.COLLECTIBLE_SAD_BOMBS) == false then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_DR_FETUS)
      balloon.drfetus = true
    end
    if balloon.drfetus and player:HasCollectible(CollectibleType.COLLECTIBLE_SAD_BOMBS) then
      player:AddCollectible(CollectibleType.COLLECTIBLE_DR_FETUS, 6, true)
      balloon.drfetus = false
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_MONSTROS_LUNG) or balloon.drfetus then
      balloon.numOfTears = 3
    else
      balloon.numOfTears = 6
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_MY_REFLECTION) then
      balloon.collisionHeight = -100
    else
      balloon.collisionHeight = -70
    end
    if player:GetCollectibleCount() ~= balloon.numOfCollectibles then
      balloon.cacheUpdate = false
      balloon.numOfCollectibles = player:GetCollectibleCount()
    end
    if balloon.cacheUpdate == false then
      player:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
      player:EvaluateItems()
      balloon.cacheUpdate = true
    end
    for i, Entity in pairs(Isaac.GetRoomEntities()) do
      if Entity.Type == EntityType.ENTITY_TEAR then
        balloon.testString = Entity.SpawnerVariant
      end
      if (Entity.Type == EntityType.ENTITY_TEAR and (Entity.SpawnerType == EntityType.ENTITY_PLAYER or (Entity.SpawnerType == EntityType.ENTITY_FAMILIAR and Entity.SpawnerVariant == incubusVariant)) and ((player:HasCollectible(CollectibleType.COLLECTIBLE_PARASITE) == false and player:HasCollectible(CollectibleType.COLLECTIBLE_COMPOUND_FRACTURE) == false and player:HasCollectible(CollectibleType.COLLECTIBLE_CRICKETS_BODY) == false) or Entity.CollisionDamage == player.Damage)) or Entity:GetData().normalTear == true then
        if Entity:IsDead() == false then
          local sprite = Entity:GetSprite()
          sprite:GetFilename()
          if Entity:GetData().normalTear == nil then
            if sprite:GetFilename() == "gfx/002.000_Tear.anm2" or sprite:GetFilename() == "gfx/002.001_Blood Tear.anm2" then
              if balloon.brimstone then
                sprite:ReplaceSpritesheet(0, balloon.brimstoneBalloonGFX)
              elseif balloon.drfetus then
                sprite:ReplaceSpritesheet(0, balloon.bombBalloonGFX)
              elseif balloon.technology or balloon.technology2 then
                sprite:ReplaceSpritesheet(0, balloon.laserBalloonGFX)
              else
                sprite:ReplaceSpritesheet(0, balloon.BalloonGFX)
              end
              sprite:LoadGraphics()
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_DEATHS_TOUCH) then
              Entity:ToTear().Scale = Entity:ToTear().Scale*.6*1.5*.95
            else
              Entity:ToTear().Scale = Entity:ToTear().Scale*1.2*1.5*.95
            end
            Entity:GetData().normalTear = true
            Entity:ToTear().CollisionDamage = Entity:ToTear().CollisionDamage*1.5
            if player:HasCollectible(CollectibleType.COLLECTIBLE_TINY_PLANET) == false then
              Entity:ToTear().FallingSpeed = player.TearHeight*.75/player.ShotSpeed*(23.75/-player.TearHeight)
              Entity:ToTear().FallingAcceleration = 1.3
            end
          end
          if Entity:ToTear().Height < balloon.collisionHeight and Entity:ToTear().EntityCollisionClass ~= 0 then
            tearMass = Entity:ToTear().EntityCollisionClass
            Entity:ToTear().EntityCollisionClass = 0
          elseif Entity:ToTear().Height >= balloon.collisionHeight and Entity:ToTear().EntityCollisionClass == 0 then
            Entity:ToTear().EntityCollisionClass = balloon.tearMass
          end
          if Entity:GetData().normalTear == true then
            sprite.Rotation = game:GetFrameCount()*20
          end
        else
          if Entity:GetData().normalTear == true then
            for i=1, math.random(balloon.numOfTears)+balloon.numOfTears do
              if balloon.brimstone then
                local haha = Isaac.Spawn(1000,balloon.ember,0, Entity.Position, Vector(0,0), player)
                local brimstonetear = player:FireDelayedBrimstone(math.random(360), haha)
              elseif balloon.drfetus then
                player:FireBomb(Entity.Position, Vector(player.ShotSpeed*5,0):Rotated(math.random(360)))
              elseif balloon.technology or balloon.technology2 then
                local lasertear = player:FireTechLaser(Entity.Position,1,Vector(1,0):Rotated(math.random(360)), false, false)
                lasertear.CollisionDamage = lasertear.CollisionDamage/1.5
              else
                local splashTear = player:FireTear(Entity.Position, Vector(player.ShotSpeed*10,0):Rotated(math.random(360)), true,true,false)
                local tearBonus = math.random()*.5+.75
                splashTear:GetData().normalTear = false
                splashTear:ToTear().CollisionDamage = Entity:ToTear().CollisionDamage/1.5*tearBonus
                if player:HasCollectible(CollectibleType.COLLECTIBLE_DEATHS_TOUCH) then
                  splashTear:ToTear().Scale = Entity:ToTear().Scale*.8*tearBonus/1.2*.5*.85/.95
                else
                  splashTear:ToTear().Scale = Entity:ToTear().Scale*.8*tearBonus/1.2*.85/.95
                end
                splashTear:ToTear().FallingSpeed = player.TearHeight*.5*(math.random()*.75+.5)
                splashTear:ToTear().FallingAcceleration = 1.3
              end
            end
            Entity:GetData().normalTear = false
          end
        end
      end
    end
  elseif balloon.cacheUpdate == true then
    player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
    player:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
    player:EvaluateItems()
    balloon.cacheUpdate = false
  elseif balloon.technology then
    player:AddCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY)
  elseif balloon.technology2 then
    player:AddCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2)
  elseif balloon.brimstone then
    player:AddCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE)
  elseif balloon.drfetus then
    player:AddCollectible(CollectibleType.COLLECTIBLE_DR_FETUS)
  end
end
balloon:AddCallback(ModCallbacks.MC_POST_RENDER, balloon.postrender)

function balloon:ballooncache(player, cacheFlag)
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.BALLOON) and player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_KNIFE) == false and player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) == false and player:HasCollectible(CollectibleType.COLLECTIBLE_EPIC_FETUS) == false and (player:HasCollectible(CollectibleType.COLLECTIBLE_CHOCOLATE_MILK) and (player:HasCollectible(CollectibleType.COLLECTIBLE_COMPOUND_FRACTURE) or player:HasCollectible(CollectibleType.COLLECTIBLE_CRICKETS_BODY) or player:HasCollectible(CollectibleType.COLLECTIBLE_PARASITE))) == false then
    if cacheFlag == CacheFlag.CACHE_FIREDELAY then
      player.MaxFireDelay = math.floor(player.MaxFireDelay*3.5)
    end
  end
end


balloon:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, balloon.ballooncache)

function balloon:debug()
    local player = Isaac.GetPlayer(0)
    Isaac.RenderText("x: " .. tostring(balloon.testString), 250, 15, 255, 0, 0, 255)
end

--balloon:AddCallback(ModCallbacks.MC_POST_RENDER, balloon.debug)

function balloon:diceFix()
  local itemAddNumber = 0
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.BALLOON) then
    if balloon.technology then
      balloon.technology = false
      itemAddNumber = itemAddNumber + 1
    end
    if balloon.technology2 then
      balloon.technology2 = false
      itemAddNumber = itemAddNumber + 1
    end
    if balloon.brimstone then
      balloon.brimstone = false
      itemAddNumber = itemAddNumber + 1
    end
    if balloon.drfetus then
      balloon.drfetus = false
      itemAddNumber = itemAddNumber + 1
    end
    local giveitemConfig
    local giveitem
    for i=1, itemAddNumber do
      repeat
        giveitem = math.random(519)
        giveitemConfig = Isaac.GetItemConfig():GetCollectible(giveitem)
      until giveitemConfig.Type == ItemType.ITEM_PASSIVE and player:HasCollectible(giveitem) == false
      player:AddCollectible(giveitem,6,true)
    end
  end
end
balloon:AddCallback(ModCallbacks.MC_USE_ITEM, balloon.diceFix, CollectibleType.COLLECTIBLE_D4)
balloon:AddCallback(ModCallbacks.MC_USE_ITEM, balloon.diceFix, CollectibleType.COLLECTIBLE_D100)

-- Virtue of Patience

local virtue = RegisterMod( "Virtue of Patience", 1);

function virtue:EvaluateCache(player, cacheFlag)
    local maxHearts = player:GetMaxHearts()
    if player:GetHearts() ~= maxHearts then return end

    local count = player:GetCollectibleNum(GENESIS_ITEMS.PASSIVE.VIRTUE)
    if count > 0 then
		local statmulti = maxHearts / 6 * count
		if cacheFlag == CacheFlag.CACHE_DAMAGE then
			player.Damage = player.Damage + statmulti * 1.5
		end
		if cacheFlag == CacheFlag.CACHE_FIREDELAY then
			player.MaxFireDelay = math.max(player.MaxFireDelay - statmulti * 3, 2)
		end
		if cacheFlag == CacheFlag.CACHE_SPEED then
			player.MoveSpeed = player.MoveSpeed + statmulti / 4
		end
		if cacheFlag == CacheFlag.CACHE_LUCK then
			player.Luck = player.Luck + statmulti * 3
		end
		if cacheFlag == CacheFlag.CACHE_SHOTSPEED then
			player.ShotSpeed = player.ShotSpeed + statmulti / 2
		end
	end
end
virtue:AddCallback( ModCallbacks.MC_EVALUATE_CACHE, virtue.EvaluateCache)

--re-check red health if players health changes
function virtue:PickupCon(player)
    local data = player:GetData()
    if not player:HasCollectible(GENESIS_ITEMS.PASSIVE.VIRTUE) then
        data.virtue = nil
        return
    end

    if not data.virtue then
        data.virtue = { redhearts = player:GetHearts() }
    end

    if player:GetHearts() ~= virtue.redhearts then
		player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
		player:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
		player:AddCacheFlags(CacheFlag.CACHE_SPEED)
		player:AddCacheFlags(CacheFlag.CACHE_LUCK)
		player:AddCacheFlags(CacheFlag.CACHE_SHOTSPEED)
		player:EvaluateItems()
        data.virtue.redhearts = player:GetHearts()
	end
end
virtue:AddCallback(ModCallbacks.MC_POST_PLAYER_UPDATE, virtue.PickupCon);

-- Inferno Soul

local Inferno = RegisterMod( "Inferno Soul" , 1 );

Inferno.INFERNAL_FAMILIAR = Isaac.GetEntityVariantByName("Inferno Soul")
Inferno.orbitalAmount = 1
Inferno.orbitals = {}
Inferno.runonce = 0

function Inferno:update()
	local player = Isaac.GetPlayer(0)
	local ents = Isaac.GetRoomEntities()

    if Inferno.runonce ~= 1 then
        for i=1,#ents do
            if ents[i].Type == EntityType.ENTITY_FAMILIAR and ents[i].Variant == Inferno.INFERNAL_FAMILIAR then
            ents[i]:Remove()
            Inferno.runonce = 1
            end
        end
    end

    if player:HasCollectible(GENESIS_ITEMS.FAMILIAR.INFERNO) then
        local orbitalAmount = 1

        for i=1,orbitalAmount,1 do
            Inferno.orbitalEntity = Inferno.orbitals[i]

            if Inferno.orbitalEntity == nil then
                Inferno.orbitalEntity = Isaac.Spawn(EntityType.ENTITY_FAMILIAR,Inferno.INFERNAL_FAMILIAR,0,player.Position,Vector(0,0),player):ToFamiliar()
                Inferno.orbitalEntity.OrbitLayer = 4
                Inferno.orbitalEntity:RecalculateOrbitOffset(Inferno.orbitalEntity.OrbitLayer, true)
                Inferno.orbitals[i] = Inferno.orbitalEntity
            end

            if Inferno.orbitalEntity ~= nil then
                if not Inferno.orbitalEntity:Exists() or Inferno.orbitalEntity:IsDead() then
                    Inferno.orbitalEntity:Remove()
                    Inferno.orbitals[i] = nil
                else
                    local targetLocation = Inferno.orbitalEntity:GetOrbitPosition(player.Position)
                    Inferno.orbitalEntity.OrbitDistance = Vector(55,55)
                    Inferno.orbitalEntity.Velocity = targetLocation - Inferno.orbitalEntity.Position
                end
            end
        end

    elseif player:HasCollectible(GENESIS_ITEMS.FAMILIAR.INFERNO) == false then
            for i=1,#ents do
                if ents[i].Type == EntityType.ENTITY_FAMILIAR and ents[i].Variant == Inferno.INFERNAL_FAMILIAR then
                ents[i]:Remove()
                end
            end
        Inferno.orbitalAmount = 0
    end
end

function Inferno:famupdate(fam)

    local closestEnemyPosition = 999999
    local closestEnemy = nil
    local entities = Isaac.GetRoomEntities()
    for i=1, #entities do
        local e = entities[i]

        local distance = (e.Position - fam.Position):Length()

        if e:IsActiveEnemy() and e:IsVulnerableEnemy() then
            if closestEnemyPosition > distance then
                closestEnemyPosition = distance
                closestEnemy = e
            end
        end

        if distance < 25 then
                if e:IsActiveEnemy() and e:IsVulnerableEnemy() then
                    e:AddBurn(EntityRef(player), 50, 0.1)
                end
        end
    end

local player = Isaac.GetPlayer(0)
    for i, entity in pairs(Isaac.GetRoomEntities()) do
        if entity.Type == EntityType.ENTITY_TEAR then
            local Tear = entity:ToTear()
                if (Tear.Parent.Type ~= EntityType.ENTITY_FAMILIAR) and (Tear.TearFlags ~= Tear.TearFlags | 1<<55) then
                    if (Tear.Position:Distance(fam.Position,Tear.Position) < 25) and (Tear:GetData()["Fired"] ~= 1) then

                    Infernal_Tear = player:FireTear(fam.Position, Tear.Velocity, true, false, false):ToTear()

                    Infernal_Tear.Velocity = Infernal_Tear.Velocity:Rotated(0)
                    Infernal_Tear.TearFlags = 1<<22
                    Infernal_Tear.CollisionDamage = Tear.CollisionDamage
                    Infernal_Tear.SpriteScale = Tear.SpriteScale
                    Infernal_Tear:GetData()["Fired"] = 1
                    Infernal_Tear:ChangeVariant(TearVariant.FIRE_MIND)
                    Tear:Remove()
                    end
                end
        end

    end
end
Inferno:AddCallback(ModCallbacks.MC_POST_UPDATE, Inferno.update, EntityType.ENTITY_PLAYER);
Inferno:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, Inferno.famupdate, Inferno.INFERNAL_FAMILIAR);

-- Eternal Resurrection

local resurrection = RegisterMod("Eternal Resurrection", 1 ); -- register mod
player = Isaac.GetPlayer(0); -- get player entity
resurrection.game = Game()
resurrection.xlives = 0
resurrection.debugText = "0"
function resurrection:PickupCon()
    player = Isaac.GetPlayer(0); -- get player entity
    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.ETERNAL) then
		local level = resurrection.game:GetLevel();
		if(player:GetEternalHearts() > 0) then --If player picks up eternal heart
			Isaac.DebugString("Pickup Eternal Heart")
            player:AddEternalHearts(-1);
            resurrection.xlives = resurrection.xlives + 1
            Isaac.DebugString("xtra lives =")
            Isaac.DebugString(resurrection.xlives / 2)
			if resurrection.xlives % 2 == 0 then
				Isaac.ExecuteCommand("g c11")
			end
        end
		--[[ commented out because its kinda dodgy and doesn't show life count
		if player:IsDead() and player:GetExtraLives() == 0 then
			if resurrection.xlives >= 2 then
				local level = resurrection.game:GetLevel();
				player:Revive();
				level:ChangeRoom(level:GetPreviousRoomIndex());
				player:AddMaxHearts(-player:GetMaxHearts());
				player:AddEternalHearts(2)
				resurrection.xlives = resurrection.xlives -2
				Isaac.DebugString("xtra lives =")
				Isaac.DebugString(resurrection.xlives / 2)
			end
		end
		]]--
    end
end
resurrection:AddCallback(ModCallbacks.MC_POST_UPDATE, resurrection.PickupCon); -- callback, call about every frame

--Even More Options
-- IDEA FROM PENTAHYBRID
local Options = RegisterMod("Options", 1 );
Options.NotSpawned = true
function Options:onSpawnPickup(pickup)
	if Genesis.moddata.EvenMoreItemSpawned then --if it exists and is not set to false
		Options.NotSpawned = false
	end
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.EVENMOREOPTIONS) and Options.NotSpawned then
		if pickup.Variant == 100 and pickup.FrameCount > 1 and pickup.FrameCount < 10 and pickup:IsShopItem() == false and pickup.Price >= 0 then
			Options.NotSpawned = false
			Genesis.moddata.EvenMoreItemSpawned = true
			local spawnpos = Game():GetRoom():FindFreePickupSpawnPosition(pickup.Position, 1, true)
			Isaac.Spawn(5,100,0,spawnpos,Vector(0,0),nil)
		end
	end
end
Options:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, Options.onSpawnPickup);

function Options:newfloor()
	Options.NotSpawned = true
	Genesis.moddata.EvenMoreItemSpawned = false
end
Options:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, Options.newfloor)

-- Lil Gabriel
local lilgabriel = RegisterMod("Lil Gabriel",1)
lilgabriel.entityvariant = Isaac.GetEntityVariantByName("Lil' Gabriel")
lilgabriel.spawn = false
local function RealignFamiliars()
  local Caboose = nil
  for _, entity in pairs(Isaac.GetRoomEntities()) do
    if entity.Type == EntityType.ENTITY_FAMILIAR
    and entity.Child == nil then
      if Caboose == nil then
        Caboose = entity
      else
        if Caboose.FrameCount < entity.FrameCount then
          Caboose.Parent = entity
          entity.Child = Caboose
        else
          Caboose.Child = entity
          entity.Parent = Caboose
        end
       end
      end
     end
    end

function lilgabriel:spawnUriel()
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.FAMILIAR.LIL_GABRIEL) then
  if lilgabriel.spawn==false then
  Isaac.ExecuteCommand("spawn 3.478")
  lilgabriel.spawn = true
end
end
end
lilgabriel:AddCallback( ModCallbacks.MC_EVALUATE_CACHE, lilgabriel.spawnUriel)
function lilgabriel:onUpdateUriel()
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.FAMILIAR.LIL_GABRIEL) then
   RealignFamiliars()
end
end
lilgabriel:AddCallback( ModCallbacks.MC_POST_UPDATE, lilgabriel.onUpdateUriel)

function lilgabriel:onFamiliarInit3(familiar3)
	local sprite = familiar3:GetSprite();
	sprite:Play("IdleDown", true);
end

function lilgabriel:onFamiliarUpdate3(familiar3)
	local sprite = familiar3:GetSprite();
  local player = Isaac.GetPlayer(0)
  local FireDir = player:GetFireDirection()
	local HeadDir = player:GetHeadDirection()
  local A = Input.IsButtonPressed(Keyboard.KEY_A,0)
  local D = Input.IsButtonPressed(Keyboard.KEY_D,0)
  local W = Input.IsButtonPressed(Keyboard.KEY_W,0)
  local S = Input.IsButtonPressed(Keyboard.KEY_S,0)
  local LEFT = Input.IsButtonPressed(Keyboard.KEY_LEFT,0)
  local RIGHT = Input.IsButtonPressed(Keyboard.KEY_RIGHT,0)
  local UP = Input.IsButtonPressed(Keyboard.KEY_UP,0)
  local DOWN = Input.IsButtonPressed(Keyboard.KEY_DOWN,0)
  local DOWN2 = Input.IsButtonTriggered(Keyboard.KEY_DOWN,0)
  local laserfire=false
  familiar3:FollowParent()
  RealignFamiliars()
  if FireDir == Direction.NO_DIRECTION then
    sprite:Play("IdleDown", false)
    end
  if player.FrameCount % 20 == 0 then
if LEFT then
  sprite:Play("FloatChargeSide",false)
  sprite.FlipX = true
  if sprite:IsFinished("FloatChargeSide") then
   sprite:Play("FloatShootSide",false)
   sprite.FlipX = true
  local homing2 = EntityLaser.ShootAngle(5,familiar3.Position,-180,30,Vector(-20,-24),familiar3)
  homing2.DepthOffset = 99
  homing2.SpriteScale = Vector(0.4,1)
  homing2:ToLaser()
  homing2.TearFlags = player.TearFlags
  end
else if RIGHT then
  sprite:Play("FloatChargeSide",false)
  sprite.FlipX = false
  if sprite:IsFinished("FloatChargeSide") then
   sprite:Play("FloatShootSide",false)
   sprite.FlipX = false
  local homing2 = EntityLaser.ShootAngle(5,familiar3.Position,0,30,Vector(20,-24),familiar3)
  homing2.DepthOffset = 99
  homing2.SpriteScale = Vector(0.4,1)
  homing2:ToLaser()
  homing2.TearFlags = player.TearFlags
  end
else if UP then
  sprite:Play("FloatChargeUp",false)
  if sprite:IsFinished("FloatChargeUp") then
   sprite:Play("FloatShootUp",false)
  local homing2 = EntityLaser.ShootAngle(5,familiar3.Position,-90,30,Vector(0,-24),familiar3)
  homing2.DepthOffset = -1
  homing2.SpriteScale = Vector(0.4,1)
  homing2:ToLaser()
  homing2.TearFlags = player.TearFlags
  end
else if DOWN then
  sprite:Play("FloatChargeDown",false)
  if sprite:IsFinished("FloatChargeDown") then
   sprite:Play("FloatShootDown",false)
  local homing2 = EntityLaser.ShootAngle(5,familiar3.Position,90,30,Vector(0,-13),familiar3)
  homing2.DepthOffset = 99
  homing2.SpriteScale = Vector(0.4,1)
  homing2:ToLaser()
  homing2.TearFlags = player.TearFlags
  laserfire=false
end
end
end
end
end
end
end
lilgabriel:AddCallback( ModCallbacks.MC_FAMILIAR_INIT, lilgabriel.onFamiliarInit3, lilgabriel.entityvariant)
lilgabriel:AddCallback( ModCallbacks.MC_FAMILIAR_UPDATE, lilgabriel.onFamiliarUpdate3, lilgabriel.entityvariant)

-- MAGIC DECK & POKER DECK --

local MagicDeck = RegisterMod("Magic Deck",1)

function MagicDeck:UseDeck()
  player = Isaac.GetPlayer(0)
  MagicDeck.rng = RNG()
  if player:HasCollectible(GENESIS_ITEMS.ACTIVE.MAGICDECK) then
    Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_TAROTCARD,math.random(52,54),player.Position,Vector(0,0),nil)
    return true
  end
end
MagicDeck:AddCallback(ModCallbacks.MC_USE_ITEM,MagicDeck.UseDeck,GENESIS_ITEMS.ACTIVE.MAGICDECK)

function MagicDeck:UsePokerDeck()
  player = Isaac.GetPlayer(0)
  MagicDeck.rng = RNG()
  if player:HasCollectible(GENESIS_ITEMS.ACTIVE.POKERDECK) then
    Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_TAROTCARD,math.random(23,31),player.Position,Vector(0,0),nil)
    return true
  end
end
MagicDeck:AddCallback(ModCallbacks.MC_USE_ITEM,MagicDeck.UsePokerDeck,GENESIS_ITEMS.ACTIVE.POKERDECK)

-- QUAKE SHOT --

local quakeshot = RegisterMod("Quake Shot", 1);

local quakeshotVars = {
	damageup = 3,
	rangeup = 4,
	fireratedown = 35,
	quakePos = {},
	quakeSpeed = {},
	quakeOrigin = {},
	quakeSpacing = 3,
	charged = false,
	fireDirection = -1,
	currentRoomIndex = 0,
	chargeicon = Sprite(),
	markedtarget = nil,
	ludotear = nil,
	knives = {},
	oldknifepos = {},
	lilithfam = {}
}

function quakeshot:cacheUpdate(player, cacheFlag)
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.QUAKESHOT) then
		if cacheFlag == CacheFlag.CACHE_DAMAGE then
			player.Damage = player.Damage + quakeshotVars.damageup
		end
		if cacheFlag == CacheFlag.CACHE_RANGE then
			player.TearHeight = player.TearHeight - quakeshotVars.rangeup
		end
		if cacheFlag == CacheFlag.CACHE_FIREDELAY then
			player.MaxFireDelay = player.MaxFireDelay + quakeshotVars.fireratedown
		end
	end
	lastdamage = player.Damage;
end

function quakeshot:tick()
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.QUAKESHOT) then
		local currentLevel = Game():GetLevel()
		local currentRoom = currentLevel:GetCurrentRoom()
		--fetus type weapons
		if player:HasWeaponType(WeaponType.WEAPON_BOMBS) or player:HasWeaponType(WeaponType.WEAPON_ROCKETS) then
			local entities = Isaac.GetRoomEntities()
			for ent = 1, #entities do
				local entity = entities[ent]
				if entity.Type == 4 then
					if entity:ToBomb().IsFetus and entity:IsDead() then
						--seems the fetus bombs take a few frames being dead
						if Isaac:GetFrameCount() % 10 == 0 then
							quakeshot:firering(entity.Position)
						end
					end
				end
				if entity.Type == 1000 and entity.Variant == 31 then
					if entity:IsDead() then
						quakeshot:firering(entity.Position)
					end
				end
			end
		--ludovico
		elseif player:HasWeaponType(WeaponType.WEAPON_LUDOVICO_TECHNIQUE) then
			if currentLevel:GetCurrentRoomIndex() ~= quakeshotVars.currentRoomIndex then
				local entities = Isaac.GetRoomEntities()
				for ent = 1, #entities do
					local entity = entities[ent]
					if entity.Type == 2 then
						quakeshotVars.ludotear = entity
						quakeshotVars.ludotear.Visible = false
						break
					end
				end
			end
			if Isaac:GetFrameCount() % 10 == 0 then
				Isaac.Spawn(1000, 62, 0, quakeshotVars.ludotear.Position, Vector(0,0), player)
				quakeshot:breakanddamage(quakeshotVars.ludotear.Position)
			end
		--knife
		elseif player:HasWeaponType(WeaponType.WEAPON_KNIFE) then
			if player.MaxFireDelay > 15 then
				player.MaxFireDelay = 15
			end
			if currentLevel:GetCurrentRoomIndex() ~= quakeshotVars.currentRoomIndex then
				quakeshotVars.knives = {}
				quakeshotVars.oldknifepos = {}
				local entities = Isaac.GetRoomEntities()
				local knifecount = 1
				for ent = 1, #entities do
					local entity = entities[ent]
					if entity.Type == 8 then
						quakeshotVars.knives[knifecount] = entity
						knifecount = knifecount + 1
					end
				end
			end
			if player:GetFireDirection() > -1 then
				for i = 1, #quakeshotVars.knives do
					quakeshotVars.oldknifepos[i] = Vector(-99,-99)
				end
			else
				local distance = 30
				for i = 1, #quakeshotVars.knives do
					local knifepos = quakeshotVars.knives[i].Position
					local knifeparent = quakeshotVars.knives[i].Parent
					if knifepos.X < knifeparent.Position.X - distance or knifepos.X > knifeparent.Position.X + distance or knifepos.Y < knifeparent.Position.Y - distance or knifepos.Y > knifeparent.Position.Y + distance then
						if knifepos.X < quakeshotVars.oldknifepos[i].X - distance or knifepos.X > quakeshotVars.oldknifepos[i].X + distance or knifepos.Y < quakeshotVars.oldknifepos[i].Y - distance or knifepos.Y > quakeshotVars.oldknifepos[i].Y + distance then
							if currentRoom:IsPositionInRoom(knifepos, 0) then
								Isaac.Spawn(1000, 62, 0, knifepos, Vector(0,0), player)
								quakeshot:breakanddamage(knifepos)
								quakeshotVars.oldknifepos[i] = knifepos
							end
						end
					end
				end
			end
		--tech X
		elseif player:HasWeaponType(WeaponType.WEAPON_TECH_X) then
			if Isaac:GetFrameCount() % quakeshotVars.quakeSpacing == 0 then
				local entities = Isaac.GetRoomEntities()
				for ent = 1, #entities do
					local entity = entities[ent]
					if entity.Type == 7 and entity.SubType == 2 then
						Isaac.Spawn(1000, 62, 0, entity.Position, Vector(0,0), player)
						quakeshot:breakanddamage(entity.Position)
					end
				end
			end
		--monstro's lung
		elseif player:HasWeaponType(WeaponType.WEAPON_MONSTROS_LUNGS) then
			local entities = Isaac.GetRoomEntities()
			for ent = 1, #entities do
				local entity = entities[ent]
				--monstro's lung + dr.fetus
				if player:HasCollectible(52) then
					if entity.Type == 4 then
						if entity:ToBomb().IsFetus and entity:IsDead() then
							--seems the fetus bombs take a few frames being dead
							if Isaac:GetFrameCount() % 10 == 0 then
								quakeshot:firering(entity.Position)
							end
						end
					end
				else
					if entity.Type == 2 and entity:IsDead() and entity.Parent.Index == player.Index then
						Isaac.Spawn(1000, 62, 0, entity.Position, Vector(0,0), player)
						quakeshot:breakanddamage(entity.Position)
					end
				end
			end
		--brimstone
		elseif player:HasWeaponType(WeaponType.WEAPON_BRIMSTONE) then
			--brimstone + ludovico
			if player:HasCollectible(329) then
				if currentLevel:GetCurrentRoomIndex() ~= quakeshotVars.currentRoomIndex then
					local entities = Isaac.GetRoomEntities()
					for ent = 1, #entities do
						local entity = entities[ent]
						if entity.Type == 7 then
							quakeshotVars.ludotear = entity
							break
						end
					end
				end
				if Isaac:GetFrameCount() % 40 == 0 then
					quakeshot:firering(quakeshotVars.ludotear.Position)
				end
			else
				player.LaserColor = Color(1.0, 1.0, 1.0, 0.0, 1, 1, 1)
				if player:GetFireDirection() > -1 then
					quakeshotVars.fireDirection = player:GetFireDirection()
				else
					if quakeshotVars.fireDirection > -1 then
						local shotbrim = false
						local entities = Isaac.GetRoomEntities()
						for ent = 1, #entities do
							local entity = entities[ent]
							if entity.Type == 7 and entity.Variant == 1 then
								if entity.Parent.Index == player.Index and player:CanShoot() then
									entity:Remove()
									quakeshot:fireshots("lines", player.Position)
									shotbrim = true
								--check for incubus shots
								elseif entity.Parent.Type == 3 and entity.Parent.Variant == 80 then
									entity:Remove()
									quakeshot:fireshots("lines", entity.Parent.Position)
									shotbrim = true
								end
							end
						end
						--brimstone + monstro's lung
						if shotbrim and player:HasCollectible(229) then
							quakeshot:randomlines(player.Position)
						end
						quakeshotVars.fireDirection = -1
					end
				end
			end
		--lasers and tears
		else
			if player.FireDelay < 3 then
				player.FireDelay = 3
			end
			if currentLevel:GetCurrentRoomIndex() ~= quakeshotVars.currentRoomIndex then
				quakeshotVars.quakePos = {}
				quakeshotVars.quakeSpeed = {}
				--check for marked
				if player:HasCollectible(394) and player:HasWeaponType(WeaponType.WEAPON_LASER) == false then
					local entities = Isaac.GetRoomEntities()
					for ent = 1, #entities do
						local entity = entities[ent]
						if entity.Type == 1000 and entity.Variant == 30 then
							quakeshotVars.markedtarget = entity
							break
						end
					end
				end
				--get incubi
				quakeshotVars.lilithfam = {}
				local entities = Isaac.GetRoomEntities()
				for ent = 1, #entities do
					local entity = entities[ent]
					if entity.Type == 3 and entity.Variant == 80 then
						table.insert(quakeshotVars.lilithfam, entity)
					end
				end
			end
			--check for marked
			if player:HasCollectible(394) and player:HasWeaponType(WeaponType.WEAPON_LASER) == false then
				if player.FireDelay <= 3 then
					quakeshot:fireshots("waves", player.Position)
					player.FireDelay = player.MaxFireDelay + 3
				end
			else
				if player:GetFireDirection() > -1 then
					quakeshotVars.charged = false
					if player.FireDelay <= 3 then
						quakeshotVars.charged = true
					end
					quakeshotVars.fireDirection = player:GetFireDirection()
					--stop incubi shooting lasers/tears
					for i = 1, #quakeshotVars.lilithfam do
						quakeshotVars.lilithfam[i]:ToFamiliar().FireCooldown = 99
					end
				else
					if quakeshotVars.charged then
						if player:HasWeaponType(WeaponType.WEAPON_LASER) then
							if player:CanShoot() then
								quakeshot:fireshots("lines", player.Position)
								--technology + monstro's lung
								if player:HasCollectible(229) then
									quakeshot:randomlines(player.Position)
								end
							end
							--make incubi shoot
							for i = 1, #quakeshotVars.lilithfam do
								quakeshot:fireshots("lines", quakeshotVars.lilithfam[i].Position)
								--technology + monstro's lung
								if player:HasCollectible(229) then
									quakeshot:randomlines(quakeshotVars.lilithfam[i].Position)
								end
							end
						else
							if player:CanShoot() then
								quakeshot:fireshots("waves", player.Position)
							end
							--make incubi shoot
							for i = 1, #quakeshotVars.lilithfam do
								quakeshot:fireshots("waves", quakeshotVars.lilithfam[i].Position)
							end
						end
					end
					quakeshotVars.charged = false
					player.FireDelay = player.MaxFireDelay + 3
				end
			end
		end
		--manage wave movements
		if Isaac:GetFrameCount() % quakeshotVars.quakeSpacing == 0 then
			for i = 1, #quakeshotVars.quakePos do
				if currentRoom:IsPositionInRoom(quakeshotVars.quakePos[i], 0) and quakeshot:isnotbeyondrange(i) then
					Isaac.Spawn(1000, 62, 0, quakeshotVars.quakePos[i], Vector(0,0), player)
					quakeshot:breakanddamage(quakeshotVars.quakePos[i])
					local tempX = quakeshotVars.quakePos[i].X + quakeshotVars.quakeSpeed[i].X
					local tempY = quakeshotVars.quakePos[i].Y + quakeshotVars.quakeSpeed[i].Y
					quakeshotVars.quakePos[i] = Vector(tempX, tempY)
				else
					table.remove(quakeshotVars.quakePos, i)
					table.remove(quakeshotVars.quakeOrigin, i)
					table.remove(quakeshotVars.quakeSpeed, i)
				end
			end
		end
		quakeshotVars.currentRoomIndex = currentLevel:GetCurrentRoomIndex()
	end
end

--used to show charge level
function quakeshot:display_UI()
	local player = Isaac.GetPlayer(0)
	local currentRoom = Game():GetLevel():GetCurrentRoom()
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.QUAKESHOT) then
		if player:HasWeaponType(WeaponType.WEAPON_TEARS) or player:HasWeaponType(WeaponType.WEAPON_LASER) then
			if player:HasCollectible(394) == false then
				quakeshotVars.chargeicon:Load("gfx/quakechargebar.anm2", true);
				local player = Isaac.GetPlayer(0)
				if player:GetFireDirection() > -1 and player.FireDelay < player.MaxFireDelay then
					if player.FireDelay <= 3 then
						quakeshotVars.chargeicon:Play("Charged", true)
					else
						local frame = math.floor(((player.MaxFireDelay-(player.FireDelay-3))/player.MaxFireDelay)*10)
						quakeshotVars.chargeicon:Play(frame, true)
					end
					local temppos = currentRoom:WorldToScreenPosition(player.Position)
					temppos = Vector(temppos.X,temppos.Y+10)
					quakeshotVars.chargeicon:Render(temppos, Vector(0,0), Vector(0,0))
				end
			end
		end
	end
end

--used to determine how many shots and at what angles
function quakeshot:fireshots(shottype, startpos)
	local player = Isaac.GetPlayer(0)
	local newspeed
	if quakeshotVars.fireDirection == 0 then
		newspeed = Vector(-10*player.ShotSpeed*quakeshotVars.quakeSpacing,0);
	end
	if quakeshotVars.fireDirection == 1 then
		newspeed = Vector(0,-10*player.ShotSpeed*quakeshotVars.quakeSpacing);
	end
	if quakeshotVars.fireDirection == 2 then
		newspeed = Vector(10*player.ShotSpeed*quakeshotVars.quakeSpacing,0);
	end
	if quakeshotVars.fireDirection == 3 then
		newspeed = Vector(0,10*player.ShotSpeed*quakeshotVars.quakeSpacing);
	end
	--check for marked
	if player:HasCollectible(394) and player:HasWeaponType(WeaponType.WEAPON_BRIMSTONE) == false and player:HasWeaponType(WeaponType.WEAPON_LASER) == false and player:HasWeaponType(WeaponType.WEAPON_LUDOVICO_TECHNIQUE) == false then
		newspeed = quakeshot:getdirectionvel(player.Position, quakeshotVars.markedtarget.Position)
	end
	local tearspread = 1
	if player:HasCollectible(153) then
		tearspread = 4
		if player:HasCollectible(2) then
			tearspread = 7
			if player:HasCollectible(245) then
				tearspread = 9
			end
		else
			if player:HasCollectible(245) then
				tearspread = 6
			end
		end
	elseif player:HasCollectible(2) then
		tearspread = 3
		if player:HasCollectible(245) then
			tearspread = 5
		end
	elseif player:HasCollectible(245) then
		tearspread = 2
	end
	local tempvelocity = {}
	if tearspread == 9 then
		tempvelocity[5] = newspeed
		if quakeshotVars.fireDirection == 0 or quakeshotVars.fireDirection == 2 then
			tempvelocity[1] = Vector(newspeed.X,newspeed.Y-12)
			tempvelocity[2] = Vector(newspeed.X,newspeed.Y-9)
			tempvelocity[3] = Vector(newspeed.X,newspeed.Y-6)
			tempvelocity[4] = Vector(newspeed.X,newspeed.Y-3)
			tempvelocity[6] = Vector(newspeed.X,newspeed.Y+3)
			tempvelocity[7] = Vector(newspeed.X,newspeed.Y+6)
			tempvelocity[8] = Vector(newspeed.X,newspeed.Y+9)
			tempvelocity[9] = Vector(newspeed.X,newspeed.Y+12)
		else
			tempvelocity[1] = Vector(newspeed.X-12,newspeed.Y)
			tempvelocity[2] = Vector(newspeed.X-9,newspeed.Y)
			tempvelocity[3] = Vector(newspeed.X-6,newspeed.Y)
			tempvelocity[4] = Vector(newspeed.X-3,newspeed.Y)
			tempvelocity[6] = Vector(newspeed.X+3,newspeed.Y)
			tempvelocity[7] = Vector(newspeed.X+6,newspeed.Y)
			tempvelocity[8] = Vector(newspeed.X+9,newspeed.Y)
			tempvelocity[9] = Vector(newspeed.X+12,newspeed.Y)
		end
	elseif tearspread == 7 then
		tempvelocity[4] = newspeed
		if quakeshotVars.fireDirection == 0 or quakeshotVars.fireDirection == 2 then
			tempvelocity[1] = Vector(newspeed.X,newspeed.Y-24)
			tempvelocity[2] = Vector(newspeed.X,newspeed.Y-16)
			tempvelocity[3] = Vector(newspeed.X,newspeed.Y-8)
			tempvelocity[5] = Vector(newspeed.X,newspeed.Y+8)
			tempvelocity[6] = Vector(newspeed.X,newspeed.Y+16)
			tempvelocity[7] = Vector(newspeed.X,newspeed.Y+24)
		else
			tempvelocity[1] = Vector(newspeed.X-24,newspeed.Y)
			tempvelocity[2] = Vector(newspeed.X-16,newspeed.Y)
			tempvelocity[3] = Vector(newspeed.X-8,newspeed.Y)
			tempvelocity[5] = Vector(newspeed.X+8,newspeed.Y)
			tempvelocity[6] = Vector(newspeed.X+16,newspeed.Y)
			tempvelocity[7] = Vector(newspeed.X+24,newspeed.Y)
		end
	elseif tearspread == 6 then
		if quakeshotVars.fireDirection == 0 or quakeshotVars.fireDirection == 2 then
			tempvelocity[1] = Vector(newspeed.X,newspeed.Y-7)
			tempvelocity[2] = Vector(newspeed.X,newspeed.Y-4)
			tempvelocity[3] = Vector(newspeed.X,newspeed.Y-1)
			tempvelocity[4] = Vector(newspeed.X,newspeed.Y+1)
			tempvelocity[5] = Vector(newspeed.X,newspeed.Y+4)
			tempvelocity[6] = Vector(newspeed.X,newspeed.Y+7)
		else
			tempvelocity[1] = Vector(newspeed.X-7,newspeed.Y)
			tempvelocity[2] = Vector(newspeed.X-4,newspeed.Y)
			tempvelocity[3] = Vector(newspeed.X-1,newspeed.Y)
			tempvelocity[4] = Vector(newspeed.X+1,newspeed.Y)
			tempvelocity[5] = Vector(newspeed.X+4,newspeed.Y)
			tempvelocity[6] = Vector(newspeed.X+7,newspeed.Y)
		end
	elseif tearspread == 5 then
		tempvelocity[3] = newspeed
		if quakeshotVars.fireDirection == 0 or quakeshotVars.fireDirection == 2 then
			tempvelocity[1] = Vector(newspeed.X,newspeed.Y-7)
			tempvelocity[2] = Vector(newspeed.X,newspeed.Y-4)
			tempvelocity[4] = Vector(newspeed.X,newspeed.Y+4)
			tempvelocity[5] = Vector(newspeed.X,newspeed.Y+7)
		else
			tempvelocity[1] = Vector(newspeed.X-7,newspeed.Y)
			tempvelocity[2] = Vector(newspeed.X-4,newspeed.Y)
			tempvelocity[4] = Vector(newspeed.X+4,newspeed.Y)
			tempvelocity[5] = Vector(newspeed.X+7,newspeed.Y)
		end
	elseif tearspread == 4 then
		if quakeshotVars.fireDirection == 0 or quakeshotVars.fireDirection == 2 then
			tempvelocity[1] = Vector(newspeed.X,newspeed.Y-12)
			tempvelocity[2] = Vector(newspeed.X,newspeed.Y-4)
			tempvelocity[3] = Vector(newspeed.X,newspeed.Y+4)
			tempvelocity[4] = Vector(newspeed.X,newspeed.Y+12)
		else
			tempvelocity[1] = Vector(newspeed.X-12,newspeed.Y)
			tempvelocity[2] = Vector(newspeed.X-4,newspeed.Y)
			tempvelocity[3] = Vector(newspeed.X+4,newspeed.Y)
			tempvelocity[4] = Vector(newspeed.X+12,newspeed.Y)
		end
	elseif tearspread == 3 then
		tempvelocity[2] = newspeed
		if quakeshotVars.fireDirection == 0 or quakeshotVars.fireDirection == 2 then
			tempvelocity[1] = Vector(newspeed.X,newspeed.Y-12)
			tempvelocity[3] = Vector(newspeed.X,newspeed.Y+12)
		else
			tempvelocity[1] = Vector(newspeed.X-12,newspeed.Y)
			tempvelocity[3] = Vector(newspeed.X+12,newspeed.Y)
		end
	elseif tearspread == 2 then
		if quakeshotVars.fireDirection == 0 or quakeshotVars.fireDirection == 2 then
			tempvelocity[1] = Vector(newspeed.X,newspeed.Y-4)
			tempvelocity[2] = Vector(newspeed.X,newspeed.Y+4)
		else
			tempvelocity[1] = Vector(newspeed.X-4,newspeed.Y)
			tempvelocity[2] = Vector(newspeed.X+4,newspeed.Y)
		end
	else
		tempvelocity[1] = newspeed
	end
	for i = 1, #tempvelocity do
		if shottype == "waves" then
			quakeshot:fireshockwave(startpos, tempvelocity[i])
		end
		if shottype == "lines" then
			quakeshot:fireline(startpos, tempvelocity[i])
		end
	end
end

--used for fetus shots
function quakeshot:firering(pos)
	local currentRoom = Game():GetLevel():GetCurrentRoom()
	local spawnpos = {}
	spawnpos[1] = Vector(pos.X-40,pos.Y)
	spawnpos[2] = Vector(pos.X,pos.Y-40)
	spawnpos[3] = Vector(pos.X+40,pos.Y)
	spawnpos[4] = Vector(pos.X,pos.Y+40)
	spawnpos[5] = Vector(pos.X-30,pos.Y-30)
	spawnpos[6] = Vector(pos.X+30,pos.Y-30)
	spawnpos[7] = Vector(pos.X+30,pos.Y+30)
	spawnpos[8] = Vector(pos.X-30,pos.Y+30)
	for i = 1, #spawnpos do
		if currentRoom:IsPositionInRoom(spawnpos[i], 0) then
			Isaac.Spawn(1000, 62, 0, spawnpos[i], Vector(0,0), player)
			quakeshot:breakanddamage(spawnpos[i])
		end
	end
end

--used for normal tear shots
function quakeshot:fireshockwave(startpos, speed)
	local player = Isaac.GetPlayer(0)
	table.insert(quakeshotVars.quakePos, startpos)
	table.insert(quakeshotVars.quakeOrigin, startpos)
	local moveadjust = player:GetTearMovementInheritance(speed)
	local tempX = speed.X + moveadjust.X
	local tempY = speed.Y + moveadjust.Y
	speed = Vector(tempX,tempY)
	table.insert(quakeshotVars.quakeSpeed, speed)
end

--used for lasers/brimstone
function quakeshot:fireline(startpos, linedirection)
	local player = Isaac.GetPlayer(0)
	local currentRoom = Game():GetLevel():GetCurrentRoom()
	while currentRoom:IsPositionInRoom(startpos, 0) do
		Isaac.Spawn(1000, 62, 0, startpos, Vector(0,0), player)
		quakeshot:breakanddamage(startpos)
		startpos = Vector(startpos.X + linedirection.X,startpos.Y + linedirection.Y)
	end
end

--used for laser/brimstone + monstro's lung
function quakeshot:randomlines(startpos)
	local roomcenter = Game():GetLevel():GetCurrentRoom():GetCenterPos()
	for i = 1, 4 do
		local pos = Vector(math.random(roomcenter.X * 2),math.random(roomcenter.Y * 2))
		local direction = quakeshot:getdirectionvel(startpos, pos)
		quakeshot:fireline(startpos, direction)
	end
end

--damage enemies and break blocks
function quakeshot:breakanddamage(quakepos)
	SFXManager():Play(137, 0.4, 0, false, 1.0)
	local player = Isaac.GetPlayer(0)
	local distance = 45
	local entities = Isaac.GetRoomEntities()
	for ent = 1, #entities do
		local entity = entities[ent]
		if (quakepos.X - entity.Position.X)^2 + (quakepos.Y - entity.Position.Y)^2 < distance^2 then
			if entity:IsVulnerableEnemy() or entity.Type == 33 then
				entity:TakeDamage(player.Damage+5, DamageFlag.DAMAGE_EXPLOSION, EntityRef(player), 1)
			end
			if entity.Type == 5 and entity.Variant == 51 then
				entity:ToPickup():TryOpenChest()
			end
		end
	end
	local currentRoom = Game():GetLevel():GetCurrentRoom()
	local gridindex = currentRoom:GetGridIndex(quakepos)
	currentRoom:DestroyGrid(gridindex, true)
end

--check if wave is going beyond players range
function quakeshot:isnotbeyondrange(i)
	local player = Isaac.GetPlayer(0)
	local distance = -player.TearHeight*10
	if (quakeshotVars.quakePos[i].X - quakeshotVars.quakeOrigin[i].X)^2 + (quakeshotVars.quakePos[i].Y - quakeshotVars.quakeOrigin[i].Y)^2 > distance^2 then
		return false;
	end
	return true;
end

--get wave/line direction for marked
function quakeshot:getdirectionvel(begin, ending)
	local speed = Vector(0,0)
	local distX = ending.X - begin.X
	local distY = ending.Y - begin.Y
	local hypot = (distY*distY)+(distX*distX)
	local dist = math.sqrt(hypot/1600)
	speed = Vector(distX/dist,distY/dist)
	return speed
end

quakeshot:AddCallback(ModCallbacks.MC_EVALUATE_CACHE , quakeshot.cacheUpdate);
quakeshot:AddCallback(ModCallbacks.MC_POST_UPDATE, quakeshot.tick);
quakeshot:AddCallback(ModCallbacks.MC_POST_RENDER, quakeshot.display_UI);


local MicroMarioMods = RegisterMod("micromario", 1)
MicroMarioMods.charge2 = 0
MicroMarioMods.transformed = false
MicroMarioMods.stop = false
MicroMarioMods.stop1 = false
MicroMarioMods.HasCostume = false

MicroMarioMods.transformationItems = {68,95,152,244,267,395}

function checkForTransformation()
  local player = Isaac.GetPlayer(0)
	local c = 0
	for i=0, #MicroMarioMods.transformationItems do
    if MicroMarioMods.transformationItems[i] ~= nil then
      if player:HasCollectible(MicroMarioMods.transformationItems[i]) then
        c = c + 1
      end
    end
	end
	if c >= 3 then
  return true else return false end
end

function MicroMarioMods:Update()
	local shootnow = false
	local meme = false
	local meme3 = false
	local game = Game()
	local room = game:GetRoom()
	local level = game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local entities = Genesis.getRoomEntities()
	local direction = player:GetFireDirection()
	if player.FrameCount == 1 then
		MicroMarioMods.Charge = 0
		MicroMarioMods.stop = false
		MicroMarioMods.stop1 = false
		MicroMarioMods.transformed = false
	end

	if checkForTransformation() then
    MicroMarioMods.transformed = true
  end
	if MicroMarioMods.transformed == true then
		for i = 1, #entities do
			if entities[i]:IsVulnerableEnemy() and math.random(player.MaxFireDelay*3) == 1 then
				local dir = entities[i].Position:__sub(player.Position)
				player:FireTechLaser(player.Position, 2, dir, false, true)
			end
		end
	if MicroMarioMods.transformed == true and MicroMarioMods.stop1 == false then
		MicroMarioMods.stop1 = true
	end
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.TECHNOLOGY_3) then
		if MicroMarioMods.stop == false then
			player:AddNullCostume(GENESIS_COSTUMES.TECH_3)
			player:AddCollectible(115, 0, false)
			player:TryRemoveCollectibleCostume(115, false)
			MicroMarioMods.stop = true
		end
		player.FireDelay = 99
		if direction == Direction.UP then
			aimDirection = Vector(0,-1)
			MicroMarioMods.Charge = MicroMarioMods.Charge + 0.5
		elseif direction == Direction.LEFT then
			aimDirection = Vector(-1,0)
			MicroMarioMods.Charge = MicroMarioMods.Charge + 0.5
		elseif direction == Direction.DOWN then
			aimDirection = Vector(0,1)
			MicroMarioMods.Charge = MicroMarioMods.Charge + 0.5
		elseif direction == Direction.RIGHT then
			aimDirection = Vector(1,0)
			MicroMarioMods.Charge = MicroMarioMods.Charge + 0.5
		else
			shootnow = true
		end
    if player.MaxFireDelay < 3 then player.MaxFireDelay = 3 end
    local percentual = math.floor(player.MaxFireDelay / 3)
    Isaac.DebugString(tostring(percentual))
		if MicroMarioMods.Charge > player.MaxFireDelay then
			MicroMarioMods.Charge = player.MaxFireDelay
		end
   -- Isaac.DebugString(tostring(MicroMarioMods.Charge))
		if MicroMarioMods.Charge > 0 then
			--player:SetColor(Color(2, 2, 1, 1, MicroMarioMods.Charge * 155 / player.MaxFireDelay, MicroMarioMods.Charge * 155 / player.MaxFireDelay, MicroMarioMods.Charge * 155 / player.MaxFireDelay), 1, 1, false, false)
      --player:SetColor(Color(1,1,1,1,(MicroMarioMods.Charge * 155 / player.MaxFireDelay),(MicroMarioMods.Charge * 155 / player.MaxFireDelay,MicroMarioMods.Charge * 155 / player.MaxFireDelay),1,1,false,false)
      player.Color = Color(1+(MicroMarioMods.Charge / player.MaxFireDelay), 1+(MicroMarioMods.Charge / player.MaxFireDelay), 1+(MicroMarioMods.Charge / player.MaxFireDelay), 1, 0,0,0)
		end
		if MicroMarioMods.Charge > 0 and shootnow == true then
			MicroMarioMods.Charge = MicroMarioMods.Charge - 1
		end
    if MicroMarioMods.Charge <= 0 then
      MicroMarioMods.Charge = 0
      shootnow = false
    end
    if MicroMarioMods.Charge > player.MaxFireDelay then
      MicroMarioMods.Charge = player.MaxFireDelay
    end
		if MicroMarioMods.Charge % percentual == 0 and shootnow == true then
			for i=0, math.floor(MicroMarioMods.Charge / percentual) do

        player:FireTechLaser(player.Position, 2, aimDirection, false, true)

        if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
          player:FireTechLaser(player.Position, 2, aimDirection, false, true)
        end
      end
		end
	end
end
end

function MicroMarioMods:EvalCache(player,cacheFlag)
if MicroMarioMods.transformed == true then
    if MicroMarioMods.HasCostume ~= true then
    player:UsePill(Isaac.GetPillEffectByName("ROBOSACC"),0)
    player:AddNullCostume(GENESIS_COSTUMES.ROBOSACC)
    SFXManager():Play(SoundEffect.SOUND_1UP,1,0,false,1)
    MicroMarioMods.HasCostume = true
  end
end
end
MicroMarioMods:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, MicroMarioMods.EvalCache)
MicroMarioMods:AddCallback(ModCallbacks.MC_POST_UPDATE, MicroMarioMods.Update)

--[[local Enigma = RegisterMod("Enigma", 1)
Enigma.HasCostume = false

Enigma.transformationItems = {299,300,301,302,303,304,305,306,307,308,309}

function checkForEnigmaTransformation()
  local player = Isaac.GetPlayer(0)
	local c = 0
	for i=0, #Enigma.transformationItems do
    if Enigma.transformationItems[i] ~= nil then
      if player:HasCollectible(Enigma.transformationItems[i]) then
        c = c + 1
      end
    end
	end
	if c >= 3 then
  return true else return false end
end

function Enigma:Update()
	local shootnow = false
	local meme = false
	local meme3 = false
	local game = Game()
	local room = game:GetRoom()
	local level = game:GetLevel()
	local player = Isaac.GetPlayer(0)
	local entities = Genesis.getRoomEntities()
	local direction = player:GetFireDirection()
	if player.FrameCount == 1 then
		Enigma.Charge = 0
		Enigma.stop = false
		Enigma.stop1 = false
		Enigma.transformed = false
	end

	if checkForEnigmaTransformation() then
    Enigma.transformed = true
    player.TearFlags = 1 << 16
    if player.FrameCount % 20 == 0 then
    if direction == Direction.LEFT then
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
    elseif direction == Direction.RIGHT then
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      elseif direction == Direction.UP then
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      elseif direction == Direction.DOWN then
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      player:FireTear(player.Position,Vector(10,0),false,false,false,false)
      end
  end
end
end

function Enigma:EvalCache(player,cacheFlag)
if Enigma.transformed ~= false then
    if Enigma.HasCostume ~= true then
    player:AddCollectible(CollectibleType.COLLECTIBLE_ZODIAC,0,false)
    player:UsePill(Isaac.GetPillEffectByName("Enigma"),0)
    player:AddNullCostume(GENESIS_COSTUMES.ENIGMA)
    SFXManager():Play(SoundEffect.SOUND_1UP,1,0,false,1)
    Enigma.HasCostume = true
end
end
end
Enigma:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, Enigma.EvalCache)
Enigma:AddCallback(ModCallbacks.MC_POST_UPDATE, Enigma.Update)]]--

local   MyMod = RegisterMod( "Book of Randomness", 1 );
local   Bookofrandomness = GENESIS_ITEMS.ACTIVE.BOOK_OF_RANDOMNESS;
local function PlaySoundAtPos(sound, volume, pos)
  local soundDummy = Isaac.Spawn(EntityType.ENTITY_FLY, 0, 0, pos, Vector(0,0), Isaac.GetPlayer(0));
  local soundDummyNPC = soundDummy:ToNPC();
  soundDummyNPC:PlaySound(sound, volume, 0, false, 1.0);
  soundDummy:Remove();
end
function MyMod:use_BookofrandomnessEffect()
	local player = Isaac.GetPlayer(0);
	local bookchance = math.random(11);
	if bookchance == 1 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BOOK_OF_SHADOWS, true, false, false, false)
	elseif bookchance == 2 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_ANARCHIST_COOKBOOK, true, false, false, false)
	elseif bookchance == 3 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BOOK_OF_BELIAL, true, false, false, false)
	elseif bookchance == 4 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_TELEPATHY_BOOK, true, false, false, false)
	elseif bookchance == 5 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BOOK_REVELATIONS, true, false, false, false)
	elseif bookchance == 6 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_SATANIC_BIBLE, true, false, false, false)
	elseif bookchance == 7 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_HOW_TO_JUMP, true, false, false, false)
	elseif bookchance == 8 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_NECRONOMICON, true, false, false, false)
	elseif bookchance == 9 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BIBLE, true, false, false, false)
	elseif bookchance == 10 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_MONSTER_MANUAL, true, false, false, false)
	elseif bookchance == 11 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BOOK_OF_SECRETS, true, false, false, false)
	end
end
MyMod:AddCallback( ModCallbacks.MC_USE_ITEM, MyMod.use_BookofrandomnessEffect, GENESIS_ITEMS.ACTIVE.BOOK_OF_RANDOMNESS );

-- DAD'S MOUSTACHE --
local dadsmoustache = RegisterMod("Dad's Moustache", 1);
dadsmoustache.hasitem = false
function dadsmoustache:update()
    local player = Isaac.GetPlayer(0)
	local moustache = GENESIS_ITEMS.PASSIVE.MOUSTACHE;
	local firedir = player:GetFireDirection()
	if player:HasCollectible(moustache) then
		if dadsmoustache.hasitem == false then
			player:AddNullCostume(Isaac.GetCostumeIdByPath("gfx/characters/dadsmoustache.anm2"))
		end
		if player.FrameCount % 30 == 1 then
			if firedir == Direction.DOWN then
				player:FireTear(player.Position,Vector(0,-10),false,false,false)
				player:FireTear(player.Position,Vector(10,-10),false,false,false)
				player:FireTear(player.Position,Vector(-10,-10),false,false,false)
			elseif firedir == Direction.RIGHT then
				player:FireTear(player.Position,Vector(-10,0),false,false,false)
				player:FireTear(player.Position,Vector(-10,10),false,false,false)
				player:FireTear(player.Position,Vector(-10,-10),false,false,false)
			elseif firedir == Direction.UP then
				player:FireTear(player.Position,Vector(0,10),false,false,false)
				player:FireTear(player.Position,Vector(10,10),false,false,false)
				player:FireTear(player.Position,Vector(-10,10),false,false,false)
			elseif firedir == Direction.LEFT then
				player:FireTear(player.Position,Vector(10,0),false,false,false)
				player:FireTear(player.Position,Vector(10,10),false,false,false)
				player:FireTear(player.Position,Vector(10,-10),false,false,false)
			end
		end
		dadsmoustache.hasitem = true
	end
end
dadsmoustache:AddCallback( ModCallbacks.MC_POST_UPDATE, dadsmoustache.update, GENESIS_ITEMS.PASSIVE.MOUSTACHE );

-- PUKE --

local puke = RegisterMod("Puke",1)
puke.spawnedextra = false
function puke:update()
	local player = Isaac.GetPlayer(0)
	local pukeitem = GENESIS_ITEMS.PASSIVE.PUKE;
	local firedir = player:GetFireDirection()
	if player:HasCollectible(pukeitem) then
		local entities = Isaac.GetRoomEntities()
		for i=1, #entities do
			if entities[i].Type == EntityType.ENTITY_TEAR and entities[i].FrameCount < 2 then
				local tear = entities[i]:ToTear()
				tear:GetSprite():ReplaceSpritesheet(0,"gfx/puke_tears.png")
				tear:GetSprite():LoadGraphics()
			end
		end
		if player.FireDelay == math.floor(player.MaxFireDelay/2) then
			puke.spawnedextra = false
			local extrachance = math.random(1,6)
			if extrachance == 6 then
				player.FireDelay = -1
				puke.spawnedextra = true
			end
		end
		if puke.spawnedextra and player.FireDelay == player.MaxFireDelay then
			player.FireDelay = math.floor(player.MaxFireDelay/2) - 1
		end
	end
end
puke:AddCallback( ModCallbacks.MC_POST_UPDATE, puke.update, GENESIS_ITEMS.PASSIVE.PUKE );

-- Skippy Tears --

local skippy = RegisterMod("Skippy Tears",1.0);

local skippytears = GENESIS_ITEMS.PASSIVE.SKIPPYTEARS;
local cricket, reflection, parasite, fetus, efetus, knife, brim = false;
local tech = 0;

function playSound(sound, vol, pit)
  local player = Isaac.GetPlayer(0);
  local soundGuy = Isaac.Spawn(EntityType.ENTITY_FLY, 0, 0, player.Position, Vector(0,0), player):ToNPC();
  soundGuy:PlaySound(sound, vol, 0, false, pit);
  soundGuy:Remove();
end

function skippy.onPlayerInit(player)
  cricket, reflection, parasite, fetus, efetus, knife, brim = false;
  tech = 0;
end
skippy:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, skippy.onPlayerInit)

function skippy:LowerTears(player, cacheFlag)
  if player:HasCollectible(skippytears) then
    if cacheFlag == CacheFlag.CACHE_FIREDELAY then
      if brim then
        player.MaxFireDelay = player.MaxFireDelay + 20;
      elseif knife then
        player.MaxFireDelay = player.MaxFireDelay + 15;
      else
        player.MaxFireDelay = player.MaxFireDelay + 5;
      end
    elseif cacheFlag == CacheFlag.CACHE_DAMAGE then
      if knife then
        player.Damage = player.Damage * 2.25;
      end
    end
  end
end

function skippy:HydrophobicityEffect()
  local player = Isaac.GetPlayer(0);
  if player:HasCollectible(skippytears) then
    local didCacheUpdate = false
    if player:HasCollectible(CollectibleType.COLLECTIBLE_CRICKETS_BODY) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_CRICKETS_BODY)
      cricket = true;
      didCacheUpdate = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_PARASITE) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_PARASITE)
      parasite = true;
      didCacheUpdate = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_MY_REFLECTION) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_MY_REFLECTION)
      reflection = true;
      didCacheUpdate = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_DR_FETUS) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_DR_FETUS)
      fetus = true;
      didCacheUpdate = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_EPIC_FETUS) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_EPIC_FETUS)
      efetus = true;
      didCacheUpdate = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_TECH_X)
      tech = tech + 1;
      didCacheUpdate = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY)
      tech = tech + 1;
      didCacheUpdate = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2)
      tech = tech + 1;
      didCacheUpdate = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE)
      brim = true;
      didCacheUpdate = true
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_KNIFE) then
      player:RemoveCollectible(CollectibleType.COLLECTIBLE_MOMS_KNIFE)
      knife = true;
      didCacheUpdate = true
    end
    if didCacheUpdate then
        if knife then
            player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
        end
        player:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
        player:EvaluateItems()
    end

    if knife then
        player.TearHeight = -13;
        player.ShotSpeed = 2
    else
      player.TearHeight = -5;
      player.TearFallingAcceleration = 3;
      player.TearFallingSpeed = 18
    end
    local entities = Isaac.GetRoomEntities();
    for i = 1, #entities do
      if entities[i].Type == EntityType.ENTITY_TEAR and entities[i].FrameCount < 2 and entities[i]:GetData().bounce ~= true and entities[i]:GetData().bounce ~= false and (entities[i].SpawnerType == EntityType.ENTITY_PLAYER
      or entities[i].SpawnerVariant == FamiliarVariant.INCUBUS or entities[i].SpawnerType == EntityType.ENTITY_NULL) then
      entities[i]:GetSprite():ReplaceSpritesheet(0,"gfx/skippy_tears.png")
      entities[i]:GetSprite():LoadGraphics()
        if entities[i]:GetData().normalTear == false or entities[i]:GetData().normalTearRC == false then
        else
          entities[i]:GetData().bounce = true;
          entities[i]:GetData().bounces = 0;
          entities[i]:ToTear().Scale = entities[i]:ToTear().Scale * 1.5;
          entities[i].CollisionDamage = entities[i].CollisionDamage * 1.25;
          if fetus or efetus then
            entities[i]:GetSprite():Load("gfx/bomb_tear.anm2", true)
            entities[i]:GetSprite():Play("RotateSlow", true)
          end
          if reflection then
            entities[i].Velocity = entities[i].Velocity:__mul(1.5)
          end
          if knife then
            entities[i]:GetSprite():Load("gfx/knife_tear.anm2", true)
            entities[i]:GetSprite():Play("RotateSlow", true)
            entities[i]:GetData().bounces = 1;
            entities[i]:ToTear().TearFlags = entities[i]:ToTear().TearFlags | TearFlags.TEAR_PIERCING
            entities[i].CollisionDamage = entities[i].CollisionDamage * 2.5;
          end
        end
      end
      if entities[i].Type == EntityType.ENTITY_TEAR and entities[i]:IsDead() and entities[i]:GetData().bounce == true then
              entities[i]:GetSprite():ReplaceSpritesheet(0,"gfx/skippy_tears.png")
      entities[i]:GetSprite():LoadGraphics()
        if entities[i]:GetData().bounces == 0 or entities[i]:GetData().bounces == 2  or entities[i]:GetData().bounces == 4 then
          entities[i]:GetSprite():ReplaceSpritesheet(0,"gfx/skippy_tears.png")
      entities[i]:GetSprite():LoadGraphics()


          if parasite then
            local para1 = Isaac.Spawn(EntityType.ENTITY_TEAR, entities[i].Variant, 0, entities[i].Position, Vector(entities[i].Velocity.Y, entities[i].Velocity.X), player):ToTear();
            local para2 = Isaac.Spawn(EntityType.ENTITY_TEAR, entities[i].Variant, 0, entities[i].Position, Vector(entities[i].Velocity.Y, -entities[i].Velocity.X), player):ToTear();
            para1:GetData().bounce = false; para1.Scale = entities[i]:ToTear().Scale * 0.6; para1.Height = -5; para1.FallingSpeed = -15; para1.FallingAcceleration = 3;
            para2:GetData().bounce = false; para2.Scale = entities[i]:ToTear().Scale * 0.6; para2.Height = -5; para2.FallingSpeed = -15; para2.FallingAcceleration = 3;
          end
        end
        if entities[i]:GetData().bounces < 4 then
          local sproing;
          if reflection then
            if entities[i]:GetData().bounces == 1 or entities[i]:GetData().bounces == 3 then
              sproing = Isaac.Spawn(EntityType.ENTITY_TEAR, entities[i].Variant, 0, entities[i].Position, (entities[i].Velocity + Vector((math.random()-0.5)*3, (math.random()-0.5)*3)):__mul(-2), player):ToTear();
            else
              sproing = Isaac.Spawn(EntityType.ENTITY_TEAR, entities[i].Variant, 0, entities[i].Position, (entities[i].Velocity + Vector((math.random()-0.5)*3, (math.random()-0.5)*3)):__mul(-0.5), player):ToTear();
            end
          else
            sproing = Isaac.Spawn(EntityType.ENTITY_TEAR, entities[i].Variant, 0, entities[i].Position, entities[i].Velocity + Vector((math.random()-0.5)*3, (math.random()-0.5)*3), player):ToTear();
          end
          sproing:GetSprite():ReplaceSpritesheet(0,"gfx/skippy_tears.png")
      sproing:GetSprite():LoadGraphics()
          sproing.TearFlags = player.TearFlags
          if fetus or efetus then
            sproing:GetSprite():Load("gfx/bomb_tear.anm2", true)
            sproing:GetSprite():Play(entities[i]:GetData().bounces + 1, true)
            sproing:GetSprite().Scale = Vector(1 - (0.25 * entities[i]:GetData().bounces), 1 - (0.25 * entities[i]:GetData().bounces))
          end
          if knife then
            sproing:GetSprite():Load("gfx/knife_tear.anm2", true)
            sproing:GetSprite():Play("RotateSlow", true)
            sproing:ToTear().TearFlags = sproing:ToTear().TearFlags | TearFlags.TEAR_PIERCING
            if entities[i]:GetData().bounces == 1 then
              sproing.Velocity = entities[i].Velocity:__mul(-0.8);
            end
          end
          sproing.Parent = player
          sproing:GetData().bounce = true;
          sproing:GetData().bounces = entities[i]:GetData().bounces + 1
          sproing.CollisionDamage = entities[i].CollisionDamage * (1.2 - (0.2 * sproing:GetData().bounces));
          sproing.Height = -5
          sproing.FallingSpeed = -18 + (3*entities[i]:GetData().bounces)
          sproing.FallingAcceleration = 3;
          if brim then
            local vectors = {Vector(5,0), Vector(-5,0), Vector(0,5), Vector(0,-5)}
            for laser = 1, #vectors do
              local blargh = player:FireBrimstone(vectors[laser]);
              blargh.Parent = sproing;
              blargh:SetActiveRotation(0, 180, 35, false)
              blargh.MaxDistance = 50;
              blargh.Timeout = 1;
              blargh:GetSprite().Scale = Vector(0.75,0.75);
            end
          end
        else
          if cricket then
            local cricketVectors = {Vector(8,8), Vector(8,-8), Vector(-8,-8), Vector(-8, 8)}
            for tear = 1, 4 do
              local crick = Isaac.Spawn(EntityType.ENTITY_TEAR, entities[i].Variant, 0, entities[i].Position, cricketVectors[tear], player):ToTear();
              crick.TearFlags = player.TearFlags
              crick.Parent = player
              crick:GetData().bounce = false
              crick.Scale = 0.5;
              crick.Height = -5
              crick.FallingSpeed = -15
              crick.FallingAcceleration = 3
            end
          end
          if player:HasCollectible(CollectibleType.COLLECTIBLE_FIRE_MIND) then
            local fire = Isaac.Spawn(1000, 51, 0, entities[i].Position, Vector(0,0), player):ToEffect();
            fire:GetSprite().Scale = Vector(0.7,0.7)
            fire.Timeout = 60
          end
          if fetus or efetus then
            Isaac.Explode(entities[i].Position, player, player.Damage)
          end
        end
        entities[i].Visible = false
      end
      if entities[i].Type == EntityType.ENTITY_EFFECT and entities[i].Variant == EffectVariant.TEAR_POOF_B then
        entities[i]:GetSprite().Scale = Vector(0.5,0.5);
      end
      if entities[i].Type == EntityType.ENTITY_TEAR and entities[i]:GetData().bounce == true then
        if tech > 0 then
          if entities[i]:GetData().zap == nil then
            entities[i]:GetData().zap = player:FireTechXLaser(entities[i].Position + entities[i].Velocity:__mul(2), entities[i].Velocity, 25 + ((tech - 1)*10) - (4 * entities[i]:GetData().bounces))
            entities[i]:GetData().zap.Timeout = 13 - (entities[i]:GetData().bounces * 2);
            entities[i]:GetData().zap.CollisionDamage = player.Damage / 2;
          else
            entities[i]:GetData().zap.Position = entities[i].Position + entities[i].Velocity
          end
        end
      end
    end
  end
end

skippy:AddCallback(ModCallbacks.MC_POST_UPDATE, skippy.HydrophobicityEffect);
skippy:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, skippy.LowerTears);

-- Bucket O' Creep --

local bucket = RegisterMod("Bucket O' Creep",1.0);

local bucketitem = GENESIS_ITEMS.ACTIVE.CREEP_BUCKET;

function bucket:Use()
  local player = Isaac.GetPlayer(0)
    local entities = Isaac.GetRoomEntities()
    if player:HasCollectible(bucketitem) then
    for i = 1, #entities do
      if entities[i]:IsVulnerableEnemy() then
        local target = entities[i]
        Isaac.Spawn(EntityType.ENTITY_EFFECT,EffectVariant.PLAYER_CREEP_GREEN,0,target.Position,Vector(0,0),player)
        SFXManager():Play(SoundEffect.SOUND_GASCAN_POUR,1,0,false,1)
    if player:HasCollectible(CollectibleType.COLLECTIBLE_IPECAC) then
      Isaac.Explode(target.Position,EntityRef(player),1)
    end
  end
  end
end
end
bucket:AddCallback(ModCallbacks.MC_USE_ITEM,bucket.Use)

-- Sparkle Bombs --

local sparkBombs = RegisterMod("Sparkle Bombs",1.0);

local sparkleBombs = GENESIS_ITEMS.PASSIVE.SPARKLEBOMB;

local bombsAdded =
{
  [1] = 0, -- Sparkle
}

function sparkBombs.onPlayerInit(player)
  bombsAdded[1] = 0;
end
sparkBombs:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, sparkBombs.onPlayerInit)

function sparkBombs:sparkle_bombs_effect()
  local player = Isaac.GetPlayer(0);
  if player:HasCollectible(sparkleBombs) then
    if bombsAdded[1] == 0 then
      player:AddBombs(5);
      bombsAdded[1] = 1;
    end
    local entities = Isaac.GetRoomEntities();
    for i = 1, #entities do
      if entities[i].Type == EntityType.ENTITY_BOMBDROP and entities[i].Variant ~= BombVariant.BOMB_TROLL and entities[i].Variant ~= BombVariant.BOMB_SUPERTROLL and entities[i].FrameCount < 2 and entities[i].SpawnerType == EntityType.ENTITY_PLAYER then
        entities[i]:GetSprite():Load("/gfx/effects/sparkle_bomb.anm2", true); entities[i]:GetSprite():Play("Pulse", true);
        Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ULTRA_GREED_BLING, 0, entities[i].Position, Vector(0,0), player);
      end
      if entities[i].Type == EntityType.ENTITY_BOMBDROP and entities[i]:GetSprite():IsPlaying("Explode") and entities[i].Variant ~= BombVariant.BOMB_TROLL and entities[i].Variant ~= BombVariant.BOMB_SUPERTROLL and entities[i]:GetSprite():GetFrame() == 1 and entities[i].SpawnerType == EntityType.ENTITY_PLAYER then
         if entities[i]:GetSprite():IsPlaying("Explode") and entities[i]:GetSprite():GetFrame() == 1 then
          Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ULTRA_GREED_BLING, 0, entities[i].Position, Vector(0,0), player);
          local targets = Isaac.GetRoomEntities();
          for j = 1, #targets do
            if targets[j].Type == EntityType.ENTITY_PICKUP and targets[j].Variant == PickupVariant.PICKUP_COIN and targets[j].SubType == CoinSubType.COIN_PENNY then
              if (targets[j].Position - entities[i].Position):Length() < 600 then
                Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, CoinSubType.COIN_NICKEL, targets[j].Position, Vector(0,0), player);
              end
            elseif targets[j]:IsEnemy() and (targets[j].Position - entities[i].Position):Length() < 200 then
              targets[j]:AddMidasFreeze(EntityRef(player),10)
            end
          end

        end
      end
    end
  end
end

sparkBombs:AddCallback(ModCallbacks.MC_POST_UPDATE, sparkBombs.sparkle_bombs_effect);

-- Angel Blade --

local AngelBlade = RegisterMod("AngelBlade", 1)
local Items = {
  PASSIVE_BLADE = GENESIS_ITEMS.PASSIVE.ANGELBLADE
}
local Effects = {
  ANGELBLADE_SWORD = Isaac.GetEntityVariantByName("Angel Blade"),
  ANGELBLADE_LASER = Isaac.GetEntityVariantByName("Laser Projectile"),
  ANGELBLADE_SLASH = Isaac.GetEntityVariantByName("Slash"),
  ANGELBLADE_HALO = Isaac.GetEntityVariantByName("Deadeye halo"),
  ANGELBLADE_ORBITAL = Isaac.GetEntityVariantByName("Ludo Blade"),
  ANGELBLADE_THROW = Isaac.GetEntityVariantByName("Thrown Blade")
}
local Sounds = {
  SWORD_SWIPE = Isaac.GetSoundIdByName("Sword Swipe"),
  SWORD_SWIPE_LASER = Isaac.GetSoundIdByName("Sword Swipe Laser")
}
local sound = SFXManager()
local hascostume = false
local damagemult = 1
local hasspawned = false
local recharge = 0
function AngelBlade:AngelBlade()
  local player = Isaac.GetPlayer(0)
  local sound = SFXManager()
  local entities = Isaac.GetRoomEntities()
  local costume = Isaac.GetCostumeIdByPath("gfx/characters/angelblade.anm2")
  local rangeboost = 0
  local rangeboost2 = 0
  if Game():GetFrameCount() == 1 then
    hascostume = false
    rangeboost = 0
    hasspawned = false
  end
  if recharge > 0 then
    recharge = recharge - 1
  end
  if player:HasCollectible(Items.PASSIVE_BLADE) then
  player.FireDelay = 99
  if hascostume == false then
    player:AddNullCostume(costume)
    hascostume = true
  end
  if player:HasCollectible(CollectibleType.COLLECTIBLE_GODHEAD) then
    rangeboost = 100
  else
    rangeboost = 0
  end
  if damagemult > 1 and Game():GetFrameCount() % player.MaxFireDelay == 0 then
    damagemult = damagemult - 0.1
  end
    for i = 1, #entities do
    local e = entities[i]
      if e.Type == 2 and e.Variant == Effects.ANGELBLADE_LASER then
        e.Velocity = e.Velocity * 1.1
      end
      if e.Type == 1000 then
        if e.Variant == Effects.ANGELBLADE_SWORD then
          e.Position = player.Position+Vector(0,-10)
          if e.FrameCount > 10 then
            e:Remove()
          end
        end
      end
      if e.Type == 1000 then
        if e.Variant == Effects.ANGELBLADE_HALO then
          if e.FrameCount > 30 then
            e:Remove()
          end
          for i3 = 1, #entities do
          local q = entities[i3]
            if e.Position:Distance(q.Position) < 50 and q:IsActiveEnemy() and q:IsVulnerableEnemy() and q.FrameCount % 2 == 0 then
              q:TakeDamage(1,0,EntityRef(player),1)
            end
            if q:IsActiveEnemy() and q:IsVulnerableEnemy() and q.Velocity ~= Vector(0,0) then
              q.Velocity = (e.Position - q.Position)/10
            end
          end
        end
      end
      if e.Variant == Effects.ANGELBLADE_SLASH or e.Variant == Effects.ANGELBLADE_ORBITAL or e.Variant == Effects.ANGELBLADE_THROW then
        if e.Variant ~= Effects.ANGELBLADE_THROW then
          e.Velocity = player.Velocity*2
        end
        if player:HasTrinket(TrinketType.TRINKET_PULSE_WORM) and e.Variant == Effects.ANGELBLADE_THROW and player.FrameCount % 4 == 0 then
            e.SpriteScale = Vector(math.sin(e.FrameCount/4),math.sin(e.FrameCount/4))+Vector(2,2)/2
            rangeboost2 = (math.sin(e.FrameCount/4)+2)*30
        end
        if player:HasCollectible(CollectibleType.COLLECTIBLE_LUMP_OF_COAL) and e.Variant == Effects.ANGELBLADE_THROW and player.FrameCount % 4 == 0 then
            e.SpriteScale = Vector(e.FrameCount/30,e.FrameCount/30)+Vector(1,1)
            rangeboost2 = e.FrameCount
        end
        if player:HasCollectible(CollectibleType.COLLECTIBLE_MY_REFLECTION) and e.Variant == Effects.ANGELBLADE_THROW then
          e.Velocity = e.Velocity - (e.Position - player.Position)/(e.Position:Distance(player.Position)*4)
        end
        if player:HasCollectible(CollectibleType.COLLECTIBLE_ANTI_GRAVITY) and e.Variant == Effects.ANGELBLADE_THROW then
          if Input.IsButtonPressed(Keyboard.KEY_DOWN,0) or Input.IsButtonPressed(Keyboard.KEY_UP,0) or Input.IsButtonPressed(Keyboard.KEY_RIGHT,0) or Input.IsButtonPressed(Keyboard.KEY_LEFT,0) then
          e.Velocity = Vector(0,0)
        else
          e.Velocity = ((e.Position - player.Position)*8)/(e.Position:Distance(player.Position))
          end
        end
        for i = 1, Game():GetRoom():GetGridSize() do
          local gridEntity = Game():GetRoom():GetGridEntity(i)
          if gridEntity ~= nil then
            if gridEntity.Desc.Type == GridEntityType.GRID_POOP or gridEntity.Desc.Type == GridEntityType.GRID_TNT then
              if (Game():GetRoom():GetGridPosition(i) - e.Position):Length() < 45+rangeboost then
                gridEntity:Destroy()
              end
            end
            if gridEntity.Desc.Type == GridEntityType.GRID_ROCK and player:HasCollectible(CollectibleType.COLLECTIBLE_SULFURIC_ACID) then
              if (Game():GetRoom():GetGridPosition(i) - e.Position):Length() < 45+rangeboost then
                gridEntity:Destroy()
              end
            end
          end
        end
        for i2 = 1, #entities do
          local p = entities[i2]
            if p.Type == 9 and e.Position:Distance(p.Position) < 45 then
              if e.Variant == Effects.ANGELBLADE_SLASH then
                if player:HasCollectible(CollectibleType.COLLECTIBLE_RUBBER_CEMENT) then
                  local bounce = Isaac.Spawn(2,1,0,p.Position,(p.Velocity*-1)+RandomVector()+(player.Velocity/2),player)
                  bounce.CollisionDamage = 4
                  p:Remove()
                elseif player:HasCollectible(CollectibleType.COLLECTIBLE_LOST_CONTACT) then
                  p:Remove()
                else
                  local bounce = Isaac.Spawn(2,1,0,p.Position,(p.Velocity*-1)+RandomVector()+(player.Velocity/2),player)
                  bounce.CollisionDamage = 0
                  p:Remove()
                end
              end
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_SPOON_BENDER) and e.Variant == Effects.ANGELBLADE_THROW and p:IsActiveEnemy() and p:IsVulnerableEnemy() then
              e.Velocity = (p.Position - e.Position)/30
            end
            if p.Type > 9 and p.Type < 1000 and player:HasCollectible(CollectibleType.COLLECTIBLE_STRANGE_ATTRACTOR) then
              p.Velocity = (player.Position - p.Position)/25
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_HEAD_OF_THE_KEEPER) and math.random(100) == 1 and p:IsActiveEnemy() and p:IsVulnerableEnemy() and e.Position:Distance(p.Position) < 45 then
              Isaac.Spawn(5,20,1,player.Position,Vector(0,0),player)
            end
          if e.Position:Distance(p.Position) < 45+rangeboost and p:IsActiveEnemy() and e.Variant ~= Effects.ANGELBLADE_THROW and p:IsVulnerableEnemy() and Game():GetFrameCount() % 5 == 1 or e.Position:Distance(p.Position) < 45+rangeboost+rangeboost2 and p:IsActiveEnemy() and e.Variant == Effects.ANGELBLADE_THROW and p:IsVulnerableEnemy() and Game():GetFrameCount() % 5 == 1 then
            if player:HasCollectible(CollectibleType.COLLECTIBLE_PROPTOSIS) then
              p:TakeDamage((player.Damage*damagemult*(e.Position:Distance(p.Position))/20),0,EntityRef(player),1)
            else
              p:TakeDamage((player.Damage*damagemult),0,EntityRef(player),1)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_DARK_MATTER) then
              p:AddFear(EntityRef(player), 15)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_SINUS_INFECTION) and math.random(3) == 1 and p:IsActiveEnemy() and p:IsVulnerableEnemy() then
              local snot = Isaac.Spawn(2,26,0,p.Position+RandomVector()*100,Vector(0,0),player)
              snot.Velocity = (p.Position - snot.Position)/10
              snot.CollisionDamage = player.Damage
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_MULLIGAN) and math.random(20) == 1 then
              player:AddBlueFlies(1, player.Position, p)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_HOLY_LIGHT) and math.ranodm(20) == 1 then
              Isaac.Spawn(1000,19,0,p.Position+(RandomVector()*3),Vector(0,0),nil)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_MYSTERIOUS_LIQUID) or player:HasCollectible(CollectibleType.COLLECTIBLE_COMMON_COLD) or player:HasCollectible(CollectibleType.COLLECTIBLE_SCORPIO) or player:HasCollectible(CollectibleType.COLLECTIBLE_SERPENTS_KISS) then
              p:AddPoison(EntityRef(player), 15, player.Damage/4)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_GODS_FLESH) and math.random(25) == 1 then
              p:AddShrink(EntityRef(player), 30)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_EYE_OF_GREED) and math.random(100) == 1 then
              p:AddMidasFreeze(EntityRef(player), 10)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_CONTACTS) and math.random(10) == 1 then
              p:AddFreeze(EntityRef(player), 40)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_EYESHADOW) and math.random(20) == 1 then
              p:AddCharmed(100)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_SERPENTS_KISS) and p:IsDead() or p == nil then
              Isaac.Spawn(5,10,6,p.Position,Vector(0,0),nil)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_FIRE_MIND) then
              p:AddBurn(EntityRef(player), 15, player.Damage/3)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_IRON_BAR) then
              p:AddConfusion(EntityRef(player), 30, false)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_LITTLE_HORN) and math.random(300) == 1 then
              p:Die()
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_EYE_OF_BELIAL) and p.Type ~= 33 then
              if p.Type > 999 then
                damagemult = damagemult + 0.2
              end
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_PARASITOID) and math.random(20) == 1 then
              player:AddBlueFlies(1, player.Position, p)
              player:AddBlueSpider(player.Position)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_BACKSTABBER) then
              p:AddEntityFlags(EntityFlags.FLAG_BLEED_OUT)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_COMPOUND_FRACTURE) and player.FrameCount % 3 == 0 and p.Type ~= 33 then
              local bone = Isaac.Spawn(2,29,0,player.Position,(player:GetAimDirection()*10)+Vector(math.random(-1.0,1.0),math.random(-1.0,1.0)),player)
              bone.CollisionDamage = player.Damage
              bone.Color = player.TearColor
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_EUTHANASIA) and p.Type ~= 33 then
              for i = 1, 8 do
                local needle = Isaac.Spawn(2,31,0,p.Position+RandomVector()*15,RandomVector()*15,player)
                needle.CollisionDamage = player.Damage
              end
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_JACOBS_LADDER) and player.FrameCount % 3 == 0 and p.Type ~= 33 then
              local jacob = player:FireTechLaser(e.Position,10,p.Position - e.Position, false, false)
              jacob:SetColor(Color(255, 255, 255, 255, 255, 255, 255), 100000, 1000000, false, false)
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_PARASITE) and player.FrameCount % 3 == 0 and p.Type ~= 33 then
              for i = 1, 2 do
                local para_tear = Isaac.Spawn(2,0,0,player.Position,(player:GetAimDirection()*5)+Vector(math.random(-3.0,3.0),math.random(-3.0,3.0)),player)
                para_tear.CollisionDamage = player.Damage/2
                para_tear.Color = player.TearColor
              end
            end
            if player:HasCollectible(CollectibleType.COLLECTIBLE_DEAD_EYE) and player.FrameCount % 3 == 0 and p.Type ~= 33 then
              local deadeye_halo = Isaac.Spawn(1000,Effects.ANGELBLADE_HALO,0,player.Position,Vector(0,0),player)
            end
          end
        end
        if e.FrameCount > 10 and e.Variant == Effects.ANGELBLADE_SLASH then
          e:Remove()
        end
        if e.FrameCount > 150 and e.Variant == Effects.ANGELBLADE_THROW then
          e:Remove()
        end
      end
    end
    if player:HasCollectible(CollectibleType.COLLECTIBLE_LUDOVICO_TECHNIQUE) then
      if Game():GetRoom():GetFrameCount() == 1 or hasspawned == false then
        Isaac.Spawn(1000, Effects.ANGELBLADE_ORBITAL, 0, player.Position, Vector(0,0), player)
        hasspawned = true
      end
      for i4 = 1, #entities do
      for i5 = 1, #entities do
      local w = entities[i4]
      local c = entities[i5]
        if c.Type == 2 then
          c.Visible = false
          if w.Type == 1000 and w.Variant == Effects.ANGELBLADE_ORBITAL then
            w.Position = c.Position
            w.Velocity = c.Velocity
          end
        end
      end
      end
    end
      if recharge == 0 and player:GetCollectibleNum(CollectibleType.COLLECTIBLE_LUDOVICO_TECHNIQUE) == 0 and not player:IsDead() then
        local number = 0  local negnum = 0  local sidenum = 0 local sidenum2 = 0  local posdif = Vector(0,0)  local shootdir = Vector(0,0)
        if Input.IsActionPressed(ButtonAction.ACTION_SHOOTDOWN, player.ControllerIndex) then
          number = 4  negnum = 3  sidenum = 2 sidenum2 = 1  posdif = Vector(0,40) shootdir = Vector(0,10)
        elseif Input.IsActionPressed(ButtonAction.ACTION_SHOOTUP, player.ControllerIndex) then
          number = 3  negnum = 4  sidenum = 1 sidenum2 = 2  posdif = Vector(0,-40)  shootdir = Vector(0,-10)
        elseif Input.IsActionPressed(ButtonAction.ACTION_SHOOTRIGHT, player.ControllerIndex) then
          number = 2 negnum = 1 sidenum = 4 sidenum2 = 3  posdif = Vector(40,0) shootdir = Vector(10,0)
        elseif Input.IsActionPressed(ButtonAction.ACTION_SHOOTLEFT, player.ControllerIndex) then
          number = 1  negnum = 2  sidenum = 3 sidenum2 = 4  posdif = Vector(-40,0) shootdir = Vector(-10,0)
        end
        if number ~= 0 then
        recharge = player.MaxFireDelay*3
        AngelBlade:Swipe(number,Vector(0,0),posdif,shootdir,player)
          if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
            AngelBlade:Swipe(negnum,Vector(0,0),posdif*(-1),shootdir*(-1),player)
          end
          if player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_EYE) and math.random(2) == 1 then
            AngelBlade:Swipe(negnum,Vector(0,0),posdif*(-1),shootdir*(-1),player)
          end
          if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
            AngelBlade:Swipe(number,(RandomVector()*3),posdif+(RandomVector()*3),shootdir+(RandomVector()*3),player)
            AngelBlade:Swipe(number,(RandomVector()*3),posdif+(RandomVector()*3),shootdir+(RandomVector()*3),player)
          end
          if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
            AngelBlade:Swipe(4,Vector(0,0),Vector(0,40),Vector(0,10),player)
            AngelBlade:Swipe(3,Vector(0,0),Vector(0,-40),Vector(0,-10),player)
            AngelBlade:Swipe(2,Vector(0,0),Vector(40,0),Vector(10,0),player)
            AngelBlade:Swipe(1,Vector(0,0),Vector(-40,0),Vector(-10,0),player)
          end
          if player:HasCollectible(CollectibleType.COLLECTIBLE_LOKIS_HORNS) and math.random(4) == 1 then
            AngelBlade:Swipe(4,Vector(0,0),Vector(0,40),Vector(0,10),player)
            AngelBlade:Swipe(3,Vector(0,0),Vector(0,-40),Vector(0,-10),player)
            AngelBlade:Swipe(2,Vector(0,0),Vector(40,0),Vector(10,0),player)
            AngelBlade:Swipe(1,Vector(0,0),Vector(-40,0),Vector(-10,0),player)
          end
        end
      end
    end
  end
AngelBlade:AddCallback(ModCallbacks.MC_POST_UPDATE, AngelBlade.AngelBlade)
function AngelBlade:Swipe(dir,bladeoffset,slashoffset,laservel,parent)
local player = Isaac.GetPlayer(0)
if math.random(100) <= 90 then
local blade = Isaac.Spawn(1000,Effects.ANGELBLADE_SWORD,0,player.Position,Vector(0,0),parent)
local slash = Isaac.Spawn(1000,Effects.ANGELBLADE_SLASH,0,blade.Position,Vector(0,0),parent)
local bladesprite = blade:GetSprite()
local slashsprite = slash:GetSprite()
blade.Position = player.Position + bladeoffset
slash.Position = player.Position + slashoffset
sound:Play(Sounds.SWORD_SWIPE,1,0,false,2)
bladesprite.Scale = player.SpriteScale / 1.1
slash.Color = player.TearColor
if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) then
  sound:Stop(Sounds.SWORD_SWIPE)
  sound:Play(Sounds.SWORD_SWIPE_LASER,1,2,false,2)
end
if player:HasCollectible(CollectibleType.COLLECTIBLE_KIDNEY_STONE) or player:HasCollectible(CollectibleType.COLLECTIBLE_MONSTROS_LUNG) then
  sound:Stop(Sounds.SWORD_SWIPE)
  sound:Play(SoundEffect.SOUND_BOSS_SPIT_BLOB_BARF,1,2,false,2)
  end
if dir == 1 then
  bladesprite:Play("AttackLeft",false)
  slashsprite:Play("AttackLeft",false)
  if Game():GetFrameCount() % 2 == 0 then
    bladesprite.Y = true
    slashsprite.FlipY = true
  end
end
if dir == 2 then
  bladesprite:Play("AttackRight",false)
  slashsprite:Play("AttackRight",false)
  slashsprite.FlipY = true
  if Game():GetFrameCount() % 2 == 0 then
    bladesprite.FlipY = true
    slashsprite.FlipY = false
  end
end
if dir == 3 then
  bladesprite:Play("AttackUp",false)
  slashsprite:Play("AttackUp",false)
  bladesprite.FlipY = true
  if Game():GetFrameCount() % 2 == 0 then
    bladesprite.FlipX = true
    slashsprite.FlipX = true
  end
end
if dir == 4 then
  bladesprite:Play("AttackDown",false)
  slashsprite:Play("AttackDown",false)
  bladesprite.FlipY = true
  slashsprite.FlipY = true
  if Game():GetFrameCount() % 2 == 0 then
    bladesprite.FlipX = true
    slashsprite.FlipX = true
  end
end
blade.Velocity = player.Velocity
slash.Velocity = slash.Velocity
blade.DepthOffset = -1
slash.DepthOffset = 0
if player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_KNIFE) then
    local laser = player:FireTear(player.Position,laservel,false,false,false)
    local laserspr = laser:GetSprite()
    laser.Scale = 1
    laser.DepthOffset = -1
    laser:IsInvincible()
    laser.CollisionDamage = player.Damage*3
    laserspr.Color = player.TearColor
    laser.TearFlags = player.TearFlags
    laser:ToTear()
    laser.Variant = TearVariant.BONE
    laserspr:Load("gfx/knifetear.anm2",true)
    laserspr:LoadGraphics()
    laserspr:Play("Stone3Move")
    local en = Isaac.GetRoomEntities()
    for b=1, #en do
      if en[b] ~= nil and en[b].Type == EntityType.ENTITY_KNIFE then
        en[b].Visible = false
        en[b].CollisionDamage = 0
      end
    end
end
if player:GetNumKeys() > 50 or player:HasGoldenKey() == true then
  local room = Game():GetRoom()
  for k,v in pairs(DoorSlot) do
	local door = room:GetDoor(v)
		if door ~= nil and v ~= DoorSlot.NUM_DOOR_SLOTS and door:IsLocked() then
		local difference = room:GetDoorSlotPosition(v) - slash.Position
		local distance = difference:Length()
			if distance <= 45 then
        door:TryUnlock(true)
			end
    end
    local en = Isaac.GetRoomEntities()
    for a=1, #en do
      if en[a] ~= nil and en[a].Type == EntityType.ENTITY_PICKUP and en[a].Variant == PickupVariant.PICKUP_LOCKEDCHEST and en[a].SubType == ChestSubType.CHEST_CLOSED then
      local pickup = en[a]:ToPickup()
      local difference = pickup.Position - slash.Position
      local distance = difference:Length()
				if distance <= 32 then
           pickup:TryOpenChest()
        end
      end
    end
  end
end
if player:HasCollectible(CollectibleType.COLLECTIBLE_KIDNEY_STONE) or player:HasCollectible(CollectibleType.COLLECTIBLE_LEAD_PENCIL) then
  if math.random(10) == 1 then
    for i = 1, (player.Luck + math.random(-3,3) + 7) do
      local laser = player:FireTear(player.Position,laservel+(RandomVector()*math.random(3)),false,false,false)
      local laserspr = laser:GetSprite()
      laser.DepthOffset = -1
      laser:IsInvincible()
      laser.CollisionDamage = player.Damage*2
      laserspr.Color = player.TearColor
      laser.TearFlags = player.TearFlags
      local RandomScale = {0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5}
      laser.Scale = RandomScale[math.random(#RandomScale)]
      laser.CollisionDamage = player.Damage*laser.Scale
    end
  if player:HasCollectible(CollectibleType.COLLECTIBLE_KIDNEY_STONE) then
    local stone = Isaac.Spawn(2,22,0,player.Position,laservel*1.5,player)
    stone.Scale = 8.0
    stone.CollisionDamage = player.Damage*3
  end
  end
end
if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) then
  local laser = Isaac.Spawn(2,Effects.ANGELBLADE_LASER,0,player.Position,laservel,slash)
  local laserspr = laser:GetSprite()
  laser.DepthOffset = -1
  laser:IsInvincible()
  laser.CollisionDamage = player.Damage*2
  if dir == 1 then
    laserspr.Rotation = 180
  end
  if dir == 2 then
    laserspr.Rotation = 0
  end
  if dir == 3 then
    laserspr.Rotation = 270
  end
  if dir == 4 then
    laserspr.Rotation = 90
  end
end
if player:HasCollectible(CollectibleType.COLLECTIBLE_GHOST_PEPPER) then
  if math.random(40) == 1 then
    player:ShootRedCandle(laservel/10)
  end
end
if player:HasCollectible(CollectibleType.COLLECTIBLE_DR_FETUS) then
  player:FireBomb(player.Position,laservel*1.3)
end
if player:HasCollectible(CollectibleType.COLLECTIBLE_IPECAC) then
  local poison = player:FireTear(player.Position, laservel/1.5 , false, false, false)
end
if player:HasCollectible(CollectibleType.COLLECTIBLE_EYE_OF_BELIAL) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_belialblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_COMPOUND_FRACTURE) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_boneblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_brimblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_CROWN_OF_LIGHT) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_crownblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_DEATHS_TOUCH) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_deathblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_EUTHANASIA) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_euthanasiablade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_FIRE_MIND) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_flameblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_GODS_FLESH) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_fleshblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_GODHEAD) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_godblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_EYE_OF_GREED) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_greedblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_KIDNEY_STONE) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_kidneyblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_KNIFE) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_knifeblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_JACOBS_LADDER) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_ladderblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_MYSTERIOUS_LIQUID) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_poisonblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_DARK_MATTER) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_shadowblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_SINUS_INFECTION) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_snotblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_SPOON_BENDER) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_spoonblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_techblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_SOY_MILK) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_soyblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_HEAD_OF_THE_KEEPER) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_keeperblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_ANTI_GRAVITY) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_gravityblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_3_DOLLAR_BILL) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_raindowblade.png")
elseif player:HasGoldenKey() == true or player:GetNumKeys() >= 50 then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_keyblade.png")
else
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_angelblade.png")
end
bladesprite:LoadGraphics()
else
local throw = Isaac.Spawn(1000,Effects.ANGELBLADE_THROW,0,player.Position,(laservel*player.ShotSpeed)+player.Velocity/2,player)
local bladesprite = throw:GetSprite()
if player:HasCollectible(CollectibleType.COLLECTIBLE_EYE_OF_BELIAL) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_belialblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_COMPOUND_FRACTURE) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_boneblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_brimblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_CROWN_OF_LIGHT) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_crownblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_DEATHS_TOUCH) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_deathblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_EUTHANASIA) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_euthanasiablade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_FIRE_MIND) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_flameblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_GODS_FLESH) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_fleshblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_GODHEAD) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_godblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_EYE_OF_GREED) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_greedblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_KIDNEY_STONE) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_kidneyblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_KNIFE) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_knifeblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_JACOBS_LADDER) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_ladderblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_MYSTERIOUS_LIQUID) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_poisonblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_DARK_MATTER) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_shadowblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_SINUS_INFECTION) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_snotblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_SPOON_BENDER) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_spoonblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_techblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_SOY_MILK) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_soyblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_HEAD_OF_THE_KEEPER) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_keeperblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_ANTI_GRAVITY) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_gravityblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_3_DOLLAR_BILL) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_raindowblade.png")
elseif player:HasCollectible(CollectibleType.COLLECTIBLE_PROPTOSIS) then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_propblade.png")
elseif player:HasGoldenKey() == true or player:GetNumKeys() >= 50 then
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_keyblade.png")
else
bladesprite:ReplaceSpritesheet(0, "gfx/effects/effect_angelblade.png")
end
bladesprite:LoadGraphics()
end
end
function AngelBlade:dmg(target,amount,flag,source,cdframes)
  if flag == DamageFlag.DAMAGE_EXPLOSION and Isaac.GetPlayer(0):HasCollectible(Isaac.GetItemIdByName("Angel Blade")) then
    return false
  end
end
AngelBlade:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, AngelBlade.dmg, 1)



-- Too Much Water --

local TooMuchWater = RegisterMod("Too Much Water!", 1)
local Item = GENESIS_ITEMS.PASSIVE.TOO_MUCH_WATER
local sources = {}
local r = RNG()
local TooMuchWaterCostume = Isaac.GetCostumeIdByPath("gfx/characters/costume_toomuchwater.anm2")

function TooMuchWater:cacheUpdate(player, cacheFlag)
	player = Isaac.GetPlayer(0)

	-- Change tear color on item pickup
	if player:HasCollectible(Item) then
		if cacheFlag == CacheFlag.CACHE_TEARCOLOR then
			if player:HasCollectible(CollectibleType.COLLECTIBLE_SPOON_BENDER) or player:HasCollectible(CollectibleType.COLLECTIBLE_NUMBER_ONE) or player:HasCollectible(CollectibleType.COLLECTIBLE_LUMP_OF_COAL)
			or player:HasCollectible(CollectibleType.COLLECTIBLE_MYSTERIOUS_LIQUID) or player:HasCollectible(CollectibleType.COLLECTIBLE_EVES_MASCARA) or player:HasCollectible(CollectibleType.COLLECTIBLE_DEAD_ONION)
			or player:HasCollectible(CollectibleType.COLLECTIBLE_IPECAC) or player:HasCollectible(CollectibleType.COLLECTIBLE_CHOCOLATE_MILK) or player:HasCollectible(CollectibleType.COLLECTIBLE_DR_FETUS)
			or player:HasCollectible(CollectibleType.COLLECTIBLE_SACRED_HEART) then
				-- If we have one of those items, dont change tear color.
			else
				player.TearColor = Color(0, 0.633, 0.933, 1, 0, 0, 0) -- Change tear color
			end
		end
	end
  end

 --[[ -- Change costume on pickup
  if player:HasCollectible(Item) then
    player:AddNullCostume(TooMuchWaterCostume)
end
end]]--

-- On taking damage
function TooMuchWater:onDamage(entity, dmgAmount, dmgFlag, source, dmgCountdownFrames)
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(Item) then
		local entities = Isaac.GetRoomEntities()
		local tearVelocity = Vector(player.ShotSpeed*10,0)
		local angle = 360
		local direction = 0
		local sourceIndex = -1

		if (source.Entity ~= nil) then
			sourceIndex = source.Entity.Index -- The index of the entity that did the damage to the enemy
		end

		-- Loop through entities to check if we did the damage to the enemy
		for i ,entity2 in pairs(entities) do
			if entity2.Index == sourceIndex then -- The entity that did the damage to the enemy
				if entity2.Type == EntityType.ENTITY_TEAR and entity2.SpawnerType == EntityType.ENTITY_PLAYER then -- If we did the damage to the enemy with our tears
					if sources[entity.Index] == nil then
						sources[entity.Index] = 0
					end
					if sources[entity.Index] < 9 then -- The enemy can explode into 10 tears MAX (1 more tear added when the enemy die)
						sources[entity.Index] = sources[entity.Index] + 1 -- Increase number of hits for that enemy.
					end
				end
				break
			end
		end

		if entity.HitPoints - dmgAmount <= 0 and sources[entity.Index] ~= nil then -- Enemy died and got hit by our tears
			if entity:IsVulnerableEnemy() and entity:IsActiveEnemy() and entity.Type ~= EntityType.ENTITY_POOP then
				sources[entity.Index] = sources[entity.Index] + 1
				angle = math.floor(angle / sources[entity.Index]) -- The angle of the tear depends on how many times the enemy got hit by our tears
				direction = r:RandomInt(angle) -- Set random direction for the tears

				for i=1, sources[entity.Index] do
					local tear = player:FireTear(entity.Position, tearVelocity:Rotated(i*angle + direction), true, true, false) -- Fire the tear
          local creep = Isaac.Spawn(EntityType.ENTITY_EFFECT,EffectVariant.PLAYER_CREEP_HOLYWATER_TRAIL,0,entity.Position,Vector(0,0),nil) -- spawn the creep
				end
				sources[entity.Index] = nil -- Reset number of hits
			end
		end
	end
end

TooMuchWater:AddCallback( ModCallbacks.MC_ENTITY_TAKE_DMG, TooMuchWater.onDamage )
TooMuchWater:AddCallback( ModCallbacks.MC_EVALUATE_CACHE, TooMuchWater.cacheUpdate )

Isaac.DebugString("Genesis+: Loaded Items (3/3)!")
error({ hack = true })