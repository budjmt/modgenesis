# The Binding of Isaac: Genesis+ Demonic Descent
The Binding of Isaac Genesis +: Demonic Descent is the first of many large content patches for TBOI.

This update will 
- Include 3 new characters, and Improved graphics for the 2 current characters and numerous items,enemies, and pickups added from the mod.
- Aswell as one of our brand new alt chapters with a boss and a new special room.
- 65+ new items! 25+ trinkets and over 30+ New enemies.
- 33 New Challenges
- 12 New Pills 
- 2 New Runes
- 14 New Cards
- New Satanic Ritual Room
- A brand new (Not Replacing) Septic Floor with Unique Enemies, mechanics and more.
- Alot of New Achievements 
&amp; Loads of more stuff!

We wanna thank:
Alphabirth - for using their Achievement and Giantbook Animation code, so thank you for that.
Sentinel - For making the floor possible.
Ashkal - For creating a awesome backdrop for our septic floor.
Edmund Mcmillen - For his amazing roguelike masterpiece

check our Wiki:
http://tboigenesis.gamepedia.com

Listen to the full Soundtrack of BroskiPlays on:
http://www.soundcloud.com/tboigenesis

And thanks to you for giving suggestions and advice to make this mod as best as it can be.

WARNING: We are not approving anyone of stealing/copying our code, if you want to use our code then please ask for permission to either BroskiPlays or RetroGamerSP thank you.
