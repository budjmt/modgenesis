local misc = {
    VEC_ZERO = Vector(0, 0),
    VEC_ONE = Vector(1, 1),
    replaceChanceMult = 1 --the chance to replace an enemy is multiplied by this
}

-----------------
--MUSIC MACHINE--
-----------------

local MusicMachine = RegisterMod("Music Machine", 1)

MusicMachine.State = {}
MusicMachine.NumberSelected = nil

MusicMachine.Track = {
    Music.MUSIC_BOSS,
    Music.MUSIC_BOSS2,
    Music.MUSIC_CATHEDRAL,
    Music.MUSIC_SHEOL,
    Music.MUSIC_ARCADE_ROOM,
    Music.MUSIC_TITLE,
    Music.MUSIC_CREDITS,
    Music.MUSIC_BASEMENT,
    Music.MUSIC_CELLAR
}

MusicMachine.State.STATE_IDLE = 51
MusicMachine.State.STATE_COIN = 52
MusicMachine.State.STATE_CHOOSING = 53
MusicMachine.State.STATE_PLAYING = 54

-- callback to make music machine work in custom stages
StageAPI.AddCallback("Genesis+", "POST_SELECT_STAGE_MUSIC", 0, function(stage, musicID, roomType, rng)
    if MusicMachine.NumberSelected then
        return MusicMachine.Track[MusicMachine.NumberSelected]
    end
end)

function MusicMachine:update(e)
    if e.Variant ~= GENESIS_ENTITIES.VARIANT.MUSIC_MACHINE then return end

    local sprite, data = e:GetSprite(), e:GetData()

    if MusicMachine.NumberSelected then
        local anim = "OverlayNumber" .. MusicMachine.NumberSelected
        if not sprite:IsOverlayPlaying(anim) then
            sprite:PlayOverlay(anim, true)
        end
    end

    --if MusicManager():GetCurrentMusicID() ~= MusicMachine.Track[MusicMachine.NumberSelected] and
    if MusicMachine.NumberSelected and StageAPI.Music:GetCurrentMusicID() ~= MusicMachine.Track[MusicMachine.NumberSelected] then
        --MusicManager():Play(MusicMachine.Track[MusicMachine.NumberSelected], 1)
        --MusicManager():UpdateVolume()
        StageAPI.Music:Play(MusicMachine.Track[MusicMachine.NumberSelected], 1)
        StageAPI.Music:UpdateVolume()
    end

    if not data.State then --Init
        data.State = MusicMachine.State.STATE_IDLE
        data.NumberGlitchDelay = 0
        e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_PLAYERONLY
        data.Pos = e.Position
        sprite:Play("Initiate", false)
        e:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
        e.SpriteOffset = Vector(-4, 0)
        e.StateFrame = 59
        e:AddEntityFlags(EntityFlag.FLAG_NO_FLASH_ON_DAMAGE)
        Genesis.RegisterEntity(e, false)
    end

    e.Velocity = data.Pos - e.Position
    e.StateFrame = e.StateFrame + 1

    if data.State == MusicMachine.State.STATE_COIN then
        if not sprite:IsOverlayPlaying("CoinInsert") then
            sprite:PlayOverlay("CoinInsert", true)
        end

        if sprite:IsFinished("WiggleEnd") then
            data.State = MusicMachine.State.STATE_CHOOSING
        end

        if not sprite:IsPlaying("WiggleEnd") then
            sprite:Play("WiggleEnd", true)
        end
    elseif data.State == MusicMachine.State.STATE_CHOOSING then
        if not (sprite:IsOverlayPlaying("CoinInsert") or sprite:IsPlaying("Wiggle")) then
            sprite:Play("Wiggle", true)
        end

        data.NumberGlitchDelay = data.NumberGlitchDelay + 1
        if data.NumberGlitchDelay > 9 then
            MusicMachine.NumberSelected = math.random(9)
            data.NumberGlitchDelay = 0
        end
    end

    local player
    for i = 0, 3 do
        local activePlayer = Isaac.GetPlayer(i)
        if MusicMachine:touchingPlayer(activePlayer, e) then
            player = activePlayer
            break
        end
    end

    if not player then return end

    if data.State == MusicMachine.State.STATE_IDLE and player:GetNumCoins() > 0 and e.StateFrame > 60 then
        data.State = MusicMachine.State.STATE_COIN
        SFXManager():Play(SoundEffect.SOUND_COIN_SLOT, 1, 0, false, 1)
        player:AddCoins(-1)
    elseif data.State == MusicMachine.State.STATE_CHOOSING then
        data.NumberGlitchDelay = 0
        data.State = MusicMachine.State.STATE_PLAYING
        sprite:Play("MusicPlaying", true)
        e.StateFrame = 0
    elseif data.State == MusicMachine.State.STATE_PLAYING and e.StateFrame > 60 then
        sprite:Play("Idle", true)
        data.NumberGlitchDelay = 0
        MusicMachine.NumberSelected = nil
        data.State = MusicMachine.State.STATE_IDLE
        Game():GetRoom():PlayMusic()
        e.StateFrame = 0
    end
end

function MusicMachine:touchingPlayer(player, e)
    --  Isaac.DebugString((player.Position - e.Position):Length()..","..e.Size..","..player.Size)
    return (player.Position - e.Position):Length() <= e.Size + player.Size
end

MusicMachine:AddCallback(ModCallbacks.MC_NPC_UPDATE, MusicMachine.update, GENESIS_ENTITIES.MUSIC_MACHINE)

function MusicMachine:damage(e)
    if e.Variant == GENESIS_ENTITIES.VARIANT.MUSIC_MACHINE then
        return false
    end
end

MusicMachine:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, MusicMachine.damage, GENESIS_ENTITIES.MUSIC_MACHINE)

MusicMachine:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function() MusicMachine.NumberSelected = nil end)

------------------
--CHARGE MACHINE--
------------------

local ChargeMachine = RegisterMod("Charge Machine", 1)

ChargeMachine.State = {
    STATE_IDLE = 51,
    STATE_STAT = 53
}

ChargeMachine.StatBonus = {
    -0.2,
    0.2,
    0.3,
    -0.5,
    0.1,
    0.3
}

function ChargeMachine:update(e)
    if e.Variant ~= GENESIS_ENTITIES.VARIANT.CHARGE_MACHINE then
        return
    end

    local sprite, data = e:GetSprite(), e:GetData()

    if not data.State then --Init
        data.State = ChargeMachine.State.STATE_IDLE
        e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_PLAYERONLY
        data.Pos = e.Position
        sprite:Play("Initiate", false)
        e:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
        e.SpriteOffset = Vector(-4, 0)
        data.StatFrames = 0
        e:AddEntityFlags(EntityFlag.FLAG_NO_FLASH_ON_DAMAGE)
        Genesis.RegisterEntity(e, false)
    end

    if data.dead then
        if sprite:IsFinished("Death") then
            sprite:Play("Broken")
        end
        e.Velocity = e.Velocity * 0.8
        return
    end

    e.Velocity = data.Pos - e.Position

    if data.State == ChargeMachine.State.STATE_STAT then
        data.StatFrames = data.StatFrames + 1
        if data.StatFrames > 19 then
            if math.random() > 0.4 - player.Luck / 60 then
                sprite:Play("WiggleEnd", true)
                local stat = math.random(8)
                if stat > 6 then
                    data.RewardedPlayer:AnimateSad()
                else
                    data.RewardedPlayer:AnimateHappy()
                    Genesis.moddata.statsup[Genesis.STATSNAMES[stat]] =
                        Genesis.moddata.statsup[Genesis.STATSNAMES[stat]] + ChargeMachine.StatBonus[stat]
                    --            Isaac.DebugString(Genesis.STATSNAMES[stat]..";"..ChargeMachine.StatBonus[stat]..";"..Genesis.moddata.statsup[Genesis.STATSNAMES[stat]])
                    data.RewardedPlayer:AddCacheFlags(CacheFlag.CACHE_ALL)
                    data.RewardedPlayer:EvaluateItems()
                end
                data.State = ChargeMachine.State.STATE_IDLE
            else
                SFXManager():Play(SoundEffect.SOUND_BOSS1_EXPLOSIONS, 1, 0, false, 1)
                Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BOMB_EXPLOSION, 0, e.Position, Genesis.VEC_ZERO, e)
                e:TakeDamage(99999, DamageFlag.DAMAGE_EXPLOSION, EntityRef(player), 0)
            end
            data.RewardedPlayer = nil
        end
        return
    end

    local player
    for i = 0, 3 do
        local activePlayer = Isaac.GetPlayer(i)
        if ChargeMachine:touchingPlayer(activePlayer, e) then
            player = activePlayer
            break
        end
    end

    if not player then return end

    if data.State == ChargeMachine.State.STATE_IDLE and player:GetActiveCharge() > 0 then
        SFXManager():Play(SoundEffect.SOUND_COIN_SLOT, 1, 0, false, 1)
        data.StatFrames = 0
        data.State = ChargeMachine.State.STATE_STAT
        sprite:Play("Wiggle", false)
        player:SetActiveCharge(player:GetActiveCharge() - 2)
        data.RewardedPlayer = player
    end
end

function ChargeMachine:touchingPlayer(player, e)
    return (player.Position - e.Position):Length() <= (e.Size + player.Size) * 1.5
end

ChargeMachine:AddCallback(ModCallbacks.MC_NPC_UPDATE, ChargeMachine.update, GENESIS_ENTITIES.CHARGE_MACHINE)

function ChargeMachine:Damage(e, dmg, flag, source, countdown)
    if e.Variant == GENESIS_ENTITIES.VARIANT.CHARGE_MACHINE then
        if flag & DamageFlag.DAMAGE_EXPLOSION > 0 then
            e:GetSprite():Play("Death", true)
            for i = 1, math.random(3) do
                Isaac.Spawn(
                    EntityType.ENTITY_PICKUP,
                    PickupVariant.PICKUP_LIL_BATTERY,
                    0,
                    e.Position,
                    Vector(math.random(10) - 5, math.random(10) - 5),
                    e
                )
            end
            e:GetData().dead = true
            Genesis.RegisterEntity(e, true)
        end
        return false
    end
end

ChargeMachine:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, ChargeMachine.Damage, GENESIS_ENTITIES.CHARGE_MACHINE)

local miscEnms = RegisterMod("Genesis Enemies", 1)

miscEnms.rng = RNG()

miscEnms.nerve = {}

------------
--SOY WORM--
------------

local function SoyShoot(npc, player)
    local angle = (player.Position - npc.Position):GetAngleDegrees()
    local offset = angle + (npc.FrameCount % 2 == 0 and 90 or -90)

    local tear =
        Isaac.Spawn(
        EntityType.ENTITY_PROJECTILE,
        4,
        0,
        npc.Position + (Vector.FromAngle(offset) * 3),
        Vector.FromAngle(1 * angle):Resized(12),
        npc
    )
    tear:GetSprite().Scale = Vector(0.7, 0.7)
    tear:SetColor(Color(4, 3, 3, 1, 0, 0, 0), 99999, 1, false, false)
    tear.SplatColor = Color(4, 3, 3, 1, 0, 0, 0)
    tear:GetSprite():Load("gfx/002.000_tear.anm2", true)
    --tear:SetColor(Color(1,0.9,0.6,1,0,0,0), 99999, 1, false, false)
end

function miscEnms:SoyWorm(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.SOY_WORM then
        local SoySprite = npc:GetSprite()
        local player = npc:GetPlayerTarget()

        if SoySprite:IsEventTriggered("Shoot Init") then
            npc:PlaySound(SoundEffect.SOUND_WORM_SPIT, 1, 0, false, 1)
            SoyShoot(npc, player)
        elseif SoySprite:IsEventTriggered("Shoot Extra") then
            SoyShoot(npc, player)
        end
    end
end

------------------
--HANGING SPIDER--
------------------

function miscEnms:HangingSpider(npc)
    --  npc = npc:ToNPC()
    if npc.Variant == GENESIS_ENTITIES.VARIANT.HANGING_SPIDER then
        local Room = Game():GetRoom()
        npc:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
        local SpiderSprite = npc:GetSprite()
        local player = npc:GetPlayerTarget()
        local dist = player.Position:Distance(npc.Position)

        if npc.FrameCount >= 5 then
            if npc.State == 0 and (dist <= 110 or Room:IsClear()) then
                npc.State = 30
                npc.EntityCollisionClass = 4
            end

            if npc.State == 30 then
                SpiderSprite:Play("Fall", 0)
                npc.State = 31
            end
            if npc.State ~= 4 and npc.State ~= 30 and npc.State ~= 31 and npc.State ~= 0 then
                npc.State = 0
                npc.EntityCollisionClass = 0
            end
            if SpiderSprite:IsEventTriggered("Fall") then
                npc:PlaySound(SoundEffect.SOUND_GOOATTACH0, 1, 0, false, 1)
            elseif SpiderSprite:IsEventTriggered("Land") then
                npc:PlaySound(SoundEffect.SOUND_MEAT_IMPACTS, 1, 0, false, 1)
                npc:Morph(EntityType.ENTITY_SPIDER, 0, 0, -1)
                npc.HitPoints = npc.MaxHitPoints
                npc.State = 4
            end
        else
            npc.EntityCollisionClass = 0
        end
    end
end

-------------
--BIG BONES--
-------------

function miscEnms:BigBones(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.BIG_BONES then
        --    Isaac.DebugString(npc.State)
        local BigBonesSprite = npc:GetSprite()
        local data = npc:GetData()
        data.HostIdleFrame = data.HostIdleFrame or 0
        data.JumpsSinceShoot = data.JumpsSinceShoot or 0
        local player = npc:GetPlayerTarget()
        local dist = player.Position:Distance(npc.Position)
        npc.Friction = npc.Friction * 0.5
        npc.SplatColor = Color(0, 0, 0, 0, 81, 54, 46)
        npc.StateFrame = npc.StateFrame + 1
        if (BigBonesSprite:IsFinished("Appear") or npc.FrameCount > 5) and npc.State == 0 then
            npc.State = 3
        end

        if npc.State == 3 then
            data.HostIdleFrame = data.HostIdleFrame + 1
            if data.HostIdleFrame == 1 then
                BigBonesSprite:Play("Idle", 0)
                data.HostIdleFrame = 0
            end
            if npc.StateFrame == 20 then
                npc.State = 30
            end
        elseif npc.State == 30 then
            if math.random(math.max(data.JumpsSinceShoot - 1, 1), data.JumpsSinceShoot + 1) > 2 then
                BigBonesSprite:Play("Shoot", 0)
                npc.State = 31
                data.JumpsSinceShoot = 0
            else
                BigBonesSprite:Play("Jump", 0)
                data.JumpsSinceShoot = data.JumpsSinceShoot + 1
                npc.State = 32
            end
        elseif npc.State == 33 then
            npc:GetData().targetVelocity = (player.Position - npc.Position):Resized(50)
            npc.State = 34
        elseif npc.State == 34 then
            npc:GetData().targetVelocity = npc:GetData().targetVelocity * 0.9
            npc.Velocity = npc:GetData().targetVelocity
        end

        if BigBonesSprite:IsEventTriggered("Open") then
            npc:PlaySound(SoundEffect.SOUND_ANIMAL_SQUISH, 1, 0, false, 1)
        elseif BigBonesSprite:IsEventTriggered("Shoot") then
            npc:PlaySound(SoundEffect.SOUND_HEARTOUT, 1, 0, false, 1)
            local tear = Isaac.Spawn(9, 1, 0, npc.Position, (player.Position - npc.Position):Resized(7.5), npc):ToProjectile()
            tear.ProjectileFlags = ProjectileFlags.EXPLODE | ProjectileFlags.SMART
            tear.Color = Color(1, 1, 1, 1.35, 137, 0, 110)
        elseif BigBonesSprite:IsEventTriggered("Hop") then
            npc.State = 33
        elseif BigBonesSprite:IsEventTriggered("Land") then
            npc:PlaySound(SoundEffect.SOUND_MEAT_IMPACTS, 1, 0, false, 1)
            npc.State = 32
            npc.Velocity = npc.Velocity * 0
        end

        if BigBonesSprite:IsFinished("Shoot") or BigBonesSprite:IsFinished("Jump") then
            npc.State = 3
            npc.StateFrame = 0
            miscEnms.HostIdleFrame = 0
        end
    end
end

---------------
--SPIDERS FIX--
--not optimal, need to find reason they get set to higher offset in the first place
function miscEnms:SpiderFix(npc)
    if npc.SpriteOffset.Y < 0 then
        npc.SpriteOffset.Y = 0
    end
end

miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.SpiderFix, EntityType.ENTITY_SPIDER)

---------
--MYOTE--
---------

function miscEnms:Myote(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.MYOTE then
        local height = math.sin(npc.FrameCount / 10) * 2
        npc.SpriteOffset = Vector(0, height + 2)
        local MyoteSprite = npc:GetSprite()
        npc.Friction = npc.Friction * 0.5
        local player = npc:GetPlayerTarget()
        npc.StateFrame = npc.StateFrame + 1
        if MyoteSprite:IsFinished("Attack") and npc.State == 31 then
            npc.StateFrame = 0
            npc.State = 3
        end
        if npc.State == 3 then
            if MyoteSprite:IsPlaying("Idle") == false then
                MyoteSprite:Play("Idle", 0)
            end
            npc.Velocity = (player.Position - npc.Position):Resized(2.5)
            if npc.StateFrame == 40 then
                npc.State = 30
            end
        elseif npc.State == 30 then
            MyoteSprite:Play("Attack", 0)
            npc.State = 31
        elseif npc.State ~= 31 then
            npc.State = 3
        end
        --    Isaac.DebugString(npc.State)
        if MyoteSprite:IsEventTriggered("Charge") then
            npc:PlaySound(SoundEffect.SOUND_MEAT_JUMPS, 1, 0, false, 1)
        elseif MyoteSprite:IsEventTriggered("Shoot") then
            npc:PlaySound(SoundEffect.SOUND_BLOODSHOOT, 1, 0, false, 1)
            local angle = (player.Position - npc.Position):GetAngleDegrees()
            local tear = Isaac.Spawn(9, 0, 0, npc.Position, Vector.FromAngle(1 * angle):Resized(7), npc)
            tear.SpriteOffset = Vector(0, -18 + height)
        end
    end
end

---------
--PUKIE--
---------

function miscEnms:Pukie(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.PUKIE then
        local PukieSprite = npc:GetSprite()
        npc.SplatColor = Color(0.5, 0.5, 0.5, 1, 0, 168, 19)
        if npc.State == 8 then
            local pukieCount = Isaac.CountEntities(nil, EntityType.ENTITY_SUCKER, 1, -1)
            if pukieCount >= 4 then
                npc.State = 4
            end
        end
        if PukieSprite:IsEventTriggered("Swarm") then
            npc:PlaySound(SoundEffect.SOUND_WORM_SPIT, 1, 0, false, 1)
            local spit = Isaac.Spawn(EntityType.ENTITY_SUCKER, 1, 0, npc.Position, Genesis.VEC_ZERO, npc)
            spit:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
        end
    end
end

----------------
--BONE CRUSHER--
----------------

function miscEnms:BoneCrusher(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.BONE_CRUSHER then
        local BoneCrusherSprite = npc:GetSprite()
        local player = npc:GetPlayerTarget()

        npc.SpriteOffset = Vector(0, -16 + math.sin(npc.FrameCount / 10) * 2)
        local dist = player.Position:Distance(npc.Position)
        npc.StateFrame = npc.StateFrame + 1

        if npc.FrameCount <= 2 then
            npc.State = NpcState.STATE_MOVE
        end

        if npc.StateFrame >= 5 and npc.State == NpcState.STATE_MOVE and dist <= 125 then
            npc:PlaySound(SoundEffect.SOUND_LOW_INHALE, 0.9, 0, false, 1.1)
            npc.State = NpcState.STATE_IDLE --attack gets immediately set to move with maggot mavriants, so gotta use idle for it to work
            npc.Velocity:Lerp(Genesis.VEC_ZERO, 3)
            BoneCrusherSprite.PlaybackSpeed = 2.5
            Isaac.DebugString(
                GetDirectionFromVelocity(player.Position - npc.Position) .. ";" .. BoneCrusherSprite.Scale.X
            )
            npc:GetData().flipped =
                GetDirectionFromVelocity(npc.Velocity) == Direction.LEFT and BoneCrusherSprite:IsPlaying("Move Hori")
        end
        if npc.State == NpcState.STATE_MOVE then
            --      Isaac.DebugString(mult..";"..BoneCrusherSprite:GetFrame())
            local mult
            if BoneCrusherSprite:GetFrame() >= 12 then --this and the else are to make him at max speed at the "blowing out" frame, and slow down after
                mult = -math.asin((BoneCrusherSprite:GetFrame() - 12) / 23 - 1) * 4 / math.pi
            else
                mult = -math.asin((BoneCrusherSprite:GetFrame() + 12) / 23 - 1) * 4 / math.pi
            end
            npc.Velocity = npc.Velocity:Resized(math.max(npc.Velocity:Length() * mult, 1))
        elseif npc.State == NpcState.STATE_IDLE then
            if dist <= 140 then
                local dir = GetDirectionFromVelocity(player.Position - npc.Position) or Direction.NO_DIRECTION
                player:SetColor(Color(0.5, 0.5, 0.5, 1, 132, 92, 107), 15, 1, true, false)
                player:AddVelocity((npc.Position - player.Position):Resized(0.75))

                if dir == Direction.UP then
                    BoneCrusherSprite:Play("Move Up", false)
                elseif dir == Direction.DOWN then
                    BoneCrusherSprite:Play("Move Down", false)
                elseif
                    (dir == Direction.LEFT and not npc:GetData().flipped) or
                        (npc:GetData().flipped and dir == Direction.RIGHT)
                 then
                    BoneCrusherSprite:Play("Move Left", false)
                elseif
                    (dir == Direction.RIGHT and not npc:GetData().flipped) or
                        (npc:GetData().flipped and dir == Direction.LEFT)
                 then
                    BoneCrusherSprite:Play("Move Right", false)
                end
            else
                npc.State = NpcState.STATE_MOVE
                npc.StateFrame = 0
                BoneCrusherSprite.PlaybackSpeed = 1
            end
        else
            npc.State = NpcState.STATE_MOVE
        end
    end
end

function miscEnms:DrownedSpitty(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.DROWNED_SPITTY then
        local player = npc:GetPlayerTarget()

        for i, v in ipairs(Genesis.RoomProjectiles) do
            if not v:IsDead() and v.Variant ~= 4 and v.SpawnerVariant == GENESIS_ENTITIES.VARIANT.DROWNED_SPITTY then
                v:Remove()
                Isaac.Spawn(9, 4, 0, v.Position, v.Velocity, npc)
            end
            if v.SpawnerVariant == GENESIS_ENTITIES.VARIANT.DROWNED_SPITTY and v.FrameCount % 5 == 0 then
                Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CREEP_WHITE, 0, v.Position, Genesis.VEC_ZERO, nil):SetColor(
                    Color(0, 0.5, 1, 0.2, 0, 55, 155),
                    0,
                    0,
                    false
                )
            end
        end
        for i, v in ipairs(Genesis.RoomEffects) do
            if v.Variant == EffectVariant.CREEP_WHITE and player.Position:Distance(v.Position) < 20 then
                miscEnms.slide = true
            end
        end
    end
end

function miscEnms:Infamous(npc)
    if npc.Variant ~= GENESIS_ENTITIES.VARIANT.INFAMOUS then
        return
    end

    local sprite = npc:GetSprite()
    npc.StateFrame = npc.StateFrame + 1

    if npc.FrameCount == 1 then
        local nerve =
            Isaac.Spawn(
            GENESIS_ENTITIES.NERVELESS,
            GENESIS_ENTITIES.VARIANT.NERVELESS,
            0,
            Game():GetRoom():GetRandomPosition(0),
            Vector(0, 0),
            npc
        )
    end

    if (npc.State == 4 or npc.State == 8) and not sprite:IsPlaying("Pissed") then
        sprite:Play("Pissed", true)
    end

    if sprite:IsEventTriggered("Grunt") then
        local snd = math.random(3)
        if snd == 1 then
            npc:PlaySound(SoundEffect.SOUND_MONSTER_GRUNT_1, 1, 0, false, 1)
        elseif snd == 2 then
            npc:PlaySound(SoundEffect.SOUND_MONSTER_GRUNT_2, 1, 0, false, 1)
        elseif snd == 3 then
            npc:PlaySound(SoundEffect.SOUND_MONSTER_GRUNT_4, 1, 0, false, 1)
        end
    end
end

function miscEnms:Nerveless(npc) -- Unused, until (if ever) Infamous is properly implemented
    if npc.Variant ~= GENESIS_ENTITIES.VARIANT.NERVELESS then
        return
    end

    npc.StateFrame = npc.StateFrame + 1
    if npc.StateFrame == 40 then
        npc:PlaySound(SoundEffect.SOUND_HELL_PORTAL1, 1, 0, false, 1)
        npc.Position = Game():GetRoom():GetRandomPosition(0)
        npc.StateFrame = 0
    end
end

miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.SoyWorm, GENESIS_ENTITIES.ROUND_WORM)
miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.Infamous, GENESIS_ENTITIES.INFAMOUS)
miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.Nerveless, GENESIS_ENTITIES.NERVELESS)
--miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.MutatedVis, GENESIS_ENTITIES.VIS)
miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.Pukie, GENESIS_ENTITIES.DUKIE)
miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.HangingSpider, GENESIS_ENTITIES.HANGING_SPIDER)
miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.BigBones, GENESIS_ENTITIES.BIG_BONES)
miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.Myote, GENESIS_ENTITIES.MYOTE)
miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.BoneCrusher, GENESIS_ENTITIES.BONE_CRUSHER)
miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.DrownedSpitty, EntityType.ENTITY_SPITY)

-----------------
--ENEMIES STUFF--
-----------------

function miscEnms:MyoteUpdate()
    local entities = Genesis.getRoomEntities()
    for k, v in pairs(entities) do
        if v.Type == 9
        and v.SpawnerType == GENESIS_ENTITIES.MYOTE
        and v.SpawnerVariant == GENESIS_ENTITIES.VARIANT.MYOTE then
            v:GetSprite():Play("RegularTear11", 0)
            if v:IsDead() then
                Isaac.Spawn(9, 0, 0, v.Position, Vector.FromAngle(1 * 45):Resized(12), v)
                Isaac.Spawn(9, 0, 0, v.Position, Vector.FromAngle(1 * 135):Resized(12), v)
                Isaac.Spawn(9, 0, 0, v.Position, Vector.FromAngle(1 * 225):Resized(12), v)
                Isaac.Spawn(9, 0, 0, v.Position, Vector.FromAngle(1 * 315):Resized(12), v)
                Isaac.Spawn(9, 0, 0, v.Position, Vector.FromAngle(1 * 90):Resized(12), v)
                Isaac.Spawn(9, 0, 0, v.Position, Vector.FromAngle(1 * 180):Resized(12), v)
                Isaac.Spawn(9, 0, 0, v.Position, Vector.FromAngle(1 * 270):Resized(12), v)
                Isaac.Spawn(9, 0, 0, v.Position, Vector.FromAngle(1 * 360):Resized(12), v)
            end
        end
    end
end

miscEnms:AddCallback(ModCallbacks.MC_POST_UPDATE, miscEnms.MyoteUpdate)

-----------------
--BLOODY SUCKER--
-----------------

function miscEnms:SuckerUpdate(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.BLOODY_SUCKER then
        local player = npc:GetPlayerTarget()
        local data = npc:GetData()

        if
            not npc:GetSprite():IsPlaying("Succ") and npc.FrameCount > 16 and
                player.Position:Distance(npc.Position) < player.Size + npc.Size + 6
         then
            npc:GetSprite():Play("Succ", true)
            npc.Friction = 0
            npc.State = 4
            npc:PlaySound(SoundEffect.SOUND_BLOODSHOOT, 1, 0, false, 1)
            npc.SpriteOffset = Genesis.VEC_ZERO
        end
        if npc.State == 4 then
            if player.Position:Distance(npc.Position) < player.Size + npc.Size + 16 then
                npc.Position = player.Position
                npc.EntityCollisionClass = EntityCollisionClass.ENTCOLL_PLAYEROBJECTS
                npc.Friction = 0
            else --sometimes the npc inits at state 4 even when its not near the player
                npc.Friction = 1
                npc.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
                npc.SpriteOffset = Vector(0, -3)
                npc.State = 2
                npc:GetSprite():Play("MoveDown", false)
            end
        end

        --    local dir = GetDirectionFromVelocity(player.Position - npc.Position) or Direction.NO_DIRECTION

        if npc.State == 0 or not data.init then
            npc.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
            data.init = true
            npc.SpriteOffset = Vector(0, -3)
            npc.State = 2
            npc:GetSprite():Play("MoveDown", false)
            npc:GetSprite().PlaybackSpeed = 1
        end
        if npc.State == 2 then
            local orb = GetOrbitPosition(player, (npc.Index * 10 + npc.FrameCount) / 100, 96) --the index stuff is to make more bloody suckers in the same room orbit at different positions
            npc:AddVelocity((orb - npc.Position) / 50)
            npc.Velocity = npc.Velocity:Resized(math.min(npc.Velocity:Length(), 6))
            if npc.FrameCount % 50 == 0 and npc.FrameCount > 49 then --every 50 frames
                local r = math.random()
                --          Isaac.DebugString(r)
                if r > 0.5 then
                    npc.State = 3
                    npc.Velocity = Genesis.VEC_ZERO
                    npc:GetSprite().PlaybackSpeed = 1.5
                    npc:PlaySound(SoundEffect.SOUND_BOSS_LITE_HISS, 1, 0, false, 1)
                end
            end
        end
        --    Isaac.DebugString(npc.State)
        if npc.State == 3 then --charging
            npc.StateFrame = npc.StateFrame + 1
            npc:AddVelocity((player.Position - npc.Position):Resized(1.5))
            local room = Game():GetRoom()
            if npc.StateFrame >= 20 or npc:CollidesWithGrid() then --after 20 frames or colliding with a wall
                npc.Velocity:Lerp(Genesis.VEC_ZERO, 6)
                npc.State = 2
                npc.StateFrame = 0
                npc:GetSprite().PlaybackSpeed = 1
                data.sucker_charge = 0
            end
        end
    end
end

miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.SuckerUpdate, GENESIS_ENTITIES.BLOODY_SUCKER)

function miscEnms:TakeDamage(entity, amount, flag, source, countdownFrames)
    if not source.Entity then
        return
    else
        source = source.Entity
    end

    -- PUKIE IMMUNITY TO ITS OWN IPECAC --
    if entity.Variant == GENESIS_ENTITIES.VARIANT.PUKIE and source.Type == 9 and source.Variant == 0 then
        return false
    end

    if entity.Variant == GENESIS_ENTITIES.VARIANT.DIP_DOUBLE and amount >= entity.HitPoints then --
        Isaac.Spawn(EntityType.ENTITY_DIP, 0, 0, entity.Position + Vector(14, 0), entity.Velocity, entity)
        Isaac.Spawn(EntityType.ENTITY_DIP, 0, 0, entity.Position + Vector(-14, 0), entity.Velocity, entity)
    end
end

miscEnms:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, miscEnms.TakeDamage)

--[[Chances with mult = 1:
Dip -> Double Dip: 10%
    -> Dank Dip: 3%
    -> Bomb Dip: 3%
Spider -> Hanging Spider: 10%
Charger -> Bone Crusher: 10%
Spitty -> Drowned Spitty: always, on flooded caves
Any land nonboss enemy -> Big Bones: 6%, on depths or sheol
Any nonboss enemy -> Myote: 6%, on womb
Any nonboss enemy -> Necromancer: 6%, on depths or sheol
Bat -> 3 Fangs, 13%
    -> Bomb Tooth, 5%
Fat fly -> Breeder fly: 10%
Stoney -> Tinted Stoney: 10%
       -> Bomb Stoney: 10%
Baby -> Holy Baby: 10%
Embyro -> Dr. Embyro: 7%
Any land nonboss enemy -> Pimple man: 3% on caves, 8% on womb
Clotty -> Holy clotty: always, on cathedral
Slot machine -> Music Machine: 10%
             -> Charge Machine: 10%
Worm -> Soy Worm: 10%
Any enemy -> Septic version: 70%, on Septic or 5*70/100 %, outside of Septic
]]
miscEnms.randSepEnms = true --if septic enemies should replace all enemies in their floor, and not only their normal counterpart
-- BUDJ EDIT: I'm changing this to make replacements guaranteed in the floor
--[[If random septic chances are enabled (on top of the normal chance for a septic enemy in septic):
Septic Gaper: 35/200
Septic Fatty: 35/200
Pukie: (flying only) 30/200
Green Ghost: (flying only) 20/200
]]
function miscEnms:SepticNewRoomEnt(npc)
    if npc.Type == EntityType.ENTITY_PESTILENCE and not room:GetType() == RoomType.ROOM_BOSS then
        npc.HitPoints = npc.MaxHitPoints / 2 - 1
    end
end

--local vanillaMorph = EntityNPC.inst.Morph

--function EntityNPC.inst:Morph(a, b, c, d)
--  Isaac.DebugString(tostring(a).."type;variant"..tostring(b))
--  vanillaMorph(self,a,b,c,d)
--end

--Kinda different than roll_new_room, so we cant use the same function as above
function miscEnms:RollUpdate(type, variant, subtype, pos, vel, spawner, seed)
    local r = RNG()
    r:SetSeed(seed, 0)
    local seedPercent = r:RandomInt(100) / misc.replaceChanceMult --this is always the same for the same enemy in the same run, so it will be the same eevn if we get out of the room and go back in and with the same seed
    local level = Game():GetLevel()
    local room = Game():GetRoom()
    local stage = level:GetAbsoluteStage()
    local sType = level:GetStageType()

    --Isaac.DebugString(npc.Type..";"..seedPercent)

    --70% chance to replace enemy with septic version in septic, and much lower chance outside of septic
    --Different from NEW_ROOM one as I removed all enemies that don't replace a specific enemy so they don't get spawned by enemies that shouldn't spawn them
    if Genesis.IsSeptic() then
        if type == 33 and (variant == 2 or variant == 3) then
            return {type, GENESIS_ENTITIES.VARIANT.GREEN_FIRE, 0, seed}
        end

        --if r:RandomInt(100) <= 2 and not miscEnms.randSepEnms and seedPercent <= 90 then
        if r:RandomInt(100) <= 2 or (miscEnms.randSepEnms and seedPercent <= 90) then
            if type == EntityType.ENTITY_GAPER then
                return {EntityType.ENTITY_GAPER, GENESIS_ENTITIES.VARIANT.SEPTIC_GAPER, 0, seed}
            elseif type == EntityType.ENTITY_DIP then
                return {EntityType.ENTITY_DIP, GENESIS_ENTITIES.VARIANT.SEPTIC_DIP, 0, seed}
            elseif
                type == EntityType.ENTITY_WALL_CREEP or type == EntityType.ENTITY_BLIND_CREEP or
                    type == EntityType.ENTITY_THE_THING
             then
                return {EntityType.ENTITY_WALL_CREEP, GENESIS_ENTITIES.VARIANT.IPECAC_CREEP, 0, seed}
            elseif type == EntityType.ENTITY_GUSHER and variant < 2 then
                return {EntityType.ENTITY_GUSHER, GENESIS_ENTITIES.VARIANT.SEPTIC_GUSHER + variant, 0, seed}
            elseif type == EntityType.ENTITY_FATTY then
                return {EntityType.ENTITY_FATTY, GENESIS_ENTITIES.VARIANT.SEPTIC_FATTY, 0, seed}
            elseif type == EntityType.ENTITY_HOST or type == EntityType.ENTITY_MUSHROOM then
                return {EntityType.ENTITY_HOST, GENESIS_ENTITIES.VARIANT.SEPTIC_HOST, 0, seed}
            elseif type == EntityType.ENTITY_DUKIE then
                return {EntityType.ENTITY_DUKIE, GENESIS_ENTITIES.VARIANT.PUKIE, 0, seed}
            elseif type == EntityType.ENTITY_WIZOOB then
                return {EntityType.ENTITY_WIZOOB, GENESIS_ENTITIES.VARIANT.GREEN_GHOST, 0, seed}
            end
        end
    end

    if type == EntityType.ENTITY_ROUND_WORM then
        if seedPercent <= 10 then
            return {EntityType.ENTITY_ROUND_WORM, GENESIS_ENTITIES.VARIANT.SOY_WORM, 0, seed}
        end
    elseif type == EntityType.ENTITY_STONEY then
        if seedPercent <= 10 then
            return {EntityType.ENTITY_STONEY, GENESIS_ENTITIES.VARIANT.TINTED_STONEY, 0, seed}
        elseif seedPercent <= 20 then
            return {EntityType.ENTITY_STONEY, GENESIS_ENTITIES.VARIANT.BOMB_STONEY, 0, seed}
        end
    elseif type == EntityType.ENTITY_EMBRYO then
        if seedPercent <= 7 then
            return {GENESIS_ENTITIES.DR_EMBRYO, 0, 0, seed}
        end
    elseif type == EntityType.ENTITY_SUCKER then
        if seedPercent <= 10 then
            return {EntityType.ENTITY_SUCKER, GENESIS_ENTITIES.VARIANT.BREEDER_FLY, 0, seed}
        end
    elseif type == EntityType.ENTITY_DIP then
        if
            (stage == LevelStage.STAGE3_1 or stage == LevelStage.STAGE3_2) and
                sType == StageType.STAGETYPE_AFTERBIRTH and seedPercent <= 30
         then
            return {EntityType.ENTITY_DIP, GENESIS_ENTITIES.VARIANT.DANK_DIP, 0, seed}
        elseif seedPercent <= 7 then
            return {EntityType.ENTITY_DIP, GENESIS_ENTITIES.VARIANT.BOMB_DIP, 0, seed}
        elseif seedPercent <= 10 and (not spawner or spawner.Variant ~= GENESIS_ENTITIES.VARIANT.DIP_DOUBLE) then
            return {EntityType.ENTITY_DIP, GENESIS_ENTITIES.VARIANT.DIP_DOUBLE, 0, seed}
        end
    elseif type == EntityType.ENTITY_SPIDER then
        if seedPercent <= 10 and not spawner then
            return {GENESIS_ENTITIES.HANGING_SPIDER, GENESIS_ENTITIES.VARIANT.HANGING_SPIDER, 0, seed}
        end
    elseif type == EntityType.ENTITY_CHARGER then
        if seedPercent <= 10 then
            return {EntityType.ENTITY_CHARGER, GENESIS_ENTITIES.VARIANT.BONE_CRUSHER, 0, seed}
        end
    elseif type == EntityType.ENTITY_ONE_TOOTH or type == EntityType.ENTITY_FAT_BAT then
        if seedPercent <= 13 then
            Isaac.Spawn(
                GENESIS_ENTITIES.FANGS,
                GENESIS_ENTITIES.VARIANT.FANGS,
                0,
                pos - Vector(math.random(-10, 10), math.random(-10, 10)),
                Genesis.VEC_ZERO,
                spawner
            )
            Isaac.Spawn(
                GENESIS_ENTITIES.FANGS,
                GENESIS_ENTITIES.VARIANT.FANGS,
                0,
                pos - Vector(math.random(-10, 10), math.random(-10, 10)),
                Genesis.VEC_ZERO,
                spawner
            )
            Isaac.Spawn(
                GENESIS_ENTITIES.FANGS,
                GENESIS_ENTITIES.VARIANT.FANGS,
                0,
                pos - Vector(math.random(-10, 10), math.random(-10, 10)),
                Genesis.VEC_ZERO,
                spawner
            )
            return {StageAPI.E.DeleteMeNPC.T, StageAPI.E.DeleteMeNPC.V, 0, seed}
        elseif seedPercent <= 18 then
            return {EntityType.ENTITY_ONE_TOOTH, GENESIS_ENTITIES.VARIANT.BOMB_TOOTH, 0, seed}
        end
    elseif
        type == EntityType.ENTITY_SPITY and (stage == LevelStage.STAGE2_1 or stage == LevelStage.STAGE2_2) and
            sType == StageType.STAGETYPE_AFTERBIRTH
     then
        return {EntityType.ENTITY_SPITY, GENESIS_ENTITIES.VARIANT.DROWNED_SPITTY, 0, seed}
    end

    if stage == LevelStage.STAGE5 and sType == StageType.STAGETYPE_WOTL then
        if type == EntityType.ENTITY_CLOTTY then
            return {EntityType.ENTITY_CLOTTY, GENESIS_ENTITIES.VARIANT.HOLY_CLOTTY, 0, seed}
        elseif type == EntityType.ENTITY_BABY then
            if seedPercent <= 10 then
                return {EntityType.ENTITY_BABY, GENESIS_ENTITIES.VARIANT.HOLY_BABY, 0, seed}
            end
        end
    end

    if not spawner then
        -- todo fix
        if
            false and
                (stage == LevelStage.STAGE3_1 or stage == LevelStage.STAGE3_2 or
                    (stage == LevelStage.STAGE5 and sType == StageType.STAGETYPE_ORIGINAL))
         then
            if seedPercent <= 6 then
                return {GENESIS_ENTITIES.BIG_BONES, GENESIS_ENTITIES.VARIANT.BIG_BONES, 0, seed}
            elseif seedPercent <= 11 then
                return {GENESIS_ENTITIES.NECROMANCER, GENESIS_ENTITIES.VARIANT.NECROMANCER, 0, seed}
            end
        elseif (stage == LevelStage.STAGE4_1 or stage == LevelStage.STAGE4_2) then
            if type == EntityType.ENTITY_EYE then
                if seedPercent <= 5 then
                    return {GENESIS_ENTITIES.MYOTE, GENESIS_ENTITIES.VARIANT.MYOTE, 0, seed}
                end
            elseif type == EntityType.ENTITY_GUSHER then
                if seedPercent <= 9 then
                    return {GENESIS_ENTITIES.PIMPLE_MAN, GENESIS_ENTITIES.VARIANT.PIMPLE_MAN, 0, seed}
                end
            end
        elseif type == EntityType.ENTITY_GUSHER and stage >= LevelStage.STAGE2_1 and stage <= LevelStage.STAGE2_2 then
            if seedPercent <= 3 then
                return {GENESIS_ENTITIES.PIMPLE_MAN, GENESIS_ENTITIES.VARIANT.PIMPLE_MAN, 0, seed}
            end
        end
    end

    if type == EntityType.ENTITY_SLOT then
        if seed % 50 <= 10 then
            return {GENESIS_ENTITIES.MUSIC_MACHINE, GENESIS_ENTITIES.VARIANT.MUSIC_MACHINE, 0, seed}
        elseif seed % 50 <= 20 then
            return {GENESIS_ENTITIES.CHARGE_MACHINE, GENESIS_ENTITIES.VARIANT.CHARGE_MACHINE, 0, seed}
        end
    end
end

miscEnms:AddCallback(ModCallbacks.MC_PRE_ENTITY_SPAWN, miscEnms.RollUpdate)

function miscEnms:StatusEffects()
    for i = 0, 3 do
        local player = Isaac.GetPlayer(i)
        if miscEnms.slide then
            miscEnms.slide = false
            player:AddVelocity(player.Velocity / 4)
        end
    end
end

miscEnms:AddCallback(ModCallbacks.MC_POST_UPDATE, miscEnms.StatusEffects)

---------------------------
--SEPTIC GAPER AND BODIES--
---------------------------

function Genesis.SpawnIpecacProjectile(pos, vel, spawner, scale)
    local tear = Isaac.Spawn(9, 0, 0, pos, vel, spawner):ToProjectile()
    local sprite = tear:GetSprite()
    tear.ProjectileFlags = ProjectileFlags.EXPLODE
    tear.FallingSpeed = -11
    tear.FallingAccel = 0.3
    tear.Scale = scale
    tear.Color = Color(0.2, 1.5, 0.5, 1, 0, 0, 0)
    sprite:ReplaceSpritesheet(0, "gfx/Ipecac_Enemy_Bullets.png")
    sprite:LoadGraphics()
    return tear
end

function miscEnms:ipecac_fireplace(e)
    if e.Variant ~= GENESIS_ENTITIES.VARIANT.GREEN_FIRE then
        return
    end

    if e.HitPoints <= 1 then
        return
    end

    if (e.FrameCount + 1) % 150 == 0 then
        local target = e:GetPlayerTarget()
        local dir = target.Position - e.Position
        Genesis.SpawnIpecacProjectile(e.Position, dir:Resized(6), e, 1.5)
    end
end

miscEnms:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, e, amt, flags, src)
    if e.Variant == GENESIS_ENTITIES.VARIANT.GREEN_FIRE and flags & DamageFlag.DAMAGE_EXPLOSION ~= 0 then
        e:ToNPC():Morph(e.Type, 2, e.SubType, -1)
        return true
    end
end, EntityType.ENTITY_FIREPLACE)

miscEnms:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, e)
    if e.SpawnerType == 33 and e.SpawnerVariant == GENESIS_ENTITIES.VARIANT.GREEN_FIRE then
        local sprite = e:GetSprite()
        sprite.Color = Color(0.5, 1.5, 1.1, 1, 0, 0, 0, 0)
    end
end, 66)

miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.ipecac_fireplace, EntityType.ENTITY_FIREPLACE)

function miscEnms:SepticGaper(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.SEPTIC_GAPER and not npc:GetData().septicGaper then
        npc:GetData().septicGaper = true
    end
end

miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.SepticGaper, EntityType.ENTITY_GAPER)

function miscEnms:SepticGusher(npc)
    if
        npc.Variant ~= GENESIS_ENTITIES.VARIANT.SEPTIC_GUSHER and npc.Variant ~= GENESIS_ENTITIES.VARIANT.SEPTIC_PACER and
            npc:GetData().septicGaper
     then
        if npc.Variant == 0 then
            npc:Morph(EntityType.ENTITY_GUSHER, GENESIS_ENTITIES.VARIANT.SEPTIC_GUSHER, 0, -1)
        else
            npc:Morph(EntityType.ENTITY_GUSHER, GENESIS_ENTITIES.VARIANT.SEPTIC_PACER, 0, -1)
        end
    end

    if npc.Variant == GENESIS_ENTITIES.VARIANT.SEPTIC_GUSHER then
        local sprite = npc:GetSprite()
        if not sprite:IsOverlayPlaying("Blood") then
            sprite:PlayOverlay("Blood", true)
        end
    end
end

miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.SepticGusher, EntityType.ENTITY_GUSHER)

function miscEnms:SepticEnemyCreep(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.SEPTIC_GAPER and npc.FrameCount % 6 == 0 then
        local e =
            Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CREEP_GREEN, 0, npc.Position, Genesis.VEC_ZERO, nil)
        e:Update()
    end
end

miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.SepticEnemyCreep, EntityType.ENTITY_GAPER)
miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.SepticEnemyCreep, EntityType.ENTITY_GUSHER)

function miscEnms:SepticHost(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.SEPTIC_HOST then
        for i, e in ipairs(Genesis.RoomProjectiles) do
            if e.SpawnerVariant == GENESIS_ENTITIES.VARIANT.SEPTIC_HOST and e.SpawnerType == EntityType.ENTITY_HOST then
                if e.SubType ~= GENESIS_ENTITIES.VARIANT.SEPTIC_HOST then
                    e.SubType = GENESIS_ENTITIES.VARIANT.SEPTIC_HOST
                    e:GetSprite():ReplaceSpritesheet(0, "gfx/enemybullets_creep.png")
                    e:GetSprite():LoadGraphics()
                end
                if e.FrameCount % 5 == 0 then
                    local c =
                        Isaac.Spawn(
                        EntityType.ENTITY_EFFECT,
                        EffectVariant.CREEP_GREEN,
                        0,
                        e.Position,
                        Genesis.VEC_ZERO,
                        nil
                    )
                    c:Update()
                end
            end
        end
    end
end

miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.SepticHost, EntityType.ENTITY_HOST)

function miscEnms:SepticFatty(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.SEPTIC_FATTY then
        if npc:IsDead() and not npc:GetData().spawn then
            npc:GetData().spawn = true
            local e =
                Isaac.Spawn(
                EntityType.ENTITY_DIP,
                GENESIS_ENTITIES.VARIANT.SEPTIC_DIP,
                0,
                npc.Position,
                npc.Velocity,
                npc
            )
            e:GetData().checkedReplacer = true
        end

        if npc:GetSprite():IsEventTriggered("Shoot") then
            npc:GetData().fartNextFrame = true
        elseif npc:GetData().fartNextFrame then
            npc:GetData().fartNextFrame = false
            local player = npc:GetPlayerTarget()
            player:GetData().vel = player.Velocity
            for i, e in ipairs(Genesis.RoomEffects) do
                if e.Variant == EffectVariant.FART then
                    e:Remove()
                end
            end
            local f = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.FART, 3, npc.Position, Genesis.VEC_ZERO, npc)
            f.SpriteScale = Vector(1.3, 1.3)
            if (player.Position - npc.Position):Length() <= 96 then
                player:TakeDamage(1, DamageFlag.DAMAGE_FAKE, EntityRef(npc), 30)
                player.Velocity = player:GetData().vel
            end
        end
    end
end

miscEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, miscEnms.SepticFatty, EntityType.ENTITY_FATTY)

--------------------
--MICROMARIO STUFF--
--------------------

local micromario = RegisterMod("Genesis: Micromario Enemies", 1)

function micromario:newghost(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.GREEN_GHOST then
        local count = 9
        for i, v in ipairs(Genesis.RoomProjectiles) do
            if v.SpawnerVariant == GENESIS_ENTITIES.VARIANT.GREEN_GHOST then
                if math.random(1, 10 - count) < 3 then
                    Genesis.SpawnIpecacProjectile(v.Position, v.Velocity * 0.3, v, 0.75)
                    count = count - 1
                end
                v:Remove()
            end
        end
    end
end

micromario:AddCallback(ModCallbacks.MC_NPC_UPDATE, micromario.newghost, EntityType.ENTITY_WIZOOB)

---------
--FANGS--
---------

local FangsMod = RegisterMod("Fangs", 1)

function FangsMod:OnFangs(entity)
    if entity.Variant == GENESIS_ENTITIES.VARIANT.FANGS then
        local data = entity:GetData()
        local sprite = entity:GetSprite()
        local target = entity:GetPlayerTarget()
        local angle = (target.Position - entity.Position):GetAngleDegrees()
        if entity.FrameCount == 1 then
            sprite:Play("Appear", false)
        end
        if entity.State == 0 then
            data.State2Frame = 0
            entity.CollisionDamage = 0
            data.HasSavedPos = false
            entity.Velocity = (target.Position - entity.Position):Normalized() * 3
            sprite:Play("Idle", false)
            if target.Position:Distance(entity.Position) < 100 then
                entity.State = 2
            end
        elseif entity.State == 2 then
            sprite:Play("Dash", false)
            entity.CollisionDamage = 1
            if data.HasSavedPos == false then
                data.PreviousPos = entity.Position
                data.HasSavedPos = true
            end
            entity.Velocity = (target.Position - entity.Position):Normalized() * 6
            data.State2Frame = data.State2Frame + 1
            if data.State2Frame > 40 then
                entity.State = 3
                data.State2Frame = 0
                sprite:Play("Idle", false)
                data.State3Frame = 0
            end
            if target.Position:Distance(entity.Position) < 30 then
                entity.State = 5
                data.State2Frame = 0
                data.State3Frame = 0
                data.State5Frame = 0
            end
            if data.State2Frame == 1 then
                SFXManager():Play(SoundEffect.SOUND_SHAKEY_KID_ROAR, 1, 0, false, 1.5)
            end
        elseif entity.State == 3 then
            entity.CollisionDamage = 0
            sprite:Play("Idle", false)
            entity.Velocity = (data.PreviousPos - entity.Position):Normalized() * 3
            data.State3Frame = data.State3Frame + 1
            if data.State3Frame >= 80 or data.PreviousPos:Distance(entity.Position) < 20 then
                data.State3Frame = 0
                data.State4Frame = 0
                entity.State = 4
            end
        elseif entity.State == 4 then
            sprite:Play("Spit", false)
            entity.CollisionDamage = 0
            data.State4Frame = data.State4Frame + 1
            if data.State4Frame == 5 then
                local shot =
                    Isaac.Spawn(
                    EntityType.ENTITY_PROJECTILE,
                    0,
                    0,
                    entity.Position,
                    Vector.FromAngle(1 * angle):Resized(10),
                    entity
                )
                SFXManager():Play(SoundEffect.SOUND_SHAKEY_KID_ROAR, 1, 0, false, 1.5)
            end
            if data.State4Frame >= 11 then
                data.State4Frame = 0
                entity.State = 0
            end
        elseif entity.State == 5 then
            sprite:Play("Bite", false)
            SFXManager():Play(SoundEffect.SOUND_BOSS_LITE_ROAR, 0.5, 0, false, 1.7)
            entity.Velocity = Vector(0, 0)
            data.State5Frame = data.State5Frame + 1
            if data.State5Frame > 6 then
                data.State5Frame = 0
                data.State3Frame = 0
                data.State2Frame = 0
                entity.State = 3
            end
        end
    end
end

FangsMod:AddCallback(ModCallbacks.MC_NPC_UPDATE, FangsMod.OnFangs, GENESIS_ENTITIES.FANGS)

-------------------
--BROSKIS ENEMIES--
-------------------

local brEnms = RegisterMod("Broski's Enemies", 1)

------------
--DIP DANK--
------------

function brEnms:DipDank(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.DANK_DIP then
        if npc.FrameCount % 6 == 0 then
            local e =
                Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CREEP_BLACK, 0, npc.Position, Genesis.VEC_ZERO, nil)
            e:Update()
        end

        local player = npc:GetPlayerTarget()

        if npc.FrameCount % 30 == 0 and npc.Velocity:Length() < 3 then
            npc.Pathfinder:FindGridPath(player.Position, 2, 1, false)
            npc:GetSprite():Play("Move", true)
        end
    end
end

brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.DipDank, EntityType.ENTITY_DIP)

--------------
--DIP SEPTIC--
--------------

function brEnms:DipSeptic(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.SEPTIC_DIP then
        if npc:GetSprite():GetFrame() == 0 then
            local e =
                Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CREEP_GREEN, 0, npc.Position, Genesis.VEC_ZERO, nil)
            e:Update()
        end
    end
end
brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.DipSeptic, EntityType.ENTITY_DIP)

--------------
--DIP BOMB  --
--------------

function brEnms:DipBomb(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.BOMB_DIP then
        if npc.Velocity:Length() < 1.5 and math.random() > 0.7 then
            npc.Pathfinder:FindGridPath(player.Position, 2, 1, false)
            npc:GetSprite():Play("Move", true)
        end
        for i = 0, 3 do
            local player = Isaac.GetPlayer(i)
            if (player.Position - npc.Position):Length() < 25 then
                Isaac.Explode(npc.Position, npc, 3)
                npc:Die()
            end
        end
        if (npc:IsDead() or npc.HitPoints <= 0) then
            Isaac.Explode(npc.Position, npc, 3)
            npc:Remove()
        end
    end
end
brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.DipBomb, EntityType.ENTITY_DIP)

brEnms.LaserType = {LASER_HOLY = 5}

--------------
--BOMB TOOTH--
--------------

function brEnms:BombTooth(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.BOMB_TOOTH then
        local sprite = npc:GetSprite()
        for i = 0, 3 do
            local player = Isaac.GetPlayer(i)
            if (player.Position - npc.Position):Length() < 40 then
                if sprite:IsPlaying("Dash") then
                    npc:Die()
                end
            end
        end
        if (npc:IsDead() or npc.HitPoints <= 0) then
            Isaac.Explode(npc.Position, npc, 3)
        end
    end
end
brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.BombTooth, EntityType.ENTITY_ONE_TOOTH)

----------------
--IPECAC CREEP--
----------------

function brEnms:IpecacCreep(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.IPECAC_CREEP then
        local sprite, data = npc:GetSprite(), npc:GetData()
        local player = npc:GetPlayerTarget()

        if sprite:IsEventTriggered("ShootIpecac") then
            local p =
                Genesis.SpawnIpecacProjectile(
                npc.Position,
                Vector(0, 4 * npc.SpriteScale.Y):Rotated(npc.SpriteRotation),
                npc,
                1
            )
            p:GetData().ipecacCreep = true
        end
        if (npc:IsDead() or npc.HitPoints <= 0) and not data.spawn then
            local e =
                Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CREEP_GREEN, 0, npc.Position, Vector(0, 0), nil)
            e:Update()
            data.spawn = true
        end

        for i, e in ipairs(Genesis.RoomProjectiles) do
            if
                e.SpawnerType == EntityType.ENTITY_WALL_CREEP and
                    e.SpawnerVariant == GENESIS_ENTITIES.VARIANT.IPECAC_CREEP and
                    not e:GetData().ipecacCreep
             then
                e:Remove()
            end
        end
    end
end

brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.IpecacCreep, EntityType.ENTITY_WALL_CREEP)

-----------------
--TINTED STONEY--
-----------------

function brEnms:TintedStoney(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.TINTED_STONEY then
        if math.random() > 0.2 then
            Isaac.Spawn(
                EntityType.ENTITY_PICKUP,
                PickupVariant.PICKUP_HEART,
                HeartSubType.HEART_SOUL,
                npc.Position,
                Vector(0, 0),
                nil
            )
        else
            Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_LOCKEDCHEST, 0, npc.Position, Vector(0, 0), nil)
        end
    end
end
brEnms:AddCallback(ModCallbacks.MC_POST_NPC_DEATH, brEnms.TintedStoney, EntityType.ENTITY_STONEY)

---------------
--BOMB STONEY--
---------------

function brEnms:BombStoney(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.BOMB_STONEY then
        local nearPlayers = #Isaac.FindInRadius(npc.Position, 25, EntityPartition.PLAYER) > 0
        if nearPlayers then
            npc:Die()
        end
    end
end

function brEnms:BombStoneyDie(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.BOMB_STONEY then
        Isaac.Explode(npc.Position, npc, 3)
    end
end
brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.BombStoney, EntityType.ENTITY_STONEY)
brEnms:AddCallback(ModCallbacks.MC_POST_NPC_DEATH, brEnms.BombStoneyDie, EntityType.ENTITY_STONEY)

--------------
--PIMPLE MAN--
--------------

function brEnms:PimpleMan(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.PIMPLE_MAN then
        local sprite = npc:GetSprite()
        if npc.State == 0 then
            if npc.FrameCount % 6 == 0 then
                local e =
                    Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CREEP_RED, 0, npc.Position, Vector(0, 0), nil)
                e:Update()
            end
            npc.Velocity = npc.Velocity:Normalized() * 2.5
            npc:AnimWalkFrame("WalkHori", "WalkVert", 0.1)
            if npc.FrameCount % 80 == 0 and math.random() > 0.6 then
                npc.State = 2
            else
                npc.Pathfinder:MoveRandomly(false)
            end
        elseif npc.State == 2 then
            if sprite:IsFinished("Blood") then
                npc.State = 0
            elseif not sprite:IsPlaying("Blood") then
                npc.Velocity = Vector(0, 0)
                sprite:Play("Blood", true)
                for i = 1, 4 do
                    local tear = Isaac.Spawn(EntityType.ENTITY_PROJECTILE, 0, 0, npc.Position, RandomVector() * 8, npc)
                    tear.Color = Color(3, 27, 0, 0.8, 0, 0, 0)
                end
            end
        else
            npc.State = 0
        end
    end
end
brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.PimpleMan, GENESIS_ENTITIES.PIMPLE_MAN)

---------------
--NECROMANCER--
---------------

function brEnms.AnimWalkFrameUpDown(npc, hori, up, down, treshold)
    local dir = GetDirectionFromVelocity(npc.Velocity)
    local sprite = npc:GetSprite()
    if dir == Direction.UP then
        sprite:Play(up, false)
    elseif dir == Direction.DOWN then
        sprite:Play(down, false)
    else
        npc:AnimWalkFrame(hori, down, treshold)
    end
end

function brEnms:Necromancer(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.NECROMANCER then
        local player = npc:GetPlayerTarget()
        local npcsprite = npc:GetSprite()
        npc.SpriteOffset = Vector(0, math.sin(npc.FrameCount / 10) * 3)

        if npc.State == 0 then
            npc.Pathfinder:MoveRandomly(false)

            local effect =
                Isaac.Spawn(
                EntityType.ENTITY_EFFECT,
                EffectVariant.DARK_BALL_SMOKE_PARTICLE,
                0,
                Vector(npc.Position.X, npc.Position.Y - 25),
                Genesis.VEC_ZERO,
                nil
            )
            effect:SetColor(Color(1, 1, 1, 1.35, 127, 0, 240), 99999, 1, false, false)
            effect.Velocity = npc.Velocity
            npc.Velocity = npc.Velocity:Resized(3)
            brEnms.AnimWalkFrameUpDown(npc, "Hori", "Up", "Down", 1)

            if (npc.StateFrame + 1) % 240 == 0 and (player.Position - npc.Position):Length() > 40 + player.Size + npc.Size then
                    npc.State = 2
                    npc.StateFrame = 0
                    npcsprite:Play("Summon", false)
            elseif (npc.FrameCount + 1) % 65 == 0 and math.random(10) > 5 then
                        local angle = (player.Position - npc.Position):GetAngleDegrees()
                        SFXManager():Play(SoundEffect.SOUND_GHOST_SHOOT, 1, 0, false, 1)
                        local shot =
                            Isaac.Spawn(
                            EntityType.ENTITY_PROJECTILE,
                            0,
                            0,
                            npc.Position,
                            Vector.FromAngle(1 * angle):Resized(10),
                            npc
                        )
                        shot.Color = Color(0, 0, 1, 0.6, 120, 0, 160)
                        local entities = Genesis.getRoomEntities()
                        for k, v in pairs(entities) do
                            if v.Type == GENESIS_ENTITIES.NECROMANCER
                            and v.SpawnerVariant == GENESIS_ENTITIES.VARIANT.NECROMANCER then
                                v.Velocity = (player.Position - npc.Position):Resized(7.5)
                            end
                        end
            end
        elseif npc.State == 2 then
            npc.Velocity = Genesis.VEC_ZERO
            if npcsprite:IsEventTriggered("Summon") then
                Isaac.Spawn(EntityType.ENTITY_KNIGHT, 0, 0, npc.Position, Genesis.VEC_ZERO, nil)
            elseif npcsprite:IsFinished("Summon") then
                npc.State = 0
                npc.StateFrame = 0
            end
        else
            npc.State = 0
            npc.StateFrame = 0
        end

        npc.StateFrame = npc.StateFrame + 1
    end
end
brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.Necromancer, GENESIS_ENTITIES.NECROMANCER)

---------------
--HOLY CLOTTY--
---------------

function brEnms:HolyClotty(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.HOLY_CLOTTY then
        local npcsprite = npc:GetSprite()
        if npc.State == 8 and npcsprite:GetFrame() == 11 then
            local r = math.random() * 7 - 3
            local offset = 8
            local absOffset = 6
            SFXManager():Play(SoundEffect.SOUND_CHILD_HAPPY_ROAR_SHORT, 1, 0, false, 2)
            local tear1 =
                Isaac.Spawn(
                EntityType.ENTITY_PROJECTILE,
                2,
                0,
                npc.Position + Vector(0, -offset + absOffset),
                Vector(r, -7),
                nil
            )
            local tear2 =
                Isaac.Spawn(
                EntityType.ENTITY_PROJECTILE,
                2,
                0,
                npc.Position + Vector(0, offset + absOffset),
                Vector(-r, 7),
                nil
            )
            local tear3 =
                Isaac.Spawn(
                EntityType.ENTITY_PROJECTILE,
                2,
                0,
                npc.Position + Vector(offset, absOffset),
                Vector(7, r),
                nil
            )
            local tear4 =
                Isaac.Spawn(
                EntityType.ENTITY_PROJECTILE,
                2,
                0,
                npc.Position + Vector(-offset, absOffset),
                Vector(-7, -r),
                nil
            )
            tear1:SetColor(Color(1, 1, 1, 1, 255, 255, 255), 99999, 1, false, false)
            tear2:SetColor(Color(1, 1, 1, 1, 255, 255, 255), 99999, 1, false, false)
            tear3:SetColor(Color(1, 1, 1, 1, 255, 255, 255), 99999, 1, false, false)
            tear4:SetColor(Color(1, 1, 1, 1, 255, 255, 255), 99999, 1, false, false)
        end

        for i, e in ipairs(Genesis.RoomProjectiles) do
            if
                e.Variant == 0 and e.SpawnerVariant == GENESIS_ENTITIES.VARIANT.HOLY_CLOTTY and
                    e.SpawnerType == EntityType.ENTITY_CLOTTY
             then
                e:Remove()
            end
        end
    end
end
brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.HolyClotty, EntityType.ENTITY_CLOTTY)

---------------
--BREEDER FLY--
---------------

function brEnms:BreederFly(npc)
    if npc.Variant == GENESIS_ENTITIES.VARIANT.BREEDER_FLY then
        local npcsprite = npc:GetSprite()
        local data = npc:GetData()
        if npc.FrameCount % 60 == 0 and math.random() > 0.6 then
            npc.State = NpcState.STATE_SUMMON
            data.summonFrames = -1
            npcsprite:Play("Summon", true)
        end
        if npc.State == NpcState.STATE_SUMMON then
            npc.Velocity = Genesis.VEC_ZERO
            data.summonFrames = data.summonFrames + 1
            if not npcsprite:IsPlaying("Summon") then
                npcsprite:SetAnimation("Summon")
                npcsprite:SetFrame("Summon", data.summonFrames)
            end
            if npcsprite:IsEventTriggered("Spawn") then
                Isaac.Spawn(EntityType.ENTITY_ATTACKFLY, 0, 0, npc.Position, Genesis.VEC_ZERO, nil)
            elseif data.summonFrames > 9 then
                npc.State = NpcState.STATE_MOVE
            end
        end

        if npc.HitPoints <= npc.MaxHitPoints / 3 and not npc:GetData().spawn then
            npc:Morph(EntityType.ENTITY_FLY, 0, 0, -1)
            Isaac.Spawn(EntityType.ENTITY_BOIL, 2, 0, npc.Position, Genesis.VEC_ZERO, nil)
        end
    end
end
brEnms:AddCallback(ModCallbacks.MC_NPC_UPDATE, brEnms.BreederFly, EntityType.ENTITY_SUCKER)

-------------
--DR EMBYRO--
-------------

local EmbryoMod = RegisterMod("DrEmbryo", 1)

function EmbryoMod:OnUpdate()
    local entities = Isaac.GetRoomEntities()
    for i = 1, #entities do
        local e = entities[i]
        if e.Type == 77 and e:GetData().Calculated ~= true then
            e:GetData().Calculated = true
            if math.random(1, 8) == 7 then
                Isaac.Spawn(
                    GENESIS_ENTITIES.DR_EMBRYO,
                    0,
                    0,
                    e.Position - Vector(math.random(-10, 10), math.random(-10, 10)),
                    Genesis.VEC_ZERO,
                    nil
                )
                e:Remove()
            end
        end
    end
end

function EmbryoMod:OnEmbryo(entity)
    local data = entity:GetData()
    local sprite = entity:GetSprite()
    local target = entity:GetPlayerTarget()
    local angle = (target.Position - entity.Position):GetAngleDegrees()
    if sprite:IsEventTriggered("Landed") then
        SFXManager():Play(SoundEffect.SOUND_GOOATTACH0, 1, 0, false, 1)
    end
    if entity.FrameCount <= 1 then
        entity.State = 0
        data.State0Frames = 0
    end
    if entity.State == 0 then
        entity:AnimWalkFrame("Jump", "Jump", 0.5)
        if data.State0Frames ~= nil then
            data.State0Frames = data.State0Frames + 1
        end
        if entity.StateFrame < 10 then
            entity.Pathfinder:MoveRandomly(false)
            entity.Velocity = entity.Velocity:Normalized() * 2.5
        else
            entity.Velocity = Vector(0, 0)
        end
        if data.State0Frames % 23 == 0 then
            data.State0Frames = 0
            if math.random(1, 3) == 2 then
                entity.State = 2
            else
                entity.State = 3
            end
        end
    elseif entity.State == 2 then
        sprite:Play("Puke", false)
        entity.Velocity = Vector(0, 0)
        if sprite:IsEventTriggered("Puke") then
            Isaac.Spawn(
                EntityType.ENTITY_BOMBDROP,
                BombVariant.BOMB_NORMAL,
                BombSubType.BOMB_NORMAL,
                entity.Position,
                Vector.FromAngle(1 * angle):Resized(50),
                entity
            )
        end
        if sprite:IsFinished("Puke") then
            entity.State = 3
        end
    elseif entity.State == 3 then
        sprite:Play("Reload", false)
        entity.Velocity = Vector(0, 0)
        if sprite:IsFinished("Reload") then
            entity.State = 0
        end
    else
        entity.State = 0
    end
end

EmbryoMod:AddCallback(ModCallbacks.MC_NPC_UPDATE, EmbryoMod.OnEmbryo, GENESIS_ENTITIES.DR_EMBRYO)
EmbryoMod:AddCallback(ModCallbacks.MC_POST_UPDATE, EmbryoMod.OnUpdate)

-------------
--Holy Baby--
-------------
local HolyBabyMod = RegisterMod("HolyBaby", 1)

function HolyBabyMod:HolyProjectile(proj)
    if proj.SpawnerType == EntityType.ENTITY_BABY and proj.SpawnerVariant == GENESIS_ENTITIES.VARIANT.HOLY_BABY then
        Isaac.Spawn(
            EntityType.ENTITY_EFFECT,
            EffectVariant.CRACK_THE_SKY,
            0,
            proj.Position,
            Vector(0, 0),
            proj.SpawnerEntity
        )
    end
end

HolyBabyMod:AddCallback(ModCallbacks.MC_POST_ENTITY_REMOVE, HolyBabyMod.HolyProjectile, EntityType.ENTITY_PROJECTILE)

function HolyBabyMod:Update(Baby)
    local ent = Isaac.GetRoomEntities()

    for i = 1, #ent do
        if
            ent[i].Type == EntityType.ENTITY_PROJECTILE and ent[i].SpawnerType == EntityType.ENTITY_BABY and
                ent[i].SpawnerVariant == GENESIS_ENTITIES.VARIANT.HOLY_BABY and
                ent[i].FrameCount == 1
         then
            ent[i]:GetSprite():Load("gfx/holytears.anm2", true)
        end
    end
end

HolyBabyMod:AddCallback(ModCallbacks.MC_NPC_UPDATE, HolyBabyMod.Update, EntityType.ENTITY_BABY)

-----------
--SPIDERS--
-----------

function miscEnms:OnSpidersUpdate()
    local entities = Genesis.getRoomEnemies()
    for i = 1, #entities do
        if
            entities[i].Type == GENESIS_ENTITIES.CUSTOM_SPIDERS or
                entities[i].Type == GENESIS_ENTITIES.VARIANT.SPLIT_SPIDER
         then
            if entities[i].Variant == GENESIS_ENTITIES.VARIANT.SUCK_SPIDER then
                if math.random(0, 10) < 3 then
                    Isaac.Spawn(
                        EntityType.ENTITY_EFFECT,
                        EffectVariant.CREEP_RED,
                        0,
                        entities[i].Position,
                        Vector(0, 0),
                        entities[i]
                    ):SetSize(0.3, Vector(0.3, 0.3), 6)
                end
            end
            if entities[i].Variant == GENESIS_ENTITIES.VARIANT.SPLIT_SPIDER then
                if math.random(0, 10) < 3 then
                    Isaac.Spawn(
                        EntityType.ENTITY_EFFECT,
                        EffectVariant.CREEP_GREEN,
                        0,
                        entities[i].Position,
                        Vector(0, 0),
                        entities[i]
                    ):SetSize(0.3, Vector(0.3, 0.3), 6)
                end
            end
        end
    end
end

function miscEnms:SpidersTakeDamage(entity, amount, flag, source, countdownFrames)
    if entity.Type == GENESIS_ENTITIES.CUSTOM_SPIDERS and entity.Variant == GENESIS_ENTITIES.VARIANT.SPLIT_SPIDER then
        --    Isaac.DebugString(tostring(source.Type))
        if source.Type == 250 then
            return false
        end
        if entity.HitPoints <= amount and entity.SubType == 0 then
            --      Isaac.DebugString("It splits")
            local e1 =
                Isaac.Spawn(
                GENESIS_ENTITIES.CUSTOM_SPIDERS,
                GENESIS_ENTITIES.VARIANT.SPLIT_SPIDER,
                1,
                entity.Position,
                Vector(0.2, 0),
                entity
            )
            e1:ToNPC().Scale = 0.5
            e1:ToNPC().HitPoints = 6
            local e2 =
                Isaac.Spawn(
                GENESIS_ENTITIES.CUSTOM_SPIDERS,
                GENESIS_ENTITIES.VARIANT.SPLIT_SPIDER,
                1,
                entity.Position,
                Vector(-0.2, 0),
                entity
            )
            e2:ToNPC().Scale = 0.5
            e2:ToNPC().HitPoints = 6
        end
    end
    -- There is also no way to distinguish things like Krampus' Head lasers and player Brimstone lasers
end
miscEnms:AddCallback(ModCallbacks.MC_POST_UPDATE, miscEnms.OnSpidersUpdate)
miscEnms:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, miscEnms.SpidersTakeDamage)

------------------
--Shitty Charger--
------------------

local shitty_charger_mod = RegisterMod("Shitty Charger", 1)

--spawn shitty charger
function shitty_charger_mod:Update()
    local room = Game():GetRoom()

    for i = 1, room:GetGridSize() do
        if room:GetGridEntity(i) ~= nil then
            local entity = room:GetGridEntity(i)
            if
                entity:ToPoop() and room:GetGridCollision(i) == GridCollisionClass.COLLISION_NONE and
                    entity.VarData ~= 1000
             then
                entity.VarData = 1000
                if math.random(100) >= 85 then
                    Isaac.Spawn(
                        EntityType.ENTITY_CHARGER,
                        GENESIS_ENTITIES.VARIANT.SHITTY_CHARGER,
                        0,
                        room:GetGridPosition(i),
                        Vector(0, 0),
                        nil
                    )
                end
            end
        end
    end
end

--update charger
function shitty_charger_mod:UpdateCharger(charger)
    if charger.Variant == GENESIS_ENTITIES.VARIANT.SHITTY_CHARGER and charger:IsDead() and math.random(100) >= 60 then
        Isaac.Spawn(EntityType.ENTITY_DIP, 0, 0, charger.Position, Vector(0, 0), charger)
    end
end

--Callbacks
shitty_charger_mod:AddCallback(ModCallbacks.MC_POST_UPDATE, shitty_charger_mod.Update)
shitty_charger_mod:AddCallback(ModCallbacks.MC_NPC_UPDATE, shitty_charger_mod.UpdateCharger, EntityType.ENTITY_CHARGER)

------------
--Rock Fly--
------------
local rock_fly_mod = RegisterMod("Rock Fly", 1)

--spawn rock fly
function rock_fly_mod:Update()
    for k, entity in pairs(Isaac.GetRoomEntities()) do
        if
            entity.Type == EntityType.ENTITY_EFFECT and entity.Variant == EffectVariant.ROCK_PARTICLE and
                entity.FrameCount == 1 and
                math.random(100) >= 97
         then
            Isaac.Spawn(
                EntityType.ENTITY_ATTACKFLY,
                GENESIS_ENTITIES.VARIANT.ROCK_FLY,
                0,
                entity.Position,
                entity.Velocity,
                entity
            )
            entity:Remove()
        end
    end
end

--Callbacks
rock_fly_mod:AddCallback(ModCallbacks.MC_POST_UPDATE, rock_fly_mod.Update)

Isaac.DebugString("Genesis+: Loaded Enemies!")
error({ hack = true })
