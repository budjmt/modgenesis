---------
--Cards--
---------

--for some reason Genesis.getRoomEntities didn't work in this file

local Cards = RegisterMod("Cards",1)
Cards.CardChance = 15 --percent now
Cards.CustomCards = {
	GENESIS_CARDS.ACE_OF_RICHES,
	GENESIS_CARDS.LUST,
	GENESIS_CARDS.RIFFLE,
	GENESIS_CARDS.TEIWAZ,
	GENESIS_CARDS.PERPS,
	GENESIS_CARDS.DIVERSITY,
	GENESIS_CARDS.KING_OF_HEARTS,
	GENESIS_CARDS.DAMAGER
}
Cards.CardPoolAll = {
  "ACE_OF_RICHES",
  "LUST",
  "LUST",
  "RIFFLE",
  "RIFFLE",
  "RIFFLE",
  "DIVERSITY",
  "DIVERSITY",
  "DIVERSITY",
  "KING_OF_HEARTS",
  "KING_OF_HEARTS",
  "KING_OF_HEARTS",
  "PERPS",
  "TEIWAZ",
  "SAINT",
  "SAINT",
  "HERO",
  "HERO",
  "CRUCIFIX",
  "CRUCIFIX",
  "SALVATION",
  "SALVATION",
  "HUMBLE",
  "HUMBLE",
  "GALAXY",
  "GALAXY",
  "WISE",
  "WISE",
  "RIVER",
  "RIVER",
  "DAMAGER",
  "DAMAGER",
  "PAIN",
  "PAIN"
}
Cards.CardPoolRune = {
  "PERPS",
  "TEIWAZ"
}
Cards.CardPoolCard = {
  "ACE_OF_RICHES",
  "LUST",
  "LUST",
  "RIFFLE",
  "RIFFLE",
  "RIFFLE",
  "DIVERSITY",
  "DIVERSITY",
  "DIVERSITY",
  "KING_OF_HEARTS",
  "KING_OF_HEARTS",
  "KING_OF_HEARTS",
  "SAINT",
  "SAINT",
  "HERO",
  "HERO",
  "CRUCIFIX",
  "CRUCIFIX",
  "SALVATION",
  "SALVATION",
  "HUMBLE",
  "HUMBLE",
  "GALAXY",
  "GALAXY",
  "WISE",
  "WISE",
  "RIVER",
  "RIVER",
  "DAMAGER",
  "DAMAGER",
  "PAIN",
  "PAIN"
}
Cards.HasCardSpawned = false
Cards.HasCardMorphed = false
Cards.HasDeckOfCards = false
Cards.HasPolydactyly = false
Cards.HasBox = false
Cards.HasChaos = false
-- Giant Book Overlay by the AlphaBirth team
local playGiantbookOverlay
do
	local sprite
	local anim
	playGiantbookOverlay = function(filename, shake)
		filename = "gfx/ui/giantbook/"..filename
		sprite = Sprite()
		sprite:Load("gfx/ui/giantbook/animation_giantbook.anm2", true)
		sprite:ReplaceSpritesheet(0, filename)
		sprite:LoadGraphics()
		if shake then
			anim = "Shake"
		else
			anim = "Appear"
		end
		sprite:Play(anim, true)
	end
	local function render()
		if not sprite then return end
		local pos = getScreenCenterPosition()
		sprite:Render(pos, Vector(0, 0), Vector(0, 0))
	end
	Cards:AddCallback(ModCallbacks.MC_POST_RENDER, render)
	local function update()
		if not sprite then return end
		sprite:Update()
		if sprite:IsFinished(anim) then
			sprite = nil
		end
	end
	Cards:AddCallback(ModCallbacks.MC_POST_UPDATE, update)
end

-- Changes the rune's sprite when on the ground
function Cards:UpdateRune()
	for i,e in ipairs(Genesis.RoomPickups) do
		if e.Variant == PickupVariant.PICKUP_TAROTCARD and e.SubType == GENESIS_CARDS.TEIWAZ then
			local sprite = e:GetSprite()
				if sprite:GetFilename() ~= "gfx/animations/pickups/custom_rune.anm2" then
					sprite:Load("gfx/animations/pickups/custom_rune.anm2", true)
					if e.FrameCount >= 0 then
          sprite:Play("Appear", true)
					end
				end
		elseif e.Variant == PickupVariant.PICKUP_TAROTCARD and e.SubType == GENESIS_CARDS.PERPS then
			local sprite = e:GetSprite()
				if sprite:GetFilename() ~= "gfx/animations/pickups/custom_rune2.anm2" then
					sprite:Load("gfx/animations/pickups/custom_rune2.anm2", true)
					if e.FrameCount == 1 then
						sprite:Play("Idle", true)
					else
						sprite:Play("Appear", true)
					end
				end
		end
	end
end

--ACE OF RICHES
function Cards:UseAceOfRiches()
  local ents = Genesis.getRoomEntities()
  local player = Isaac.GetPlayer(0)
  for i,e in ipairs(ents) do
    if e:IsVulnerableEnemy() and not e:IsBoss() then
      Isaac.Spawn(EntityType.ENTITY_PICKUP, math.random(1,4)*10, 1, e.Position, e.Velocity, player)
      Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.POOF01 , 0, e.Position, Vector(0,0), player)
      e:Kill()
    end
  end
end

--RIFFLE
function Cards:UseRiffle()
  local player = Isaac.GetPlayer(0)
  local Permutations = {
  {2, 1, 3},
  {2, 3, 1},
  {3, 1, 2},
  }
  local PreviousPickups = {
  player:GetNumCoins(),
  player:GetNumBombs(),
  player:GetNumKeys()
  }
  local NewPermutation = Permutations[math.random(1,3)]
  player:AddCoins(PreviousPickups[NewPermutation[1]] - PreviousPickups[1])
  player:AddBombs(PreviousPickups[NewPermutation[2]] - PreviousPickups[2])
  player:AddKeys(PreviousPickups[NewPermutation[3]] - PreviousPickups[3])
  Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.IMPACT 	, 0, player.Position, Vector(0,0), player)
end

--KING OF HEARTS
function Cards:UseKing()
  local ents = Genesis.getRoomEntities()
  for i,e in ipairs(ents) do
    if e:IsVulnerableEnemy() or e:IsEnemy() or e:IsActiveEnemy(false) then
      if e:ToNPC():IsChampion() then
		Isaac.Spawn(e.Type, e.Variant, e.SubType, e.Position, e.Velocity, e)
        --e:ToNPC():Morph(e.Type, e.Variant, e.SubType, -1) --this wont work for some reason
		e:Remove()
      end
    end
  end
end

--DIVERSITY
function Cards:UseDiversity()
  local player = Isaac.GetPlayer(0)
  local Slot = Isaac.Spawn(EntityType.ENTITY_SLOT, 10, 0, Game():GetRoom():FindFreeTilePosition(player.Position - Vector(40,0), 0), Vector(0,0), player)
  Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.POOF01, 0, Slot.Position, Vector(0,0), player)
end

--LUST
function Cards:UseLust()
  local ents = Genesis.getRoomEntities()
  for i,e in ipairs(ents) do
    Isaac.DebugString(tostring(e:IsVulnerableEnemy())..";"..tostring(e:IsBoss()))
    if e:IsVulnerableEnemy() and not e:IsBoss() then
      e:AddCharmed(100000)
      Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.POOF01, 0, e.Position, Vector(0,0), Isaac.GetPlayer(0))
    end
  end
end

--PERPS
function Cards:UsePerps()
  local player = Isaac.GetPlayer(0)
  local LuckMod = math.floor((player.Luck + 1) * 5)
  for var=1,LuckMod,1 do
    Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, 0, Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 1, false), Vector(0,0), player)
  end
  playGiantbookOverlay("sheet_effect_teiwaz.png", false)
end

--TEIWAZ
function Cards:UseTeiwaz()
  local player = Isaac.GetPlayer(0)
  player:AddBlackHearts(2)
  player:AddSoulHearts(2)
  player:AddMaxHearts(2, true)
  player:AddHearts(2)
  playGiantbookOverlay("sheet_effect_teiwaz.png", false)
end

-- THE SAINT
function Cards:onSaint()
  local player = Isaac.GetPlayer(0)
  local bonusroom = Game():GetLevel():GetRoomByIdx(-1)
  if bonusroom and bonusroom.Data and bonusroom.Data.Type == 14 then--if devil room has already been spawned
	  player:AnimateTeleport(true)
	  Isaac.ExecuteCommand("goto s.angel")
  else
	  Game():GetLevel():InitializeDevilAngelRoom(true, false)
	  player:UseCard(31)
  end
end

-- CRUCIFIX
function Cards:onCrucifix()
  local player = Isaac.GetPlayer(0)
  player:GetEffects():AddCollectibleEffect(CollectibleType.COLLECTIBLE_BIBLE,true)
end

-- SALVATION
function Cards:onSalvation()
  local player = Isaac.GetPlayer(0)
	local entities = Isaac.GetRoomEntities()
	for i=1, #entities do
		if entities[i].Variant == PickupVariant.PICKUP_HEART then
			if entities[i].SubType == 1 or entities[i].SubType == 2 or entities[i].SubType == 5 or entities[i].SubType == 9 then
				normalheart = entities[i]
				local newsoulhearts = 2
				if entities[i].SubType == 2 then
					newsoulhearts = 1
				end
				if entities[i].SubType == 5 then
					newsoulhearts = 4
				end
				local spawnoffset = Vector(0,0)
				for j = 1, newsoulhearts do
					local newheart = Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_HEART,HeartSubType.HEART_SOUL,normalheart.Position+spawnoffset,Vector(0,0),nil)
					spawnoffset.X = spawnoffset.X + 0.01
				end
				normalheart:Remove()
			end
		end
	end
end

-- HUMBLE
function Cards:onHumble()
  local player = Isaac.GetPlayer(0)
  local room = Game():GetRoom()
  local enemycount = room:GetAliveEnemiesCount()
  if room:IsClear() then
      if enemycount == 0 then
         room:SpawnClearAward()
       end
   end
end

-- GALAXY
function Cards:onGalaxy()
  local player = Isaac.GetPlayer(0)
    local room = Game():GetRoom()
	if room:GetType() == RoomType.ROOM_BLACK_MARKET then
		--if in black market do nothing
		Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.FART, 0, player.Position, Vector(0,0), player)
	elseif room:GetType() == RoomType.ROOM_DUNGEON then
		--if already in crawlspace open way to black market
		for i = 1, room:GetGridSize() do
			local gridEntity = room:GetGridEntity(i)
			if 	gridEntity and
				gridEntity.Desc.Type == GridEntityType.GRID_WALL and
				(i == 58 or i == 59 or i == 73 or i == 74) then
				gridEntity:SetType(GridEntityType.GRID_GRAVITY)
			end
		end
		Game():GetLevel():ChangeRoom(Game():GetLevel():GetCurrentRoomIndex())
	else
		--spawn ladder to crawlspace
		local gridIndex = room:GetGridIndex(player.Position)
		room:SpawnGridEntity(gridIndex, GridEntityType.GRID_STAIRS, 0, 0, 0)
	end
end

-- HERO
function Cards:onHero()
  local player = Isaac.GetPlayer(0)
     shopkeeper = Isaac.Spawn(17,480,0,Vector(320,250),Vector(0,0),nil)
     chest = Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_LOCKEDCHEST,0,Vector(shopkeeper.Position.X,shopkeeper.Position.Y+60),Vector(0,0),nil)
     heart = Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_HEART,0,Vector(shopkeeper.Position.X+20,shopkeeper.Position.Y+60),Vector(0,0),nil)
     Isaac.Spawn(EntityType.ENTITY_PICKUP,PickupVariant.PICKUP_KEY,0,Vector(shopkeeper.Position.X,shopkeeper.Position.Y+80),Vector(0,0),nil)
      if chest.SubType == ChestSubType.CHEST_OPENED then
        chest:GetData().IsCardChest = true
        if chest:GetData().IsCardChest == true then
          shopkeeper:Remove()
        end
      end
end

-- WISE ONE
function Cards:onWise()
	local player = Isaac.GetPlayer(0)
	Cards.chance = math.random(11);
	if Cards.chance == 1 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BOOK_OF_SHADOWS, true, false, false, false)
	elseif Cards.chance == 2 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_ANARCHIST_COOKBOOK, true, false, false, false)
	elseif Cards.chance == 3 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BOOK_OF_BELIAL, true, false, false, false)
	elseif Cards.chance == 4 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_TELEPATHY_BOOK, true, false, false, false)
	elseif Cards.chance == 5 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BOOK_REVELATIONS, true, false, false, false)
	elseif Cards.chance == 6 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_SATANIC_BIBLE, true, false, false, false)
	elseif Cards.chance == 7 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_HOW_TO_JUMP, true, false, false, false)
	elseif Cards.chance == 8 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_NECRONOMICON, true, false, false, false)
	elseif Cards.chance == 9 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BIBLE, true, false, false, false)
	elseif Cards.chance == 10 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_MONSTER_MANUAL, true, false, false, false)
	elseif Cards.chance == 11 then
		player:UseActiveItem (CollectibleType.COLLECTIBLE_BOOK_OF_SECRETS, true, false, false, false)
	end
end

-- RIVER
function Cards:onRiver()
  local player = Isaac.GetPlayer(0)
    player:UseActiveItem(CollectibleType.COLLECTIBLE_FLUSH,false,false,false,false)
end

-- PAINFUL
Cards.PainfulActivated = -1
function Cards:onPainful()
	if Cards.PainfulActivated < 0 then --load value
		if Genesis.moddata.PainfulActivations ~= nil then
			Cards.PainfulActivated = Genesis.moddata.PainfulActivations
		else
			Cards.PainfulActivated = 0
			Genesis.moddata.PainfulActivations = Cards.PainfulActivated
		end
	end
	local player = Isaac.GetPlayer(0)
    player:TakeDamage(2, DamageFlag.DAMAGE_RED_HEARTS, EntityRef(player), 0)
	Cards.PainfulActivated = Cards.PainfulActivated + 1
	Genesis.moddata.PainfulActivations = Cards.PainfulActivated
    player:SetColor(Color(12.9,0.3,0.3,1,1,0,0),99999,1000,false,false)
	player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
	player:EvaluateItems()
end
function Cards:PainfulonCache(player, cacheFlag)
	if cacheFlag == CacheFlag.CACHE_DAMAGE and Cards.PainfulActivated > 0 then
		player.Damage = player.Damage * (2^Cards.PainfulActivated)
	end
end
Cards:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, Cards.PainfulonCache)
function Cards:PainfulOnNewFloor()
	Cards.PainfulActivated = 0
	Genesis.moddata.PainfulActivations = Cards.PainfulActivated
end
Cards:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, Cards.PainfulOnNewFloor)

-- DAMAGER
Cards.DamagerActivated = false
function Cards:onDamager()
	Cards.DamagerActivated = true
end
function Cards:DamagerOnNewRoom()
	Cards.DamagerActivated = false
end
Cards:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, Cards.DamagerOnNewRoom)
function Cards:damagerRedirect(dmg_target, dmg_amount, dmg_flags, dmg_source, dmg_frames)
	if Cards.DamagerActivated and dmg_amount > 0 then
	  local player = dmg_target:ToPlayer()
	  for i, Entity in pairs(Isaac.GetRoomEntities()) do
		if Entity:IsVulnerableEnemy() and Entity.Type > 9 and Entity.Type ~= 1000 then
		  Entity:TakeDamage(player.Damage,0,EntityRef(player),0)
		end
	  end
	end
end
Cards:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, Cards.damagerRedirect, EntityType.ENTITY_PLAYER);

--new callback, to add cusotm cards to pools
--return the card you need
function Cards:GetCard(rng, CurrentCard, Playing, Runes, OnlyRunes)
  local pool = Cards.CardPoolAll
  if OnlyRunes then pool = Cards.CardPoolRune end

  local chance = Cards.CardChance/#Cards.CardPoolAll*#pool --make the card chance proportional to the pool, so that it decreases if there are more cards than vanilla to keep it about the same overall
  if rng:RandomInt(40) <= chance then
    return GENESIS_CARDS[pool[1+rng:RandomInt(#pool)]]
  else
    return CurrentCard
  end
end

Cards:AddCallback(ModCallbacks.MC_GET_CARD, Cards.GetCard)
Cards:AddCallback(ModCallbacks.MC_POST_UPDATE, Cards.UpdateRune)
--Cards:AddCallback(ModCallbacks.MC_POST_UPDATE, Cards.SpawningFunction)
--Cards:AddCallback(ModCallbacks.MC_POST_RENDER, Cards.ActiveItemsSpawningFunction)
Cards:AddCallback(ModCallbacks.MC_USE_CARD, Cards.UseAceOfRiches, GENESIS_CARDS.ACE_OF_RICHES)
Cards:AddCallback(ModCallbacks.MC_USE_CARD, Cards.UseLust, GENESIS_CARDS.LUST)
Cards:AddCallback(ModCallbacks.MC_USE_CARD, Cards.UseRiffle, GENESIS_CARDS.RIFFLE)
Cards:AddCallback(ModCallbacks.MC_USE_CARD, Cards.UseTeiwaz, GENESIS_CARDS.TEIWAZ)
Cards:AddCallback(ModCallbacks.MC_USE_CARD, Cards.UsePerps, GENESIS_CARDS.PERPS)
Cards:AddCallback(ModCallbacks.MC_USE_CARD, Cards.UseDiversity, GENESIS_CARDS.DIVERSITY)
Cards:AddCallback(ModCallbacks.MC_USE_CARD, Cards.UseKing, GENESIS_CARDS.KING_OF_HEARTS)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onSaint, GENESIS_CARDS.SAINT)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onHero, GENESIS_CARDS.HERO)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onCrucifix, GENESIS_CARDS.CRUCIFIX)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onSalvation, GENESIS_CARDS.SALVATION)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onHumble, GENESIS_CARDS.HUMBLE)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onGalaxy, GENESIS_CARDS.GALAXY)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onWise, GENESIS_CARDS.WISE)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onRiver, GENESIS_CARDS.RIVER)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onPainful, GENESIS_CARDS.PAIN)
Cards:AddCallback( ModCallbacks.MC_USE_CARD, Cards.onDamager, GENESIS_CARDS.DAMAGER)

Isaac.DebugString("Genesis+: Loaded Cards!")
error({ hack = true })