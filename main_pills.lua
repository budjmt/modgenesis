local Pill = RegisterMod("Pills",1)

  --Satan Loves You (One black heart and 0% angel chance)--
function Pill:PillEffect1()
    local player = Isaac.GetPlayer(0)
    Game():GetLevel():AddAngelRoomChance(-100)
    player:AddBlackHearts(2)
end

  --Jesus Loves You (One soul heart and 100% angel chance)--
function Pill:PillEffect2()
    local player = Isaac.GetPlayer(0)
    Game():GetLevel():AddAngelRoomChance(100)
    player:AddSoulHearts(2)
end

  --Coins are Keys--
function Pill:PillEffect3()
    local player = Isaac.GetPlayer(0)
    local coins = player:GetNumCoins()
    local keys = player:GetNumKeys()
    player:AddKeys(-keys)
    player:AddCoins(-coins)
    player:AddKeys(coins)
	player:AddCoins(keys)
end

  --Bombs are Coins--
function Pill:PillEffect4()
    local player = Isaac.GetPlayer(0)
    local bombs = player:GetNumBombs()
    local coins = player:GetNumCoins()
    player:AddCoins(-coins)
    player:AddBombs(-bombs)
	player:AddCoins(bombs)
	player:AddBombs(coins)
 end
  
  
--Damage Down--
function Pill:PillEffect6()
  local player = Isaac.GetPlayer(0)
  Genesis.moddata.statsup.DAMAGE = Genesis.moddata.statsup.DAMAGE - 1
  player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
  player:EvaluateItems()
end

--Damage Up--
function Pill:PillEffect7()
  local player = Isaac.GetPlayer(0)
  Genesis.moddata.statsup.DAMAGE = Genesis.moddata.statsup.DAMAGE + 1
  player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
  player:EvaluateItems()
end

--I know everything!--
function Pill:PillEffect9()
  local player = Isaac.GetPlayer(0)
  local level = Game():GetLevel()
  level:ApplyMapEffect()
  level:ApplyBlueMapEffect()
  level:ApplyCompassEffect()
end

--Callbacks
Pill:AddCallback(ModCallbacks.MC_USE_PILL,Pill.PillEffect1,GENESIS_PILLS.HELLPILL)
Pill:AddCallback(ModCallbacks.MC_USE_PILL,Pill.PillEffect2,GENESIS_PILLS.GODPILL)
Pill:AddCallback(ModCallbacks.MC_USE_PILL,Pill.PillEffect3,GENESIS_PILLS.KEYPILL)
Pill:AddCallback(ModCallbacks.MC_USE_PILL,Pill.PillEffect4,GENESIS_PILLS.BOMBPILL)
Pill:AddCallback(ModCallbacks.MC_USE_PILL,Pill.PillEffect6,GENESIS_PILLS.DAMAGEDOWN)
Pill:AddCallback(ModCallbacks.MC_USE_PILL,Pill.PillEffect7,GENESIS_PILLS.DAMAGEPILL)
Pill:AddCallback(ModCallbacks.MC_USE_PILL,Pill.PillEffect9,GENESIS_PILLS.MINDPILL)

Isaac.DebugString("Genesis+: Loaded Pills!")
error({ hack = true })