------------
--TRINKETS--
------------

local Trinkets = RegisterMod("Genesis Trinkets",1)

--Trinkets--
	local BannedCards = {nil, Card.CARD_HIEROPHANT, Card.CARD_LOVERS, Card.CARD_JUSTICE, Card.CARD_STRENGTH, Card.CARD_SUN, Card.CARD_HEARTS_2, Card.CARD_ACE_OF_HEARTS, Card.RUNE_DAGAZ}
	local TrinketVars = {
		CardAdd = false,
		HeldCard = nil,
		Current_Keys2 = 0,
		Previous_Keys2 = 0,
		CanOpen = true
	}

--Main update function--
function Trinkets:Update()
	local player = Isaac.GetPlayer(0)
	local room = Game():GetRoom()
	--Bloody Card--
	if player:GetCard(0) ~= Card.CARD_NULL then
		TrinketVars.HeldCard = player:GetCard(0)
	end

	if player:HasTrinket(GENESIS_ITEMS.TRINKET.BLOODY_CARD) and player:GetLastActionTriggers() == ActionTriggers.ACTIONTRIGGER_CARDPILLUSED then
		TrinketVars.CardAdd = true
		for i = 1, #BannedCards do
			if TrinketVars.HeldCard == BannedCards[i] then
				TrinketVars.CardAdd = false
			end
		end
	end
    if TrinketVars.CardAdd and player:GetHearts() + player:GetSoulHearts() > 3 then
		player:AddCard(TrinketVars.HeldCard)
		player:TakeDamage(2, DamageFlag.DAMAGE_RED_HEARTS, EntityRef(player), 1)
		TrinketVars.CardAdd = false
	end

	--LOCKPICKS--
	if player:HasTrinket(GENESIS_ITEMS.TRINKET.LOCKPICKS) then
		if Genesis.moddata.UsedLockpick then
			TrinketVars.CanOpen = false
		end
		TrinketVars.Current_Keys2 = player:GetNumKeys()
		Current_Level2 = Game():GetLevel():GetStage()
		if Current_Level ~= Previous_Level then
			TrinketVars.CanOpen = true
			Genesis.moddata.UsedLockpick = false
			Previous_Level = Game():GetLevel():GetStage()
		end
		if TrinketVars.Current_Keys2 < TrinketVars.Previous_Keys2 and TrinketVars.CanOpen then
			player:AddKeys(1)
			TrinketVars.CanOpen = false
			Genesis.moddata.UsedLockpick = true
		end
		if TrinketVars.CanOpen and player:GetNumKeys() == 0 and TrinketVars.Previous_Keys2 == 0 then
			for i = 0, 9 do
				local door = Game():GetRoom():GetDoor(i)
				if door ~= nil then
					if door:IsLocked() and (door.Position - player.Position):Length() < 50 then
						player:AddKeys(1)
						TrinketVars.CanOpen = false
						Genesis.moddata.UsedLockpick = true
					end
				end
			end
		end
		TrinketVars.Previous_Keys2 = TrinketVars.Current_Keys2
  end

	--Glowing Rock--

	if player:HasTrinket(GENESIS_ITEMS.TRINKET.GLOWING_ROCK) and room:GetFrameCount() <= 0  and room:IsFirstVisit() then
		local c = 0
		for i = 1, Game():GetRoom():GetGridSize() do
			local grid = Game():GetRoom():GetGridEntity(i)
			if grid ~= nil and grid:ToRock() ~= nil and math.random(1, 400) == 1 and c == 0 then
				-- room:RemoveGridEntity(i, 0, false)
				Isaac.GridSpawn(GridEntityType.GRID_ROCKT, 0, grid.Position, true)--grid:SetType(GridEntityType.GRID_ROCKT)

				grid:SetType(GridEntityType.GRID_ROCKT)
				grid:Update()
				grid:Init(math.random(0,1000000))
				c = 1
				--grid.Sprite():Update()
			end
		end
	end
	--Lost CD--
	if player:HasTrinket(GENESIS_ITEMS.TRINKET.LOST_CD) and math.random (1,160) == 1 and player:GetFireDirection() > -1 then
		FireTetrachromacyBurst(player,1)
	end
end

--Main on entity damage function--
function Trinkets:TakeDmg(target, amount, flag, soruce, num)
	local player = Isaac.GetPlayer(0)
	--Stamp--
	if target.Type == EntityType.ENTITY_PLAYER and player:HasTrinket(GENESIS_ITEMS.TRINKET.STAMP) and math.random(1, 12) == 1 then
		local entities = _G.Genesis.getRoomEntities()
		for i = 1, #entities do
			local e = entities[i]
			if e:IsEnemy() then
				e:AddEntityFlags(EntityFlag.FLAG_SLOW)
			end
		end
	end
end

--Reset Function--
function Trinkets:Reset()
	--Bloody Card--
	TrinketVars.CardAdd = false
	TrinketVars.HeldCard = nil
	--LOCKPICKSs--
	TrinketVars.Current_Keys2 = 0
	TrinketVars.Previous_Keys2 = 0
	Current_Level = 0
	Previous_Level = 0
	TrinketVars.CanOpen = true
end

--Callbacks--
Trinkets:AddCallback(ModCallbacks.MC_POST_RENDER, Trinkets.Update)
Trinkets:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, Trinkets.TakeDmg)
Trinkets:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, Trinkets.Reset)

------------
--DYSLEXIA--
------------

--Setting up General Variables--
local Dyslexia = RegisterMod("Dyslexia", 1)
local DyslexiaVars = {
	DislexiaPickupEntity = 5,
	DislexiaPickupVariant = GENESIS_ENTITIES.VARIANT.DYSLEXIA_PICKUP,
	DislexiaHeartEntity = 5,
	DislexiaHeartVariant = GENESIS_ENTITIES.VARIANT.DYSLEXIA_HEART
}

--Main Function--
function Dyslexia:Main ()
	local player = Isaac.GetPlayer(0)
	local entities = _G.Genesis.getRoomEntities()
	for i = 1, #entities do
		local e = entities[i]
		--Check for pickups in the room, and change them into Dyslexia ones--
		if player:HasCollectible(GENESIS_ITEMS.PASSIVE.DYSLEXIA) then

      -- DYSLEXIA COINS --
      if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_COIN and e:ToPickup():IsShopItem() == false and (e.SubType == CoinSubType.COIN_PENNY or e.SubType == CoinSubType.COIN_LUCKYPENNY) then
        e:ToPickup():Morph(DyslexiaVars.DislexiaPickupEntity, DyslexiaVars.DislexiaPickupVariant, 1, true)
        if e:GetSprite():IsPlaying("Appear") then
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_DROP, 1, 0, false, 1)
        end
      end

      if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_COIN and e:ToPickup():IsShopItem() == false and e.SubType == CoinSubType.COIN_DOUBLEPACK then
        e:ToPickup():Morph(DyslexiaVars.DislexiaPickupEntity, DyslexiaVars.DislexiaPickupVariant, 2, true)
        if e:GetSprite():IsPlaying("Appear") then
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_DROP, 1, 0, false, 2)
        end
      end

      -- DYSLEXIA KEYS --
      if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_KEY and e:ToPickup():IsShopItem() == false and (e.SubType == KeySubType.KEY_NORMAL or e.SubType == KeySubType.KEY_CHARGED) then
        e:ToPickup():Morph(DyslexiaVars.DislexiaPickupEntity, DyslexiaVars.DislexiaPickupVariant, 1, true)
        if e:GetSprite():IsPlaying("Appear") then
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_DROP, 1, 0, false, 1)
        end
      end

      if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_KEY and e:ToPickup():IsShopItem() == false and e.SubType == KeySubType.KEY_DOUBLEPACK then
        e:ToPickup():Morph(DyslexiaVars.DislexiaPickupEntity, DyslexiaVars.DislexiaPickupVariant, 2, true)
        if e:GetSprite():IsPlaying("Appear") then
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_DROP, 1, 0, false, 1)
        end
      end

      -- DYSLEXIA BOMBS --
      if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_BOMB and e.SubType == BombSubType.BOMB_NORMAL and e:ToPickup():IsShopItem() == false then
        e:ToPickup():Morph(DyslexiaVars.DislexiaPickupEntity, DyslexiaVars.DislexiaPickupVariant, 1, true)
        if e:GetSprite():IsPlaying("Appear") then
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_DROP, 1, 0, false, 1)
        end
      end

      if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_BOMB and e.SubType == BombSubType.BOMB_DOUBLEPACK and e:ToPickup():IsShopItem() == false then
        e:ToPickup():Morph(DyslexiaVars.DislexiaPickupEntity, DyslexiaVars.DislexiaPickupVariant, 2, true)
        if e:GetSprite():IsPlaying("Appear") then
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_DROP, 1, 0, false, 1)
        end
      end

      -- DYSLEXIA HEARTS --
      if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_HEART and e.SubType ~= HeartSubType.HEART_DOUBLEPACK and e.SubType ~= HeartSubType.HEART_GOLDEN and e.SubType ~= HeartSubType.HEART_ETERNAL and e:ToPickup():IsShopItem() == false then
        e:ToPickup():Morph(DyslexiaVars.DislexiaHeartEntity, DyslexiaVars.DislexiaHeartVariant, 3, true)
        if e:GetSprite():IsPlaying("Appear") then
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_HEART_DROP, 1, 0, false, 1)
        end
      end
      if e.Type == EntityType.ENTITY_PICKUP and e.Variant == PickupVariant.PICKUP_HEART and e.SubType == HeartSubType.HEART_DOUBLEPACK and e:ToPickup():IsShopItem() == false then
        e:ToPickup():Morph(DyslexiaVars.DislexiaHeartEntity, DyslexiaVars.DislexiaHeartVariant, 3, true)
        if e:GetSprite():IsPlaying("Appear") then
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_HEART_DROP, 1, 0, false, 1)
        end
      end
		end

		--On pickup of a Dyslexia collectible--
		--On pickup of a Dyslexia Pickup--
		if e.Type == DyslexiaVars.DislexiaPickupEntity and e.Variant == DyslexiaVars.DislexiaPickupVariant and e.SubType < 3 then
			if e.Position:Distance(player.Position) < 24 then
        e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
			--Play animation--
        local Sprite = e:GetSprite()
				if Sprite:IsPlaying("Idle") then
          Sprite:Play("Collect", true)
          --Add pickups--
          local roll = math.random (1,3)
          local c = e.SubType
          if roll == 1 then
            player:AddCoins(c)
          elseif roll == 2 then
            player:AddKeys(c)
          elseif roll == 3 then
            player:AddBombs(c)
          end
          --Play Sound--
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_PICKUP, 1, 0, false, 1)
				end
			end
			--Remove the entity--
			if e:GetSprite():IsFinished("Collect") then
        e:Remove()
			end
		end

    if e.Type == DyslexiaVars.DislexiaHeartEntity and e.Variant == DyslexiaVars.DislexiaHeartVariant and e.SubType == 3 then
      local Sprite = e:GetSprite()
			if e.Position:Distance(player.Position) < 24 then
        e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
			--Play animation--
				if Sprite:IsPlaying("Idle") then
          Sprite:Play("Collect", true)
          --Add pickups--
          local roll = math.random (1,5)
          local c = e.SubType

          if roll <= 3 then
            player:AddHealth(2)
          elseif roll == 4 then
            player:AddSoulHearts(2)
          elseif roll == 5 then
            player:AddBlackHearts(2)
          end
          --Play Sound--
          SFXManager():Play(GENESIS_SFX.DYSLEXIA_HEART_PICKUP, 1, 0, false, 1)
				end
			end
			--Remove the entity--
			if e:GetSprite():IsFinished("Collect") then
        e:Remove()
			end
		end
	end
end

--Callbacks--
Dyslexia:AddCallback(ModCallbacks.MC_POST_RENDER, Dyslexia.Main)

---------
--TOWEL--
---------
local TowelMod = RegisterMod("Towel",1)
local TowelVars = {
	TowelNumTearsSucked = 0,
	UsedTowel = false,
	TowelTearSpeed = 10
}

function TowelMod:OnPlayerInit()
	TowelVars.TowelNumTearsSucked = 0
	TowelVars.UsedTowel = false
end

function TowelMod:SpawnTear()
	local player = Isaac.GetPlayer(0)
	local tear = player:FireTear(player.Position, Vector(math.random(-TowelVars.TowelTearSpeed,TowelVars.TowelTearSpeed), math.random(-TowelVars.TowelTearSpeed,TowelVars.TowelTearSpeed)), false, true, false)
	tear:ToTear().FallingAcceleration = 1
	tear:ToTear().FallingSpeed = math.random(-20,-5)
	tear:GetData().IsShotTear = true
end

function TowelMod:OnUseTowel()
	TowelVars.UsedTowel = true
	TowelVars.TowelNumTearsSucked = 0
	local entities = _G.Genesis.getRoomEntities()
	for i=1,#entities do
		if entities[i].Type == EntityType.ENTITY_PROJECTILE and entities[i]:GetData().IsShotTear ~= true then
			entities[i]:Die()
			TowelVars.TowelNumTearsSucked = TowelVars.TowelNumTearsSucked + 1
		end
	end
	return true
end

function TowelMod:ResetTowel()
	if TowelVars.UsedTowel == true then
		for var = 1, TowelVars.TowelNumTearsSucked, 1 do
			TowelMod:SpawnTear()
		end
		TowelVars.UsedTowel = false
	end
end

TowelMod:AddCallback(ModCallbacks.MC_USE_ITEM, TowelMod.OnUseTowel, GENESIS_ITEMS.ACTIVE.TOWEL)
TowelMod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, TowelMod.OnPlayerInit)
TowelMod:AddCallback(ModCallbacks.MC_POST_UPDATE, TowelMod.ResetTowel)

------------
--SOUL KEY--
------------
--TRINKET---
------------

--Setting up general variables--
local SoulKey = RegisterMod("Soul Key", 1)
local SoulkeyVars = {
	Current_Keys = 0,
	Previous_Keys = 0
}

--Soul Key Trinket Logic--
function SoulKey:KeyLogic()
	local player = Isaac.GetPlayer(0)
	--Check if player has the trinket--
	if player:HasTrinket(GENESIS_ITEMS.TRINKET.SOUL_KEY) then
		SoulkeyVars.Current_Keys = player:GetNumKeys()
		--Check if player's current key number is lower than the numer he had before the update and roll a probability--
		if SoulkeyVars.Current_Keys < SoulkeyVars.Previous_Keys and math.random(1,20) == 1 then
			--Spawn the heart-
			Isaac.Spawn(5, 10, 3, Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 1, true), Vector(0, 0), nil)
		end
		--Get the number of keys the player has at the end of the update--
		SoulkeyVars.Previous_Keys = player:GetNumKeys()
		--Checks if player wants to use a key on a chest, removes a soul heart and gives player a key if player doesn't have keys--
		local entities = _G.Genesis.getRoomEntities()
		for i = 1, #entities do
			local e = entities[i]
			if e.Type == EntityType.ENTITY_PICKUP and (e.Variant == PickupVariant.PICKUP_LOCKEDCHEST or e.Variant == PickupVariant.PICKUP_ETERNALCHEST) and e.SubType ~= ChestSubType.CHEST_OPENED  and  e.Position:Distance(player.Position) < 20 and  player:GetNumKeys() == 0 and player:GetSoulHearts() >= 2 and player:GetBlackHearts() == 0  then
				player:AddKeys(1)
				player:AddSoulHearts(-2)
			end
		end
		--Checks if the player wants to use a key on a room, removes a soul heart and gives player a key if player doesn't have any--
		local doorID = {Game():GetRoom():GetDoor(DoorSlot.RIGHT0), Game():GetRoom():GetDoor(DoorSlot.RIGHT1), Game():GetRoom():GetDoor(DoorSlot.UP0), Game():GetRoom():GetDoor(DoorSlot.UP1), Game():GetRoom():GetDoor(DoorSlot.DOWN0), Game():GetRoom():GetDoor(DoorSlot.DOWN1), Game():GetRoom():GetDoor(DoorSlot.LEFT0), Game():GetRoom():GetDoor(DoorSlot.LEFT1)}
		for i = 1, 8 do
			if doorID[i] ~= nil and player:GetNumKeys() == 0 and player:GetSoulHearts() >= 2 and player:GetBlackHearts() == 0 and doorID[i].Position:Distance(player.Position) < 40 and doorID[i]:IsLocked() then
				player:AddKeys(1)
				player:AddSoulHearts(-2)
			end
		end
	end
end

--Callbacks--
SoulKey:AddCallback(ModCallbacks.MC_POST_RENDER, SoulKey.KeyLogic)

-----------
--Deja Vu--
-----------

local DejaVuMod = RegisterMod('DejaVu',1)

function DejaVuMod:CacheUpdate(player,cacheFlag)
    if cacheFlag == CacheFlag.CACHE_DAMAGE then
	    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.DEJA_VU) then
			if Game():GetRoom():IsFirstVisit() then
				player.Damage = player.Damage * 2
			end
		end
	end
end

function DejaVuMod:ActivateDejaVu()
    for i = 0, 3 do
        local player = Isaac.GetPlayer(i)
	    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.DEJA_VU) then
            player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
            player:EvaluateItems()
        end
	end
end

DejaVuMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, DejaVuMod.CacheUpdate)
DejaVuMod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, DejaVuMod.ActivateDejaVu)
-------------
--Chameleon--
--Supposed to prevent you from taking damage twice from an enemy in one room, but changes after you take damage from a new enemy (You take damage from a Gaper, if you take damage from it again, it's blocked. If you then take damage from a fly, the next time you take damage from a fly it's blocked, but not from a gaper)
--------------------------------------------------------------------------------
local ChameleonMod = RegisterMod("Chameleon", 1)
local ChameleonVars = {
	ChameleonHits = 0,
	EntityIsBoss = false,
	marker = Sprite()
	}
	ChameleonVars.marker:Load("gfx/shield_marker.anm2", true)
	ChameleonVars.marker:Play("Idle", true)

local function IsEntityBoss(eType,eVariant)
	local player = Isaac.GetPlayer(0)
	local DummyBoss = Isaac.Spawn(eType, eVariant, 0, player.Position, Vector(0,0), player)
	if DummyBoss:IsBoss() then
		DummyBoss:Remove()
		ChameleonVars.EntityIsBoss = true
	elseif DummyBoss:IsBoss() == false then
		DummyBoss:Remove()
		ChameleonVars.EntityIsBoss = false
	end
end

function ChameleonMod:OnInit()
	ChameleonEnemy = {Type = nil, Variant = nil}
	ChameleonVars.ChameleonHits = 0
	ChameleonVars.EntityIsBoss = false
end

function ChameleonMod:OnCacheUpdate(player, cacheFlag)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.CHAMELEON) then
		if cacheFlag == CacheFlag.CACHE_SPEED then
			player.MoveSpeed = player.MoveSpeed + 0.3
		end
	end
end

function ChameleonMod:ResetEnemy()
	local player = Isaac.GetPlayer(0)
	if Game():GetRoom():GetFrameCount() <= 1 then
		ChameleonEnemy = {Type = nil, Variant = nil}
		ChameleonVars.ChameleonHits = 0
	end
end

function ChameleonMod:SpawnMarker()
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.CHAMELEON) then
		local entities = _G.Genesis.getRoomEntities()
		for i=1,#entities do
			if entities[i].Type == ChameleonEnemy.Type and entities[i].Variant == ChameleonEnemy.Variant then
				ChameleonVars.marker:Update()
				ChameleonVars.marker:LoadGraphics()
				ChameleonVars.marker:Render(Game():GetRoom():WorldToScreenPosition(entities[i].Position), Vector(0,0), Vector(0,0))
			end
		end
	end
end

function ChameleonMod:OnTakeDamage(target, amount, flag, source, num)
  local player = Isaac.GetPlayer(0)
  if player:HasCollectible(GENESIS_ITEMS.PASSIVE.CHAMELEON) and (source.Entity:IsEnemy() or source.Entity.Type == EntityType.ENTITY_PROJECTILE or source.Entity.Type == EntityType.ENTITY_LASER) then
    if target.Type == EntityType.ENTITY_PLAYER and source.Entity:IsEnemy() == false then
      if ChameleonEnemy.Type ~= source.Entity.SpawnerType and ChameleonEnemy.Variant ~= source.Entity.SpawnerVariant then
      IsEntityBoss(source.Entity.SpawnerType, source.Entity.SpawnerVariant)
        if ChameleonVars.EntityIsBoss == false then
          ChameleonEnemy.Type = source.Entity.SpawnerType
          ChameleonEnemy.Variant = source.Entity.SpawnerVariant
          ChameleonVars.ChameleonHits = 0
        end
      end
		end
		if target.Type == EntityType.ENTITY_PLAYER and source.Entity:IsEnemy() and source.Entity:IsBoss() == false then
			if ChameleonEnemy.Type ~= source.Entity.Type and ChameleonEnemy.Variant ~= source.Entity.Variant then
				ChameleonEnemy.Type = source.Entity.Type
                ChameleonEnemy.Variant = source.Entity.Variant
				ChameleonVars.ChameleonHits = 0
			end
		end
        if target.Type == EntityType.ENTITY_PLAYER and not source.Entity:IsEnemy() then
           	if ChameleonEnemy.Type == source.Entity.SpawnerType and ChameleonEnemy.Variant == source.Entity.SpawnerVariant then
				ChameleonVars.ChameleonHits = ChameleonVars.ChameleonHits + 1
				if ChameleonVars.ChameleonHits > 1 then
					return false
				end
			end
		end
        if target.Type == EntityType.ENTITY_PLAYER and source.Entity:IsEnemy() then
			if ChameleonEnemy.Type == source.Entity.Type and ChameleonEnemy.Variant == source.Entity.Variant then
				ChameleonVars.ChameleonHits = ChameleonVars.ChameleonHits + 1
				if ChameleonVars.ChameleonHits > 1 then
					return false
				end
			end
		end
	end
end

ChameleonMod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, ChameleonMod.OnCacheUpdate)
ChameleonMod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, ChameleonMod.OnInit)
ChameleonMod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, ChameleonMod.OnTakeDamage)
ChameleonMod:AddCallback(ModCallbacks.MC_POST_UPDATE, ChameleonMod.ResetEnemy)
ChameleonMod:AddCallback(ModCallbacks.MC_POST_RENDER, ChameleonMod.SpawnMarker)

--------
--DASH--
--------

local dash = RegisterMod("Dash", 1);
dash.dashing = -1
dash.speed = 4

function dash:use_dash()
  local game = Game()
  local room = game:GetRoom()
  local player = Isaac.GetPlayer(0)
  local sprite = player:GetSprite()
	dash.dashing = 30
	player:AddNullCostume(GENESIS_COSTUMES.DASH)
  player.ControlsEnabled = false
  player:AddVelocity(player:GetMovementInput():Resized(dash.speed));
  local rockid = room:GetGridIndex(player.Position+ player.Velocity)
  for i=rockid-1,rockid+1 do --destroy rocks in next player positio, and left and right of it
    local rock = room:GetGridEntity(i)
    if rock ~= nil then
      if rock:ToRock() or rock:ToPoop() then
        rock:Destroy(true)
      end
    end
  end
  local w = room:GetGridWidth()
  for i=rockid-w,rockid+w,2*w do --destroy rocks above and below
    local rock = room:GetGridEntity(i)
    if rock ~= nil then
      if rock:ToRock() then
        rock:Destroy(true)
      end
    end
  end
  if player:HasCollectible(118) then
    player:FireBrimstone (Vector(0,6))
    player:FireBrimstone (Vector(6,0))
    player:FireBrimstone (Vector(-6,0))
    player:FireBrimstone (Vector(0,-6))
  elseif player:HasCollectible(52) then
    player:FireBomb(player.Position,Vector(6,0))
    player:FireBomb(player.Position,Vector(-6,0))
    player:FireBomb(player.Position,Vector(0,6))
    player:FireBomb(player.Position,Vector(0,-6))
  elseif player:HasCollectible(395) then
    player:FireTechXLaser(player.Position, Vector (6,0), 60)
    player:FireTechXLaser(player.Position, Vector (-6,0), 60)
    player:FireTechXLaser(player.Position, Vector (0,6), 60)
    player:FireTechXLaser(player.Position, Vector (0,-6), 60)
  elseif player:HasCollectible(68) then
    player:FireTechLaser(player.Position, 0, Vector(0,6), true, true)
    player:FireTechLaser(player.Position, 0, Vector(0,-6), true, true)
    player:FireTechLaser(player.Position, 0, Vector(6,0), true, true)
    player:FireTechLaser(player.Position, 0, Vector(-6,0), true, true)
  elseif player:HasCollectible(CollectibleType.COLLECTIBLE_EPIC_FETUS) then
    Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ROCKET, 0, Vector(125,0), player.Position, player)
    Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ROCKET, 0, Vector(0,125), player.Position, player)
    Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ROCKET, 0, Vector(-125,0), player.Position, player)
    Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ROCKET, 0, Vector(0,-125), player.Position, player)
  else
    local vec = Vector(0,7) --Could rewrite the synergies this way too, but I just don't have time, next time make it actually work from the start -Filloax
    for i=0,3 do
      player:FireTear (player.Position, vec:Rotated(90*i), true, true, false)
    end
  end
end

function dash:Update()
  local player = Isaac.GetPlayer(0)

  if player:HasCollectible(GENESIS_ITEMS.ACTIVE.DASH) then
    local game = Game()
    local room = game:GetRoom()
    local level = game:GetLevel()
    local gridid = room:GetGridIndex(player.Position+ player.Velocity)
    local grid = room:GetGridEntity(gridid)

    if dash.dashing==0 or (grid and not (grid:ToRock() or grid:ToPoop() or grid:ToSpikes())) then
      player.ControlsEnabled = true
      dash.dashing = -1
      player:TryRemoveNullCostume(GENESIS_COSTUMES.DASH)
    elseif dash.dashing > 0 then
      dash.dashing = dash.dashing-1
      player.Velocity = player.Velocity:Resized(dash.speed)
      for i=gridid-1,gridid+1 do --destroy rocks in next player positio, and left and right of it
        local rock = room:GetGridEntity(i)
        if rock ~= nil then
          if rock:ToRock() or rock:ToPoop() then
            rock:Destroy(true)
          end
        end
      end
      local w = room:GetGridWidth()
      for i=gridid-w,gridid+w,2*w do --destroy rocks above and below
        local rock = room:GetGridEntity(i)
        if rock ~= nil then
          if rock:ToRock() then
            rock:Destroy(true)
          end
        end
      end

      for i=1, #Genesis.RoomEffects do
        if Genesis.RoomEffects[i].Variant == EffectVariant.ROCKET then
          if Genesis.RoomEffects[i].FrameCount == 1 then
            Isaac.Explode(Genesis.RoomEffects[i].Position, player, player.Damage*2)
            Genesis.RoomEffects[i]:Remove()
          end
        end
      end
    end
  end
end

function dash:TakeDamage()
  if dash.dashing > 0 then return false end
end

dash:AddCallback( ModCallbacks.MC_USE_ITEM, dash.use_dash, GENESIS_ITEMS.ACTIVE.DASH )
dash:AddCallback( ModCallbacks.MC_POST_UPDATE, dash.Update)
dash:AddCallback( ModCallbacks.MC_ENTITY_TAKE_DMG, dash.TakeDamage, EntityType.ENTITY_PLAYER)

-------------------
--Book of Genesis--
-------------------

local GenesisBook = RegisterMod("BookofGenesis", 1)

function GenesisBook:onUse(rng)
  local player = Game():GetPlayer(0)
  local level = Game():GetLevel()

  level:SetStage(LevelStage.STAGE1_1, math.random(0,2))
  Game():StartStageTransition(true, 1)
  Game():GetSeeds():SetStartSeed("")
  player:RemoveCollectible(GENESIS_ITEMS.ACTIVE.BOOK_OF_GENESIS)
  return true
end

GenesisBook:AddCallback(ModCallbacks.MC_USE_ITEM, GenesisBook.onUse, GENESIS_ITEMS.ACTIVE.BOOK_OF_GENESIS)

----------------------
--Broken Golden Calf--
----------------------

local calf = RegisterMod("GoldenCalf", 1)

local goldcalf = {
  id = Isaac.GetTrinketIdByName("Broken Golden Calf"),
  has = false
}

function calf:onUpdate()
  local player = Game():GetPlayer(0)
  local level = Game():GetLevel()

  if Game():GetFrameCount() == 1 then
    --Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TRINKET, goldcalf.id, Vector(280,300), Vector(0,0), nil)
    goldcalf.has = false
  end

  if player:HasTrinket(goldcalf.id) then
    goldcalf.has = true
    if player:GetName() ~= "Lucifer" then
      level:AddAngelRoomChance(100)
    end
  end
  if player:HasTrinket(goldcalf.id) == false and goldcalf.has == true then
    level:AddAngelRoomChance(-level:GetAngelRoomChance())
    goldcalf.has = false
  end
end

calf:AddCallback(ModCallbacks.MC_POST_UPDATE, calf.onUpdate)

--------
--Time--
--------

local BrokenClock = RegisterMod("Time", 1)

local TimeTrink = {
  id = Isaac.GetTrinketIdByName("Broken Clock"),
  rush = false
}

function BrokenClock:onupdate()
  local player = Game():GetPlayer(0)
  local room = Game():GetRoom()

  if Game():GetFrameCount() == 1 then
    --Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TRINKET, TimeTrink.id, Vector(360, 300), Vector(0, 0), nil)
    -- Game().TimeCounter = 300000
    TimeTrink.rush = false
  end

  if player:HasTrinket(TimeTrink.id) then
    if room:GetType() == RoomType.ROOM_BOSS and room:GetAliveEnemiesCount() == 0 and TimeTrink.rush == false then
      room:TrySpawnBossRushDoor(true)
      TimeTrink.rush = true
    end
  end

end

BrokenClock:AddCallback(ModCallbacks.MC_POST_UPDATE, BrokenClock.onupdate)


--------------
--Broken Map--
--------------

local BMap = RegisterMod("BrokenMap", 1)

local BrknMap = {
  id = Isaac.GetTrinketIdByName("Broken Map"),
  has = false
}

function BMap:onUpd()
  local player = Game():GetPlayer(0)
  local room = Game():GetRoom()
  local effects = player:GetEffects()
  local level = Game():GetLevel()

  if Game():GetFrameCount() == 1 then
    --Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TRINKET, BrknMap.id, Vector(320, 300), Vector(0, 0), nil)
    BrknMap.has = false
  end

  if player:HasTrinket(BrknMap.id) and room:GetAliveEnemiesCount() > 0 then
    level:RemoveCurse(LevelCurse.CURSE_OF_THE_LOST)
    level:ApplyCompassEffect(false)
    level:ApplyMapEffect()
  end
  if player:HasTrinket(BrknMap.id) and room:GetAliveEnemiesCount() == 0 then
    level:AddCurse(LevelCurse.CURSE_OF_THE_LOST, false)
    BrknMap.has = true
  end
  if player:HasTrinket(BrknMap.id) == false and BrknMap.has == true then
    level:AddCurse(LevelCurse.CURSE_OF_THE_LOST, false)
    BrknMap.has = false
  end
end

BMap:AddCallback(ModCallbacks.MC_POST_UPDATE, BMap.onUpd)

-----------------
--Tetrachromacy--
-----------------

local Eye = RegisterMod("Eye", 1)

local eyeItem = {
  id = Isaac.GetItemIdByName("Tetrachromacy"),
  fire = false,
  c = false,
  costume = Isaac.GetCostumeIdByPath("gfx/characters/EyeCostume.anm2"),
  has = false,
  knife = false,
  frame = 0,
  lud = false,
  ludframe = 0,
  brimlud = false,
  brimludframe = 0
}

function Eye:onUpdate()
  local player = Game():GetPlayer(0)
  local ent = _G.Genesis.getRoomEntities()

  if Game():GetFrameCount() == 1 then
    --Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, eyeItem.id, Vector(320, 340), Vector(0, 0), nil)
    eyeItem.fire = false
    eyeItem.c = false
    eyeItem.has = false
    eyeItem.knife = false
    eyeItem.frame = 0
    eyeItem.lud = false
    eyeItem.ludframe = 0
    eyeItem.brimlud = false
    eyeItem.brimludframe = 0
  end

  if player:HasCollectible(eyeItem.id) then
    player:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
    player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
    player:EvaluateItems()

    if eyeItem.has == false then
      player:AddNullCostume(eyeItem.costume)
      eyeItem.has = true
      eyeItem.knife = false
    end


	--FireTetrachromacyBurst(player,shottype) to let other items cause tetra shots
	--shottype is number from 1 to 10
    if eyeItem.fire == true and eyeItem.c == false then
      if player:HasWeaponType(WeaponType.WEAPON_TEARS) and player:HasCollectible(CollectibleType.COLLECTIBLE_TRACTOR_BEAM) == false then
        for i = 1, #ent do
          if ent[i].Type == EntityType.ENTITY_TEAR and ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete == nil then
            ent[i]:Remove()
          end
        end
		FireTetrachromacyBurst(player,1)
		eyeItem.c = true
      end

      if player:HasWeaponType(WeaponType.WEAPON_TEARS) and player:HasCollectible(CollectibleType.COLLECTIBLE_TRACTOR_BEAM) then
		for i = 1, #ent do
          if ent[i].Type == EntityType.ENTITY_TEAR and ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete == nil then
            ent[i]:Remove()
          end
        end
		FireTetrachromacyBurst(player,2)
		eyeItem.c = true
      end

      if player:HasWeaponType(WeaponType.WEAPON_LASER) then
		for i = 1, #ent do
          if ent[i].Type == EntityType.ENTITY_LASER and ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete == nil then
            ent[i]:Remove()
          end
        end
        FireTetrachromacyBurst(player,3)
		eyeItem.c = true
      end

      if player:HasWeaponType(WeaponType.WEAPON_BOMBS) then
		for i = 1, #ent do
          if ent[i].Type == EntityType.ENTITY_BOMBDROP and ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete == nil then
            ent[i]:Remove()
          end
        end
        FireTetrachromacyBurst(player,4)
		eyeItem.c = true
      end

      if player:HasWeaponType(WeaponType.WEAPON_ROCKETS) then
		for i = 1, #ent do
          if ent[i].Type == EntityType.ENTITY_EFFECT and ent[i].Variant == EffectVariant.BOMB_EXPLOSION and ent[i]:GetData().dontdelete == nil then
			FireTetrachromacyBurst(player,5)
            eyeItem.c = true
          end
        end
      end

      if player:HasWeaponType(WeaponType.WEAPON_TECH_X) then
		local radiusk = nil
		for i = 1, #ent do
          if (ent[i].Type == 7 and ent[i].Variant == 2 and ent[i].SubType == 2 and ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete == nil)
          or (ent[i].Type == 7 and ent[i].Variant == 1 and ent[i].SubType == 2 and ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete == nil) then
            enty = ent[i]:ToLaser()
            radiusk = enty.Radius
            ent[i]:Remove()
          end
        end
        if radiusk ~= nil then
		  FireTetrachromacyBurst(player,6)
		  eyeItem.c = true
        end
      end
    end

    if player:HasWeaponType(WeaponType.WEAPON_BRIMSTONE) and player:HasCollectible(CollectibleType.COLLECTIBLE_LUDOVICO_TECHNIQUE) == false then
      for i = 1, #ent do
        if ent[i].Type == EntityType.ENTITY_LASER and ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete == nil and ent[i].FrameCount < 5 then
          ent[i]:Remove()
		  FireTetrachromacyBurst(player,7)
		  break
        end
      end
    end

    if player:HasWeaponType(WeaponType.WEAPON_BRIMSTONE) and player:HasCollectible(CollectibleType.COLLECTIBLE_LUDOVICO_TECHNIQUE) then
      for i = 1, #ent do
        if Game():GetFrameCount() > eyeItem.brimludframe + player.MaxFireDelay then
          eyeItem.brimlud = false
        end
        if ent[i].Type == EntityType.ENTITY_LASER and ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete == nil and eyeItem.brimlud == false then
          -- ent[i]:Remove()
		  FireTetrachromacyBurst(player,8)
		  eyeItem.brimlud = true
          eyeItem.brimludframe = Game():GetFrameCount()
		  break
        end
      end
    end

    if player:HasWeaponType(WeaponType.WEAPON_KNIFE) then
      for i = 1, #ent do
        if ent[i].Type == EntityType.ENTITY_KNIFE then
          knife = ent[i]:ToKnife()
          if knife:IsFlying() == false and Game():GetFrameCount() > eyeItem.frame + player.MaxFireDelay then
            eyeItem.knife = false
          end
          if knife:IsFlying() == true and eyeItem.knife == false then
			FireTetrachromacyBurst(player,9)
            eyeItem.knife = true
            eyeItem.frame = Game():GetFrameCount()
          end
        end
      end
    end


    if player:HasWeaponType(WeaponType.WEAPON_LUDOVICO_TECHNIQUE) then
      for i = 1, #ent do
        if Game():GetFrameCount() > eyeItem.ludframe + player.MaxFireDelay then
          eyeItem.lud = false
        end
        if ent[i].Type == EntityType.ENTITY_TEAR and ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete == nil and eyeItem.lud == false then
          FireTetrachromacyBurst(player,10)
		  eyeItem.lud = true
          eyeItem.ludframe = Game():GetFrameCount()
        end
      end
    end

  end
    for i = 1, #ent do
      if ent[i].SpawnerType == EntityType.ENTITY_PLAYER and ent[i]:GetData().dontdelete then
        local ro = math.random(0, 255)
        local go = math.random(0, 255)
        local bo = math.random(0, 255)
        ent[i].Color = Color(1, 1, 1, 1, ro, go, bo)
      end
    end
end

Eye:AddCallback(ModCallbacks.MC_POST_UPDATE, Eye.onUpdate)

--fire tetrachromacy shot
function FireTetrachromacyBurst(player,shottype)
	if shottype < 1 or shottype > 10 then shottype = 1 end
	--different shot types
	if shottype == 1 then --tears
        player:FireTear(player.Position, Vector(player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(0, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(-player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(0, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true

        if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2) then
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(player.ShotSpeed*10, 0), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(player.ShotSpeed*10, player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(0, player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(-player.ShotSpeed*10, 0), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(0, -player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), false, false):GetData().dontdelete = true

          if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
          end

          if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 8), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(8, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -8), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-8, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 8), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(8, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -8), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-8, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true

            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(2, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-2, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(2, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-2, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
          end

          if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 6.5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(6.5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -6.5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-6.5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 6.5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(6.5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -6.5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-6.5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true

            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 0), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(0, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 0), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(0, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          end
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 5), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, -5), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 5), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, -5), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 8), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(8, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, -8), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-8, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 8), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(8, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, -8), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-8, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true

          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(2, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, -2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-2, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(2, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, -2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-2, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 6.5), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(6.5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, -6.5), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-6.5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 6.5), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(6.5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, -6.5), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-6.5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true

          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 0), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(0, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 0), false, true, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(0, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
        end
	end
	if shottype == 2 then --tractor beam
        player:FireTear(player.Position, Vector(player.ShotSpeed*10, 0), false, false, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(player.ShotSpeed*10, player.ShotSpeed*10), false, false, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(0, player.ShotSpeed*10), false, false, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(-player.ShotSpeed*10, 0), false, false, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), false, false, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(0, -player.ShotSpeed*10), false, false, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), false, false, false):GetData().dontdelete = true
        player:FireTear(player.Position, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), false, false, false):GetData().dontdelete = true

        if player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2) then
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(player.ShotSpeed*10, 0), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(player.ShotSpeed*10, player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(0, player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(-player.ShotSpeed*10, 0), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(0, -player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH2_OFFSET, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), false, false):GetData().dontdelete = true

          if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
          end

          if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 8), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(8, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -8), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-8, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 8), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(8, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -8), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-8, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true

            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(2, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-2, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(2, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-2, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
          end

          if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 6.5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(6.5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -6.5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-6.5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 6.5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(6.5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -6.5), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-6.5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true

            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 0), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(0, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 0), false, false):GetData().dontdelete = true
            player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(0, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          end
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 5), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(5, player.ShotSpeed*10+2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, -5), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-5, -player.ShotSpeed*10-2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 5), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(5, -player.ShotSpeed*10-2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, -5), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-5, player.ShotSpeed*10+2), false, false, false):GetData().dontdelete = true
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 8), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(8, player.ShotSpeed*10+2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, -8), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-8, -player.ShotSpeed*10-2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 8), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(8, -player.ShotSpeed*10-2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, -8), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-8, player.ShotSpeed*10+2), false, false, false):GetData().dontdelete = true

          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(2, player.ShotSpeed*10+2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, -2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-2, -player.ShotSpeed*10-2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(2, -player.ShotSpeed*10-2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, -2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-2, player.ShotSpeed*10+2), false, false, false):GetData().dontdelete = true
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 6.5), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(6.5, player.ShotSpeed*10+2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, -6.5), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-6.5, -player.ShotSpeed*10-2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 6.5), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(6.5, -player.ShotSpeed*10-2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, -6.5), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-6.5, player.ShotSpeed*10+2), false, false, false):GetData().dontdelete = true

          player:FireTear(player.Position, Vector(player.ShotSpeed*10+2, 0), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(0, player.ShotSpeed*10+2), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(-player.ShotSpeed*10-2, 0), false, false, false):GetData().dontdelete = true
          player:FireTear(player.Position, Vector(0, -player.ShotSpeed*10-2), false, false, false):GetData().dontdelete = true
        end
	end
	if shottype == 3 then --tech laser
        player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10, 0), false, false):GetData().dontdelete = true
        player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10, player.ShotSpeed*10), false, false):GetData().dontdelete = true
        player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(0, player.ShotSpeed*10), false, false):GetData().dontdelete = true
        player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10, 0), false, false):GetData().dontdelete = true
        player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), false, false):GetData().dontdelete = true
        player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(0, -player.ShotSpeed*10), false, false):GetData().dontdelete = true
        player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), false, false):GetData().dontdelete = true
        player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), false, false):GetData().dontdelete = true

        if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 5), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -5), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 5), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -5), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 8), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(8, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -8), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-8, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 8), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(8, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -8), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-8, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true

          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(2, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-2, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(2, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-2, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 6.5), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(6.5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, -6.5), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-6.5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 6.5), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(6.5, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, -6.5), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-6.5, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true

          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(player.ShotSpeed*10+2, 0), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(0, player.ShotSpeed*10+2), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(-player.ShotSpeed*10-2, 0), false, false):GetData().dontdelete = true
          player:FireTechLaser(player.Position, LaserOffset.LASER_TECH1_OFFSET, Vector(0, -player.ShotSpeed*10-2), false, false):GetData().dontdelete = true
        end
	end
	if shottype == 4 then --bombs
        player:FireBomb(player.Position, Vector(player.ShotSpeed*10, 0)):GetData().dontdelete = true
        player:FireBomb(player.Position, Vector(player.ShotSpeed*10, player.ShotSpeed*10)):GetData().dontdelete = true
        player:FireBomb(player.Position, Vector(0, player.ShotSpeed*10)):GetData().dontdelete = true
        player:FireBomb(player.Position, Vector(-player.ShotSpeed*10, 0)):GetData().dontdelete = true
        player:FireBomb(player.Position, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10)):GetData().dontdelete = true
        player:FireBomb(player.Position, Vector(0, -player.ShotSpeed*10)):GetData().dontdelete = true
        player:FireBomb(player.Position, Vector(-player.ShotSpeed*10, player.ShotSpeed*10)):GetData().dontdelete = true
        player:FireBomb(player.Position, Vector(player.ShotSpeed*10, -player.ShotSpeed*10)):GetData().dontdelete = true


        if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
          player:FireBomb(player.Position, Vector(player.ShotSpeed*10+2, 5)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(5, player.ShotSpeed*10+2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-player.ShotSpeed*10-2, -5)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-5, -player.ShotSpeed*10-2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-player.ShotSpeed*10-2, 5)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(5, -player.ShotSpeed*10-2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(player.ShotSpeed*10+2, -5)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-5, player.ShotSpeed*10+2)):GetData().dontdelete = true
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
          player:FireBomb(player.Position, Vector(player.ShotSpeed*10+2, 8)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(8, player.ShotSpeed*10+2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-player.ShotSpeed*10-2, -8)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-8, -player.ShotSpeed*10-2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-player.ShotSpeed*10-2, 8)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(8, -player.ShotSpeed*10-2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(player.ShotSpeed*10+2, -8)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-8, player.ShotSpeed*10+2)):GetData().dontdelete = true

          player:FireBomb(player.Position, Vector(player.ShotSpeed*10+2, 2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(2, player.ShotSpeed*10+2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-player.ShotSpeed*10-2, -2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-2, -player.ShotSpeed*10-2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-player.ShotSpeed*10-2, 2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(2, -player.ShotSpeed*10-2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(player.ShotSpeed*10+2, -2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-2, player.ShotSpeed*10+2)):GetData().dontdelete = true
        end

        if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
          player:FireBomb(player.Position, Vector(player.ShotSpeed*10+2, 6.5)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(6.5, player.ShotSpeed*10+2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-player.ShotSpeed*10-2, -6.5)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-6.5, -player.ShotSpeed*10-2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-player.ShotSpeed*10-2, 6.5)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(6.5, -player.ShotSpeed*10-2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(player.ShotSpeed*10+2, -6.5)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-6.5, player.ShotSpeed*10+2)):GetData().dontdelete = true

          player:FireBomb(player.Position, Vector(player.ShotSpeed*10+2, 0)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(0, player.ShotSpeed*10+2)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(-player.ShotSpeed*10-2, 0)):GetData().dontdelete = true
          player:FireBomb(player.Position, Vector(0, -player.ShotSpeed*10-2)):GetData().dontdelete = true
        end
	end
	if shottype == 5 then --rockets
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(0, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(0, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
	end
	if shottype == 6 then --tech X
	  player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10, 0), radiusk):GetData().dontdelete = true
	  player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10, player.ShotSpeed*10), radiusk):GetData().dontdelete = true
	  player:FireTechXLaser(player.Position, Vector(0, player.ShotSpeed*10), radiusk):GetData().dontdelete = true
	  player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10, 0), radiusk):GetData().dontdelete = true
	  player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), radiusk):GetData().dontdelete = true
	  player:FireTechXLaser(player.Position, Vector(0, -player.ShotSpeed*10), radiusk):GetData().dontdelete = true
	  player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), radiusk):GetData().dontdelete = true
	  player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), radiusk):GetData().dontdelete = true


	  if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
		player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10+2, 5), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(5, player.ShotSpeed*10+2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10-2, -5), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-5, -player.ShotSpeed*10-2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10-2, 5), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(5, -player.ShotSpeed*10-2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10+2, -5), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-5, player.ShotSpeed*10+2), radiusk):GetData().dontdelete = true
	  end

	  if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
		player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10+2, 8), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(8, player.ShotSpeed*10+2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10-2, -8), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-8, -player.ShotSpeed*10-2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10-2, 8), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(8, -player.ShotSpeed*10-2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10+2, -8), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-8, player.ShotSpeed*10+2), radiusk):GetData().dontdelete = true

		player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10+2, 2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(2, player.ShotSpeed*10+2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10-2, -2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-2, -player.ShotSpeed*10-2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10-2, 2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(2, -player.ShotSpeed*10-2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10+2, -2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-2, player.ShotSpeed*10+2), radiusk):GetData().dontdelete = true
	  end

	  if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
		player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10+2, 6.5), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(6.5, player.ShotSpeed*10+2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10-2, -6.5), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-6.5, -player.ShotSpeed*10-2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10-2, 6.5), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(6.5, -player.ShotSpeed*10-2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10+2, -6.5), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-6.5, player.ShotSpeed*10+2), radiusk):GetData().dontdelete = true

		player:FireTechXLaser(player.Position, Vector(player.ShotSpeed*10+2, 0), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(0, player.ShotSpeed*10+2), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(-player.ShotSpeed*10-2, 0), radiusk):GetData().dontdelete = true
		player:FireTechXLaser(player.Position, Vector(0, -player.ShotSpeed*10-2), radiusk):GetData().dontdelete = true
	  end
	end
	if shottype == 7 then --brimstone
		player:FireBrimstone(Vector(player.ShotSpeed*10, 0)):GetData().dontdelete = true
          player:FireBrimstone(Vector(player.ShotSpeed*10, player.ShotSpeed*10)):GetData().dontdelete = true
          player:FireBrimstone(Vector(0, player.ShotSpeed*10)):GetData().dontdelete = true
          player:FireBrimstone(Vector(-player.ShotSpeed*10, 0)):GetData().dontdelete = true
          player:FireBrimstone(Vector(-player.ShotSpeed*10, -player.ShotSpeed*10)):GetData().dontdelete = true
          player:FireBrimstone(Vector(0, -player.ShotSpeed*10)):GetData().dontdelete = true
          player:FireBrimstone(Vector(-player.ShotSpeed*10, player.ShotSpeed*10)):GetData().dontdelete = true
          player:FireBrimstone(Vector(player.ShotSpeed*10, -player.ShotSpeed*10)):GetData().dontdelete = true
          -- eyeItem.c = true

          if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
            player:FireBrimstone(Vector(player.ShotSpeed*10+2, 5)):GetData().dontdelete = true
            player:FireBrimstone(Vector(5, player.ShotSpeed*10+2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-player.ShotSpeed*10-2, -5)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-5, -player.ShotSpeed*10-2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-player.ShotSpeed*10-2, 5)):GetData().dontdelete = true
            player:FireBrimstone(Vector(5, -player.ShotSpeed*10-2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(player.ShotSpeed*10+2, -5)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-5, player.ShotSpeed*10+2)):GetData().dontdelete = true
          end

          if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
            player:FireBrimstone(Vector(player.ShotSpeed*10+2, 8)):GetData().dontdelete = true
            player:FireBrimstone(Vector(8, player.ShotSpeed*10+2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-player.ShotSpeed*10-2, -8)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-8, -player.ShotSpeed*10-2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-player.ShotSpeed*10-2, 8)):GetData().dontdelete = true
            player:FireBrimstone(Vector(8, -player.ShotSpeed*10-2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(player.ShotSpeed*10+2, -8)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-8, player.ShotSpeed*10+2)):GetData().dontdelete = true

            player:FireBrimstone(Vector(player.ShotSpeed*10+2, 2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(2, player.ShotSpeed*10+2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-player.ShotSpeed*10-2, -2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-2, -player.ShotSpeed*10-2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-player.ShotSpeed*10-2, 2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(2, -player.ShotSpeed*10-2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(player.ShotSpeed*10+2, -2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-2, player.ShotSpeed*10+2)):GetData().dontdelete = true
          end

          if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
            player:FireBrimstone(Vector(player.ShotSpeed*10+2, 6.5)):GetData().dontdelete = true
            player:FireBrimstone(Vector(6.5, player.ShotSpeed*10+2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-player.ShotSpeed*10-2, -6.5)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-6.5, -player.ShotSpeed*10-2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-player.ShotSpeed*10-2, 6.5)):GetData().dontdelete = true
            player:FireBrimstone(Vector(6.5, -player.ShotSpeed*10-2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(player.ShotSpeed*10+2, -6.5)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-6.5, player.ShotSpeed*10+2)):GetData().dontdelete = true

            player:FireBrimstone(Vector(player.ShotSpeed*10+2, 0)):GetData().dontdelete = true
            player:FireBrimstone(Vector(0, player.ShotSpeed*10+2)):GetData().dontdelete = true
            player:FireBrimstone(Vector(-player.ShotSpeed*10-2, 0)):GetData().dontdelete = true
            player:FireBrimstone(Vector(0, -player.ShotSpeed*10-2)):GetData().dontdelete = true
          end
	end
	if shottype == 8 then --brimstone+ludovico
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
          player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
          player:FireTear(ent[i].Position, Vector(0, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
          player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
          player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
          player:FireTear(ent[i].Position, Vector(0, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
          player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
          player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true


          if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
            player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 5), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, -5), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 5), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, -5), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
          end

          if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
            player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 8), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(8, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, -8), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-8, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 8), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(8, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, -8), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-8, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true

            player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(2, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, -2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-2, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(2, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, -2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-2, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
          end

          if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
            player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 6.5), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(6.5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, -6.5), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-6.5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 6.5), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(6.5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, -6.5), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-6.5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true

            player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 0), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(0, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 0), false, true, false):GetData().dontdelete = true
            player:FireTear(ent[i].Position, Vector(0, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
          end
	end
	if shottype == 9 then --knife
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(0, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(0, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
	end
	if shottype == 10 then --ludovico
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(0, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, 0), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(0, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10, player.ShotSpeed*10), false, true, false):GetData().dontdelete = true
		player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10, -player.ShotSpeed*10), false, true, false):GetData().dontdelete = true


		if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
			player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 5), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, -5), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 5), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, -5), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
		end

		if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
			player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 8), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(8, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, -8), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-8, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 8), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(8, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, -8), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-8, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true

			player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(2, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, -2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-2, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(2, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, -2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-2, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
		end

		if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
			player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 6.5), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(6.5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, -6.5), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-6.5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 6.5), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(6.5, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, -6.5), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-6.5, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true

			player:FireTear(ent[i].Position, Vector(player.ShotSpeed*10+2, 0), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(0, player.ShotSpeed*10+2), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(-player.ShotSpeed*10-2, 0), false, true, false):GetData().dontdelete = true
			player:FireTear(ent[i].Position, Vector(0, -player.ShotSpeed*10-2), false, true, false):GetData().dontdelete = true
		end
	end
end



function Eye:OnRender()
  local player = Game():GetPlayer(0)
  if player.FireDelay == player.MaxFireDelay then
    eyeItem.fire = true
    eyeItem.c = false
  end
  if player.FireDelay == 0 then
    eyeItem.fire = false
  end
end

Eye:AddCallback(ModCallbacks.MC_POST_RENDER, Eye.OnRender)

function Eye:cache(player, cacheflag)
    if cacheflag & (CacheFlag.CACHE_FIREDELAY | CacheFlag.CACHE_DAMAGE) == 0 then return end

    if not player:HasCollectible(eyeItem.id) then return end

    if cacheflag == CacheFlag.CACHE_FIREDELAY then
        player.MaxFireDelay = player.MaxFireDelay + 20

        if player:HasCollectible(CollectibleType.COLLECTIBLE_SOY_MILK) then
            player.MaxFireDelay = player.MaxFireDelay - 15
        end
    elseif cacheflag == CacheFlag.CACHE_DAMAGE then
        if player:HasCollectible(CollectibleType.COLLECTIBLE_SOY_MILK) then
            player.Damage = player.Damage * 0.5
        end
        if player:HasCollectible(CollectibleType.COLLECTIBLE_20_20) then
            player.Damage = player.Damage * 0.82
        end
        if player:HasCollectible(CollectibleType.COLLECTIBLE_MUTANT_SPIDER) then
            player.Damage = player.Damage * 0.6
        end
        if player:HasCollectible(CollectibleType.COLLECTIBLE_INNER_EYE) then
            player.Damage = player.Damage * 0.71
        end
    end
end

Eye:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, Eye.cache)



-------------
--Oven Mitt--
-------------
local Mitt = RegisterMod("OvenMitt", 1)

local OvenM = {
  id = Isaac.GetItemIdByName("Oven Mitt"),
  has = false
}

function Mitt:onUpd()
  local player = Game():GetPlayer(0)
  local ent = _G.Genesis.getRoomEntities()

  if Game():GetFrameCount() == 1 then
    OvenM.has = false
  end

  if player:HasCollectible(OvenM.id) then
    if OvenM.has == false then
      player:AddSoulHearts(2)
      OvenM.has = true
    end

    for i = 1, #ent do
      if ent[i].Type == EntityType.ENTITY_FIREPLACE then
        ent[i].EntityCollisionClass = EntityCollisionClass.ENTCOLL_ENEMIES
      elseif ent[i].Type == EntityType.ENTITY_PICKUP and ent[i].Variant == PickupVariant.PICKUP_SPIKEDCHEST then
        ent[i].EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
      end
    end
  end
end

Mitt:AddCallback(ModCallbacks.MC_POST_UPDATE, Mitt.onUpd)

-------------------
--CURSED CONTRACT--
-------------------
local CursedContract = RegisterMod ("Cursed Contract", 1)
local DevilPool2 = {8, 34, 35, 51, 67, 79, 80, 81, 82, 83, 84, 97, 113, 114, 118, 122, 126, 133, 134, 145, 159, 163, 172, 187, 212, 215, 216, 225, 230, 237, 241, 259, 262, 269, 268, 275, 278, 292, 311, 412, 408, 399, 391, 360, 409, 433, 431, 420, 417, 441, 498, 477, 475, 462, 442, 468}
local ContractVars = {
	HasActivated = false,
	CurrentLevel = 0,
	PreviousLevel = 0
}

--Curses array--
local Curses = {
	LevelCurse.CURSE_OF_DARKNESS,
	LevelCurse.CURSE_OF_LABYRINTH,
	LevelCurse.CURSE_OF_THE_LOST,
	LevelCurse.CURSE_OF_THE_UNKNOWN,
	LevelCurse.CURSE_OF_THE_CURSED,
	LevelCurse.CURSE_OF_MAZE,
	LevelCurse.CURSE_OF_BLIND,
	Isaac.GetCurseIdByName("Curse of the Opposite"),
	Isaac.GetCurseIdByName("Curse of Rewind"),
	Isaac.GetCurseIdByName("Curse of the Chest"),
	Isaac.GetCurseIdByName("What is That?!"),
	Isaac.GetCurseIdByName("Curse of Seperation")
}

--Main Function--
function CursedContract:Main()
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.CURSED_CONTRACT) then
		ContractVars.CurrentLevel = Game():GetLevel()
		if not ContractVars.HasActivated then
		local Roll = math.random(1, 3)
			for i = 1, Roll do
				Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, DevilPool2 [math.random(1, #DevilPool2)], Game():GetRoom():FindFreePickupSpawnPosition(player.Position, 1, true), Vector(0, 0), nil)
			end
			ContractVars.HasActivated = true
		end
		if ContractVars.CurrentLevel ~= ContractVars.PreviousLevel and Game():GetLevel():GetCurses() == LevelCurse.CURSE_NONE then
			Game():GetLevel():AddCurse(Curses [math.random(1, #Curses)])
			ContractVars.PreviousLevel = ContractVars.CurrentLevel
		end
	end
end

--Reset Function--
function CursedContract:Reset()
	ContractVars = {
	HasActivated = false,
	CurrentLevel = 0,
	PreviousLevel = 0
}
end

--Callbacks--
CursedContract:AddCallback(ModCallbacks.MC_POST_UPDATE, CursedContract.Main)
CursedContract:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, CursedContract.Reset)

local D666Mod = RegisterMod("D666",1)
local DevilPool = {8, 34, 35, 51, 67, 79, 80, 81, 82, 83, 84, 97, 113, 114, 118, 122, 126, 133, 134, 145, 159, 163, 172, 187, 212, 215, 216, 225, 230, 237, 241, 259, 262, 269, 268, 275, 278, 292, 311, 412, 408, 399, 391, 360, 409, 433, 431, 420, 417, 441, 498, 477, 475, 462, 442, 468}
local D666Vars = {
	NumRedChests = 0,
	SpawnedKrampus = false
	}

function D666Mod:Reset()
	D666Vars = {
	NumRedChests = 0,
	SpawnedKrampus = false
	}
end

function D666Mod:OnUseD666()
	D666Vars.SpawnedKrampus = false
	D666Vars.NumRedChests = 0
	local player = Isaac.GetPlayer(0)
	local entities = _G.Genesis.getRoomEntities()
	for i=1,#entities do
		if Game():GetRoom():GetType() ~= RoomType.ROOM_DEVIL then
			if entities[i].Type == EntityType.ENTITY_PICKUP and entities[i].Variant == PickupVariant.PICKUP_REDCHEST  and entities[i].SubType == ChestSubType.CHEST_CLOSED then
				entities[i]:ToPickup():Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, DevilPool[math.random(1,#DevilPool)], false)
				Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BLOOD_PARTICLE , 0, entities[i].Position, Vector(0,0), player)
				D666Vars.NumRedChests = D666Vars.NumRedChests + 1
				entities[i]:GetSprite():ReplaceSpritesheet(0,"gfx/BloodyChest.png")
				entities[i]:GetSprite():LoadGraphics()
			end
			if D666Vars.NumRedChests >= 2 and D666Vars.SpawnedKrampus == false then
				Isaac.Spawn(EntityType.ENTITY_FALLEN, 1 , 0, Game():GetRoom():GetCenterPos(), Vector(0,0), nil)
				D666Vars.SpawnedKrampus = true
			end
		elseif Game():GetRoom():GetType() == RoomType.ROOM_DEVIL then
			Isaac.ExecuteCommand("goto s.devil")
		end
	end
	SFXManager():Play(SoundEffect.SOUND_SATAN_APPEAR, 1, 0, false, 0.7)
	return true
end

D666Mod:AddCallback(ModCallbacks.MC_USE_ITEM, D666Mod.OnUseD666, GENESIS_ITEMS.ACTIVE.D666)
D666Mod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, D666Mod.Reset)

------------------
--Wish For A Day--
------------------

local Wish = RegisterMod("WishForADay", 1)

local WishDay = {
  id = GENESIS_ITEMS.ACTIVE.WISH,
  used = false
}

function Wish:onUse()
  local player = Game():GetPlayer(0)
  if player:HasCollectible(WishDay.id) then
    RandItem = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, 700, Vector(-500, -500), Vector(0, 0), nil)
    while RandItem ~= nil and RandItem:ToPickup().Charge > 0 do
      RandItem:Remove()
      RandItem = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, 700, Vector(-500, -500), Vector(0, 0), nil)
    end
    RandItemId = RandItem.SubType
    RandItem:Remove()
    CurrentFloorWish = Game():GetLevel():GetStage()
    player:AddCollectible(RandItemId, 0, true)
    player:AnimateCollectible(RandItemId, "UseItem", "Idle")
    WishDay.used = true

  end
end

Wish:AddCallback(ModCallbacks.MC_USE_ITEM, Wish.onUse, WishDay.id)

function Wish:OnUp()
  local player = Game():GetPlayer(0)
  if CurrentFloorWish == Game():GetLevel():GetStage() and WishDay.used and player:GetActiveCharge() ~= 0 and player:GetActiveItem() == WishDay.id then
    player:SetActiveCharge(0)
  end
  if Game():GetFrameCount() == 1 then
    WishDay.used = false
  end

  if CurrentFloorWish ~= Game():GetLevel():GetStage() and WishDay.used == true then
    player:RemoveCollectible(RandItemId)
    player:SetActiveCharge(12)
    WishDay.used = false
  end
end

Wish:AddCallback(ModCallbacks.MC_POST_UPDATE, Wish.OnUp)

--------------
--LIGHT SHOT--
--------------

local lightshotMod = RegisterMod("lightshot",1)
local lightshotItem = Isaac.GetItemIdByName("Lightshot")
local lightshotCostume = Isaac.GetCostumeIdByPath("gfx/characters/lightshot_costume.anm2")

local hasLightshot = false

-- This is for ipecac --
local tears = { }

function lightshotMod:onUpdate()

  local player = Isaac.GetPlayer(0)

  if hasLightshot == true and not player:HasCollectible(lightshotItem) then
    hasLightshot = false
  end
  if hasLightshot == false and player:HasCollectible(lightshotItem) then
    hasLightshot = true
    player:AddNullCostume(lightshotCostume, 100)
  end

  -- Check if the player has Lightshot
  if player:HasCollectible(lightshotItem) then

    -- Limit the player max fire delay --
    if player.MaxFireDelay < 7 then
      player.MaxFireDelay = 7
    end

    -- Remove the tear sound --
    if SFXManager():IsPlaying(SoundEffect.SOUND_TEARIMPACTS) then
      SFXManager():Stop(SoundEffect.SOUND_TEARIMPACTS)
    end
    if SFXManager():IsPlaying(SoundEffect.SOUND_TEARS_FIRE) then
      SFXManager():Stop(SoundEffect.SOUND_TEARS_FIRE)
    end

    -- Check all the tear entities and laser entities --
    local ents = _G.Genesis.getRoomEntities()
    for k, v in pairs(ents) do

      -- Remove laser sounds that come from the player aka brimstone --
      if v.Type == EntityType.ENTITY_LASER and v.SpawnerType == EntityType.ENTITY_PLAYER and v.FrameCount <= 2 and v.Parent ~= nil and v.Parent.Type == EntityType.ENTITY_TEAR then
        if SFXManager():IsPlaying(SoundEffect.SOUND_BLOOD_LASER) then
          SFXManager():Stop(SoundEffect.SOUND_BLOOD_LASER)
        end
        if SFXManager():IsPlaying(SoundEffect.SOUND_BLOOD_LASER_LARGE) then
          SFXManager():Stop(SoundEffect.SOUND_BLOOD_LASER_LARGE)
        end
      end

      -- Transform tears into lightshots --
      if v.Type == EntityType.ENTITY_TEAR and v.FrameCount <= 1 and v.Variant ~= 6514 then
        if player:GetName() ~= "Azazel" and not player:HasCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE) then
          if player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) then
            return
          end
          v.Position = v.Position + Vector(0,-10)
          table.insert(tears, v:ToTear())
          local laser = EntityLaser.ShootAngle(3, Vector(-500,-400), v.Velocity:GetAngleDegrees(), 0, Vector(0,-10), player)
          laser:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
          laser:GetSprite():Load("gfx/lightshot.anm2", true)
          laser:GetSprite():Play("LargeRedLaser")
          laser.Parent = v
          laser.Velocity = v.Velocity


          laser.ParentOffset = Vector(0,-80)

          -- The range is fixed and it only changes if you have brimstone --
          if player:HasCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE) then

            laser.MaxDistance = 3000
          elseif player:HasCollectible(CollectibleType.COLLECTIBLE_LUDOVICO_TECHNIQUE) then
            laser.MaxDistance = 1
          else
            laser.MaxDistance = 30
          end

          -- Nerf damage because its op as fuck --
          laser.CollisionDamage = player.Damage / 3

          -- Color the laser --
          laser.Color = player.TearColor

          -- This prevents the laser from dying before the ghost tear dies --
          laser.Timeout = 10000

          -- Prevents the ghost tear from doing damage and other stuff --
          v.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
          v.Visible = false

          -- Continuum synergy! --
          if player:HasCollectible(CollectibleType.COLLECTIBLE_CONTINUUM) then
            laser.GridHit = false
          end

          -- Tear flags and movement speed --
          laser.TearFlags = player.TearFlags
          laser.Velocity = v.Velocity

          -- Lightshot sound --
          SFXManager():Play(GENESIS_SFX.LIGHTSHOT, 0.85,0,false, 1)
        end
      end

      -- Tech X synergy --
      if v.Type == EntityType.ENTITY_LASER and v.FrameCount <= 1 and v.SpawnerType == EntityType.ENTITY_PLAYER and v.Parent ~= nil and v.Parent.Type ~= EntityType.ENTITY_TEAR and player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) then -- 2 and 7
        v:GetSprite():Load("gfx/lightshot.anm2", true)
        v:GetSprite():Play("LargeRedLaser")
        --laser.TearFlags = player.TearFlags
        --local velocity = v.Velocity
        --local tear = player:FireTear(v.Position, velocity, true, false, false)
        --v.Parent = tear
        --v.Velocity = Vector(0,0)
        local velocity = v.Parent.Velocity
        local tear = player:FireTear(v.Position, velocity, true, false, false)
        v.Parent.Parent = tear
        laser.TearFlags = player.TearFlags

      end
      -- In case you have a laser and not a tear --
      if v.Type == EntityType.ENTITY_LASER and v.FrameCount <= 1 and v.SpawnerType == EntityType.ENTITY_PLAYER and v.Parent ~= nil and v.Parent.Type ~= EntityType.ENTITY_TEAR and v.Variant ~= 7 then
        if player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) then
         -- if v.Variant ~= 2 then
            return
          --end
          --return
        else
          if not v:ToLaser().BounceLaser then
            return
          end
        end
        if v.Parent ~= nil and v.Parent.Type == EntityType.ENTITY_LASER then
          if v.Parent.Parent ~= nil and v.Parent.Parent.Type == EntityType.ENTITY_TEAR then
            return
          end
          if v.Parent.Parent.Parent ~= nil then
            return
          end
        end
        if player:HasCollectible(CollectibleType.COLLECTIBLE_LUDOVICO_TECHNIQUE) then
          return
        end
        -- Gets the player laser --
        local laser2 = v:ToLaser()
        laser2:GetSprite():Load("gfx/lightshot.anm2", true)
        laser2:GetSprite():Play("LargeRedLaser")
        Isaac.DebugString("Angle: " .. tostring(laser2.Angle))
        -- This gets the velocity vector from the angle --
        local velocity = Vector(math.cos(math.rad(laser2.StartAngleDegrees))*((player.ShotSpeed)*15), math.sin(math.rad(laser2.StartAngleDegrees))*((player.ShotSpeed)*15))

        -- Spawns the ghost tear and removes the laser --
        local tear = player:FireTear(v.Position, velocity, true, false, false)--Isaac.Spawn(2, 1, 0, v.Position, Vector(0,0), player):ToTear()
        --local tear = Isaac.Spawn(2, 22, 0, v.Position, velocity, player)

        table.insert(tears, tear:ToTear())
        tear.Position = v.Position + Vector(0,-10)
        laser2:Remove()
        --if tear ~= nil then
        --  return
        --end

        -- Spawns the lightshot and do stuff --
        laser = EntityLaser.ShootAngle(3, Vector(-500,-400), v.Velocity:GetAngleDegrees(), 0, Vector(0,-10), player)
        laser:GetSprite():Load("gfx/lightshot.anm2", true)
        laser:GetSprite():Play("LargeRedLaser")
        laser.Parent = tear
        laser.Velocity = tear.Velocity
        laser.ParentOffset = Vector(0,-80)

        -- Check range if you have or not brimstone --
        if player:HasCollectible(CollectibleType.COLLECTIBLE_BRIMSTONE) then
          laser.MaxDistance = 3000
        else
          laser.MaxDistance = 30
        end

        -- Same shit than above --
        laser.CollisionDamage = player.Damage / 3
        laser.Color = player.TearColor
        laser.SplatColor = player.TearColor

        laser.Timeout = 10000
        tear.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
        if player:HasCollectible(CollectibleType.COLLECTIBLE_CONTINUUM) then
          laser.GridHit = false
        end

        --tear.Visible = false

        laser.TearFlags = player.TearFlags
        laser.Velocity = tear.Velocity

        --SFXManager():Play(GENESIS_SFX.DYSLEXIA_DROP, 1,0,false, 1)
      end

      -- Check for lightshot impact particles --
      if v.Type == EntityType.ENTITY_EFFECT and v.Variant == EffectVariant.LASER_IMPACT and v.FrameCount <= 1 and v.Parent ~= nil and v.Parent.SpawnerType == EntityType.ENTITY_PLAYER then
        if player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) then
          --v.Visible = false

          -- LASER CONVERSION --
          --v.Visible = false

          --v.Parent = tear

         -- v.
          --v.Velocity = Vector(0,0)

          return
        end
        v:GetSprite():Load("gfx/lightshot_impact.anm2", true)
        v:GetSprite():Play("Start")
      end

      if v.Type == EntityType.ENTITY_LASER and v.Variant == EffectVariant.LASER_IMPACT and player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) and v.Parent.SpawnerType == EntityType.ENTITY_PLAYER and v.Variant == 2 then
        v.Position = v.Parent.Position
        v.Velocity = v.Parent.Velocity
      end

      if v.Type == EntityType.ENTITY_EFFECT and v.Variant == EffectVariant.LASER_IMPACT and v.FrameCount <= 2 and v.Parent ~= nil and v.Parent.SpawnerType == EntityType.ENTITY_PLAYER then
        if player:HasCollectible(CollectibleType.COLLECTIBLE_TECH_X) then

          return
        end
        v:GetSprite():Load("gfx/lightshot_impact.anm2", true)
        v:GetSprite():Play("Start")
      end

      -- This updates the laser angle, for example for ludovico --
      if v.Type == EntityType.ENTITY_LASER and v.Parent ~= nil and v.Parent.Type == EntityType.ENTITY_TEAR and v.Parent.SpawnerType == EntityType.ENTITY_PLAYER then
        v:ToLaser().Angle = v.Parent.Velocity:GetAngleDegrees()
        if player:HasCollectible(GENESIS_ITEMS.PASSIVE.TETRACHROMACY) then
          local ro = math.random(0, 255)
          local go = math.random(0, 255)
          local bo = math.random(0, 255)
          v:ToLaser().Color = Color(ro/255, ro/255, ro/255, ro/255, ro, go, bo)
        end
      end

      -- This removes the laser slowly when the tear dies --
      if v.Type == EntityType.ENTITY_LASER and v.Parent == nil and v:ToLaser().Timeout > 10 then
        v:ToLaser().Timeout = 10
      end

      -- Ipecac synergy, first part --
      if v.Type == EntityType.ENTITY_LASER and v.Parent ~= nil then
        if player:HasCollectible(CollectibleType.COLLECTIBLE_IPECAC) then

          local value = -30
          --v.Parent.Variant = 5
          value = -30

          v:ToLaser().Parent:ToTear().Height = value
          v:ToLaser().Parent:ToTear().FallingSpeed = 0
          laser.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ENEMIES
        end
      end

      -- Ipecac synergy, part two --
      if v.Type == EntityType.ENTITY_TEAR and v.SpawnerType == EntityType.ENTITY_PLAYER then
        if player:HasCollectible(CollectibleType.COLLECTIBLE_IPECAC) then

          -- check for all entities, this will be laggy --
          local ients = _G.Genesis.getRoomEntities()
          for k, e in pairs(ients) do
            if v.Position:Distance(e.Position) < 55 and e:IsEnemy() then
              Isaac.DebugString("EXPLODE DAMN!")
              v:Die()
            end
          end
        end
      end

      -- If the laser is from rubber cemment and stuff like that --
      if v.Parent ~= nil then
        if v.Type == EntityType.ENTITY_LASER and v.Parent.Type == EntityType.ENTITY_LASER and v.FrameCount <= 3 then
          v:GetSprite():Load("gfx/lightshot.anm2", true)
          v:GetSprite():Play("LargeRedLaser")
        end
      end

      -- Removes the ghost tear particles --
      if v.Type == EntityType.ENTITY_EFFECT then
        if v.Variant == EffectVariant.TEAR_POOF_A or v.Variant == EffectVariant.TEAR_POOF_B or v.Variant == EffectVariant.TEAR_POOF_SMALL or v.Variant == EffectVariant.TEAR_POOF_VERYSMALL then
          v:Remove()
        end
      end
    end
  end
end


--Callbacks--
lightshotMod:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, lightshotMod.onUpdate)

-------------------
--SATAN'S CHALICE--
-------------------

-- Local variables --
local satanChalice = RegisterMod("satans chalice", 1)
local satan_chalice = Isaac.GetItemIdByName("Satan's Chalice")
local bloodBallVariant = Isaac.GetEntityVariantByName("Blood Orbital")
local chaliceringVariant = Isaac.GetEntityVariantByName("Chalice Ring")

local chaliceRing

local angle = 0

-- Iem code --
function satanChalice:Chalice()
  local player = Isaac.GetPlayer(0)
  local entities = _G.Genesis.getRoomEntities()
  local room = Game():GetRoom()
  local ball_count = 0

  if player:HasCollectible(satan_chalice) then

    if room:GetFrameCount() == 1 then
      Isaac.DebugString("Time!")
      chaliceRing = Isaac.Spawn(1000, chaliceringVariant, 0, player.Position, Vector(0,0), player)
      --chaliceRing:AddEntityFlags(EntityFlag.FLAG)
      chaliceRing.Parent = player
    end
    if chaliceRing ~= nil then
      chaliceRing.Position = player.Position + Vector(0,-110)
      chaliceRing.Velocity = player.Velocity
    end
    -- This adds the blood balls to the array when you get to a room --
    if room:GetFrameCount() <= 1 then
      Genesis.moddata.bloodOrbitals = { }
      for i=0, #entities do
        if entities[i] ~= nil then
          if entities[i].Type == EntityType.ENTITY_FAMILIAR and entities[i].Variant == bloodBallVariant then
            table.insert(Genesis.moddata.bloodOrbitals, entities[i])
          end
        end
      end
    end

    -- The angle offset for all the blood balls --
    angle = Game():GetFrameCount() / 50.00

    -- Checking for the blood balls and hearts --
    for i = 1, #entities do

      -- Normal Heart --
      if entities[i].Type == 5 and entities[i].Variant == 10 and entities[i].SubType == 1 then
        if entities[i].Position:Distance(player.Position) < 22 and player:HasFullHearts() then
          Isaac.DebugString(tostring(player:GetHearts()) .. " / " .. tostring(player:GetMaxHearts()))
          SFXManager():Play(SoundEffect.SOUND_BOSS2_BUBBLES, 1,0, false, 1)
          local b = Isaac.Spawn(3, bloodBallVariant, 0, entities[i].Position, Vector(0,0), player)
          b:ToFamiliar().State = 6
          b:GetSprite():Play("HP4")
          b.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ENEMIES
          table.insert(Genesis.moddata.bloodOrbitals, b)
          entities[i]:Remove()
        end
      end

      -- Half Heart --
      if entities[i].Type == 5 and entities[i].Variant == 10 and entities[i].SubType == HeartSubType.HEART_HALF then
        if entities[i].Position:Distance(player.Position) < 22 and player:HasFullHearts() then
          SFXManager():Play(SoundEffect.SOUND_BOSS2_BUBBLES, 1,0, false, 1)
          Isaac.DebugString(tostring(player:GetHearts()) .. " / " .. tostring(player:GetMaxHearts()))
          local b = Isaac.Spawn(3, bloodBallVariant, 0, entities[i].Position, Vector(0,0), player)
          b:ToFamiliar().State = 4
          b:GetSprite():Play("HP2")
          b.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ENEMIES
          table.insert(Genesis.moddata.bloodOrbitals, b)
          entities[i]:Remove()
        end
      end

      -- Double Hearts --
      if entities[i].Type == 5 and entities[i].Variant == 10 and entities[i].SubType == 5 then
        if entities[i].Position:Distance(player.Position) < 22 and player:HasFullHearts() then
          SFXManager():Play(SoundEffect.SOUND_BOSS2_BUBBLES, 1,0, false, 1)
          entities[i]:Remove()
          local b = Isaac.Spawn(3, bloodBallVariant, 0, entities[i].Position, Vector(0,0), player)
          b:ToFamiliar().State = 6
           b:GetSprite():Play("HP4")
          b.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
          --b.State = 2
          table.insert(Genesis.moddata.bloodOrbitals, b)
          local b2 = Isaac.Spawn(3, bloodBallVariant, 0, entities[i].Position, Vector(0,0), player)
          b2:ToFamiliar().State = 6
          b2:GetSprite():Play("HP4")
          b2.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
          --b2.State = 2
          table.insert(Genesis.moddata.bloodOrbitals, b2)
        end
      end
    end

    -- Update the blood balls --
    for i = 1, #Genesis.moddata.bloodOrbitals do

        -- Some math for their orbit --
        local ang = ((math.pi * 2.0) / #Genesis.moddata.bloodOrbitals) * i
        local x1 = player.Position.X + (55.0 * math.cos(angle + ang))
        local y1 = player.Position.Y + (52.0 * math.sin(angle + ang))

        -- In case the blood ball died --
        if Genesis.moddata.bloodOrbitals[i] == nil then
          return
        end

        Genesis.moddata.bloodOrbitals[i].Position = Vector(x1,y1)
        Genesis.moddata.bloodOrbitals[i].Velocity = Vector(0,0)

        -- This is to check inside each blood ball if they collided --
        for k, v in pairs(entities) do

          -- If it collides with a projectile --
          if (v.Type == EntityType.ENTITY_PROJECTILE) and v.Position:Distance(Genesis.moddata.bloodOrbitals[i].Position) < 16 then
            v:Die()
            Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State = Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State - 1

            -- If the blood ball should die --
            if Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State <= 2 then
              Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.PLAYER_CREEP_RED, 0, v.Position, Vector(0,0), v)
              Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BLOOD_SPLAT, 0, v.Position, Vector(0,0), v)
              SFXManager():Play(SoundEffect.SOUND_DEATH_BURST_SMALL, 1, 0, false, 1)
              Genesis.moddata.bloodOrbitals[i]:BloodExplode()
              Genesis.moddata.bloodOrbitals[i]:Die()
              table.remove(Genesis.moddata.bloodOrbitals, i)
            else
              Genesis.moddata.bloodOrbitals[i]:GetSprite():Play("HP" .. tostring(Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State - 2))
            end
          end
          if Genesis.moddata.bloodOrbitals[i] == nil then
            return
          end

          -- If it collides with an enemy --
          if (v ~= nil and v:ToNPC() ~= nil and v:IsVulnerableEnemy()) and v.Position:Distance(Genesis.moddata.bloodOrbitals[i].Position) < 16 then
            Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State = Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State - 1

            -- If the blood ball should die --
            if Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State <= 2 then
              Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.PLAYER_CREEP_RED, 0, v.Position, Vector(0,0), v)
              Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BLOOD_SPLAT, 0, v.Position, Vector(0,0), v)
              SFXManager():Play(SoundEffect.SOUND_DEATH_BURST_SMALL, 1, 0, false, 1)
              Genesis.moddata.bloodOrbitals[i]:BloodExplode()
              Genesis.moddata.bloodOrbitals[i]:Die()
              table.remove(Genesis.moddata.bloodOrbitals, i)
            else
              Genesis.moddata.bloodOrbitals[i]:GetSprite():Play("HP" .. tostring(Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State - 2))
            end
          end
          if Genesis.moddata.bloodOrbitals[i] == nil then
            return
          end

          -- If the blood ball collides with a boss --
          if (v ~= nil and v:ToNPC() ~= nil and v:IsBoss()) and v.Position:Distance(Genesis.moddata.bloodOrbitals[i].Position) < 16 then
            Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State = Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State - 1

            -- If the blood ball should die --
            if Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State <= 2 then
              Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.PLAYER_CREEP_RED, 0, v.Position, Vector(0,0), v)
              Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BLOOD_SPLAT, 0, v.Position, Vector(0,0), v)
              SFXManager():Play(SoundEffect.SOUND_DEATH_BURST_SMALL, 1, 0, false, 1)
              Genesis.moddata.bloodOrbitals[i]:BloodExplode()
              Genesis.moddata.bloodOrbitals[i]:Die()
              table.remove(Genesis.moddata.bloodOrbitals, i)
            else
              Genesis.moddata.bloodOrbitals[i]:GetSprite():Play("HP" .. tostring(Genesis.moddata.bloodOrbitals[i]:ToFamiliar().State - 2))
            end
          end
        end
    end
  end

end



--satanChalice:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, satanChalice.Init)
satanChalice:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, satanChalice.Chalice)

--------------
--Lost Power--  - 2 Damage, Isaac gains +1 dmg for every 12 rooms explored.
--------------
local mod = RegisterMod("Lost Power", 1)
local LostPowerVars = {
	LPowerRoomsCleared = 0,
	LPowerDmgBonus = 0,
	LPowerHasItem = false,
	LPowerHasCostume = false
}

function mod:OnInit()
	LostPowerVars.LPowerRoomsCleared = 0
	LostPowerVars.LPowerDmgBonus = 0
	LostPowerVars.LPowerHasItem = false
	LostPowerVars.LPowerHasCostume = false
end

function mod:OnNewRoom()
    if not Game():GetRoom():IsFirstVisit() then return end
    for i = 0, 3 do
        local player = Isaac.GetPlayer(i)
	    if player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_POWER) then
	    	LostPowerVars.LPowerRoomsCleared = LostPowerVars.LPowerRoomsCleared + 1
	    	if LostPowerVars.LPowerRoomsCleared > 11 then
	    		LostPowerVars.LPowerRoomsCleared = 0
	    		LostPowerVars.LPowerDmgBonus = LostPowerVars.LPowerDmgBonus + 1
	    		player:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
	    		player:EvaluateItems()
	    		if LostPowerVars.LPowerDmgBonus <= 6 then
	    			SFXManager():Play(SoundEffect.SOUND_HEARTBEAT , 1, 0, false, 2 / LostPowerVars.LPowerDmgBonus)
	    		else
	    			SFXManager():Play(SoundEffect.SOUND_HEARTBEAT , 1, 0, false, 0.2)
	    		end
	    	end
        end
    end
end

function mod:GiveCostume(player, cacheFlag)
  if cacheFlag == CacheFlag.CACHE_DAMAGE and player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_POWER) then
		player.Damage = player.Damage + LostPowerVars.LPowerDmgBonus
	end
end

function mod:UpdateDamage()
	local player = Isaac.GetPlayer(0)
	if player:HasCollectible(GENESIS_ITEMS.PASSIVE.LOST_POWER) then
		LostPowerVars.LPowerHasItem = true
	else
		LostPowerVars.LPowerHasItem = false
		LostPowerVars.LPowerRoomsCleared = 0
		LostPowerVars.LPowerDmgBonus = 0
	end
	if LostPowerVars.LPowerHasItem == true and LostPowerVars.LPowerHasCostume == false then
		player:AddNullCostume(GENESIS_COSTUMES.LOST_POWER)
		LostPowerVars.LPowerHasCostume = true
	elseif LostPowerVars.LPowerHasItem == false and LostPowerVars.LPowerHasCostume == true then
		player:TryRemoveNullCostume(GENESIS_COSTUMES.LOST_POWER)
		LostPowerVars.LPowerHasCostume = false
	end
end

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, mod.OnNewRoom)
mod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, mod.OnInit)
mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, mod.GiveCostume)
mod:AddCallback(ModCallbacks.MC_POST_UPDATE, mod.UpdateDamage)

Isaac.DebugString("Genesis+: Loaded Items(2/3)!")
error({ hack = true })