-- >> SATANIC RITUAL << --
-- LOCAL VARIABLES --

--------------------------------------------
-- HOW TO ADD A PUZZLE:                   --
--                                        --
-- Make two functions with the name:      --
--                                        --
-- generatePuzzle_PuzzleName              --
-- updatePuzzle_PuzzleName                --
--                                        --------------------------------------------------------------------------------------------------
-- PuzzleName is the name of your puzzle                                                                                                  --
-- Follow the same path the other methods use and spawn the entities you want to use and make an update.                                  --
-- Puzzles may have phases, feel free to use the current variables for puzzles and make sure they save correctly.                         --
-- Add the two methods in the satanic ritual function under generate puzzle and update puzzle, you just need to give the puzzle a number. --
-- Change the number of possible puzzles below to the number of puzzles you have                                                          --
--------------------------------------------------------------------------------------------------------------------------------------------

local mod = RegisterMod("SatanicRitual", 1)
mod.force_spawn = false
mod.r = 0

-- ENVIONMENT
mod.Background_Floor = nil
mod.Background_Wall = nil
--satanicMusicId = 104
--satanicPuzzleMusicId = 105

-- GENERAL STUFF --
mod.chanceSatanicSpawn = 30
mod.Ritual_Timer = 0
mod.satanic_check_delay = 0
mod.Ritual_Puzzle_Phase = 0
mod.stage = -56783568

-- PUZZLE STUFF --
mod.Satanic_Entities = { }
mod.Satanic_Pattern = { }
mod.Ritual_Wave = 0
mod.Puzzle_Speed = 0
mod.Ritual_Entity_Timer = 0
mod.Number_of_Puzzles = 1

-- ENTITIES --
mod.satanicCandleId = Isaac.GetEntityTypeByName("Satanic Candle")
mod.satanicCandleVariant = Isaac.GetEntityVariantByName("Satanic Candle")

-- DOOR ENTITY --
mod.doorPos = Vector(0,0)
mod.doorWall = nil

-- REWARDS AND POOLS --
CARD_REWARDS = { Card.CARD_CHAOS, Card.CARD_JOKER, Card.CARD_CREDIT, Card.CARD_DICE_SHARD, Card.CARD_SUICIDE_KING, Card.RUNE_JERA, Card.RUNE_PERTHRO, Card.RUNE_BLACK }

mod.DevilPool = { 8, 34, 35, 51, 67, 79, 80, 81, 82, 83, 84, 97, 113, 114, 118, 122, 126, 133, 134, 145, 159, 163, 172, 187, 212, 215, 216, 225, 230, 237, 241, 259, 262, 269, 268, 275, 278, 292, 311, 412, 408, 399, 391, 360, 409, 433, 431, 420, 417, 441, 498, 477, 475, 462, 442, 468, GENESIS_ITEMS.PASSIVE.LUCIFERS_WALLET}

-- GENERATE THE RITUAL ROOM --
-- SHOULD GENERATE AT THE START OF THE LEVEL --
function mod:generateRitualRoom()

  if Genesis.DEBUG then Isaac.DebugString("Genesis+:Generating new Ritual Room...") end
  Genesis.moddata.isSatanicRitualThisFloor = false
  Genesis.moddata.Payed_For_Ritual = false


  -- LOCAL VARIABLES
  local player = Isaac.GetPlayer(0)
	local currentLevel = Game():GetLevel()

  --UPDATE STAGE (used in NEW_ROOM to prevent running before this callback)
  mod.stage = currentLevel:GetStage()

  -- CHECK IF THE DATA IS ALREADY GENERATED FOR THE CURRENT LEVEL
  if Genesis.moddata.resetData == false then return false end
  if Genesis.DEBUG then Isaac.DebugString("Genesis+:Generating new Data for Ritual Room...") end

  -- RESETS THE CURSE ROOM
  Genesis.moddata.Curse_Room_Id = -1

--  --LOOP THROUGH ALL ROOMS TO FIND A CURSE ROOM
--  local rooms = currentLevel:GetRooms()

--  for i = 0, rooms.Size - 1 do
--    local rType = rooms:Get(i).Data.Type
--    if rType == RoomType.ROOM_CURSE then
--      Genesis.moddata.Curse_Room_Id = rooms:Get(i).GridIndex
--    end
--  end

  -- CHECK IF YOU HAD 2 OR MORE RITUALS
  if Genesis.moddata.NumOfSatanicRitualsOnRun >= 2 and not mod.force_spawn then return false end
  if Genesis.DEBUG then Isaac.DebugString("Genesis+:You have seen " .. tostring(Genesis.moddata.NumOfSatanicRitualsOnRun) .. " Ritual Rooms in this Run...") end

  -- CHECK CHANCE FOR SATANIC RITUAL IN THE LEVEL
  if player:HasTrinket(GENESIS_ITEMS.TRINKET.RITUAL_CANDLE) then
    if Genesis.DEBUG then Isaac.DebugString("Genesis+:Found Ritual Candle in your Trinkets...") end
    mod.chanceSatanicSpawn = 60
  else
    mod.chanceSatanicSpawn = 30
  end

  -- CHECK IF GENERATES THE ROOM IN THE LEVEL
  if Genesis.DEBUG then Isaac.DebugString("Genesis+:Trying to generate a new Ritual Room...") end
  if math.random(100) <= mod.chanceSatanicSpawn or mod.force_spawn == true then
    Genesis.moddata.isSatanicRitualThisFloor = true
    Genesis.moddata.NumOfSatanicRitualsOnRun = Genesis.moddata.NumOfSatanicRitualsOnRun + 1
    Genesis.moddata.Satanic_State = -1
    Genesis.moddata.IsSatanicRitualEnd = false
    Genesis.moddata.Payed_For_Ritual = false

    --GETS A RANDOM PUZZLE THAT MATCHES THE NUMBER OF PUZZLES
    Genesis.moddata.Ritual_Puzzle = math.random(mod.Number_of_Puzzles)
    mod.Ritual_Wave = 0
    mod.Ritual_Puzzle_Phase = 0
    mod.Ritual_Entity_Timer = 0
    mod.Ritual_Timer = 0
    mod.Satanic_Pattern = { }
    if Genesis.DEBUG then Isaac.DebugString("Genesis+:Ritual Room has been generated...") end
    Genesis.moddata.isSatanicRitualThisFloor = true
  else
    if Genesis.DEBUG then Isaac.DebugString("Genesis+:Ritual Room has not been generated for this floor...") end
  end
end

--not in new_level anymore as it bugged out sometimes as its called after new_room
--mod:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, mod.generateRitualRoom)

function mod:Init(fromSave)
  if not fromSave then
    Genesis.moddata.NumOfSatanicRitualsOnRun = 0
  end
end

mod:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, mod.Init)

function mod:satanic_ritual()
  mod.r=mod.r+1
  math.randomseed(mod.r)
  -- LOCAL VARIABLES
  local player = Isaac.GetPlayer(0)
	local currentLevel = Game():GetLevel()
	local room = Game():GetRoom()
	local currentRoomType = room:GetType()
	local entities = Isaac.GetRoomEntities()
  --math.randomseed(RNG():GetSeed())

  -- RETURN IF THERE IS NO RITUAL ROOM IN THE LEVEL
  if Genesis.moddata.isSatanicRitualThisFloor == false then return false end

  -- CHECK IF PLAYER IS INSIDE RITUAL ROOM
  if room:GetType() == RoomType.ROOM_CURSE and currentLevel:GetCurrentRoomIndex() == Genesis.moddata.Curse_Room_Id and Genesis.moddata.isSatanicRitualThisFloor then

    -- IS SATANIC ROOM
    Genesis.moddata.isSatanicRitualRoom = true

    -- UPDATE BACKGROUND
    if mod.Background_Floor ~= nil then
      if mod.Background_Floor:GetSprite():IsFinished("FloorLight") then
        mod.Background_Floor:GetSprite():Play("FloorLightIdle")
      end
    end

    -- UPDATE PUZZLES
    if Genesis.moddata.Ritual_Puzzle == 1 then
      mod:updatePuzzle_CandleRitual(player, room)
    end

  --IF ITS NOT A SATANIC RITUAL ROOM, UPDATE THE SATANIC DOOR
  else
    Genesis.moddata.isSatanicRitualRoom = false
    for num=0, DoorSlot.NUM_DOOR_SLOTS-1 do
      if room:GetDoor(num) ~= nil then
        -- UPDATE THE DOOR --
        if Genesis.moddata.Curse_Room_Id == room:GetDoor(num).TargetRoomIndex and Genesis.moddata.isSatanicRitualThisFloor then

          -- CLOSE THE DOOR IF YOU DIDNT PAY
          if Genesis.moddata.Payed_For_Ritual == false then
            if room:IsClear() then
              room:GetDoor(num):Close(true)
              room:GetDoor(num).State = 2
            end

            -- CHECK IF THE PLAYER TOUCHES THE DOOR WHEN THE DOOR IS CLEAR
            if player.Position:Distance(mod.doorPos) < 45 and room:IsClear() == true then

              -- IF YOU ARE THE LOST, THE SOULLESS OR BLUE BABY YOU DONT NEED TO PAY
              if player:GetPlayerType() == PlayerType.PLAYER_THELOST or player:GetPlayerType() == PlayerType.PLAYER_XXX or player:GetPlayerType() == Isaac.GetPlayerTypeByName("TheSoulless") then
                mod.doorWall:ToNPC():PlaySound(157,100,0,false,1)
                mod.doorWall:Remove()
                Genesis.moddata.Payed_For_Ritual = true
                Genesis.moddata.Satanic_State = -1
                Genesis.moddata.isSatanicRitualEnd = false

                room:GetDoor(num).State = 0
                room:GetDoor(num):Open()
              else

                -- YOU CAN ONLY OPEN THE DOOR IF YOU HAVE MORE THAN 1 HEART (SO YOU SURVIVE)
                -- OPENS THE DOOR WITH A HEART --
                if player:GetMaxHearts() >= 2 then
                  local h = player:GetMaxHearts()
                  local h2 = player:GetHearts()
                  player:AddMaxHearts(-(h - h2 + 2),false)
                  player:AddMaxHearts(h - h2 + 2,false)
                  mod.doorWall:ToNPC():PlaySound(157,100,0,false,1)
                  mod.doorWall:Remove()
                  Genesis.moddata.Payed_For_Ritual = true
                  Genesis.moddata.Satanic_State = -1
                  Genesis.moddata.isSatanicRitualEnd = false
                  room:GetDoor(num).State = 0
                  room:GetDoor(num):Open()
                  if player:GetHearts() + player:GetSoulHearts() <= 0 then
                    player:Die()
                  end
                end
              end
              end
          else

            -- IN CASE YOU PAYED TO THE DOOR --
            room:GetDoor(num).TargetRoomType = RoomType.ROOM_TREASURE
            room:GetDoor(num).CurrentRoomType = RoomType.ROOM_TREASURE
            room:GetDoor(num).CloseAnimation = "SatanicClose"
            room:GetDoor(num).OpenAnimation = "SatanicOpen"
            room:GetDoor(num):Open()
          end
        end
      end
    end

    -- MAKES THE ENTITY FOR THE DOOR STATIC --
    if mod.doorWall ~= nil then
      mod.doorWall.Position = mod.doorPos
      mod.doorWall.Velocity = Vector(0,0)
    end
  end
end

mod:AddCallback(ModCallbacks.MC_POST_UPDATE, mod.satanic_ritual)

-------------
-- PUZZLES ---------------
-- CANDLE RITUAL PUZZLE --
-- FOLLOW THE CANDLES ----
--------------------------

function mod:generatePuzzle_CandleRitual()

  -- LOCAL --
  local room = Game():GetRoom()

  -- SET UP --
  mod.Ritual_Wave = 0
  Genesis.moddata.satanic_puzzle_phase = 0
  mod.Ritual_Entity_Timer = 0
  if Genesis.moddata.Satanic_State ~= -1 then
    Genesis.moddata.Satanic_State = 3
    Genesis.moddata.isSatanicRitualEnd = true

  end
  mod.Ritual_Timer = 0

  -- SPAWN THE CANDLES
  mod.Satanic_Entities[1] = Isaac.Spawn(mod.satanicCandleId, mod.satanicCandleVariant, 0, room:GetGridPosition(97), Vector(0,0), nil)
  mod.Satanic_Entities[2] = Isaac.Spawn(mod.satanicCandleId, mod.satanicCandleVariant, 0, room:GetGridPosition(35) + Vector(25,15), Vector(0,0), nil)
  mod.Satanic_Entities[3] = Isaac.Spawn(mod.satanicCandleId, mod.satanicCandleVariant, 0, room:GetGridPosition(39) + Vector(-25,15), Vector(0,0), nil)
  mod.Satanic_Entities[4] = Isaac.Spawn(mod.satanicCandleId, mod.satanicCandleVariant, 0, room:GetGridPosition(80) + Vector(0,-15), Vector(0,0), nil)
  mod.Satanic_Entities[5] = Isaac.Spawn(mod.satanicCandleId, mod.satanicCandleVariant, 0, room:GetGridPosition(84) + Vector(0,-15), Vector(0,0), nil)

  -- ADD TAGS TO THE CANDLES --
  for k, v in pairs(mod.Satanic_Entities) do
    v:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
    v:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)
  end
end

function mod:updatePuzzle_CandleRitual(player, room)

  -- FORCE POSITION FOR THE CANDLES
  for k, v in pairs(mod.Satanic_Entities) do
    mod.Satanic_Entities[1].Position = room:GetGridPosition(97)
    mod.Satanic_Entities[2].Position = room:GetGridPosition(35) + Vector(25,15)
    mod.Satanic_Entities[3].Position = room:GetGridPosition(39) + Vector(-25,15)
    mod.Satanic_Entities[4].Position = room:GetGridPosition(80) + Vector(0,-15)
    mod.Satanic_Entities[5].Position = room:GetGridPosition(84) + Vector(0,-15)
  end

  -- CHECK FOR PENTAGRAM --
  if player.Position:Distance(room:GetCenterPos()) < 80 and Genesis.moddata.Satanic_State == -1 and Genesis.moddata.isSatanicRitualEnd == false then

    -- SET UP --
    mod.Ritual_Timer = 0
    mod.Ritual_Wave = 0
    Genesis.moddata.Satanic_State = 0
    mod.Ritual_Puzzle_Phase = 0
    mod.Ritual_Entity_Timer = 0
    mod.Puzzle_Speed = 0

    MusicManager():Play(GENESIS_MUSIC.SATANIC_RITUAL_PUZZLE, 0.1)
    mod.Background_Floor:GetSprite():Play("FloorLight", true)

    -- CLOSE THE DOOR --
    for num=0, 4 do
      if room:GetDoor(num) ~= nil then
        room:GetDoor(num):Close(true)
      end
    end

  end

  -- RITUAL PHASE--
  -------------------------

  -- CHECK IF YOU ARE IN THE RITUAL --
  if Genesis.moddata.isSatanicRitualEnd == false and Genesis.moddata.Satanic_State ~= -1 and Genesis.moddata.isSatanicRitualEnd == false then
    mod.Ritual_Timer = mod.Ritual_Timer + 1

    -- FIRST CANDLE PATTERN --
    if mod.Ritual_Timer == 60 and mod.Ritual_Wave == 0 then -- it starts

      -- SET UP --
      mod.Ritual_Wave = 1
--      Isaac.DebugString(math.random(1,5))
      mod.Satanic_Pattern[mod.Ritual_Wave] = math.random(1,5)
      Genesis.moddata.Satanic_State = 1
      mod.Ritual_Entity_Timer = 0
      mod.Ritual_Puzzle_Phase = 0
      mod.Puzzle_Speed = 0
    end

    -- RITUAL TURN  --
    if Genesis.moddata.Satanic_State == 1 then

        -- CHECKS TO LIGHT UP THE NEXT CANDLE
      if mod.Ritual_Entity_Timer % (40 - mod.Puzzle_Speed) == 0 and mod.Ritual_Timer >= (20 - mod.Puzzle_Speed) then
        mod.Ritual_Puzzle_Phase = mod.Ritual_Puzzle_Phase + 1
        if mod.Satanic_Entities[mod.Satanic_Pattern[mod.Ritual_Puzzle_Phase]] ~= nil then
          mod.Satanic_Entities[mod.Satanic_Pattern[mod.Ritual_Puzzle_Phase]]:GetSprite():Play("Light", true)
          mod.Satanic_Entities[mod.Satanic_Pattern[mod.Ritual_Puzzle_Phase]]:ToNPC():PlaySound(460,100,0,false,1)
        else
          Genesis.moddata.Satanic_State = 2
          mod.Ritual_Puzzle_Phase = 1
        end
      end
      mod.Ritual_Entity_Timer = mod.Ritual_Entity_Timer + 1
    end

    -- COUNTS THE FRAMES BEFORE YOU CAN TOUCH ANOTHER CANDLE AGAIN
    if mod.satanic_check_delay > 0 then
      mod.satanic_check_delay = mod.satanic_check_delay - 1
    end

    -- PLAYER TURN --
    if Genesis.moddata.Satanic_State == 2 then
      -- CHECK IF THE PLAYER TOUCHES A CANDLE --
      for i= 1, #mod.Satanic_Entities do
        if mod.Satanic_Entities[i].Position:Distance(player.Position) < 24 and mod.satanic_check_delay == 0 then

          -- IF YOU TOUCH THE CORRECT CANDLE --
          if i == mod.Satanic_Pattern[mod.Ritual_Puzzle_Phase] then

            -- ANIMATE THE CANDLE
            mod.Satanic_Entities[i]:GetSprite():Play("LightPlayer", true)
            mod.Satanic_Entities[i]:ToNPC():PlaySound(460,100,0,false,1)
            mod.satanic_check_delay = 15

            -- GOES FOR THE NEXT CANDLE --
            mod.Ritual_Puzzle_Phase = mod.Ritual_Puzzle_Phase + 1

            -- IF ITS THE LAST CANDLE ON THE PATTERN --
            if mod.Satanic_Pattern[mod.Ritual_Puzzle_Phase] == nil then

              -- WHEN FINISHED WAVE 4 --
              if mod.Ritual_Wave == 4 then

                -- GIVES A REWARD --
                local reward = math.random(1,2)
                local roomcenter = room:FindFreePickupSpawnPosition(room:GetCenterPos(), 0, true)
                local heart1_pos = room:FindFreePickupSpawnPosition(room:GetCenterPos() + Vector(30,0), 0, true)
                local heart2_pos = room:FindFreePickupSpawnPosition(room:GetCenterPos() - Vector(30,0), 0, true)

                -- REWARD: 2 BLACK HEARTS --

                if reward == 1 then
                  Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_BLACK, heart1_pos ,Vector(0,0),nil)
                  Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_BLACK, heart2_pos,Vector(0,0),nil)
                end

                -- REWARD: 1 SPECIAL CARD OR RUNE
                if reward == 2 then
                  local card = CARD_REWARDS[math.random(#CARD_REWARDS)]
                  Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TAROTCARD, card, roomcenter,Vector(0,0),nil)
                end

                -- RESETS FOR THE RITUAL TURN --
                mod.Ritual_Puzzle_Phase = 0
                mod.Ritual_Entity_Timer = 0
                Genesis.moddata.Satanic_State = 1
                mod.Ritual_Timer = 0
                mod.Ritual_Wave = mod.Ritual_Wave + 1
                mod.Satanic_Pattern[mod.Ritual_Wave] = math.random(1,5)
                mod.Puzzle_Speed = mod.Puzzle_Speed + 2

              elseif mod.Ritual_Wave == 6 then

                -- GIVES A REWARD --
                local roomcenter = room:FindFreePickupSpawnPosition(room:GetCenterPos(), 0, true)

                -- REWARD: 1 DEVIL PACT ITEM --
                Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, mod.DevilPool[math.random(#mod.DevilPool)], roomcenter, Vector(0,0), player)

                -- RESETS FOR THE RITUAL TURN --
                mod.Ritual_Puzzle_Phase = 0
                mod.Ritual_Entity_Timer = 0
                Genesis.moddata.Satanic_State = 1
                mod.Ritual_Timer = 0
                mod.Ritual_Wave = mod.Ritual_Wave + 1
                mod.Satanic_Pattern[mod.Ritual_Wave] = math.random(1,5)
                mod.Puzzle_Speed = mod.Puzzle_Speed + 2

              elseif mod.Ritual_Wave == 8 then

                -- GIVES A REWARD --
                local roomcenter = room:FindFreePickupSpawnPosition(room:GetCenterPos(), 0, true)

                -- REWARD: 1 BLOODY CHEST CONTAINING A SATANIC RITUAL ITEM --
                Isaac.Spawn(5,GENESIS_ENTITIES.VARIANT.BLOODY_CHEST,0,roomcenter, Vector(0,0), nil)

                -- END THE RITUAL --
                Genesis.moddata.Satanic_State = 3
                Genesis.moddata.isSatanicRitualEnd = true

                -- PLAY COMPLETED MUSIC AND QUEUE THE RITUAL MUSIC
                MusicManager():Play(GENESIS_MUSIC.SATANIC_RITUAL_COMPLETE, 0.1)
                MusicManager():Queue(GENESIS_MUSIC.SATANIC_RITUAL_ROOM)

                -- RESET PUZZLE --
                mod.Puzzle_Speed = 0
                mod.Satanic_Pattern = { }
                mod.Ritual_Wave = 0

                -- OPEN THE DOOR --
                for num=0, 4 do
                  if room:GetDoor(num) ~= nil then
                    room:GetDoor(num).CloseAnimation = "SatanicClose"
                    room:GetDoor(num).OpenAnimation = "SatanicOpen"
                    room:GetDoor(num):Open()
                  end
                end
              else

                -- END WAVE AND START NEXT --
                mod.Ritual_Puzzle_Phase = 0
                mod.Ritual_Entity_Timer = 0
                Genesis.moddata.Satanic_State = 1
                mod.Ritual_Timer = 0
                mod.Ritual_Wave = mod.Ritual_Wave + 1
                mod.Satanic_Pattern[mod.Ritual_Wave] = math.random(1,5)
                mod.Puzzle_Speed = mod.Puzzle_Speed + 2
              end

              Isaac.DebugString(mod.Satanic_Pattern[mod.Ritual_Wave] or 'no ritual wave')
            end
          else

            -- IF YOU TOUCH A WRONG CANDLE YOU LOSE --
            player:AnimateSad()
            Genesis.moddata.isSatanicRitualEnd = true
            Genesis.moddata.Satanic_State = 3
            mod.Satanic_Pattern = { }
            mod.Ritual_Wave = 0
            MusicManager():Play(GENESIS_MUSIC.SATANIC_RITUAL_ROOM, 0.1)
            for num=0, 4 do
              if room:GetDoor(num) ~= nil then

                room:GetDoor(num).CloseAnimation = "SatanicClose"
                room:GetDoor(num).OpenAnimation = "SatanicOpen"
                room:GetDoor(num):Open()
              end
            end
            mod.satanic_check_delay = 15
            mod.Puzzle_Speed = 0
          end
        end
      end
    end
  end
end

-- DRAWING THE ROOM --
----------------------
function mod:newroom()
  local level = Game():GetLevel()
  local room = level:GetCurrentRoom()
	local currentRoomType = room:GetType()
--  Isaac.DebugString(tostring(Game():GetLevel():GetCurrentRoomIndex()).." == "..tostring(Genesis.moddata.Curse_Room_Id))
  if Genesis.moddata.lastLevel ~= level:GetStage() then
    mod:generateRitualRoom()
    Genesis.moddata.lastLevel = level:GetStage()
  end

  if currentRoomType == RoomType.ROOM_CURSE and level:GetCurrentRoomIndex() == Genesis.moddata.Curse_Room_Id and Genesis.moddata.isSatanicRitualThisFloor == true then
    -- SPAWNS THE FLOOR --
    mod.Background_Floor = Isaac.Spawn(EntityType.ENTITY_EFFECT, 82, 0, Vector(-80,-80), Vector(0,0), nil)
    mod.Background_Floor:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
    mod.Background_Floor:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)

    -- SPAWNS THE WALLS --
    mod.Background_Wall = Isaac.Spawn(EntityType.ENTITY_EFFECT, 82, 0, Vector(-80,-80), Vector(0,0), nil)
    mod.Background_Wall:ToEffect():AddEntityFlags(EntityFlag.FLAG_RENDER_WALL)
    mod.Background_Wall:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
    mod.Background_Wall:GetSprite():Load("gfx/satanic_room.anm2", true)
    mod.Background_Wall:GetSprite():Play("Walls")
    mod.Background_Wall:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)

    -- MAKES SURE THE SPRITES ARE IN THEIR POSITION --
    mod.Background_Wall.Position = room:GetTopLeftPos()+Vector(-80,-80)
    mod.Background_Floor.Position = room:GetTopLeftPos()+Vector(-80,-80)

    -- MAKES THE FLOOR BE THE FLOOR --
    local floorspr = mod.Background_Floor:GetSprite()
    floorspr:Load("gfx/satanic_room.anm2", true)
    floorspr:Play("Floor")

    -- MUSIC STARTS
    MusicManager():Play(GENESIS_MUSIC.SATANIC_RITUAL_ROOM, 0.1)

    -- CHANGE THE DOOR
    for num=0, 4 do
      if room:GetDoor(num) ~= nil then

        -- REMOVE DOOR DAMAGE --
        room:GetDoor(num).TargetRoomType = RoomType.ROOM_TREASURE
        room:GetDoor(num).CurrentRoomType = RoomType.ROOM_TREASURE

        -- SET UP ANIMATIONS --
        room:GetDoor(num).CloseAnimation = "SatanicClose"
        room:GetDoor(num).OpenAnimation = "SatanicOpen"

        -- UPDATE THE DOOR AND OPEN IT --
        room:GetDoor(num):Close(true)
        room:GetDoor(num):Open()
        room:SetClear(true)
      end
    end

    -- REMOVE CURSE ROOM CONTENT --

    for i = 1, Game():GetRoom():GetGridSize() do
      if room:GetGridEntity(i) ~= nil then
        if room:GetGridEntity(i):ToDoor() == nil then
          room:RemoveGridEntity(i, 0, true)
        end
      end
    end
    if room:IsFirstVisit() then
      for i2 = 1, #Genesis.RoomEntities do
        if Genesis.RoomEntities[i2] ~= nil then
          if Genesis.RoomEntities[i2].Type > 1 and Genesis.RoomEntities[i2].Type ~= EntityType.ENTITY_EFFECT and Genesis.RoomEntities[i2].Variant ~= 82 then
            Genesis.RoomEntities[i2]:Remove()
          end
        end
      end
    end

    -- GENERATE PUZZLE
    if Genesis.moddata.Ritual_Puzzle == 1 then
      mod:generatePuzzle_CandleRitual()
    end

  --IF ITS NOT A SATANIC RITUAL ROOM, REPLACE ANY CURSE ROOM DOOR WITH THE SATANIC ONE
  else
    for num=0, DoorSlot.NUM_DOOR_SLOTS-1 do
      if room:GetDoor(num) ~= nil then
        -- WHEN YOU SEE THE CURSE ROOM FOR THE FIRST TIME IT MAKES IT A SATANIC RITUAL ROOM --
        if room:GetDoor(num).TargetRoomType == RoomType.ROOM_CURSE and Genesis.moddata.isSatanicRitualThisFloor and Genesis.moddata.Curse_Room_Id == -1 and level:GetRoomByIdx(room:GetDoor(num).TargetRoomIndex).Data.Shape == RoomShape.ROOMSHAPE_1x1 then
          Genesis.moddata.Curse_Room_Id = room:GetDoor(num).TargetRoomIndex
          if Genesis.DEBUG then  Isaac.DebugString(tostring("Genesis+: New curse id: "..Genesis.moddata.Curse_Room_Id)) end
        end

        -- CHECK IF THE DOOR IS THE SATANIC RITUAL DOOR --
        if Genesis.moddata.Curse_Room_Id == room:GetDoor(num).TargetRoomIndex and Genesis.moddata.isSatanicRitualThisFloor then

          -- REMOVE THE DOOR DAMAGE --
          room:GetDoor(num).TargetRoomType = RoomType.ROOM_TREASURE
          room:GetDoor(num).CurrentRoomType = RoomType.ROOM_TREASURE

          --CLOSE THE DOOR IF YOU DIDNT PAY --
          if not Genesis.moddata.Payed_For_Ritual then
            room:GetDoor(num).CloseAnimation = "SatanicHeartClose"
            room:GetDoor(num).OpenAnimation = "SatanicHeartOpen"
            room:GetDoor(num).State = 2

            -- SPAWN THE ENTITY THAT ACTS AS THE DOOR --
            mod.doorWall = Isaac.Spawn(900,101,0,Vector(0,0),Vector(0,0), nil)
            mod.doorWall:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
            mod.doorWall:AddEntityFlags(EntityFlag.FLAG_DONT_OVERWRITE)
            mod.doorWall:AddEntityFlags(EntityFlag.FLAG_NO_FLASH_ON_DAMAGE)
            mod.doorWall:AddEntityFlags(EntityFlag.FLAG_NO_TARGET)
            mod.doorWall:AddEntityFlags(EntityFlag.FLAG_NO_KNOCKBACK)
            mod.doorWall:SetColor(Color(0,0,0,0,0,0,0),-1,1,false,false)
            mod.doorPos = room:GetDoor(num).Position
          else
            room:GetDoor(num).CloseAnimation = "SatanicClose"
            room:GetDoor(num).OpenAnimation = "SatanicOpen"
          end
          room:GetDoor(num):Close(true)
        end
      end
    end
  end
end

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, mod.newroom)

function mod:TakeDamage()
  return false
end

mod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, mod.TakeDamage, 900)

--BLOODY CHESTS
local BloodyChest = RegisterMod ("Bloody Chest", 1)
BloodyChest.OpenedChests = {nil}
BloodyChest.SatanicPool = {
  Isaac.GetItemIdByName("Cursed Contract"),
  Isaac.GetItemIdByName("Satan's Chalice"),
  Isaac.GetItemIdByName("Azazels lost horn"),
  Isaac.GetItemIdByName("Hellbeam"),
  Isaac.GetItemIdByName("D666"),
  Isaac.GetItemIdByName("Ceremonial Dagger"),
  Isaac.GetItemIdByName("Death's List")
}
BloodyChest.BloodyChestItems = {230, 247, 73, 51, 45, 97, 135, 119, 254, 411, 391, 452, 270, 7, 276}


--Chest Logic--
function BloodyChest:ChestLogic ()
local player = Isaac.GetPlayer(0)
local room = Game():GetRoom()
local entities = Genesis.getRoomEntities()
	for i = 1, #entities do
		local e = entities[i]
		local pos = e.Position
		--Check for closed Bloody Chests in the room--
		if e.Type == 5 and e.Variant == GENESIS_ENTITIES.VARIANT.BLOODY_CHEST and not BloodyChest.OpenedChests [e.InitSeed] and e.Position:Distance(player.Position) < 25 then
			SFXManager():Play(SoundEffect.SOUND_CHEST_OPEN , 1, 0, false, 1.5)
		if player:GetMaxHearts() > 1 and player:GetPlayerType() ~= PlayerType.PLAYER_THELOST then
			player:AddMaxHearts(-2, true)
		elseif player:GetPlayerType() ~= PlayerType.PLAYER_THELOST then
			player:AddSoulHearts(-4, true)
		end
      player:TakeDamage(0, 0,EntityRef(e), 1)
			BloodyChest.OpenedChests [e.InitSeed] = true
			if player:GetHearts() == 0 and player:GetSoulHearts() == 0 then
				player:Kill()
			end
			--Plays the open animation--
			local Sprite = e:GetSprite()
			Sprite:Load("gfx/pickup_bloodyChest.anm2", true)
			Sprite:Play("Open", true)

		--Spawns the collectible--
      e:Remove()
--      Isaac.DebugString("Curse type: " .. tostring(RoomType.ROOM_CURSE))
--      Isaac.DebugString("room type: " .. tostring(room:GetType()))

      --GET THE ITEM (normal pool if not curse/satanic room, satanic pool otherwise) AND CHECK IF ITS UNLOCKED
      --IF ITS NOT, PICK ANOTHER RANDOM ONE
      --REPEAT UNTIL YOU GET IT RIGHT
      local item
      if room:GetType() == RoomType.ROOM_CURSE or Genesis.moddata.isSatanicRitualRoom then
        local locked
        repeat
          locked = false
          item = BloodyChest.SatanicPool[math.random(#BloodyChest.SatanicPool)]
          for q=1, #Genesis.UNLOCKABLES do
            if item == Genesis.UNLOCKABLES[q].ITEM and not Genesis.moddata.unlocks[Genesis.UNLOCKABLES[q].ACHIEVEMENT] then
              locked = true
            end
          end
        until not locked
      else
        local locked
        repeat
          item = BloodyChest.BloodyChestItems[math.random(#BloodyChest.BloodyChestItems)]
          for q=1, #Genesis.UNLOCKABLES do
            if item == Genesis.UNLOCKABLES[q].ITEM and not Genesis.moddata.unlocks[Genesis.UNLOCKABLES[q].ACHIEVEMENT] then
              locked = true
            end
          end
        until not locked
      end
      local pedestal = Isaac.Spawn(5, 100, item, e.Position, Vector(0, 0), e)
      if pedestal ~= nil then
        pedestal:GetSprite():ReplaceSpritesheet(0,"gfx/BloodyChest.png")
        pedestal:GetSprite():LoadGraphics()
        if pedestal.FrameCount == 0 and pedestal.Position:Distance(player.Position) < 25 then
          player.Velocity = (player.Position - pedestal.Position) / 10
        end
      end
		end
	end
end

--Callbacks--
BloodyChest:AddCallback(ModCallbacks.MC_POST_UPDATE, BloodyChest.ChestLogic)

Isaac.DebugString("Genesis+: Loaded Satanic rooms!")
error({ hack = true })