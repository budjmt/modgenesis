local module = { }
local UNRESTRICTED_NEW_INDEX
UNRESTRICTED_NEW_INDEX = function(self, key, val)
  local self_mt = getmetatable(self)
  local propset = rawget(self_mt, "__propset")
  local parent = rawget(self_mt, "__parent")
  local parent_propset
  if parent then
    parent_propset = rawget(parent, "__propset")
  end
  if parent_propset and parent_propset[key] then
    return parent_propset[key](self, val)
  end
  if propset and propset[key] then
    return propset[key](self, val)
  end
  if type(self) == "userdata" then
    error("no writable variable '" .. tostring(key) .. "'")
  end
  return rawset(self, key, val)
end
local UNRESTRICTED_INDEX
UNRESTRICTED_INDEX = function(self, key)
  local self_mt = getmetatable(self)
  local propget = rawget(self_mt, "__propget")
  local const = rawget(self_mt, "__const")
  local parent = rawget(self_mt, "__parent")
  local parent_propget
  if parent then
    parent_propget = rawget(parent, "__propget")
  end
  if propget and propget[key] then
    return propget[key](self)
  end
  if parent_propget and parent_propget[key] then
    return parent_propget[key](self)
  end
  if parent and rawget(parent, key) then
    return rawget(parent, key)
  end
  if rawget(self_mt, key) then
    return rawget(self_mt, key)
  end
  if const and rawget(const, key) then
    return const[key]
  end
end
module.unrestrict = function(klass)
  local class_mt = getmetatable(klass)
  local inst_mt = getmetatable(class_mt.__class)
  if class_mt then
    class_mt.__newindex = UNRESTRICTED_NEW_INDEX
    class_mt.__index = UNRESTRICTED_INDEX
    klass.prop = setmetatable({ }, {
      __index = function(self, key)
        return {
          get = class_mt.__propget[key],
          set = class_mt.__propset[key]
        }
      end,
      __newindex = function(self, key, val)
        if not (type(val) == "table" and (type(val.get) == "function" or type(val.set) == "function")) then
          error("Must be a table with at least one function field out of `get` or `set`")
        end
        class_mt.__propget[key] = val.get
        class_mt.__propset[key] = val.set
      end
    })
  end
  if inst_mt then
    inst_mt.__newindex = UNRESTRICTED_NEW_INDEX
    inst_mt.__index = UNRESTRICTED_INDEX
    klass.inst = setmetatable({ }, {
      __index = function(self, key)
        return class_mt.__class[key]
      end,
      __newindex = function(self, key, val)
        class_mt.__class[key] = val
      end
    })
    klass.inst_prop = setmetatable({ }, {
      __index = function(self, key)
        return {
          get = class_mt.__class.__propget[key],
          set = class_mt.__class.__propset[key]
        }
      end,
      __newindex = function(self, key, val)
        if not (type(val) == "table" and (type(val.get) == "function" or type(val.set) == "function")) then
          error("Must be a table with at least one function field out of `get` or `set`")
        end
        class_mt.__class.__propget[key] = val.get
        class_mt.__class.__propset[key] = val.set
      end
    })
  end
  klass.__unrestricted = true
end
module.unrestrict_all = function(obj, name, cache)
  obj = obj or _G
  cache = cache or { }
  if cache[obj] then
    return obj
  end
  if type(obj) == "table" then
    cache[obj] = true
    for k, v in pairs(obj) do
      module.unrestrict_all(v, k, cache)
    end
    local mt = getmetatable(obj)
    if mt and mt.__class and mt.__class.__type then
      module.unrestrict(obj)
    end
  end
  return obj
end
if not _G.abpp_no_unrestrict then
  module.unrestrict_all()
end
return module